echo
echo "***"
echo "*** Building base"
echo "***"
echo
cd Templates/base
./build.sh
echo
echo "***"
echo "*** Building v3_00"
echo "***"
echo
cd ../../v3_00
./build.sh
echo
echo "***"
echo "*** Building v3_01"
echo "***"
echo
cd ../v3_01
./build.sh
echo
echo "***"
echo "*** Building v4_00"
echo "***"
echo
cd ../v4_00
./build.sh
cd ..
echo
echo "***"
echo "*** Complete"
echo "***"
echo


