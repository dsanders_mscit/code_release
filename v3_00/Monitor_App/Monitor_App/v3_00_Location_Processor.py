################################################################################
#
# Program:      v3_00_Location_Processor.py
# Class:        v3_00_Location_Processor
# Objects:      Location_Processor
# Component of: Monitor_App
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Location Processor is designed to be a background task, based on a
#          timer, which periodically polls a connected device for location (x,y)
#          information and then checks those coordinates against a location
#          service to find out if the location is in a hot spot.
#
################################################################################

import requests
import json
import time
from datetime import datetime

class v3_00_Location_Processor(object):
    # Constructor
    def __init__(self):
        self.sleep_timer = 30


    # The background method invoked - job
    def job(self, control_object=None):
        # Loop forever until killed.
        while True:
            state = control_object.get_state()

            # If the state is ON then a location check is performed; if not,
            # sleep
            if state.upper() == 'ON': # Only check if we've been switched on.
                self.do_location_check(control_object)

            time.sleep(self.sleep_timer)


    # Get the connected device's current location and check it.
    def do_location_check(self, control_object=None):
        recipient = control_object.get_value('recipient')
        if recipient == None:
            return

        try:
            # Set the connected device's key. The real key would be passed
            # during relationship setup (state/on, state/off).
            payload_data = {
                "key":"1234-5678-9012-3456"
            }
    
            # Get the location service URL. This is passed to the monitor app
            # during connection to the device that request it be turned on.
            #
            location_service = control_object.get_value('location-service')

            # Get the location (current loc) provider.
            location_provider = control_object.get_value('location-provider')

            # If the location service isn't defined, ignore this pass.
            if location_service in (None, [], ''):
                control_object.log('Location check requested but there is '+\
                                      'no location service defined.')
                return

            control_object.log('Location requested from {0}.'\
                .format(location_provider)
            )

            # Issue the http request to the location provider. Get x,y co-ord.
            request_response = requests.get(
                location_provider,
                data=json.dumps(payload_data)
            )

            control_object.log('Location request returned {0}'\
                .format(request_response.status_code)
            )

            # Make sure the HTTP response was good.
            if request_response.status_code in (200,201):
                returned_data = request_response.json()
                # Extract the x, y returned data.
                if returned_data['response'] == 'success':
                    x = returned_data['data']['x']
                    y = returned_data['data']['y']
                    control_object.log('Location request returned ({0},{1}).'\
                        .format(x, y))

                    # Build an HTTP request to send to the location service to
                    # check the x and y coordinates against known hot spots.
                    payload_data = {
                        "key":"1234-5678-9012-3456", "x":x, "y":y
                    }
                    control_object.log(
                        'Location Request checking if location ({0},{1})'\
                        .format(x, y)+' is in hotspot. '+\
                        'Checking with {0}'.format(location_service)
                        )

                    # Issue the HTTP request
                    check_response = requests.get(
                        location_service,
                        params=payload_data
                    )

                    control_object.log('URL is {0}'.format(check_response.url))

                    control_object.log('Response is {0}'\
                        .format(check_response.status_code)
                    )

                    # Check the response from the location service was good.
                    if check_response.status_code in (200,201)\
                    and check_response.json()['response'] == 'success':
                        xy = (x, y)
                        hotspot_yn = check_response.json()['data']['hotspot']
                        issue_notification = False
                        # If it was a hot spot, then there are some things we
                        # need to do:
                        if hotspot_yn:
                            # Get the hot spot details
                            hotspot_name = check_response.json()\
                                ['data']['hotspots'][0]['hotspot']
                            hotspot_desc = check_response.json()\
                                ['data']['hotspots'][0]['description']
                            control_object.log(
                                'Location ({0},{1}) '.format(x, y)+\
                                'is in a hotspot. Now checking last '+\
                                'reported time.')

                            # Calculate current date/time
                            timeNow = time.time()
                            timeThen = None
                            issue_notification = True

                            # Find out when the hot spot was last reported. 
                            # There is no need to report it on every pass.
                            lastXY = control_object.get_value(hotspot_name)

                            # If None, then this is the first time the hot spot
                            # has been encountered.
                            if lastXY == None:
                                lastXY = control_object.set_value(hotspot_name,
                                                                  str(timeNow))
                                control_object.log(
                                    'Location ({0},{1}) '.format(x, y)+\
                                    'has not been previously encountered.')
                            else:
                                # We have encountered it before.
                                timeString = time.ctime(lastXY)
                                tempTime = datetime.strptime(
                                               timeString,
                                               '%a %b %d %H:%M:%S %Y'
                                           )
                                timeThen = time.mktime(tempTime.timetuple())
                                timePassed = timeNow - timeThen

                                # Now, find out if it was reported within the
                                # last five minutes.
                                if timePassed < (60 * 5): # 60 * 5 = 5 minutes
                                    # It was, so DON'T report it.
                                    control_object.log(
                                        'Location ({0},{1}) '.format(x, y)+\
                                        'reported less than 5 minutes ago.')
                                    issue_notification = False
                                else:
                                    # It wasn't, so DO report it.
                                    control_object.log(
                                        'Location ({0},{1}) '.format(x, y)+\
                                        'reported more than 5 minutes ago.')
                                    control_object.set_value(hotspot_name,
                                                             str(timeNow))
                        else:
                            # Not a hot spot
                            control_object.log(
                                'Location ({0},{1}) '.format(x, y)+\
                                'is not in a hotspot')

                        # If this was a hot spot, issue the notification.
                        if issue_notification:
                            self.raise_location_notification(
                                control_object=control_object,
                                recipient=recipient,
                                xy=xy,
                                hotspot_name=hotspot_name,
                                hotspot_desc=hotspot_desc,
                                timeStamp=str(time.ctime(timeNow))
                            )
                    else:
                        control_object.log(
                            'Location check returned {0}: {1}'\
                               .format(check_response.status_code,
                                       check_response.json()))
        except requests.exceptions.ConnectionError as rce:
            control_object.log('Exception {0}) '.format(str(rce)))
        except Exception as e:
            control_object.log('Exception {0}) '.format(repr(e)))


    def raise_location_notification(
        self,
        control_object=None,
        recipient=None,
        timeStamp=None,
        xy=(),
        hotspot_name="hot spot",
        hotspot_desc="known for high rates of STI infections and drug use"
    ):
        try:
            control_object.log(
                'Location {0} '.format(xy)+' '\
                'hotspot notification being raised')

            # Get the notification service and check it
            service = control_object.get_value('service')
            if service == None:
                control_object.log
                (
                    'Location {0} '.format(xy)+' '\
                    'cannot be raised as there is no notification service '+\
                    'provider'
                )
                return

            # Build an http request
            payload_data = {
                "key":"1234-5678-9012-3456",
                "message":"You have entered an area called {0} ({1}). "\
                    .format(hotspot_name, hotspot_desc)+\
                    "Remember: Enjoy sex and enjoy life. Do not "+\
                    "expose yourself to HIV!",
                "sender":"Monitor_App",
                "recipient":recipient,
                "action":"openMap{0}".format(xy)
            }

            # Issue the HTTP request
            request_response = requests.post(
                service,
                data=json.dumps(payload_data)
            )

            # Don't analyze the response, but do log it. Nothing can be done to
            # deal with the response, so we fire it off, forget it, but log it.
            log_string = str(request_response.status_code) + ': ' + \
                         str(request_response.json())
            control_object.log('Location notification request returned: {0}'\
                .format(log_string))
        except Exception as e:
            print(repr(e))

