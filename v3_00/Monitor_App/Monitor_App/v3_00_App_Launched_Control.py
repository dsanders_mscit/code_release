################################################################################
#
# Program:      v3_00_App_Launched_Control.py
# Class:        v3_00_App_Launched_Control
# Objects:      App_Launched_Control
# Component of: Monitor_App
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: App Launched control to provide logic and functionality to emulate
#          capturing an application launch event on another device. If the app
#          is on the monitored list, then a notification is raised, via a
#          notification service, that a monitored app has been launched.
#
################################################################################

from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Monitor_App.Control import global_control
import datetime, time, json, requests

class v3_00_App_Launched_Control(object):
    controller = None

    # Constructor
    def __init__(self):
        self.controller = global_control

    # Detect an app has been launched.
    def app_launched(self, application=None, json_string=None):
        success = 'success'
        status = 200
        message = 'Monitor App detected an application launch.'
        data = {"application":application}

        try:
            self.controller.log('{0}'.format(message))

            # Get the recipient and notification service. Monitor App has a 1:1
            # relationship with another device. Every device using the monitor
            # app uses its own instance of the app; however, there should be an
            # additional check here to make sure the service state is on. If the
            # state is off, then requests should raise an error. Not changed due
            # to code freeze, D Sanders, 24 April 2016.

            # Recipient is the device connected to this monitor app. Set when a
            # device connects to the monitor app.
            recipient = self.controller.get_value('recipient')

            # Service is the notification service the connected device expects
            # to be used to notify it. Set when a device connects to the monitor
            # app.
            service = self.controller.get_value('service')

            # Find out if the app is on the monitored list.
            monitored_apps = self.controller.get_app(application=application)

            # If it's not, raise a ValueError.
            if monitored_apps in ([], None, ''):
                raise ValueError('App {0} is not being monitored.'\
                    .format(application))

            self.controller.log(
                'Application {0} is on watch list.'.format(application)+\
                  ' Raising notification.'
            )

            # The app IS on the monitored list, so prepare a notification to be
            # raised.
            payload_data = {
                "key":"1234-5678-9012-3456",
                "message":"The application {0} has been launched"\
                    .format(application)+". "+\
                    "Remember: Enjoy sex and enjoy life. Do not "+\
                    "expose yourself to HIV!",
                "sender":"Monitor_App",
                "recipient":recipient,
                "action":"none"
            }

            # Issue the notification to the notification service.
            request_response = requests.post(
                service,
                data=json.dumps(payload_data)
            )

            self.controller.log(
                'Notification raised with data: '\
                .format(payload_data))

            # Check the notification service responded ok.
            if request_response.status_code not in (200,201):
                raise ValueError(
                    '. A warning was issued with the status code '+\
                    '{0}'.format(request_response.status_code)+\
                    '. Response data was: '+\
                    '{0}'.format(request_response.json())
                )

            log_string = str(request_response.status_code) + ': ' + \
                         str(request_response.json())
            self.controller.log(log_string)
        except requests.exceptions.ConnectionError as rce:
            # Connection Errors occur because the host is, generally, not
            # reachable. Often, this can be caused by a typo or spelling error
            # in the host name.
            message = 'Unable to connect to the notification service '+\
                      'to issue notification for {0}!'.format(application)
            data = {"recipient":recipient or None,\
                    "notification-service":service or None}
            success = "error"
            status = 400
            self.controller.log(message)
        except ValueError as ve:
            # The app is not being monitored OR unable to connect to the service
            message = str(ve)
            success = "error"
            status = 404
            data = {"recipient":recipient or None,\
                    "notification-service":service or None}
            self.controller.log(message)
        except Exception as e:
            success = "error"
            data = {"recipient":recipient or None,\
                    "notification-service":service or None}
            status = 400
            message = repr(e)
            self.controller.log('Exception! {0}'.format(message))

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value


