################################################################################
#
# Program:      v3_00_App_Control.py
# Class:        v3_00_App_Control
# Objects:      App_Control
# Component of: Monitor_App
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: App control to provide logic and functionality to get, set, and clear
#          monitored apps. The model raises a notification when certain apps are
#          launched (e.g. Grindr) and this class defines the apps which are
#          monitored.
#
################################################################################
from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Monitor_App.Control import global_control
import datetime, time, json, requests

class v3_00_App_Control(object):
    controller = None

    # Constructor.
    def __init__(self):
        self.controller = global_control

    # Get all apps monitored by this model.
    def get_all(self):
        success = 'success'
        status = 200
        message = 'Monitor App list of monitored apps.'
        data = None

        try:
            # Get the list of apps.
            monitored_apps = self.controller.get_apps()

            # If there are no apps, then raise an exception.
            if monitored_apps in ([], None, ''):
                raise IndexError('No applications are being monitored.')

            # Take the app list retrieved from the persistence layer and build
            # it into a list of objects. This should really be done in the
            # persistence layer but not changed due to code freeze, D Sanders,
            # 24 April 2016.
            #
            monitored_app_list = []
            for app in monitored_apps:
                monitored_app_list.append(
                  {
                    "app":app[0].upper(),
                    "description":app[1]
                  }
                )

            data = {"apps":monitored_app_list}
        except IndexError as ie:
            # When no apps are found, a 404 (Not Found) error is raised. Not
            # sure if this is actually correct. Should we raise an error or
            # success; the fact that no apps are found is ok. Not modified due
            # to code freeze, D Sanders, 24 April 2016.
            message = str(ie)
            status = 404
            success = 'error'
            self.controller.log('{0}.'.format(message))
        except Exception as e:
            message = repr(e)
            status = 500
            success = 'error'
            self.controller.log('Unexpected Error: {0}.'.format(message))

        self.controller.log('Request to get all apps monitored.')

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value


    # Get a specific application - in effect, check if an app is being monitored
    # or not.
    def get(self, application=None):
        success = 'success'
        status = 200
        message = 'Monitor App application check.'
        data = None

        try:
            self.controller.log(
                'Check monitoring status of app {0}.'.format(application))

            # Get the application from the monitor list.
            monitor_list = self.controller.get_app(application)

            # If the list returned is empty, then the app is not being monitored
            if monitor_list in (None, [], ''):
                raise ValueError('Application {0} is not being monitored.'\
                    .format(application))

            # If no description about the app was given, use a default.
            description = monitor_list[0][1]
            if description in (None, [], ''):
                description = 'no description provided'

            # Prepare the data object
            data = {"app":application.upper(), "description":description}
            self.controller.log('{0}'.format(message))
        except ValueError as ve:
            # A 404 is the correct response here. The request was check app
            # x and if a value error is encountered it's because the app does
            # not exist, so 404 is correct.
            message = str(ve)
            status = 404
            success = 'error'
            self.controller.log('{0}.'.format(message))
        except Exception as e:
            message = repr(e)
            status = 500
            success = 'error'
            self.controller.log('Unexpected Error: {0}.'.format(message))

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value


    # Add an application to the monitored list.
    def set(self, application=None, json_string=None):
        success = 'success'
        status = 200
        message = ''
        data = None

        try:
            # Make sure the JSON is good.
            if json_string == None\
            or json_string == '':
                raise KeyError('Badly formed request!')

            # Load the JSON into a dict
            json_data = json.loads(json_string)

            # Get mandatory parameters.
            key = json_data['key']
            description = json_data['description']

            # A default key is used but this could be easily changed to a 
            # generated key. See base_lib/Key.py. Remember that any other model
            # consuming the monitor app would need to know the key and that
            # would probably be passed when configuring the monitor (see v3_00
            # Phone as an example)
            #
            if not key == '1234-5678-9012-3456':
                raise ValueError('App control key incorrect.')

            self.controller.log(
                'Request to monitor status of app {0} ({1}).'\
                .format(application, description))

            # Add the app to the monitored list.
            app_list = self.controller.set_app(application, description)

            # If the return list is empty, then something went wrong.
            if app_list in (None, [], ''):
                raise Exception('Monitor App application {0} not created!'\
                    .format(application))

            if description in (None, [], ''):
                description = 'no description provided'

            message = 'Application {0} ({1}) is now being monitored.'\
                .format(application, description)

            data = {"app":application.upper(), "description":description}
            self.controller.log('{0}'.format(message))
        except KeyError as ke:
            success = 'error'
            status = '400'
            message = 'Badly formed request! Missing {0}'.format(str(ke))
            self.controller.log('Key Error: {0}.'.format(message))
        except ValueError as ve:
            message = str(ve)
            status = 400
            success = 'warning'
            self.controller.log('{0}.'.format(message))
        except Exception as e:
            message = repr(e)
            status = 500
            success = 'error'
            self.controller.log('Unexpected Error: {0}.'.format(message))

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value


    # Remove (or stop monitoring) an application from the monitored list.
    def delete(self, application=None, json_string=None):
        success = 'success'
        status = 200
        message = 'Monitor App application set.'
        data = None

        try:
            # Make sure the JSON is valid.
            if json_string == None\
            or json_string == '':
                raise KeyError('No JSON data provided')

            json_data = json.loads(json_string)

            # Check the key is good. See notes above in Set.
            key = json_data['key']

            if not key == '1234-5678-9012-3456':
                raise ValueError('App control key incorrect.')

            self.controller.log(
                'Request to stop monitoring app {0}.'.format(application))

            # Check that the app is actually being monitored in the first place.
            app_list = self.controller.get_app(application)
            if app_list in (None, [], ''):
                raise ValueError('Application {0} is not being monitored!'\
                    .format(application))

            # Remove the app from the list of monitored apps.
            app_list = self.controller.delete_app(application)

            message = 'Application {0} is no longer being monitored.'\
                .format(application.upper())
            data = None

            self.controller.log('{0}'.format(message))
        except KeyError as ke:
            success = 'error'
            status = '400'
            message = 'Badly formed request! Missing {0}'.format(str(ke))
            self.controller.log('Key Error: {0}.'.format(message))
        except ValueError as ve:
            message = str(ve)
            status = 404
            success = 'error'
            self.controller.log('{0}.'.format(message))
        except Exception as e:
            message = repr(e)
            status = 500
            success = 'error'
            self.controller.log('Unexpected Error: {0}.'.format(message))

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value



