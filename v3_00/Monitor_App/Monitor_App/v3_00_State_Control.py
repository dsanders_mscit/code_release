################################################################################
#
# Program:      v3_00_State_Control.py
# Class:        v3_00_State_Control
# Objects:      State_Control
# Component of: Monitor_App
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: State control provides the functionality to switch the monitor app
#          on or off. When switching on, the monitor app expects to be provided
#          with key information to allow it to function:
#
#            recipient - the device that notifications should be sent to.
#            service the notification service that will relay notifications
#            location_service - the location service that will validate an x, y
#                               coordinate against known hot spots
#            location_provider - the location provider that will provide current
#                                x and y coordinates.
#
################################################################################

from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Monitor_App.Control import global_control
import datetime, time, json, requests

#
# SuperClass.
# ----------------------------------------------------------------------------
class v3_00_State_Control(object):
    controller = None

    # Constructor
    def __init__(self):
        self.controller = global_control

    # Get the current state of the monitor app.
    def get_state(self):
        success = 'success'
        status = '200'
        message = 'Monitor App state.'
        data = None

        self.controller.log('{0} Status requested'.format(message))

        # Get state values from the kv store
        state_set = self.controller.get_state()
        recipient_set = self.controller.get_value('recipient')
        service_set = self.controller.get_value('service')
        location_service_set = self.controller.get_value('location-service')
        location_provider_set = self.controller.get_value('location-provider')

        # Prepare the return data object
        data = {"state":state_set,
                "recipient":recipient_set,
                "service":service_set,
                "location-service":location_service_set,
                "location-provider":location_provider_set
               }

        self.controller.log('{0} Status reported as {1}'\
            .format(message, data))

        # Return it.
        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value


    # Set the state of the monitor app to on or off.
    def set_state(self, state=None, json_string=None):
        success = 'success'
        status = '200'
        message = 'State change request.'
        data = None

        self.controller.log('{0} Change to {1}'.format(message, state))

        try:
            # Validate JSON and state parameters. Should really validate that
            # state is 'ON' or 'OFF' but doesn't because this method is
            # encapsulated and only callable from within the model.
            #
            if state == None\
            or state == ''\
            or json_string == None\
            or json_string == '':
                raise KeyError('Badly formed request!')

            # Load the JSON into a dict
            json_data = json.loads(json_string)

            # Get the key and validate it.
            key = json_data['key']
            if not key == '1234-5678-9012-3456':
                raise ValueError('Monitor App key incorrect.')

            # Set the state to whatever was passed.
            state_set = self.controller.set_state(state)
            self.controller.log('{0} Changed to {1}'.format(message, state))

            if state.upper() == 'ON':
                # Extract values from JSON data. Notice that no checking is
                # done at this point. It probably should do a 'ping' to the
                # services being passed to ensure they really exist, but there
                # is an expectation that the calling service gets it right.
                #
                recipient = json_data['recipient']
                service = json_data['notification-service']
                location_service = json_data['location-service']
                location_provider = json_data['location-provider']

                # Store values
                recipient_set = \
                    self.controller.set_value('recipient',recipient)
                service_set = self.controller.set_value('service',service)
                location_service_set = \
                    self.controller.set_value('location-service',
                                                location_service)
                location_provider_set = \
                    self.controller.set_value('location-provider',
                                                location_provider)
            else:
                # Anything other than 'ON' is considered OFF, so clear existing
                # values stored for state.
                #
                recipient_set = self.controller.clear_value('recipient')
                service_set = self.controller.clear_value('service')
                location_service_set = \
                    self.controller.clear_value('location-service')
                location_provider_set = \
                    self.controller.clear_value('location-provider')

            # Prepare the data object for return.
            data = {"state":state_set,
                    "recipient":recipient_set,
                    "service":service_set,
                    "location-service":location_service_set,
                    "location_provider":location_provider_set
                   }
        except KeyError as ke:
            success = 'error'
            status = '400'
            message = 'Badly formed request. Missing {0}'.format(str(ke))
            self.controller.log(message)
        except ValueError as ve:
            success = 'error'
            status = '403'
            message = str(ve)
            self.controller.log(message)
        except Exception as e:
            success = 'error'
            status = '400'
            message = repr(e)
            self.controller.log(message)

        return  self.controller.do_response(message=message,
                                              data=data,
                                              status=status,
                                              response=success)

