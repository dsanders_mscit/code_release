################################################################################
#
# Program:      main.py
# Class:        None
# Objects:      None
# Component of: Monitor_App
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Main program. Defines configuration routes.
#
################################################################################
from Monitor_App import app, api
from Monitor_App.Control import global_control
from Monitor_App_Config_Boundary.Config_Logger_Boundary \
    import Config_Logger_Boundary

#
# Get the version of the API
#
version = global_control.get_value('version')

api.add_resource(Config_Logger_Boundary, '/{0}/config/logger'.format(version))

