#
# STANDARD LIBRARY CALLS. See base_lib and v1_00_Config_Logger for more
# information. In future change, this should really map to the base_lib methods
# rather than using a global object representing the logger 
#
# DO NOT MODIFY THE CODE BELOW.
#
from Monitor_App_Config_Boundary.v3_00_Config_Logger_Boundary \
    import v3_00_Config_Logger_Boundary

class Config_Logger_Boundary(v3_00_Config_Logger_Boundary):
    def __init__(self):
        super(Config_Logger_Boundary, self).__init__()

