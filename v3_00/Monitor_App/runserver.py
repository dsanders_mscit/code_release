"""
    Application: Monitor_App
    ------------------------------------------------------------------------
    Author:      David J. Sanders
    Student No:  H00035340
    Date:        26 Jan 2016
    ------------------------------------------------------------------------
    Overivew:    The Monitor_App 

    Patterns:    Larger Applications pattern from Flask website (Ronacher,
                 2013)


    Revision History
    --------------------------------------------------------------------------
    Date         | By             | Reason
    --------------------------------------------------------------------------
    24 Jan 2016  | D Sanders      | First version.


    References
    ----------
    Ronacher, A., 2013, 'Larger Applications' [ONLINE]. Available at: 
    http://flask.pocoo.org/docs/0.10/patterns/packages/ (Accessed: 04 November 
    2015)
    Souren, K., 2015, 'What is __init__.py for?' [ONLINE]. Available at:
    http://stackoverflow.com/questions/448271/what-is-init-py-for (Accessed:
    04 November 2015)

"""
import threading
# Import the module Api from the flask_restful package
from flask_restful import Api

# Import the app, and api modules from the Monitor_App app (__init__.py) so that
# they can be accessed globally by any module within this package.
from Monitor_App import app, api

# Import the boundary apiR module
from Monitor_App_Boundary import apiR

# UNCOMMENT to run locally
#app.run(host='0.0.0.0', port=5000, debug=True, threaded=True)

