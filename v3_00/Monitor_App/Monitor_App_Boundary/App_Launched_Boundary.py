################################################################################
#
# Program:      App_Launched_Boundary.py
# Class:        App_Launched_Control
# Objects:      App_Launched_Control
# Component of: Monitor_App
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: App launched provides the boundary to notify the monitor app that an
#          application has been launched on another device. This service
#          receives notification that an app has been launched, calls the 
#          control, which then checks if the app is monitored or not. It is
#          unusual in the framework in have some boundary level checking - is
#          the monitor on? If it is, check the app, if not, raise an error.
#
################################################################################
from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Monitor_App import app, api

from Monitor_App.Control import global_control as control
from Monitor_App.App_Launched_Control \
    import global_app_launched_control as app_launched_control
from Monitor_App_Boundary import apiR

class App_Launched_Boundary(Resource):
    def post(self, application=None):
        # Check the monitor app is on
        if control.get_state().upper() == 'ON':
            raw_data = None
            raw_data = reqparse.request.get_data().decode('utf-8')
            return app_launched_control.app_launched(application, raw_data)
        else:
            # It's not on, so raise an error (400). It's a client error because
            # the client has to turn monitoring on first.
            message='Monitor App is off and not tracking launches. Unable '+\
                    'to identify if {0} should be monitored or not.'\
                    .format(application)
            control.log(message)
            return control.do_response(
                    status=400,
                    response='error',
                    data={
                           "recipient":None,
                           "notification-service":None
                         },
                    message=message
                   )

