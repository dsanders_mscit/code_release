################################################################################
#
# Program:      App_Boundary.py
# Class:        App_Boundary, App_All_Boundary
# Objects:      None
# Component of: Monitor_App
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: App boundary for defining monitored apps. Supports GET, POST, and
#          DELETE. App_All_Boundary enables all apps to be fetched and supports
#          GET only.
#
################################################################################
from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Monitor_App.App_Control import global_app_control as app_control
from Monitor_App import app, api

from Monitor_App_Boundary import apiR

class App_Boundary(Resource):
    def get(self, application=None):
        return app_control.get(application)

    def post(self, application=None):
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        return app_control.set(application, raw_data)

    def delete(self, application=None):
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        return app_control.delete(application, raw_data)


class App_All_Boundary(Resource):
    def get(self):
        return app_control.get_all()

