################################################################################
#
# Program:      v3_00_Check_Control.py
# Class:        v3_00_Check_Control
# Objects:      Check_Control
# Component of: Location_Service
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Check control receives requests from the boundary to validate a set
#          of x and y co-ordinates against a known list of hot spots. When first
#          initialize, this model contains NO hot spots. See the configuration
#          classes for details on how to configure hot spots.
#
################################################################################

from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Location_Service.Control import global_control
import datetime, time, json, requests, sys

class v3_00_Check_Control(object):
    controller = None

    # Constructor
    def __init__(self):
        # Get the Global Controller
        self.controller = global_control

    # Control method for checking
    def check(self):
        success = 'success'
        status = '200'
        message = 'Location Service, check location results.'
        data = None

        try:
            # Set default X and Y coordinates.
            x = 0
            y = 0

            # Parse the request to get the X and Y. The request looks like:
            #   http://server:port/ver/check?x=100&y=100
            #
            parser = reqparse.RequestParser()
            parser.add_argument('x', type=str, required=True)
            parser.add_argument('y', type=str, required=True)
            query_data = parser.parse_args()

            self.controller.log('Request to check location')

            # Convert the passed data to floats.
            x = float(query_data['x'])
            y = float(query_data['y'])

            self.controller.log('Location service request to check '+\
                                  '({0},{1})'.format(x, y))

            # Find any hot spots where x AND y are in the bounds.
            hotspots = self.controller.get_hotspots_by_location(x, y)

            # If we found at least one hotspot:
            if hotspots != [] and type(hotspots) == list:
                # Prepare the return data
                #
                # It will look like:
                #   data:{
                #     "hotspot":true,
                #     "number":n,
                #     "x":100,
                #     "y":100,
                #     "hotspots":[
                #         {
                #           "hotspot":"Downtown",
                #           "lower-x":50,
                #           "lower-y":50,
                #           "upper-x":500,
                #           "upper-y":500,
                #           "description":"The downtown core"}
                #         },
                #         {...}
                #     ]
                #   }
                #
                # Ideally, this would be defined in a schema. See Dissertation
                # for more information on why it is not.
                #
                data={"hotspot":True}

                # Add a list hotspots to the data object
                data['hotspots'] = []
                hotspot_counter = 0
                for i, hotspot in enumerate(hotspots):
                    print(str(i))
                    hotspot_counter += 1
                    data['hotspots'].append(
                       {'hotspot':hotspot[0],
                        'lower-x':hotspot[3],
                        'lower-y':hotspot[4],
                        'upper-x':hotspot[1],
                        'upper-y':hotspot[2],
                        'description':hotspot[5]}
                    )

                data['number'] = hotspot_counter
                data['x'] = x
                data['y'] = y
                self.controller.log(
                    log_message='Location ({0},{1}) found in {2} hot spot.'\
                                .format(x, y, hotspot_counter))
            else:
                # No hot spots were return, so return False. NOTE, the sucess
                # indicator is still success with status 200 - the request was
                # good, just there are no hot spots.
                #
                success='success'
                message = 'No hotspots found.'
                data={"hotspot":False, "x":x, "y":y}
                data['hotspots'] = []
                self.controller.log(
                    log_message='Location ({0},{1}) not in any hot spot.'\
                                .format(x, y))
        except ValueError as ve:
            success = 'error'
            status = '400'
            message = str(ve)
            self.controller.log(
                log_message='Location check error: {0}'\
                    .format(message))
        except Exception as e:
            success = 'error'
            status = '400'
            message = 'Location service error: {0} - X:{1}, Y:{2}'\
               .format(sys.exc_info()[0], x, y)
            self.controller.log(log_message=message)

        return  self.controller.do_response(message=message,
                                              data=data,
                                              status=status,
                                              response=success)

