################################################################################
#
# Program:      v3_00_Control.py
# Class:        v3_00_Control
# Objects:      Control
# Component of: Location_Service
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Global control.
#
################################################################################

# Import base library modules - From symbolic link to /base_lib

import base_lib
from base_lib.Responder import Responder
from base_lib.Config_Logger import Config_Logger
from base_lib.Logger import Logger
from base_lib.Environment import Environment
from base_lib.KVStore import KVStore

# Import general modules and packages
from Location_Service.Location_Service_Database \
    import Location_Service_Database

#
# SuperClass.
# ----------------------------------------------------------------------------
class v3_00_Control(object):
    log_file = None
    pairing_db = None
    responder = None
    config_logger = None

    def __init__(self):
        # Setup environment
        self.environment = Environment()
        port_number = self.environment['port_number']
        server_name = self.environment['server_name']
        host_ip = self.environment['ip_addr']
        version = self.environment['version']
        pre_filename = 'datavolume/{0}-{1}'\
                       .format(server_name, port_number)

        # Setup Config for Logger
        # self.config_logger = Config_Logger(self)

        # Setup responder
        self.responder = Responder()
        self.do_response = self.responder.do

        # Setup KV Store
        self.kvstore = KVStore(pre_filename+'-config.db')
        self.get_value = self.kvstore.get_key
        self.set_value = self.kvstore.set_key
        self.clear_value = self.kvstore.clear_key

        # Setup Logger
        self.logger = Logger(controller=self,
                             filename=pre_filename+'-log.txt',
                             sender='LocSvc-{1}'\
                                 .format(server_name, port_number))
        self.logger.writelog('Log written')
        self.log = self.logger.writelog
        self.db_logger = self.logger.db_logger


        # General startup
        self.server_name = server_name
        self.port_number = port_number

        # Setup the Location Service database
        self.location_service_db = \
            Location_Service_Database(server_name, port_number)

        self.log('Location Service {0}:{1} Started'\
                 .format(server_name, port_number))

        self.log('Setting environment variables to {0}'\
            .format(self.environment))
        self.set_value('server_name', server_name)
        self.set_value('port_number', port_number)
        self.set_value('ip_addr', host_ip)
        self.set_value('version', version)
        self.log('Stored environment variables')


    # Get the pairing DB - OLD Should not be here. DO NOT USE.
    # Time prevents this from being removed and validated. At code freeze -
    # D Sanders, 23 April 2016.
    #
    def get_pairing_db(self):
        return self.pairing_db

    # Get all hot spots
    def get_hotspots(self):
        return self.location_service_db.get_hotspots()

    # Get all hot spots for a specific location
    def get_hotspots_by_location(self, x, y):
        return self.location_service_db.get_hotspot_by_location(x, y)

    # Get hot spot by location name
    def get_hotspot(self, location):
        return self.location_service_db.get_hotspot_by_name(location)

    # Save a hot spot
    def save_hotspot(self, location, upperX, upperY, lowerX, lowerY, desc):
        return self.location_service_db.set_hotspot(
            location,
            upperX,
            upperY,
            lowerX,
            lowerY,
            desc
        )

    # Delete a hot spot
    def delete_hotspot(self, location):
        return self.location_service_db.clear_hotspot(location)

