################################################################################
#
# Program:      v3_00_Location_Service_Database.py
# Class:        v3_00_Location_Service_Database
# Objects:      Location_Service_Database
# Component of: Location_Service
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Persistence layer for database functions.
#
################################################################################

import sqlite3

class v3_00_Location_Service_Database(object):
    db_name = None
    db_conn = None
    db_cursor = None

    # Constructor. Must be passed server name and port number. These should be
    # set to defaults. Not modified at Code Freeze. D Sanders, 23 April 2016.
    def __init__(self, server_name=None, port_number=None):
        # Validate the server name and default it if null
        if server_name == None:
            server = 'localhost'
        else:
            server = server_name

        # Validate the port number and default it if null
        if port_number == None:
            port_number = '5000'
        else:
            port_number = str(port_number)

        # Create the database if it doesn't already exist.
        self.db_name = 'datavolume/'+server+'-'+\
            port_number+'-locserv.db'
        self.db_conn = None
        self.db_cursor = None

        # Validate (or create) the hot spots table.
        self.validate_hotspots_table()


    # Helper method - open db.
    def open_db(self):
        try:
            self.db_conn = sqlite3.connect(self.db_name)
            self.db_cursor = self.db_conn.cursor()
        except Exception as e:
            print(repr(e))
            if not self.db_cursor == None:
                self.db_cursor.close()
            if not self.db_conn == None:
                self.db_conn.close()


    # Validate hot spots table. NOTE: Destructive - can only be called once
    # during model instantiation as it drops all existing hotspots. Probably
    # needs adapting BUT Code Freeze. D Sanders, 23 April 2016.
    def validate_hotspots_table(self):
        _returned = None

        try:
            self.open_db()
            self.db_exec('delete from hotspots')
        except sqlite3.OperationalError as oe:
            print(str(oe))
            self.db_cursor.execute(
                    'CREATE TABLE hotspots( '+\
                    '  location string not null primary key,'
                    '  upperX real not null, '+\
                    '  upperY real not null, '+\
                    '  lowerX real not null, '+\
                    '  lowerY real not null, '+\
                    '  description string not null'
                    ')'
                )
        except Exception as e:
            print(repr(e))
            raise
        finally:
            self.close_db()


    # Execute a SQL Statement - SHOULD NOT be callable outside the class; 
    # however, Python doesn't have private methods. Should probably be called
    # _db_exec to signify, if not enforce, private method.
    #
    def db_exec(self, sql_statement=None, sql_parameters=()):
        if sql_statement == None:
            return None

        _returned = None

        try:
            if self.db_cursor == None:
                raise Exception('Cursor does not exist!')
            self.db_cursor.execute(sql_statement, sql_parameters)
        except sqlite3.OperationalError as oe:
            raise
        except Exception as e:
            print(repr(e))
            raise


    # Helper. Close the database.
    def close_db(self):
        if not self.db_cursor == None:
            self.db_cursor.close()
        if not self.db_conn == None:
            self.db_conn.close()
        self.db_cursor = None
        self.db_conn = None


################ Functional Methods ############################################

    # Create a hot spot
    def set_hotspot(
        self,
        location=None,
        upperX=None,
        upperY=None,
        lowerX=None,
        lowerY=None,
        description=None
    ):
        if location == None \
        or upperX == None \
        or upperY == None \
        or lowerX == None \
        or lowerY == None \
        or description == None:
            return False

        returned = False
        try:
            self.open_db()
            self.db_exec('insert or replace into hotspots '+\
                           'values (?, ?, ?, ?, ?, ?)',
                           (location,
                            upperX, 
                            upperY, 
                            lowerX, 
                            lowerY, 
                            description)
                          )
            self.db_conn.commit()
            returned = True
        except Exception as e:
            print(repr(e))
        finally:
            self.close_db()
            return returned


    # Get all hot spots
    def get_hotspots(self):
        try:
            self.open_db()
            self.db_exec('select * from hotspots')
            returned = self.db_cursor.fetchall()
        except Exception as e:
            print(repr(e))
        finally:
            self.close_db()
            if returned == []:
                returned = None

        return returned


    # Get hot spot by name. This whole set of get hotspot functions need to
    # change. They should follow later models and pass parameters to signify the
    # functionality required. No changes made - at Code Freeze. D Sanders, 23
    # April 2016.
    #
    def get_hotspot_by_name(self, location=None):
        if location == None:
            return None

        try:
            self.open_db()
            self.db_exec('select * from hotspots '+\
                           'where location = ?',
                           (location,)
                          )
            returned = self.db_cursor.fetchall()
        except Exception as e:
            print(repr(e))
        finally:
            self.close_db()
            if returned == []:
                returned = None

        return returned


    # By location
    def get_hotspot_by_location(self, x=None, y=None):
        if x == None or y == None:
            return None

        try:
            self.open_db()
            self.db_exec('select * from hotspots '+\
                           'where (? >= lowerX and ? <= upperX) '+\
                           'and (? >= lowerY and ? <= upperY)',
                           (x, x, y, y)
                          )
            returned = self.db_cursor.fetchall()
        except Exception as e:
            print(repr(e))
        finally:
            self.close_db()
            if returned == []:
                returned = None

        return returned


    # Clear (Delete) a hot spot. Clears by hot spot name.
    def clear_hotspot(
        self,
        location=None
    ):
        if location == None:
            return False

        returned = False
        try:
            self.open_db()
            self.db_exec('delete from hotspots '+\
                           'where location = ? ',
                           (location,)
            )
            self.db_conn.commit()
            returned = True
        except Exception as e:
            print(repr(e))
        finally:
            self.close_db()

        return returned

