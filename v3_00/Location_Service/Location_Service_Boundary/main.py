################################################################################
#
# Program:      main.py
# Class:        None
# Objects:      None
# Component of: Location_Service
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Main program. Adds defined routes to the model.
#
################################################################################
from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Location_Service.Control import global_control
from Location_Service import app, api
from Location_Service_Boundary import apiR
from Location_Service_Boundary.Check_Boundary import Check_Boundary

#
# Get the version of the API
#
version = global_control.get_value('version')

apiR.add_resource(Check_Boundary, '/{0}/check'.format(version))

