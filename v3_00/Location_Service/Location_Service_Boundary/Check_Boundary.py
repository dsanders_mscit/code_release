################################################################################
#
# Program:      Check_Boundary.py
# Class:        Check_Boundary
# Objects:      Check_Boundary
# Component of: Location_Service
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Provide boundary functions for check location. Supports GET only.
#
################################################################################

# Import Flask and Flask Restful packages
from flask_restful import Resource, Api, reqparse, abort
from flask import Response

# Import Location Service modules and classes
from Location_Service import app, api
from Location_Service_Boundary import apiR

from Location_Service.Check_Control \
    import global_check_control as check_control

class Check_Boundary(Resource):
    def get(self):
        return check_control.check()

