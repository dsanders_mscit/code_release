# Import base library modules - From symbolic link to /base_lib
from Location_Service_Config_Control.v3_00_Config_Logger_Control \
    import v3_00_Config_Logger_Control

class Config_Logger_Control(v3_00_Config_Logger_Control):
    def __init__(self):
        super(Config_Logger_Control, self).__init__()

logger_control_object = Config_Logger_Control()

