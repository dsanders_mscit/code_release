################################################################################
#
# Program:      Config_Hotspot_Boundary.py
# Class:        Config_Hotspot_Boundary
# Objects:      Config_Hotspot_Boundary, Config_Hotspot_All_Boundary
# Component of: Location_Service
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Provide configuration boundary functions to setup and manage hot 
#          spots. Supports GET, POST, and DELETE. NOTE: This file contains two
#          classes - 
#
################################################################################

from flask_restful import Resource, reqparse
from Location_Service_Config_Control.Config_Hotspot_Control \
    import config_hotspot_control

class Config_Hotspot_Boundary(Resource):
    # Get a specific hot spot
    def get(self, location):
        return config_hotspot_control.get(location=location)


    # Create (or update) a new (or existing) hot spot
    def post(self, location):
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        return config_hotspot_control.set(
            location=location,
            json_string=raw_data)


    # Delete an existing hot spot.
    def delete(self, location):
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        return config_hotspot_control.delete(
            location=location,
            json_string=raw_data)

class Config_Hotspot_All_Boundary(Resource):
    # Get ALL hot spots
    def get(self):
        return config_hotspot_control.get_all()

