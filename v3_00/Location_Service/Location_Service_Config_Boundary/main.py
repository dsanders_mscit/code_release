################################################################################
#
# Program:      main.py
# Class:        None
# Objects:      None
# Component of: Location_Service
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Main program. Adds configuration routes for model.
#
################################################################################
from Location_Service import app, api
from Location_Service.Control import global_control
from Location_Service_Config_Boundary.Config_Hotspot_Boundary \
    import Config_Hotspot_Boundary, Config_Hotspot_All_Boundary
from Location_Service_Config_Boundary.Config_Logger_Boundary \
    import Config_Logger_Boundary

#
# Get the version of the API
#
version = global_control.get_value('version')

api.add_resource(Config_Hotspot_Boundary,
                 '/{0}/config/hotspot/<string:location>'.format(version))
api.add_resource(Config_Hotspot_All_Boundary,
                 '/{0}/config/hotspots'.format(version))
api.add_resource(Config_Logger_Boundary,
                 '/{0}/config/logger'.format(version))

