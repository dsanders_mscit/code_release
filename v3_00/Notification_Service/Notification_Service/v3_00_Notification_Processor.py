################################################################################
#
# Program:      v3_00_Notification_Processor.py
# Class:        v3_00_Notification_Processor
# Objects:      Notification_Processor
# Component of: Notification
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Notification Processor is a background task, which receives a
#          message over Redis that a notification should be delivered to a 
#          recipient. The message is composed of five words separated by <<*>>
#
#             sender<<*>>recipient<<*>>message<<*>>action<<*>>event date
#
#          The processor tries to send the message to the recipient. If it can't
#          it fails and the notification is persisted until the process is asked
#          to try again.
#
################################################################################

import redis, requests, json, copy
import time

class v3_00_Notification_Processor:

    # Constructor
    def __init__(self):
        pass

    # At Exit handler - doesn't do anything currently, but should be used to
    # gracefully exit.
    #
    def redis_close(self, thread=None, controller=None):
        # Although not used in this app, the thread close event logic would allow
        # us to do any last tasks.

        if not thread == None\
        and not controller == None:
            controller.log('Closing background thread.')
        else:
            controller.log('Thread was none!')


    # The background activity that listens for Redis messages.
    def redis_processor(self, control_object=None):
        redis_pubsub = control_object.get_queue()

        # Loop until terminated.
        for message in redis_pubsub.listen():
            # When a message arrives, make sure it is marked message. See Redis
            # doc. for other types.
            if message['type'].upper() == 'MESSAGE':
                control_object.log('Redis Processor: Message received!')

                # Split the message into 5 fields.
                message_data = message['data']
                message_fields = message['data'].decode('utf-8').split('<<*>>', 5)

                try:
                    # Set the defaults
                    persist_notification = True
                    persist_reason = 'unknown'
                    time.sleep(2) # Let any caller finish what it was doing first, 
                                  # e.g. deleting from the database.

                    control_object.log('Redis Processor: '+\
                                       'Message parsed to: {0}'\
                                           .format(message_fields))

                    # Store the Redis message into variables
                    sender = message_fields[0]
                    recipient = message_fields[1]
                    text = message_fields[2]
                    action = message_fields[3]
                    event_date = message_fields[4]

                    # Create the payload for sending to the recipient.
                    payload_data = {
                        "key":"NS1234-5678-9012-3456",
                        "message":text,
                        "sender":sender,
                        "action":action
                    }

                    # Obfuscate the key for logging purposes - we don't want the
                    # control key to be visibly logged.
                    temp_payload_data = copy.deepcopy(payload_data)
                    temp_payload_data['key']='OBFUSCATED'

                    control_object.log('Redis Processor: Payload is '+\
                                       '{0}-{1}-{2}'\
                                          .format(payload_data['message'],
                                                  payload_data['sender'],
                                                  payload_data['action']
                                                  )
                    )

                    control_object.log('Redis Processor: issuing http '+\
                                       'request to {0}'\
                      .format(recipient))

                    # Send the HTTP post request.
                    request_response = requests.post(
                        recipient,
                        data=json.dumps(payload_data),
                        timeout=30
                    )
                    control_object.log('Redis Processor: http request '+\
                                       'response {0}'\
                                           .format(request_response.status_code))

                    # Check the return status
                    if request_response.status_code not in (200,201):
                        error_text = \
                            'Unable to communicate with phone due to error. '
                        if request_response.status_code == 404:
                            error_text += 'The phone (or its URL for '+\
                                          'notifications) could not be found and '+\
                                          'the push returned a not found (404). '+\
                                          'This normally signifies a spelling or '+\
                                          'URL error in the recipient name. '+\
                                          'Message causing error was "'+\
                                          message_data.decode('utf-8')+'"'
                        else:
                            error_text += 'Response status was {0}'\
                                             .format(request_response.status_code)+\
                                          '; '+json.dumps(request_response.json())


                        # Give a reason why we are persisting the notification -
                        # this is useful when reviewing the log.
                        persist_reason = error_text
                        control_object.log('Redis Processor: Stop ERROR')
                        control_object.log(error_text)
                    else:
                        # The device got the notification okay, so don't persist
                        # it.
                        persist_notification = False
                        control_object.log('Redis Processor: dispatched to {0}'\
                          .format(recipient))
                        control_object.log('Redis Processor: Stop SUCCESS')
                        control_object.log()
                except requests.exceptions.ConnectionError as rce:
                    # Communication error typically means not able to reach the
                    # host, etc.
                    persist_reason = str(rce)
                    control_object.log('Redis Processor: Stop WARNING')
                except KeyError as ke:
                    persist_reason = str(ke)
                    control_object.log('Redis: *** STOP ERROR: {0}'.format(str(ke)))
                except Exception as e:
                    persist_reason = repr(e)
                    control_object.log('Redis: *** STOP ERROR: {0}'.format(repr(e)))

                #
                # If the persist sentinel is set, then we need to call the 
                # method to persist it. We don't care what here, just that it is
                # persisted.
                #
                if persist_notification:
                    control_object.log('Redis Processor: Persisting '+\
                                       'notification because {0}'\
                                       .format(persist_reason))
                    control_object.persist_notification(
                        sender,
                        recipient,
                        text,
                        action,
                        event_date
                    )

