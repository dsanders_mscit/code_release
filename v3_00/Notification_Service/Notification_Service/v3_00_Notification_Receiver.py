################################################################################
#
# Program:      v3_00_Notification_Receiver.py
# Class:        v3_00_Notification_Receiver
# Objects:      Notification_Receiver
# Component of: Notification
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Notification Receiver accepts requests from other devices and 
#          receives notifications which it will eventually send on. The Recevier
#          manages the initial receipt of notifications.
#
################################################################################

from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Notification_Service.Control import global_control
import datetime, time, json, requests, redis, copy

#
# SuperClass.
# ----------------------------------------------------------------------------
class v3_00_Notification_Receiver(object):
    controller = None

    # Constructor
    def __init__(self):
        self.controller = global_control

    #
    # Handle incoming notifications from devices (or services). The objective
    # for this method is too deal with the notification as quickly as possible
    # to keep latency and network issues low.
    #
    def incoming_notification(
        self,
        json_string=None
    ):
        success = 'success'
        status = '201'
        message = 'Notification received. Thank you.'
        data = None

        self.controller.log('Notification received.')

        # Check the JSON
        if json_string == None\
        or json_string == '':
            success = 'error'
            status = '400'
            message = 'Badly formed request!'
        else:
            json_data = json.loads(json_string)
            continue_sentinel = True
            try:
                # Get the required fields in the JSON.
                #
                #   message - the message to be sent
                #   key - the control key; needed to send a notification to the
                #         notification service. This is the control key NOT the
                #         phone or other device key.
                #   sender - who sent the message; free-form text.
                #   action - what should (or could) the recipient do with the
                #            message; e.g. openmap, read text, etc. Free-form
                #            text.
                #   recipient - who is the notification for? See Swagger docs
                #
                data = copy.deepcopy(json_data)
                text=json_data['message']
                key=json_data['key']
                sender=json_data['sender']
                action=json_data['action']
                recipient=json_data['recipient']

                # Check the key. If not correct, raise a 403 (Forbidden) error.
                if not key == '1234-5678-9012-3456':
                    status = '403'
                    raise ValueError('Notification control key incorrect.')

                if 'http://' not in recipient:
                    status = '400'
                    raise ValueError('Recipient must be a valid URL '+\
                                     'beginning with http://'
                    )

                # Remove the key from the dict.
                del data['key']

                self.controller.log('Notification from {0} for {1}'\
                                     .format(sender,recipient))

                self.controller.log('Notification is {0} with action: {1}'\
                                     .format(text, action))

            except KeyError as ke:
                success = 'error'
                status = '400'
                message = 'Badly formed request Missing {0}!'.format(ke)
                self.controller.log(message)
                continue_sentinel = False
            except ValueError as ve:
                success = 'error'
                message = str(ve)
                self.controller.log(message)
                continue_sentinel = False
            except Exception as e:
                success = 'error'
                status = '500'
                message = 'Exception: {0}'.format(repr(e))
                self.controller.log(message)
                continue_sentinel = False

            # If the sentinel is true, then ok to continue.
            if continue_sentinel:
                try:
                    # Get date and time.
                    now = datetime.datetime.now()
                    tz = time.tzname[0]
                    tzdst = time.tzname[1]

                    # Dispatch notification to redis pub/sub to enable fast
                    # collection of notifications and then let another thread
                    # take time to process.

                    self.controller.log(
                        'Notification Receiver is queuing Notification '+\
                        'with data: '+\
                        '{0}-{1}-{2}-{3}-{4}'\
                            .format(
                                sender,
                                recipient,
                                text,
                                action,
                                str(now)+'('+tz+'/'+tzdst+')'
                            )
                    )

                    # Queue the notification. The Notification Processor will
                    # deal with the message, we want to return control as soon
                    # as possible, to minimize latency.
                    #
                    self.controller.queue_notification(
                        sender,
                        recipient,
                        text,
                        action,
                        str(now)+'('+tz+'/'+tzdst+')'
                    )
                except:
                    success = 'error'
                    status = '500'
                    message = 'Exception: {0}'.format(repr(e))
                    self.controller.log(message)

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value

