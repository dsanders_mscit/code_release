################################################################################
#
# Program:      v3_00_Notification_Service_Database.py
# Class:        v3_00_Notification_Service_Database
# Objects:      Notification_Service_Database
# Component of: Notification
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Persistence interface.
#
################################################################################

import sqlite3, os

class v3_00_Notification_Service_Database(object):
    db_name = None
    db_conn = None
    db_cursor = None

    # Constructor. Notice defaults are in the parameters. Other models use the
    # old style. This is the style which is adopted going forward.
    #
    def __init__(self,
                 port_number='5000',
                 server_name='localhost'
    ):
        # Store the server and port details
        self.server_name = server_name
        self.port_number = port_number

        # Create (or open) the DB
        self.db_name = 'datavolume/'+server_name+'-'+str(port_number)+\
            '-notifications.db'

        self.db_conn = None
        self.db_cursor = None

        # Validate the table used to persist notifications
        self.validate_notification_table()


    # Helper method to open the db connection.
    def open_db(self):
        try:
            self.db_conn = sqlite3.connect(self.db_name)
            self.db_cursor = self.db_conn.cursor()
        except Exception as e:
            print(repr(e))
            if not self.db_cursor == None:
                self.db_cursor.close()
            if not self.db_conn == None:
                self.db_conn.close()


    # Method validates notification table exists. NOTE. most models execute a
    # delete from to validate their tables; however, notification services are
    # intended to allow persistence and do not want to destroy existing data.
    # Data is only persisted if the model is executed with the -s parameter - 
    # see the docs.
    #
    def validate_notification_table(self):
        _returned = None

        try:
            self.open_db()
            self.db_exec('select * from notifications')
        except sqlite3.OperationalError as oe:
            # Operational errors can mean many things, but here they mean the
            # select from operation failed, probably because the table didn't
            # exist, so create it.
            #
            print(str(oe))
            self.db_cursor.execute(
                    'CREATE TABLE notifications( '+\
                    '  id integer primary key autoincrement, '+\
                    '  sender string not null, '+\
                    '  recipient string not null,' +\
                    '  notification string not null, '+\
                    '  action string not null, '+\
                    '  event_date string not null '+\
                    ')'
                )
        except Exception as e:
            print(repr(e))
            raise
        finally:
            self.close_db()


    # Helper method to execute a SQL Statement. MUST BE private and not used
    # outside the object. Should really be called _db_exec but not changed due
    # to code freeze, D Sanders, 01 May 2016.
    #
    def db_exec(self, sql_statement=None, sql_parameters=()):
        if sql_statement == None:
            return None

        _returned = None

        try:
            if self.db_cursor == None:
                raise Exception('Cursor does not exist!')
            self.db_cursor.execute(sql_statement, sql_parameters)
        except sqlite3.OperationalError as oe:
            raise
        except Exception as e:
            print(repr(e))
            raise


    # Helper method.
    def close_db(self):
        if not self.db_cursor == None:
            self.db_cursor.close()
        if not self.db_conn == None:
            self.db_conn.close()
        self.db_cursor = None
        self.db_conn = None


################# Functional Methods ###########################################


    # Get all notifications persisted for a recipient.
    def get_notifications(
        self,
        recipient=None
    ):
        # If the recipient is null, just return an empty list.
        if recipient == None:
            return []

        returned = None
        try:
            self.open_db()
            self.db_exec('select * from notifications '+\
                           'where recipient = ?',
                           (recipient,))
            returned = self.db_cursor.fetchall()
            self.db_conn.commit()
        except Exception as e:
            print(repr(e))
        finally:
            self.close_db()

        return returned


    # Clear a single specific notification.
    def clear_notification(
        self,
        identifier=None
    ):
        if identifier == None:
            return []

        returned = True
        try:
            self.open_db()
            self.db_exec('delete from notifications '+\
                           'where id = ?',
                           (identifier,))
            self.db_conn.commit()
        except Exception as e:
            returned = False
            print(repr(e))
        finally:
            self.close_db()

        return returned


    # Clear ALL notifications for a specific recipient.
    def clear_notifications(
        self,
        recipient=None
    ):
        if recipient == None:
            return []

        returned = True
        try:
            self.open_db()
            self.db_exec('delete from notifications '+\
                           'where recipient = ?',
                           (recipient,))
            self.db_conn.commit()
        except Exception as e:
            returned = False
            print(repr(e))
        finally:
            self.close_db()

        return returned


    # Write the notification to the db.
    def save_notification(
        self,
        sender=None,
        recipient=None,
        text=None,
        action=None,
        event_date=None
    ):
        returned = None
        try:
            self.open_db()
            self.db_exec('insert into notifications ('+\
                           ' sender, '+\
                           ' recipient, '+\
                           ' notification, '+\
                           ' action, '+\
                           ' event_date) '+\
                           'values (?,?,?,?,?)',
                           (sender, recipient, text, action, event_date))
            self.db_conn.commit()
        except Exception as e:
            print(repr(e))
        finally:
            self.close_db()

        return True


