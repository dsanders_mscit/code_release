################################################################################
#
# Program:      Notification_Push_Boundary.py
# Class:        Notification_Push_Boundary
# Objects:      Notification_Push_Boundary
# Component of: Notification
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Push boundary for defining interface to request notification push.
#          Supports POST only. IE, you can only request notifications to be
#          pushed, you can't cancel a push or get the status of a push.
#
################################################################################
from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Notification_Service \
    import app, api
from Notification_Service.Notification_Push_Control \
    import global_notification_push_control as notification_push_control
from Notification_Service.Control import global_control as control

class Notification_Push_Boundary(Resource):
    def post(self):
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        return_state = notification_push_control\
            .push_notifications(json_string=raw_data)

        return return_state

