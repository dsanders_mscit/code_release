################################################################################
#
# Program:      main.py
# Class:        None
# Objects:      None
# Component of: Notification
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Main program. Defines routes for boundaries.
#
################################################################################

from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Notification_Service import app, api
from Notification_Service.Control import global_control
from Notification_Service_Boundary.Notification_Receive_Boundary \
    import Notification_Receive_Boundary
from Notification_Service_Boundary.Notification_Push_Boundary \
    import Notification_Push_Boundary

#
# Get the version of the API
#
version = global_control.get_value('version')

api.add_resource(Notification_Receive_Boundary,
                 '/{0}/notification'.format(version))
api.add_resource(Notification_Push_Boundary,
                 '/{0}/push'.format(version))
