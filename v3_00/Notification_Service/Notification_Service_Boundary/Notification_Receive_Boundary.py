################################################################################
#
# Program:      Notification_Receive_Boundary.py
# Class:        Notification_Receive_Boundary
# Objects:      Notification_Receive_Boundary
# Component of: Notification
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Receive boundary for creating notifications. Supports POST only. IE,
#          you can only create a notifications, not recall or get the status of
#          a notification.
#
################################################################################
from flask_restful import Resource, reqparse
from Notification_Service.Notification_Receiver \
    import global_notification_receiver_control as notification_receiver_object

class Notification_Receive_Boundary(Resource):
    def post(self):
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        return_state = notification_receiver_object\
            .incoming_notification(json_string=raw_data)

        return return_state

