################################################################################
#
# Program:      v3_00_Config_Logger_Control.py
# Class:        v3_00_Config_Logger_Control
# Objects:      None
# Component of: Notification
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: The standard base_lib provided logger functions.
#
################################################################################
#
# Import required modules and packages
#
from base_lib.Config_Logger import Config_Logger
from Notification_Service import Control

class v3_00_Config_Logger_Control(object):
    controller = None
    config_logger = None

    def __init__(self):
        self.controller = Control.global_control
        self.config_logger = Config_Logger(self.controller)

    def get_logger(self):
        return self.config_logger.get_logger()

    def remove_logger(self, json_string=None):
        return self.config_logger.remove_logger(json_string)

    def set_logger(self, json_string=None):
        return self.config_logger.set_logger(json_string)

