################################################################################
#
# Program:      Location_Boundary.py
# Class:        Location_Boundary
# Objects:      Location_Boundary
# Component of: Phone
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Location boundary handles requests to the phone to get the current
#          location. Supports GET only.
#
################################################################################
from flask_restful import Resource, reqparse
from Phone.Location_Control import location_control_object

class Location_Boundary(Resource):
    def get(self):
        return location_control_object.location_request()

