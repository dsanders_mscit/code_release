################################################################################
#
# Program:      Notification_Boundary.py
# Class:        Notification_Boundary
# Objects:      Notification_Boundary
# Component of: Phone
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Notification boundary handles handles notifications sent to the phone
#          by a notification service. Supports POST only.
#
################################################################################
from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Phone import app, api
from Phone.Notification_Control import notification_control_object
#from Phone_Boundary import apiR

class Notification_Boundary(Resource):
    def post(self):
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        return_state = notification_control_object.incoming_notification(
            json_string=raw_data
        )

        return return_state

