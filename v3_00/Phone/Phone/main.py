################################################################################
#
# Program:      main.py
# Class:        None
# Objects:      None
# Component of: Phone
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Main program. Adds header support and sets up model.
#
################################################################################
from Phone_Boundary import main
from Phone_Config_Boundary import main

