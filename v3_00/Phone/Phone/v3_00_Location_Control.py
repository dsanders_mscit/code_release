################################################################################
#
# Program:      v3_00_Location_Control.py
# Class:        v3_00_Location_Control
# Objects:      Location_Control
# Component of: Phone
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Location control emulates the GPS / Location function within a phone
#          and returns a set of coordinates (simple X and Y) to requests for
#          location. Only get requests are supported - to change the location
#          the model's configurators need to be used.
#
################################################################################

from flask_restful import Resource, Api, reqparse, abort
from flask import Response
#from Phone import Phone_Database
from Phone import Control
import datetime, time, json, requests

#
# SuperClass.
# ----------------------------------------------------------------------------
class v3_00_Location_Control(object):
    __controller = None

    # Constructor
    def __init__(self):
        self.controller = Control.global_controller

    # Handle location requests
    def location_request(self):
        self.controller.log('Location request received.',
                              screen=False)

        success = 'success'
        status = '200'
        message = 'Location'
        data = None

        try:
            # Get the current X and Y values from the key store.
            x = float(self.controller.get_value('x'))
            y = float(self.controller.get_value('y'))

            # Return them in a dict object.
            data = {"x":x,"y":y}
        except KeyError as ke:
            success = 'error'
            status = '500'
            message = 'Key Error: {0}'.format(str(ke))
            self.controller.log('Location request: Error {0}'\
                                      .format(message),
                                      screen=False
                                 )
        except ValueError as ve:
            success = 'error'
            status = '500'
            message = 'Value Error: {0}'.format(str(ve))
            self.controller.log('Location request: Error {0}'\
                                      .format(message),
                                      screen=False
                                 )
        except Exception as e:
            success = 'error'
            status = '500'
            message = 'Exception: {0}'.format(repr(e))
            self.controller.log('Location request: Error {0}'\
                                      .format(message),
                                      screen=False
                                 )

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        self.controller.log('Location request returned {0}'.format(data),
                              screen=False)
        return return_value


