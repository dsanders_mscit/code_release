################################################################################
#
# Program:      v3_00_Phone_Database.py
# Class:        v3_00_Phone_Database
# Objects:      Phone_Database
# Component of: Phone
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Persistence and Sqlite 3 interface.
#
################################################################################

import sqlite3, os

class v3_00_Phone_Database(object):
    db_name = None
    db_conn = None
    db_cursor = None
    controller = None

    # Constructor. Note defaults in parameters.
    def __init__(self,
                 controller=None,
                 server_name='localhost',
                 port_number=5000
    ):
        if controller == None:
            raise Exception('Controller cannot be none!')

        self.controller = controller

        self.db_name = 'datavolume/{0}-{1}-phone.db'\
                             .format(server_name, port_number)

        self.db_conn = None
        self.db_cursor = None

        self.validate_notification_table()


    # Get the Bluetooth device.
    def get_bluetooth_device(self):
        return self.controller.get_value('bluetooth')


    # Set the Bluetooth device.
    def set_bluetooth_device(self, devicename):
        return self.controller.set_value('bluetooth', devicename)


    # Save a notification. Persisted to the notification table. Self explanatory
    # but worth noting the notification_read column. This indicates with a 0 or
    # 1 (False or True) whether the notification has been read. This is used
    # when managing persisted notifications so owner's only see messages they
    # haven't already seen.
    def save_notification(
        self,
        sender=None,
        date_string=None,
        notification=None,
        action=None
    ):
        returned = None
        try:
            self.open_db()
            self.db_exec('insert into notifications '+\
                           ' (sender, date_string, notification, '+\
                           'action, notification_read) '+\
                           ' values (?,?,?,?, 0)',
                           (sender, date_string, notification, action))
            self.db_conn.commit()

        except Exception as e:
            raise
        finally:
            self.close_db()

        return True


    # Get all persisted notifications which are unread.
    def get_notifications(
        self
    ):
        returned = []
        try:
            self.open_db()
            self.db_exec('select sender, date_string, notification, '+\
                           'action '+\
                           'from notifications '+\
                           'where notification_read = 0')
            returned = self.db_cursor.fetchall()
        except Exception as e:
            raise
        finally:
            self.close_db()

        return returned


    # Helper method. Execute a sql statement.
    def db_exec(self, sql_statement=None, sql_parameters=()):
        if sql_statement == None:
            return None

        _returned = None

        try:
            if self.db_cursor == None:
                raise Exception('Cursor does not exist!')
            self.db_cursor.execute(sql_statement, sql_parameters)
        except Exception as e:
            raise


    # Helper method - close the database
    def close_db(self):
        if not self.db_cursor == None:
            self.db_cursor.close()
        if not self.db_conn == None:
            self.db_conn.close()
        self.db_cursor = None
        self.db_conn = None


    # Helper method - open the database
    def open_db(self):
        try:
            self.db_conn = sqlite3.connect(self.db_name)
            self.db_cursor = self.db_conn.cursor()
        except Exception as e:
            if not self.db_cursor == None:
                self.db_cursor.close()
            if not self.db_conn == None:
                self.db_conn.close()
            raise


    # Validate notification table. NOTE, most models delete from their 
    # persistance tables when validating it; the phone model does not - it 
    # persists unread notifications between instantiations, assuming the -s
    # flag is used with run_docker.sh (see docs).
    #
    def validate_notification_table(self):
        _returned = None

        try:
            self.open_db()
            self.db_exec('select * from notifications')
            self.db_exec('delete from notifications '+\
                           'where notification_read != 0'
                          )
            self.db_conn.commit()
        except sqlite3.OperationalError as oe:
            self.db_cursor.execute(
                    'CREATE TABLE notifications( '+\
                    '  id integer primary key autoincrement, '+\
                    '  sender string not null, '+\
                    '  date_string string not null, '+\
                    '  notification string not null, '+\
                    '  action string not null, '+\
                    '  notification_read int not null '+\
                    ');'
                )
        except Exception as e:
            raise
        finally:
            self.close_db()

