################################################################################
#
# Program:      v3_00_Notification_Control.py
# Class:        v3_00_Notification_Control
# Objects:      Notification_Control
# Component of: Phone
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Notification control is the controller for receiving notifications
#          that have been sent by a notification service to the phone. The phone
#          responds to the notification service and deals with the notification
#          immediately it receives it.
#
################################################################################

from flask_restful import Resource, Api, reqparse, abort
from flask import Response
#from Phone import Phone_Database
from Phone import Control
import datetime, time, json, requests, os

class v3_00_Notification_Control(object):
    controller = None

    # Constructor
    def __init__(self):
        self.controller = Control.global_controller

    # Handle incoming notifications.
    def incoming_notification(
        self,
        json_string=None
    ):
        self.controller.log('Notification received.',
                              screen=False)
        success = 'success'
        status = '200'
        message = 'Notification received.'
        data = None


        # Check the JSON is good.
        if json_string == None\
        or json_string == '':
            success = 'error'
            status = '400'
            message = 'Badly formed request!'
            self.controller.log(
                'Notification was not properly formed. There was no JSON.',
                screen=False)
        else:
            # Load the JSON into a dict
            json_data = json.loads(json_string)
            continue_sentinel = True
            try:
                # Strip out the data from the notificaiton.
                #   message - the notification.
                #   key - the control key.
                #   sender - who sent the notification, e.g. SMS, Monitor App...
                #   action - what should the user be able to do with the message
                text=json_data['message']
                key=json_data['key']
                sender=json_data['sender']
                action=json_data['action']

                # Check the key. Note that it's not the usual 1234... but starts
                # with NS1234. The idea here is that only the Notification 
                # Service should be able to issue notification, not anyone else.
                # So, this would be a key only two parties know.
                if not key == 'NS1234-5678-9012-3456':
                    raise ValueError('Notification control key incorrect.')


                # Build the acknowledgement data for return.
                data = {"sender":sender,
                        "action":action,
                        "notification":text}

                self.controller.log(
                    'Notification data: {0}'.format(data),
                    screen=False)

                # Get current date and time
                now = datetime.datetime.now()
                tz = time.tzname[0]
                tzdst = time.tzname[1]

                self.controller.log(
                    'Sending display notification request.',
                    screen=False
                )

                # Ask the phone to display the notification.
                self.display_notification(
                    sender=sender,
                    date_string='{0} ({1}/{2})'.format(now, tz, tzdst),
                    notification=text,
                    action=action
                )

                self.controller.log(
                    'Persisting notification to the database.',
                    screen=False
                )

                # Save the notification.
                self.controller.persist_notification(
                    sender=sender,
                    date_string='{0} ({1}/{2})'.format(now, tz, tzdst),
                    notification=text,
                    action=action
                )

                self.controller.log(
                    'Issuing notification to bluetooth',
                    screen=False
                )

                # Check if Bluetooth is enabled. If it is, broadcast the message
                # too. Notice only the text is transferred, not the caller.
                response = self.issue_bluetooth(
                    notification=text
                )

                # Check if Bluetooth responded with any errors. If it did, add
                # them to the return data but DON'T interrupt. Bluetooth errors
                # should be recoverable and not affect overall notification flow
                #
                if response != None and response.status_code != 200:
                    data['bluetooth-warnings'] = response.json()

            except requests.exceptions.ConnectionError as rce:
                # Connection error means we could not reach the Bluetooth
                # device. Ignore it but add a warning to the output as the
                # notification was still delivered.
                data['bluetooth-warnings'] = \
                    'Bluetooth Error: device did not respond. {0}'\
                        .format(str(rce))
                self.controller.log(data['bluetooth-warnings'],
                                      screen=False)
            except KeyError as ke:
                # The JSON is badly formed.
                success = 'error'
                status = '400'
                message = 'Badly formed request! {0}'.format(str(ke))
                self.controller.log('Incoming notification: Error {0}'\
                    .format(str(ke)), screen=False)
            except ValueError as ve:
                # This typically means the key is wrong, hence the 403 for
                # forbidden.
                success = 'error'
                status = '403'
                message = str(ve)
                self.controller.log(
                  'Incoming notification: {0}'.format(message),
                  screen=False
                )
            except Exception as e:
                # A general exception occurred. Note, this should actually be a
                # 500 error (server error) and not 400 (client error). Not
                # changed due to code freeze, D Sanders, 01 May 2016.
                #
                success = 'error'
                status = '400'
                message = 'Exception: {0}'.format(repr(e))
                self.controller.log(
                  'Incoming notification: Error {0}'.format(message),
                  screen=False
                )

        self.controller.log('Notification processed.',
                              screen=False)
        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value


    # Issue Bluetooth checks if the phone is paired to Bluetooth and, if it is,
    # asks the Bluetooth service to broadcast the message. It is a fire and
    # forget approach. Errors reported by Bluetooth are captured but not dealt
    # with.
    #
    def issue_bluetooth(
        self,
        notification=None
    ):
        request_response = None

        try:
            # Get the Bluetooth pair for this phone.
            bluetooth_device = self.controller.get_bluetooth()

            # If there isn't one, then just return as there is nothing to do.
            if bluetooth_device != []\
            and bluetooth_device != None:
                # Get the key the Bluetooth service provided when pairing.
                bluetooth_key = self.controller.get_value(bluetooth_device)

                # Get the phone's IP address to use as the device name.
                phonename = self.controller.get_value('phonename_ip')
                if not (bluetooth_key == None or phonename == None):
                    # Create the payload for Bluetooth.
                    payload_data = {"key":bluetooth_key,
                                    "message":notification}

                    # Issue the HTTP request.
                    request_response = requests.post(
                         bluetooth_device+'/broadcast/'+phonename,
                         data=json.dumps(payload_data)
                    )
            # Return whatever we got from any Bluetooth service.
            return request_response
        except requests.exceptions.ConnectionError as rce:
            raise requests.exceptions.ConnectionError(rce)
        except:
            raise

    # Display the notification to the 'screen'. 1) Send it to the log, which 
    # will perform the actual write to any screens listening on Redis; and 2)
    # write the content to the phone's actual screen, which is a file.
    #
    def display_notification(
        self,
        sender=None,
        date_string=None,
        notification=None,
        action=None
    ):
        try:
            # Log it (and write to the screen)
            self.controller.log('-'*77,
                                screen=True)
            self.controller.log('Notification received',
                                screen=True)
            self.controller.log('-'*77,
                                screen=True)
            self.controller.log('Notification from: {0}'.format(sender),
                                screen=True)
            self.controller.log('Received at      : {0}'.format(date_string),
                                screen=True)
            self.controller.log('Notification     : {0}'.format(notification),
                                screen=True)
            self.controller.log('Action           : {0}'.format(action),
                                screen=True)
            self.controller.log('-'*77,
                                screen=True)


            # Write to the screen file.
            outputfile = self.controller.get_value('output_device')
            f = open(outputfile,'a')
            f.write(('-'*80)+"\n")
            f.write('Notification from: {0}'.format(sender)+"\n")
            f.write('Received at      : {0}'.format(date_string)+"\n")
            f.write('Notification     : {0}'.format(notification)+"\n")
            f.write('Action           : {0}'.format(action)+"\n\n")
            f.close()
        except:
            raise

