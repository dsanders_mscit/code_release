################################################################################
#
# Program:      v3_00_Config_Lock_Control.py
# Class:        v3_00_Config_Lock_Control
# Objects:      Config_Lock_Control
# Component of: Phone
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: The configuration controller to for getting the lock state, locking,
#          and unlocking the phone. This control acts on behalf of two 
#          boundaries - Config_Lock_Boundary and Config_Unlock_Boundary.
#
################################################################################
#
# Import required modules and packages
#
from flask_restful import Resource
from flask import Response
from Phone import Control
import json

class v3_00_Config_Lock_Control(object):
    controller = None

    # Constructor
    def __init__(self):
        self.controller = Control.global_controller


    # Check if the phone is locked or unlocked. Default to unlocked if it is not
    # set for some reason. It may be better to fail to safe and assume locked.
    #
    def is_locked(self):
        success = 'success'
        status = '200'
        message = 'Device lock status.'
        data = None

        self.controller.log('Request to check if phone is locked.',
                              screen=False)

        # Get the current lock state from the KV store.
        current_state = self.controller.get_value('locked')

        # If it's not set, then default to unlocked.
        if current_state in (None, [], ''):
            current_state = 'unlocked'    # Default to unlocked

        # Return the state
        data = {'locked':current_state}

        self.controller.log('Request returned: {0}'.format(data),
                              screen=False)
        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value


    # A lock request contains no key validation or any other checking. As soon
    # as the lock is received, the phone is locked.
    #
    def lock_request(self):
        success = 'success'
        status = '200'
        message = 'Lock device action.'
        data = None

        self.controller.log('Locking phone.', screen=False)

        # Set the phone state to locked.
        lock_state = self.controller.set_value('locked','locked')

        # Return the state.
        data = {'locked':lock_state}
        self.controller.log('Phone locked.', screen=False)

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value


    # Unlock requests are only satisified if the control key (analoguous to the
    # phone's pin) is entered correctly.
    #
    def unlock_request(self, json_string=None):
        success = 'success'
        status = '200'
        message = 'Unlock device action.'
        data = None

        try:
            # Check the JSON is good.
            if json_string == None\
            or json_string == '':
                raise KeyError('Badly formed request!')

            self.controller.log('Request to unlock phone.', screen=False)

            # Load the JSON into a dict and get the key
            json_data = json.loads(json_string)
            key = json_data['key']

            # Check the key is correct.
            if not key == '1234-5678-9012-3456':
                raise ValueError('Unlock key incorrect.')

            # Set the phone state to unlocked.
            lock_state = self.controller.set_value('locked','unlocked')

            # Return the phone state.
            data = {'locked':lock_state}
            self.controller.log('Phone unlocked.', screen=False)
        except KeyError as ke:
            success = 'error'
            status = '400'
            message = 'Unlock, Key Error: {0}'.format(str(ke))
            self.controller.log(message, screen=False)
        except ValueError as ve:
            success = 'error'
            status = '403'
            message = 'Unlock, Value Error: {0}'.format(str(ve))
            self.controller.log(message, screen=False)
        except Exception as e:
            success = 'error'
            status = '500'
            message = 'Unlock, Exception: {0}'.format(repr(e))
            self.controller.log(message, screen=False)

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value

