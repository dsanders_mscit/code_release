###############################################################################
#
# Program:      v3_00_Config_Pair_Control.py
# Class:        v3_00_Config_Pair_Control
# Objects:      Config_Pair_Control
# Component of: Phone
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: The Bluetooth service enables devices to connect to it and broadcast
#          messages via emulation of low frequency devices. The config 
#          controller for the phone, allows the phone to connect to Bluetooth
#          and emulate the pairing of the phone.
#
################################################################################
#
# Import required modules and packages
#
from flask_restful import Resource
from flask import Response
from Phone import Control
import json, requests

class v3_00_Config_Pair_Control(object):
    controller = None

    # Constructor
    def __init__(self):
        self.controller = Control.global_controller


    # Check if the phone is paired
    def is_paired(self):
        success = 'success'
        status = '200'
        message = 'Phone Bluetooth pairing status.'
        data = None

        # Get current pair
        self.controller.log('Request to get Bluetooth pairing status.',
                              screen=False)
        device_paired = self.controller.get_bluetooth()

        # If the empty list is returned, change it to null
        if device_paired == []:
            device_paired = None

        # Build the data object
        pair_data = {'device':device_paired}

        self.controller.log('Bluetooth pairing status: {0}'.format(pair_data),
                              screen=False)

        # Return it
        return self.controller.do_response(
            message=message,
            data=pair_data,
            status=status,
            response=success
        )


    # Un-pair the phone
    def remove_pair(self, json_string=None):
        success = 'success'
        status = '200'
        message = 'Phone Bluetooth un-pairing request.'
        data = None

        try:
            self.controller.log('Request to un-pair Bluetooth',
                                  screen=False)


            # Check the JSON string is good.
            if json_string == None\
            or json_string == '':
                raise KeyError('Badly formed request!')

            # Load the JSON into a dict and get the key.
            json_data = json.loads(json_string)
            key = json_data['key']

            # Check the key
            if not key == '1234-5678-9012-3456':
                raise KeyError('Pairing key incorrect.')


            # Get the name of the phone and the Bluetooth deivce it is paired
            # with.
            self.controller.log('Getting phone friendly name',
                                  screen=False)
            phone_name = self.controller.get_value('phonename_ip')

            self.controller.log('Getting current pairing status',
                                  screen=False)
            device_paired = self.controller.get_bluetooth()

            # The device is not paired if the result is empty. So raise an error
            # and stop.
            if device_paired in (None,'',[]):
                raise ValueError('The phone is not paired, '+\
                                 'so cannot be un-paired')

            self.controller.log('Communicating with Bluetooth device {0}'\
                                      .format(device_paired),
                                  screen=False)

            # Send a message to the Bluetooth service and disconnect.
            # Build the URL
            bluetooth_url = device_paired + '/pair/' + phone_name

            # Clear the locally held pair value. Note, this should happen AFTER
            # the response back from the Bluetooth controller. Not changed due
            # to code freeze, D Sanders, 02 May 2016
            self.controller.clear_value(device_paired)
            device_paired = self.controller.set_bluetooth(None)
            data = {'device':None}

            # Send an HTTP DELETE request to the Bluetooth controller to
            # disconnect the pairing.
            request_response = requests.delete(bluetooth_url)
            status_code = request_response.status_code

            if status_code not in (200,201):
                # Here means some sort of error occurred.
                if status_code == 404:
                    raise ValueError('Unable to remove pairing as the '+\
                                     'URL cannot be located.')

                raise ValueError('Unable to remove pairing with device! '+\
                                 'Response code '+\
                                 '{0}'\
                                   .format(request_response.status_code)+\
                                 ' with data payload '+\
                                 '{0}'.format(request_response.text)
                                )

        except requests.exceptions.ConnectionError as rce:
            success = 'warning'
            status = '200'
            message = 'Phone un-paired from {0}'.format(device_paired)+\
                      '; however, the response from the Bluetooth device was'+\
                      ' a connection error: {0}'.format(str(rce))+'. '+\
                      'The Bluetooth device may be unavailable and '+\
                      'so un-pairing could not be confirmed.'
            self.controller.log(message,
                                  screen=False)
        except KeyError as ke:
            success = 'error'
            status = '400'
            message = 'Bluetooth un-pairing key error: {0}'.format(str(ke))
            self.controller.log(message, screen=False)
        except ValueError as ve:
            success = 'error'
            status = '404'
            message = 'Bluetooth un-pairing error: {0}'.format(str(ve))
            self.controller.log(message, screen=False)
        except Exception as e:
            success = 'error'
            status = '500'
            message = 'Bluetooth un-pairing exception: {0}'.format(str(ve))
            self.controller.log(message, screen=False)

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value


    # Pair the phone with Bluetooth.
    def set_pair(self, json_string=None):
        success = 'success'
        status = '200'
        message = 'Phone Bluetooth pairing request.'
        data = None

        try:
            # Check the JSON is good
            if json_string == None\
            or json_string == '':
                raise KeyError('Badly formed request!')

            # Load the JSON into a dict and get key and Bluetooth service
            # parameters
            json_data = json.loads(json_string)
            key = json_data['key']
            bluetooth = json_data['bluetooth']

            # Get the phone name
            phone_name = self.controller.get_value('phonename_ip')

            # Check the key is correct
            if not key == '1234-5678-9012-3456':
                raise ValueError('Pairing key incorrect.')

            # Build the Bluetooth URL. It is built rather than passed in so that
            # we can build the /broadcast/ call later. It would be better if the
            # config request passed both the pair and broadcast URLs as params;
            # that would let pair and broadcast scale independently and be more
            # microservice focused. Not changed due to code freeze, D Sanders,
            # 02 May 2016.
            bluetooth_url = bluetooth + '/pair/' + phone_name
            request_response = requests.post(bluetooth_url)
            status_code = request_response.status_code

            if status_code not in (200,201):
                # The pairing was unsuccessful
                if status_code == 404:
                    raise ValueError('Unable to pair with device as the URL '+\
                                     'cannot be located.')

                raise ValueError('Unable to pair with device! '+\
                                 'Response code '+\
                                 '{0}'.format(request_response.status_code)+\
                                 ' with data payload '+\
                                 '{0}'.format(request_response.text)
                                )
            else:
                bluetooth_key = request_response.json()['data']['key']
                self.controller.set_value(bluetooth, bluetooth_key)
            device_paired = self.controller.set_bluetooth(bluetooth)
            data = {'device':device_paired}
        except requests.exceptions.ConnectionError as rce:
            success = 'error'
            status = '500'
            message = 'Phone cannot be paired with {0}'.format(bluetooth)+\
                      '; the response from the Bluetooth device was'+\
                      ' a connection error: {0}'.format(str(rce))+'. '+\
                      'The Bluetooth device may be unavailable or the URL '+\
                      'may be incorrect.'
            self.controller.log(message, screen=False)
        except KeyError as ke:
            success = 'error'
            status = '400'
            message = 'Bluetooth un-pairing key error: {0}'.format(str(ke))
            self.controller.log(message, screen=False)
        except ValueError as ve:
            success = 'error'
            status = '404'
            message = 'Bluetooth un-pairing key error: {0}'.format(str(ve))
            self.controller.log(message, screen=False)
        except Exception as e:
            success = 'error'
            status = '500'
            message = 'Bluetooth un-pairing exception: {0}'.format(repr(e))
            self.controller.log(message, screen=False)

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value

