################################################################################
#
# Program:      v3_00_Config_Location_Control.py
# Class:        v3_00_Config_Location_Control
# Objects:      Config_Location_Control
# Component of: Phone
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: The configuration controller to set the phone's location. The Get 
#          call access the base Location_Control not the configurator. DRY = DO
#          NOT REPEAT YOURSELF :)
#
################################################################################
#
# Import required modules and packages
#
from flask_restful import Resource
from flask import Response
from Phone import Control
from Phone.Location_Control import location_control_object
#import Phone.Location_Control
import json, requests

class v3_00_Config_Location_Control(object):
    controller = None

    # Constructor
    def __init__(self):
        self.controller = Control.global_controller


    # Set the location of the phone.
    def set_loc(self, json_string=None):
        success = 'success'
        status = '200'
        message = 'Set location details.'
        data = None

        try:
            # Check the JSON
            if json_string == None\
            or json_string == '':
                raise KeyError('Badly formed request!')

            # Load the JSON into a dict
            json_data = json.loads(json_string)
            key = json_data['key']

            # Check the key is correct.
            if not key == '1234-5678-9012-3456':
                raise IndexError('Control key incorrect.')

            # Convert the JSON X and Y data into floats. If they are bad, e.g.
            # X:ABCD, Y:13.2 then a value error will be raised.
            x = float(json_data['x'])
            y = float(json_data['y'])

            self.controller.log('Request to set location to {0},{1}'\
                                  .format(x,y), screen=False)

            # Persist the location in the KV store.
            self.controller.set_value('x', str(x))
            self.controller.set_value('y', str(y))

            data = {"x":x, "y":y}
            self.controller.log('Location changed.', screen=False)
        except KeyError as ke:
            success = 'error'
            status = '400'
            message = 'Set location, Key Error: {0}'.format(str(ke))
            self.controller.log(message, screen=False)
        except ValueError as ve:
            success = 'error'
            status = '400'
            message = 'Set location, Value Error: {0}'.format(str(ve))
            self.controller.log(message, screen=False)
        except IndexError as ie:
            success = 'error'
            status = '403'
            message = 'Set location, Index Error: {0}'.format(str(ie))
            self.controller.log(message, screen=False)
        except Exception as e:
            success = 'error'
            status = '500'
            message = 'Set location, Exception: {0}'.format(repr(e))
            self.controller.log(message, screen=False)

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value

