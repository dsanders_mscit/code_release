################################################################################
#
# Program:      v3_00_Config_Monitor_App_Control.py
# Class:        v3_00_Config_Monitor_App_Control
# Objects:      Config_Monitor_App_Control
# Component of: Phone
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: The monitor app controls connecting, disconnecting, and check the
#          relationship of the monitor app. The monitor app can be started and
#          stopped. When started, it checks any apps launched on the phone as
#          well as checking the phone's location every 30 seconds.
#
################################################################################
#
# Import required modules and packages
#
from flask_restful import Resource
from flask import Response
from Phone import Control
import json, requests

class v3_00_Config_Monitor_App_Control(object):
    controller = None

    # Constructor
    def __init__(self):
        self.controller = Control.global_controller


    # Is the monitor app running?
    def is_launched(self):
        success = 'success'
        status = '200'
        message = 'Phone Monitor App launch status.'
        data = None

        try:
            self.controller.log('Request to check Monitor App state.',
                                  screen=False)
            state = 'connected'

            # Get the monitor app connection from the kv store.
            monitor_app = self.controller.get_value('monitor_app')

            # If it's the empty list, then the phone is not connected to a
            # monitor app, so return disconencted.
            if monitor_app == []:
                state = 'disconnected'
                monitor_app = None

            # Otherwise, return the monitor app (it is a URL) found in the KV
            # store.
            data = {'state':state,
                    'monitor-app':monitor_app}
            self.controller.log('Monitor App state returned as:{0}'\
                                      .format(data),
                                  screen=False)
        except Exception as e:
            success = 'error'
            status = '500'
            message = 'Exception: {0}'.format(repr(e))
            self.controller.log(message, screen=False)

        return self.controller.do_response(message=message,
                                             data=data,
                                             status=status,
                                             response=success)


    # Start a monitor app (connect it) with this phone.
    def start(self, json_string=None):
        success = 'success'
        status = '200'
        message = 'Phone Monitor App request.'
        data = None

        try:
            # Check the JSON is good.
            if json_string == None\
            or json_string == '':
                raise KeyError('No JSON Data!')

            self.controller.log('Request to start Monitor App',
                                  screen=False)

            # Load the JSON data into a dict.
            json_data = json.loads(json_string)

            self.controller.log('Getting JSON data',
                                  screen=False)

            # Get the parameters from the JSON
            #
            #   key                  : control key.
            #   monitor-app          : URL for the monitor app
            #   notification-service : URL for the notification service
            #   location-service     : URL for the location (hot spot checking) 
            #                          service
            #   
            key = json_data['key']
            monitor_app = json_data['monitor-app']
            service = json_data['notification-service']
            location_service = json_data['location-service']

            self.controller.log('Validating key.',
                                  screen=False)

            # Check the key
            if not key == '1234-5678-9012-3456':
                raise ValueError('Pairing key incorrect.')

            self.controller.log('Calculating recipient and location provider',
                                  screen=False)

            # Build the URL for the recipient (this phone) so it can receive
            # notifications from the monitor app. It's actually the notification
            # service that will issue them, but the monitor app needs to know
            # who to address them to (this phone).
            #
            recipient = 'http://{0}:{1}/{2}/notification'\
                        .format(self.controller.get_value('ip_addr'),
                                self.controller.get_value('port_number'),
                                self.controller.get_value('version'))

            self.controller.log('Set recipient to {0}'.format(recipient),
                                  screen=False)

            # Create the URL for the location provider. That's this phone BUT
            # not every device might have a location (i.e. GPS) provider, some
            # might have something else. So, we build the URL.
            #
            location_provider = 'http://{0}:{1}/{2}/location'\
                        .format(self.controller.get_value('ip_addr'),
                                self.controller.get_value('port_number'),
                                self.controller.get_value('version'))

            self.controller.log('Set location provider to {0}'\
                                      .format(location_provider),
                                  screen=False)

            self.controller.log('Setting Monitor App URL',
                                  screen=False)

            # We set the monitor app URL to state and on.
            monitor_app_url = monitor_app + '/state/on'

            self.controller.log('Monitor App URL set to {0}'\
                                      .format(monitor_app_url),
                                  screen=False)

            # Build the payload
            payload = {
                       "key":"1234-5678-9012-3456",
                       "notification-service":service,
                       "location-service":location_service,
                       "location-provider":location_provider,
                       "recipient":recipient
                      }

            self.controller.log('Submitting Monitor App start to {0} '\
                                  .format(monitor_app_url)+\
                                  'with payload {0}'.format(payload),
                                  screen=False
                                 )

            # Issue the HTTP POST request
            request_response = requests.post(monitor_app_url,
                                             json.dumps(payload))

            status_code = request_response.status_code

            self.controller.log('Monitor App request returned status: {0} '\
                                  .format(status_code),
                                  screen=False
                                 )

            # Check the status of the request
            if status_code not in (200,201):
                # IF here, then we've been unable to communicate successfully
                # with the monitor app, so raise the error.
                if status_code == 404:
                    raise ValueError('Cannot communicate with monitor app. '+\
                                     'Response from request was {0} {1}.'\
                                     .format(status_code, request_response.text)
                                    )

                raise ValueError('Unable to communicate with monitor app! '+\
                                 'Response code '+\
                                 '{0}'.format(request_response.status_code)+\
                                 ' with data payload '+\
                                 '{0}.'.format(request_response.text)
                                )
            else:
                # Get the returned JSON
                json_response = request_response.json()

                # Check for errors.
                if 'error' in json_response:
                    raise ValueError(json_response['message'])

                # Get the state the monitor app reported.
                state = json_response['data']['state']
                self.controller.set_value('monitor_app', monitor_app)

                # Built the return data dict.
                data = {'state':'connected',
                        'monitor-app':monitor_app}

                self.controller.log('Monitor App returned: {0} '\
                                          .format(data),
                                      screen=False
                                     )
        except requests.exceptions.ConnectionError as rce:
            success = 'error'
            status = '500'
            message = 'Phone cannot communicate with the monitor app running '+\
                      'at {0}'.format(monitor_app)+\
                      '; the response from the monitor app was'+\
                      ' a connection error: {0}'.format(str(rce))+'.'
        except KeyError as ke:
            success = 'error'
            status = '400'
            message = 'Start monitor app: {0}'.format(str(ke))
            self.controller.log(message, screen=False)
        except ValueError as ve:
            success = 'error'
            status = '400'
            message = 'Start monitor app: {0}'.format(str(ve))
            self.controller.log(message, screen=False)
        except Exception as e:
            success = 'error'
            status = '500'
            message = 'Start monitor app: {0}'.format(repr(e))
            self.controller.log(message, screen=False)

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value


    # Stop monitoring (i.e. disconnect the monitor app).
    def stop(self, json_string=None):
        success = 'success'
        status = '200'
        message = 'Phone Monitor App request.'
        data = None

        try:
            self.controller.log('Request to sop Monitor App',
                                  screen=False)

            # Check the JSON is good
            if json_string == None\
            or json_string == '':
                raise KeyError('No JSON data!')

            # Load the JSON into a dict.
            json_data = json.loads(json_string)

            self.controller.log('Validating key.',
                                  screen=False)

            # Get the key
            key = json_data['key']

            # Check the key
            if not key == '1234-5678-9012-3456':
                raise ValueError('Pairing key incorrect.')

            self.controller.log('Getting the current Monitor App config.',
                                  screen=False)


            # Check the current monitor app setting in the KV store.
            monitor_app = self.controller.get_value('monitor_app')

            # If it's not running, raise an error - can't disconnect something
            # which is not connected.
            #
            if monitor_app == [] or monitor_app == None:
                raise ValueError('The monitor app is not running.')

            self.controller.log('Monitor App currently on {0}'\
                                      .format(monitor_app),
                                  screen=False)

            # Build the monitor app state URL with state set to off.
            monitor_app_url = monitor_app + '/state/off'

            # Build the payload
            payload = {"key":"1234-5678-9012-3456"}

            self.controller.log('Submitting Monitor App stop to {0} '\
                                  .format(monitor_app_url)+\
                                  'with payload {0}'.format(payload),
                                  screen=False
                                 )

            # Issue the HTTP POST request
            request_response = requests.post(monitor_app_url,
                                             json.dumps(payload))

            status_code = request_response.status_code

            self.controller.log('Monitor App request returned status: {0} '\
                                  .format(status_code),
                                  screen=False
                                 )

            # Check the status of the post.
            if status_code not in (200,201):
                if status_code == 404:
                    raise ValueError('Cannot communicate with monitor app.'+\
                                     ' Response from request was {0} {1}.'\
                                     .format(status_code, request_response.text)
                                    )

                raise ValueError('Unable to communicate with monitor app! '+\
                                 'Response code '+\
                                 '{0}'.format(request_response.status_code)+\
                                 ' with data payload '+\
                                 '{0}.'.format(request_response.text)
                                )
            else:
                # Get the JSON returned
                json_response = request_response.json()

                # If it's in error, raise it.
                if 'error' in json_response:
                    raise ValueError(json_response['message'])

                # Get the state reported by the monitor app.
                state = json_response['data']['state']
                self.controller.clear_value('monitor_app')

                # Build the return data.
                data = {'state':'disconnected',
                        'monitor-app':None}
                self.controller.log('Monitor App returned: {0} '\
                                          .format(data),
                                      screen=False
                                     )
        except requests.exceptions.ConnectionError as rce:
            success = 'error'
            status = '500'
            message = 'Phone cannot communicate with the monitor app '+\
                      'at {0}'.format(monitor_app)+\
                      '; the response from the monitor app was'+\
                      ' a connection error: {0}'.format(str(rce))+'.'
        except KeyError as ke:
            success = 'error'
            status = '400'
            message = 'Start monitor app: {0}'.format(str(ke))
            self.controller.log(message, screen=False)
        except ValueError as ve:
            success = 'error'
            status = '400'
            message = 'Start monitor app: {0}'.format(str(ve))
            self.controller.log(message, screen=False)
        except Exception as e:
            success = 'error'
            status = '400'
            message = 'Start monitor app: {0}'.format(repr(e))
            self.controller.log(message, screen=False)

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value

