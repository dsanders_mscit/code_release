###############################################################################
#
# Program:      v3_00_Config_Push_Notifications_Control.py
# Class:        v3_00_Config_Push_Notifications_Control
# Objects:      Config_Push_Notifications_Control
# Component of: Phone
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: The Notification Service (NS) sends notifications to the phone on 
#          behalf of other services. If the phone is not available (say turned 
#          off), then NS stores them. The Push controller on the phone enables
#          the phone to be told to request NS to push the notifications - NOTE:
#          this is not a FETCH operation but a REQUEST which TELLs NS to send
#          persisted notifications.
#
################################################################################
#
# Import required modules and packages
#
from flask_restful import Resource
from flask import Response
from Phone import Control
import json, requests

class v3_00_Config_Push_Notifications_Control(object):
    controller = None

    # Constructor
    def __init__(self):
        self.controller = Control.global_controller


    # Request the push
    def request_push(self, json_string=None):
        success = 'success'
        status = '200'
        message = 'Phone requesting push of notifications.'
        data = None

        try:
            self.controller.log('Request to ask Notification Service to '+\
                                  'push persisted notifications.', screen=False)

            # Check the JSON is good
            if json_string == None\
            or json_string == '':
                raise KeyError('Badly formed request!')

            # Load the JSON into a dict and get the key and the Notification
            # Service URL. Ideally, this would be stored in the KV store and
            # retrieved, but that is not currently implemented. Not changed due
            # to code freeze, D Sanders, 02 May 2016.
            json_data = json.loads(json_string)
            key = json_data['key']
            service = json_data['notification-service']

            # If changes to the URL are required they are made here. EG, adding
            # a path to the user provided URL. currently does nothing.
            service_url = service

            self.controller.log('Validating key.',
                                  screen=False)
            # Check the key
            if not key == '1234-5678-9012-3456':
                raise ValueError('Control key incorrect.')

            self.controller.log('Calculating recipient.',
                                  screen=False)

            # Build the recipient URL - always based on the IP address {0}, port
            # number {1}, and version {2} of the phone
            recipient = 'http://{0}:{1}/{2}/notification'\
                        .format(self.controller.get_value('ip_addr'),
                                self.controller.get_value('port_number'),
                                self.controller.get_value('version'))

            # Build the payload - the key and the recipient
            payload = {
                       "key":"1234-5678-9012-3456",
                       "recipient":recipient
                      }

            self.controller.log('Issuing request to notification service '+\
                                  'with payload: {0} '.format(str(payload)),
                                  screen=False)

            # HTTP Post the request to the Notification Service
            request_response = requests.post(service_url,
                                             json.dumps(payload))

            status_code = request_response.status_code
            self.controller.log('Push request returned status: {0} '\
                                  .format(status_code),
                                  screen=False
                                 )

            # Check the return status.
            if status_code not in (200,201,404):
                # Notice that 404 (not found) errors do NOT raise an error. They
                # are handled below in the ELSE branch.
                raise ValueError('Unable to communicate with service! '+\
                                 'Response code '+\
                                 '{0}'.format(request_response.status_code)+\
                                 ' with data payload '+\
                                 '{0}.'.format(request_response.text)
                                )
            else:
                #
                # 404 messages are errors ONLY IF there is no valid notification
                # count in the return text. Note the raw text is validated as a
                # real 404 error would not populate JSON.
                #
                if (status_code == 404 or status_code == 403)\
                and not ('notification-count' in str(request_response.text)):
                    raise ValueError('Unable to communicate with service. '+\
                                     'Response from request was {0} {1}.'\
                                     .format(status_code, request_response.text)
                                    )

                self.controller.log('Response received from Notification '+\
                                      'Service', screen=False)

                # Load the returned JSON
                json_response = request_response.json()
                if 'error' in json_response:
                    # 404 and 403 errors will be catched here if there is a
                    # notification count. See above.
                    raise ValueError(json_response['message'])

                #
                # Get the data from the response - the number of messages that
                # will be sent. Note, this is a push REQUEST not a FETCH, so 
                # there are no actual notifications returned. They are returned
                # in the proper fashion by the NS.
                #
                request_status = json_response['status']
                data = {'push':json_response['data']}
                self.controller.log('Received: {0}'.format(str(data)),
                                      screen=False)
        except requests.exceptions.ConnectionError as rce:
            success = 'error'
            status = '500'
            message = 'Phone cannot communicate with notification '+\
                      'service running at {0}'.format(service)+\
                      '; the response from the monitor app was'+\
                      ' a connection error: {0}'.format(str(rce))+'.'+\
                      'The service is probably not running.'
            self.controller.log(message, screen=False)
        except KeyError as ke:
            success = 'error'
            status = '400'
            message = 'Push request key error: {0}'.format(str(ke))
            self.controller.log(message, screen=False)
        except ValueError as ve:
            success = 'error'
            status = '403'
            message = 'Push request value error: {0}'.format(str(ve))
            self.controller.log(message, screen=False)
        except Exception as e:
            success = 'error'
            status = '500'
            message = 'Push request exception: {0}'.format(repr(e))
            self.controller.log(message, screen=False)

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value

