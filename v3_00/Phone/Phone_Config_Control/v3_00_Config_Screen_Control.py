###############################################################################
#
# Program:      v3_00_Config_Screen_Control.py
# Class:        v3_00_Config_Screen_Control
# Objects:      Config_Screen_Control
# Component of: Phone
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: The screen controller allows the screen for the device to be 
#          transferred as a plain text file using built-in Flask methods.
#
################################################################################
#
# Import required modules and packages
#
from flask_restful import Resource
from flask import Response, send_file
from Phone import Control
import json

class v3_00_Config_Screen_Control(object):
    controller = None

    # Constructor
    def __init__(self):
        self.controller = Control.global_controller


    # Get the screen and send it as text.
    def get_screen(self):
        #
        # The default response is not used. The variable assignments below do
        # not do anything and should be removed. Not changed due to code freeze,
        # D Sanders, 02 May 2016.
        #
        success = 'success'
        status = '200'
        message = 'Unlock device action.'
        data = None

        return_response = None

        try:
            # Form the output device filename based on the output device name
            # stored in the KV store and the path in the container (ALWAYS
            # /Phone/).
            output_device = '/Phone/{0}'\
                .format(self.controller.get_value('output_device'))
            #
            # Send the file back to the caller
            #
            return_response = send_file(output_device)
        except Exception as e:
            # Something went wrong, so repor it.
            error_msg = 'An exception occurred: {0}'.format(repr(e))
            print(error_msg)
            self.controller.log(error_msg, screen=False)

        # Return the response - NOTE it is NOT an HTTP response but rather a
        # flat file.
        return return_response

