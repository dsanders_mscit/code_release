################################################################################
#
# Program:      v3_00_Config_Launch_Control.py
# Class:        v3_00_Config_Launch_Control
# Objects:      Config_Launch_Control
# Component of: Phone
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: The controller to emulate launching an application on the phone.
#
################################################################################
#
# Import required modules and packages
#
from flask_restful import Resource
from flask import Response
from Phone import Control
import json, requests

class v3_00_Config_Launch_Control(object):
    controller = None

    # Constructor
    def __init__(self):
        self.controller = Control.global_controller


    # The launch method detects an app
    def launch(self, app=None, json_string=None):
        success = 'success'
        status = '200'
        message = 'App Launched'
        data = None

        try:
            self.controller.log('Application launch detected.', screen=False)

            # Check the JSON and app parameters.
            if json_string == None\
            or json_string == ''\
            or app == None\
            or app == '':
                raise KeyError('Badly formed request. There was no JSON!')

            # Load the JSON into a dict and get the key.
            json_data = json.loads(json_string)
            key = json_data['key']

            # Check the key is valid.
            if not key == '1234-5678-9012-3456':
                raise ValueError('Control key incorrect.')

            self.controller.log('Check if monitor app is configured.',
                                  screen=False)

            # Find out if the monitor app is running (i.e. the phone is 
            # connected to a monitor app model).
            monitor_app = self.controller.get_value('monitor_app')

            # If it's not, then ignore the app launch.
            if monitor_app == [] or monitor_app == None:
                self.controller.log('No monitor app, so ignore.',
                                      screen=False)
                data = {'app':app, 'state':'launched'}
            else:
                # The phone IS connected to a monitor app, so notify it.
                self.controller.log('Monitor app is configured, so notify.',
                                      screen=False)

                # Call the notify helper method
                return_status = self.notify_monitor(app, monitor_app)

                # Check it returned ok.
                if 'error' in return_status:
                    # If it didn't, deal with the error.
                    status_code = return_status['status']
                    if  status_code == 500:
                        raise requests.exceptions.ConnectionError()
                    elif status_code == 400:
                        raise KeyError()
                    elif status_code == 400:
                        raise ValueError(return_status['message'])
                    else:
                        raise Exception(return_status['message'])
                else:
                    data = return_status
        except requests.exceptions.ConnectionError as rce:
            return {'success':'error',
                    'status':500,
                    'message':'Phone cannot communicate with the monitor '+\
                      'app running at {0}'.format(monitor_app)+\
                      '; the response from the monitor app was'+\
                      ' a connection error: {0}'.format(str(rce))+'.'
                   }
        except KeyError as ke:
            success = 'error'
            status = '400'
            message = 'Key Error: {0}'.format(str(ke))
        except ValueError as ve:
            success = 'error'
            status = '403'
            message = 'Value Error: {0}'.format(str(ve))
        except Exception as e:
            success = 'error'
            status = '500'
            message = 'Exception: {0}'.format(str(ve))

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value


    # Helper method to send the app launch status to the monitor app.
    def notify_monitor(self, app=None, monitor_app=None):
        try:
            # Create the URL for the monitor app. When connected to the monitor
            # app, the base url (http://server:port/version) is provided, so
            # we append /launched/app where app is the name of the app being
            # launched (e.g. Facebook, mail, Grindr, etc.)
            #
            monitor_app_url = monitor_app + '/launched/' + app

            # Create the payload for the monitor app. In this case, it is just
            # the key. The key would, in the real-world, be passed at the point
            # the relationship with the monitor app was created and we'd 
            # probably get it from the KV store.
            #
            payload = {"key":"1234-5678-9012-3456"}

            self.controller.log('Connecting to {0} with payload {1}'\
                                      .format(monitor_app_url, payload),
                                  screen=False)

            # Issue the HTTP post request
            request_response = requests.post(monitor_app_url,
                                             json.dumps(payload))

            status_code = request_response.status_code
            self.controller.log('Monitor App {0} returned status {1}'\
                                      .format(monitor_app_url, status_code),
                                  screen=False)

            # Handle the return status.
            if status_code not in (200,201,404):
                raise ValueError('Unable to communicate with monitor app! '+\
                                 'Response code '+\
                                 '{0}'.format(request_response.status_code)+\
                                 ' with data payload '+\
                                 '{0}.'.format(request_response.text)
                                )
            else:
                if status_code == 404\
                and not ('not being monitored' in str(request_response.text)):
                    raise ValueError('Unable to communicate with monitor app.'+\
                                     ' Response from request was {0} {1}.'\
                                     .format(status_code, request_response.text)
                                    )
                json_response = request_response.json()
                if 'error' in json_response:
                    raise ValueError(json_response['message'])

                return_value = {'app':app, 'state':'launched'}
                self.controller.log('Returning {0}'\
                                          .format(return_value),
                                      screen=False)
                return return_value
        except requests.exceptions.ConnectionError as rce:
            # If here, it means that requests was unable to connect to the URL
            # provided. Could mean a number of things, so report back that the
            # connection could not be made.
            return {'success':'error',
                    'status':500,
                    'message':'Phone cannot communicate with the monitor '+\
                      'app running at {0}'.format(monitor_app)+\
                      '; the response from the monitor app was'+\
                      ' a connection error: {0}'.format(str(rce))+'.'
                   }
        except KeyError as ke:
            return {'success':'error',
                    'status':400,
                    'message':'Badly formed request!'
                   }
        except ValueError as ve:
            return {'success':'error',
                    'status':403,
                    'message':str(ve)
                   }
        except Exception as e:
            return {'success':'error',
                    'status':500,
                    'message':repr(e)
                   }


