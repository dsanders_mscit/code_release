################################################################################
#
# Program:      Config_Lock_Boundary.py
# Class:        Config_Lock_Boundary
# Objects:      Config_Lock_Boundary
# Component of: Phone
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Get the lock status of the phone or lock it. Supports GET (status)
#          and POST (lock).
#
################################################################################
from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Phone import app, api
from Phone_Config_Control.Config_Lock_Control import lock_control_object

class Config_Lock_Boundary(Resource):
    def get(self):
        return lock_control_object.is_locked()


    def post(self):
        return lock_control_object.lock_request()


