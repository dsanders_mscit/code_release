################################################################################
#
# Program:      Config_Push_Notifications_Boundary.py
# Class:        Config_Push_Notifications_Boundary
# Objects:      Config_Push_Notifications_Boundary
# Component of: Phone
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Instruct the phone to request the push of all notifications intended
#          for it. The phone sends the request to the Notification Service used
#          by the phone. Supports POST only.
#
################################################################################
from flask_restful import Resource, reqparse
from Phone_Config_Control.Config_Push_Notifications_Control \
    import push_control_object

class Config_Push_Notifications_Boundary(Resource):
    def post(self):
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        return_state = push_control_object.request_push(json_string=raw_data)

        return return_state

