################################################################################
#
# Program:      Config_Location_Boundary.py
# Class:        Config_Location_Boundary
# Objects:      Config_Location_Boundary
# Component of: Phone
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Configuration boundary for getting and setting the current location
#          (X and Y coordinates) of the phone. Supports GET and PUT - get the
#          location or update it.
#
################################################################################
from flask_restful import Resource, reqparse
from Phone_Config_Control.Config_Location_Control \
    import config_location_control_object
from Phone.Location_Control import location_control_object

class Config_Location_Boundary(Resource):
    def put(self):
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        return_state = config_location_control_object\
                           .set_loc(json_string=raw_data)

        return return_state


    def get(self):
        return location_control_object.location_request()
