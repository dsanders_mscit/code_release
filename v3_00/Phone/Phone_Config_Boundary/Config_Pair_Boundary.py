################################################################################
#
# Program:      Config_Pair_Boundary.py
# Class:        Config_Pair_Boundary
# Objects:      Config_Pair_Boundary
# Component of: Phone
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Get, set or clear the pairing of this phone with a Bluetooth device.
#          Supports GET, POST, and DELETE.
#
################################################################################
from flask_restful import Resource, reqparse
from Phone_Config_Control.Config_Pair_Control import pair_control_object

class Config_Pair_Boundary(Resource):
    def get(self):
        return_state = pair_control_object.is_paired()
        return return_state


    def post(self):
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        return_state = pair_control_object.set_pair(json_string=raw_data)

        return return_state


    def delete(self):
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        return_state = pair_control_object.remove_pair(json_string=raw_data)

        return return_state

