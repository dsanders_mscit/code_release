################################################################################
#
# Program:      Config_Launch_Boundary.py
# Class:        Config_Launch_Boundary
# Objects:      Config_Launch_Boundary
# Component of: Phone
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Configuration boundary for emulating the launching of an app on the
#          phone. App can be anything and there is no validation that the app
#          is actually installed. Supports POST only - apps are launched and
#          terminate as soon as they've been validated; i.e., they don't really
#          run nor need to be stopped.
#
################################################################################
from flask_restful import Resource, reqparse
from Phone_Config_Control.Config_Launch_Control \
    import launch_control_object

class Config_Launch_Boundary(Resource):
    def post(self, app):
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        return_state = launch_control_object.launch(app=app,
                                                    json_string=raw_data)

        return return_state

