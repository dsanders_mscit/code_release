################################################################################
#
# Program:      Config_Screen_Boundary.py
# Class:        Config_Screen_Boundary
# Objects:      Config_Screen_Boundary
# Component of: Phone
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Transfer the screen of this phone as a text file. Supports GET only.
#
################################################################################
from flask_restful import Resource, reqparse
from Phone_Config_Control.Config_Screen_Control \
    import screen_control_object

class Config_Screen_Boundary(Resource):
    def get(self):
        return_state = screen_control_object.get_screen()
        return return_state

