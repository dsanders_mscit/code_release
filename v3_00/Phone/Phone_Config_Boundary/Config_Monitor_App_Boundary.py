################################################################################
#
# Program:      Config_Monitor_App_Boundary.py
# Class:        Config_Monitor_App_Boundary
# Objects:      Config_Monitor_App_Boundary
# Component of: Phone
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Get the status, connects, or disconnects to a monitor app which 
#          monitors the phone (its location and apps launched). If the phone
#          enters a hot spot or launches a monitored application, it is the 
#          monitor app connected to by this boundary that will detect it NOT the
#          phone. Supports GET, POST, and DELETE.
#
################################################################################
from flask_restful import Resource, reqparse
from Phone_Config_Control.Config_Monitor_App_Control \
    import monitor_app_control_object

class Config_Monitor_App_Boundary(Resource):
    def get(self):
        return monitor_app_control_object.is_launched()


    def post(self):
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        return_state = monitor_app_control_object.start(json_string=raw_data)

        return return_state


    def delete(self):
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        return_state = monitor_app_control_object.stop(json_string=raw_data)

        return return_state

