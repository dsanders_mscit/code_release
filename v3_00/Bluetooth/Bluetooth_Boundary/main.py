################################################################################
#
# Program:      main.py
# Class:        None
# Objects:      None
# Component of: Broadcast
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: main Python program for the Boundary package. This code defines the
#          routes that will be offered by Flask (via the NGINX/UWSGI services)
#          and provide the interfaces and boundaries to the model.
#
# References
# ----------
# Ronacher, A., 2013, 'Larger Applications' [ONLINE]. Available at: 
# http://flask.pocoo.org/docs/0.10/patterns/packages/ (Accessed: 04 November 
# 2015)
#
################################################################################

# Import Flask and Flaks Restful components
from flask_restful import Resource, Api, reqparse, abort
from flask import Response

# Import modules required from the model.
from Bluetooth import app, api
from Bluetooth.Control import global_control
from Bluetooth_Boundary import apiR
from Bluetooth_Boundary.Pair_Boundary import Pair_Boundary
from Bluetooth_Boundary.Broadcast_Boundary import Broadcast_Boundary

#
# Get the version of the API
#
version = global_control.get_value('version')

# Add a route - e.g. http://127.0.0.1:8080/v1/broadcast/friendly_name
apiR.add_resource(Broadcast_Boundary,
                  '/{0}/broadcast/<string:devicename>'.format(version))

# Pair a device - e.g. http://127.0.0.1:8080/v1/pair/friendly_name
apiR.add_resource(Pair_Boundary,
                  '/{0}/pair/<string:devicename>'.format(version))

