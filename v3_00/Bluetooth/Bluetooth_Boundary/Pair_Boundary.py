################################################################################
#
# Program:      Broadcast_Boundary.py
# Class:        Broadcast_Boundary
# Objects:      None
# Component of: Broadcast
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Define the boundary methods that will be used to respond to http
#          requests to pair a device with this Bluetooth device. Callers can GET
#          (query an existing pairing), POST (create a pairing), or DELETE 
#          (cancel an existing pairing). Note, no validation is done at the
#          boundary level.
#
# Notes:   The Bluetooth model was one of the earliest and it does show. They
#          solution is good but the code is not the most Pythonesque. This model
#          is a good candidate for optimization.
#
################################################################################
#
# Import required modules and packages
#
from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Bluetooth import app, api
from Bluetooth.Control import global_control as control
from Bluetooth.Pairing_Control \
    import global_pair_control as pair_control_object
from Bluetooth_Boundary import apiR

class Pair_Boundary(Resource):
    # Query a pairing
    def get(self, devicename):
        return_value = pair_control_object.pair_info(devicename)
        return return_value


    # Create a pairing
    def post(self, devicename):
        return_value = pair_control_object.pair_device(devicename)
        return return_value

    # Remove a pairing
    def delete(self, devicename):
        return_value = pair_control_object.pair_unpair(devicename)
        return return_value


