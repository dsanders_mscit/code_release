################################################################################
#
# Program:      Broadcast_Boundary.py
# Class:        Broadcast_Boundary
# Objects:      None
# Component of: Broadcast
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Define the boundary methods that will be used to respond to http
#          requests to broadcast a message to a paired Bluetooth device. In the
#          Bluetooth module, some of the boundaries handle validation (as this
#          one does) - other models simply pass the JSON in the request to the
#          controller.
#
#          The researcher is undecided on optimal approach and recommends this
#          as an area of further research.
#
#
# Notes:   The Bluetooth model was one of the earliest and it does show. They
#          solution is good but the code is not the most Pythonesque. This model
#          is a good candidate for optimization.
#
################################################################################
#
# Import required modules and packages
#
from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Bluetooth \
    import app, api
from Bluetooth.Control import global_control as control
from Bluetooth.Pairing_Control \
    import global_pair_control as pair_control_object
from Bluetooth.Broadcast_Control \
    import global_broadcast_control as broadcast_control_object
from Bluetooth_Boundary import apiR
import json

class Broadcast_Boundary(Resource):

    # Handle POST requests
    def post(self, devicename):
        # Get the raw data that came with the request
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')

        # If it's empty return. This will return null
        if raw_data == None or raw_data == '':
            return

        # Load the data into a dict item and validate that the key is correct
        # This is an example of the early processing in the research project.
        json_data = json.loads(raw_data)
        if not 'message' in json_data\
        or not 'key' in json_data:
            return

        # Invoke the controller.
        return_state = broadcast_control_object.broadcast_message(
                devicename = devicename,
                text = json_data['message'],
                key = json_data['key']
            )

        # Return whatever the controller gave us.
        return return_state



