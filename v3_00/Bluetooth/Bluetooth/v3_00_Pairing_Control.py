################################################################################
#
# Program:      v3_00_Pairing_Control.py
# Class:        v3_00_Pairing_Control
# Objects:      None
# Component of: Broadcast
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Pairing Control manages the pairing, un-pairing, and state querying
#          for devices connecting to the Bluetooth system.
#
# Notes:   The Bluetooth model was one of the earliest and it does show. They
#          solution is good but the code is not the most Pythonesque. This model
#          is a good candidate for optimization.
#
################################################################################
#
# Import base library modules
from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Bluetooth import Pairing_Database
from Bluetooth.Control import global_control
import datetime, time, json

class v3_00_Pairing_Control(object):
    pairing_db = None
    config_database = 'datavolume/config.db'
    display_filename = 'datavolume/display_output.txt'
    controller = global_control


    # Constructor
    def __init__(self):
        controller = global_control
        self.pairing_db = self.controller.get_pairing_db()


    # What is the device's pairing status?
    def check_pairing(self, devicename):
        return self.pairing_db.check_pairing(devicename)


    # Check if devicename is paired and return status (and key if paired) as an
    # http response.
    def pair_info(self, devicename):
        success = 'success'
        status = '200'
        message = 'Device is paired.'

        # Check the pairing
        pairing = self.check_pairing(devicename)

        self.controller.log('Pairing status request received for "{0}"'\
            .format(devicename))

        # If it's not paired, raise an HTTP error 404 (not found)
        if pairing == []:
            success = 'error'
            status = '404'
            message = 'Device is not paired'
            pairing = None
            self.controller.log('A pairing error occurred: {0}'\
                .format(message))

        # Build the return data object
        data = {"device":devicename,
                "key":pairing}

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        self.controller.log('Device "{0}" pairing request complete ({1})'\
            .format(devicename, success))

        return return_value


    # Handle a request to pair to this service
    def pair_device(self, devicename):
        success = 'success'
        status = '200'
        message = 'Device successfully paired.'

        # Check if the device is already paired.
        existing_key = self.check_pairing(devicename) or None

        self.controller.log(log_message='Pair request from "{0}"'\
            .format(devicename))

        # Build the return data object and pair the device.
        data = {"device":devicename,
                "key":self.pairing_db.pair_device(devicename)}

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)

        self.controller.log(
             log_message='Paired with "{0}" ({1} {2})'\
                .format(devicename, success, status)
        )

        return return_value


    # Handle request to unpair devicename
    def pair_unpair(self, devicename):
        self.controller.log(log_message='Un-pair request received form "{0}"'\
            .format(devicename))

        # Make sure the device is actually paired.
        if not self.check_pairing(devicename):
            self.controller.log(
                log_message='Un-pair error: {0} is not paired'\
                    .format(devicename))
            self.controller.log(
                 log_message='Un-pair error "{0}" ({1} {2})'\
                    .format(devicename, 'error', '404')
            )
            return self.controller.do_response(status="404",
                                    response='error',
                                    message="{0} is not paired."\
                                        .format(devicename))

        # Check the device unpaired successfully
        if self.pairing_db.remove_pairing(devicename):
            self.controller.log(
                 log_message='Device "{0}" un-paired. ({1} {2})'\
                    .format(devicename, 'success', '200')
            )
            return self.controller.do_response(
                message="{0} un-paired.".format(devicename))


    # What output devices (ear piece, speaker, etc.) are defined for this
    # Bluetooth service and paired device? Different devices can have unique
    # devices.
    #
    def get_output_devices(self, devicename=None):
        if devicename == None:
            return []

        return self.pairing_db.get_output_devices(devicename)


    # Remove an output device from the paired device's Bluetooth devices
    def remove_output_device(self, devicename=None, output_item=None):
        # Check that a devicename and an output item is given.
        if devicename == None or output_item == None:
            return False

        # Check that the output item is not the default audio device. This is
        # a standard device and must always be present. In future, it would
        # probably be better to have a marker for default, rather than a set
        # device
        if output_item == 'Default audio device':
            raise KeyError('Cannot remove default audio device.')

        # Check the output item actually exists
        valid_output_item = self.pairing_db.get_output_device(
             devicename,
             output_item
            )

        # If not, raise an error.
        if valid_output_item == [] or valid_output_item == None:
            raise ValueError('Output item {0} does not exist for device {1}'\
                           .format(output_item, devicename))

        return self.pairing_db.remove_output_device(devicename, output_item)


    # Add an output device to the paired device's Bluetooth devices
    def add_output_device(self,
                          devicename=None,
                          output_item=None,
                          file_name=None):

        # Check that a devicename and an output item is given.
        if devicename == None or output_item == None or file_name == None:
            return False

        # Check that the output item is not the default audio device. This is
        # a standard device and must always be present. In future, it would
        # probably be better to have a marker for default, rather than a set
        # device
        if output_item == 'Default audio device':
            raise KeyError('Cannot add the default audio device.')

        # Add the output item and return the operation status
        return self.pairing_db.add_output_device(devicename,
                                                   output_item,
                                                   file_name)


