################################################################################
#
# Program:      v3_00_Pairing_Database.py
# Class:        v3_00_Pairing_Database
# Objects:      None
# Component of: Broadcast
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Pairing Database manages the persistence layer for the Bluetooth 
#          system. This current class uses Sqlite 3, but it could be anything.
#          By abstracting the persistence layer to a class, properties, and
#          methods, we can change it at any time without affecting the base 
#          model.
#
# Notes:   The Bluetooth model was one of the earliest and it does show. They
#          solution is good but the code is not the most Pythonesque. This model
#          is a good candidate for optimization.
#
################################################################################
#
# Import base library modules
import sqlite3, os

class v3_00_Pairing_Database(object):

    db_name = 'datavolume/pairing.db'
    db_conn = None
    db_cursor = None
    server_name = None
    port_number = None

    # Constructor. Two interesting points here: first, the parameters always
    # default to None (null). After development, the question is why - why not
    # set them to real values and succeed rather than fail safe? Two, having
    # database instantiation, setup, and creation in the constructor is probably
    # not a great idea - everytime this object is called, it calls the validate
    # tables methods, they in turn initialize the tables by doing a delete from.
    #
    # The net effect is this class can ONLY be instantiated once. Not ideal.
    def __init__(self, server_name=None, port_number=None):
        # Get the server name and IP
        self.server_name = server_name
        self.port_number = port_number

        # Use it to create the database name
        self.db_name = 'datavolume/'+server_name+'-'+\
            str(port_number)+'-pairing.db'
        self.db_conn = None
        self.db_cursor = None

        # Initialize the pairing and output tables
        self.validate_pairing_table()
        self.validate_output_table()


    # Helper class - opens a DB connection if not already open.
    def open_db(self):
        try:
            self.db_conn = sqlite3.connect(self.db_name)
            self.db_cursor = self.db_conn.cursor()
        except Exception as e:
            print(repr(e))
            if not self.db_cursor == None:
                self.db_cursor.close()
            if not self.db_conn == None:
                self.db_conn.close()


    # Initializes the pairing table by calling delete from - so there is no
    # persistence between runs and data is ALWAYS fresh. Catches the Sqlite 3
    # operational error exception - if the table doesn't exist, then it gets
    # created.
    def validate_pairing_table(self):
        _returned = None

        try:
            self.open_db()
            self.db_exec('delete from paired_devices')
            self.db_conn.commit()
        except sqlite3.OperationalError as oe:
            self.db_cursor.execute(
                    'create table paired_devices '+\
                    '(device string not null primary key, key string)'
                )
        except Exception as e:
            print(repr(e))
            raise
        finally:
            self.close_db()


    # Initializes the output table by calling delete from - so there is no
    # persistence between runs and data is ALWAYS fresh. Catches the Sqlite 3
    # operational error exception - if the table doesn't exist, then it gets
    # created.
    def validate_output_table(self):
        _returned = None

        try:
            self.open_db()
            self.db_exec('delete from output_devices')
            self.db_conn.commit()
        except sqlite3.OperationalError as oe:
            self.db_cursor.execute(
                    'create table output_devices '+\
                    '(device string not null, '+\
                    ' output_item string not null,'+\
                    ' file_name string, '+\
                    ' primary key (device, output_item))'
                )
        except Exception as e:
            print(repr(e))
            raise
        finally:
            self.close_db()


    # Execute a sql statement. For security reasons, this should never be called
    # outside the helper method. Should really be _db_exec, because Python does
    # not have private methods - added to change list.
    def db_exec(self, sql_statement=None, sql_parameters=()):
        if sql_statement == None:
            return None

        _returned = None

        try:
            if self.db_cursor == None:
                raise Exception('Cursor does not exist!')
            self.db_cursor.execute(sql_statement, sql_parameters)
        except sqlite3.OperationalError as oe:
            raise   # Raise exceptions back to the caller.
        except Exception as e:
            print(repr(e))
            raise   # Raise exceptions back to the caller.


    # Close the database to avoid leaving connections open and potentially
    # introducing memory errors, etc.
    def close_db(self):
        if not self.db_cursor == None:
            self.db_cursor.close()
        if not self.db_conn == None:
            self.db_conn.close()
        self.db_cursor = None
        self.db_conn = None


########### Functional Methods #################################################

    # Check if devicename is already paired in the database.
    def check_pairing(self, devicename):
        returned = None
        try:
            # Open the DB
            self.open_db()

            # Issue the query
            self.db_exec('select key from paired_devices where device = ?',
                           (devicename,))

            # Fetch all rows
            returned = self.db_cursor.fetchall()

            # Check what was found
            if not returned == []:
                if type(returned) == list:
                    returned = returned[0][0]

        except Exception as e:
            print(repr(e))
        finally:
            self.close_db()

        return returned


    # Get all devices paired with this Bluetooth controller.
    def get_paired(self):
        returned = []
        try:
            self.open_db()
            self.db_exec('select device, key '+\
                         'from paired_devices ')
            returned = self.db_cursor.fetchall()
        except Exception as e:
            print(repr(e))
            raise
        finally:
            self.close_db()

        return returned


    # Pair a devices with this Bluetooth controller. Pairing emulation is simply
    # persisting a relationship between the device and the controller and using
    # a key to keep it between those two devices.
    #
    def pair_device(self, devicename):
        try:
            # Provide a default pairing key. Could use the key generations
            # service from the base library - see v1_00_Key.py
            #
            pairing_key = '1234-5678-9012-3456'

            self.open_db()
            self.db_exec('insert or replace into '+\
                           'paired_devices values (?, ?)',
                           (devicename, pairing_key))
            self.db_conn.commit()
            self.add_output_device(devicename=devicename,
                                  output_item='Default audio device',
                                  file_name='datavolume/'+devicename+\
                                    '-default-audio.txt')
        except:
            raise
        finally:
            self.close_db()

        # The key is returned and the caller should keep the value of the key
        # so it can be used to broadcast messages.
        return pairing_key


    # Un-pair the device from the Bluetooth service. The devicename is removed
    # from the pairing table, therefore the key is destroyed. Notice that
    # referential integrity is pseudo enforced by deleting all output devices
    # that were created for the paired device.
    def remove_pairing(self, devicename):
        try:
            self.open_db()
            self.db_exec('delete from paired_devices where device = ?',
                           (devicename,))
            self.db_exec('delete from output_devices where device = ?',
                           (devicename,))
            self.db_conn.commit()
        except:
            raise
        finally:
            self.close_db()

        return True


    # Add an output device to the device. This is meant to emulate an ear-piece,
    # hands free car kit, etc. Every entry in this table will receive a message
    # which is broadcast and the output written to file_name
    #
    def add_output_device(self,
                          devicename=None,
                          output_item=None,
                          file_name=None
    ):
        try:
            if devicename == None or output_item == None or file_name == None:
                return None

            self.open_db()
            self.db_exec('insert or replace into '+\
                           'output_devices values (?, ?, ?)',
                           (devicename, output_item, file_name))
            self.db_conn.commit()
        except:
            raise
        finally:
            self.close_db()

        return True


    # Remove an output device.
    def remove_output_device(self, devicename=None, output_item=None):
        try:
            if devicename == None or output_item == None:
                return False

            self.open_db()
            self.db_exec('delete from output_devices '+\
                           'where device = ? '+\
                           'and output_item = ?',
                           (devicename, output_item))
            self.db_conn.commit()
        except:
            raise
        finally:
            self.close_db()

        return True


    # Get the list of output devices configured for this device.
    def get_output_devices(self, devicename=None):
        if devicename == None:
            return []

        returned = []
        try:
            self.open_db()
            self.db_exec('select output_item, file_name '+\
                           'from output_devices '+\
                           'where device = ?',
                           (devicename,))
            returned = self.db_cursor.fetchall()
        except Exception as e:
            print(repr(e))
        finally:
            self.close_db()

        return returned


    # Get a specific output device for a device.
    def get_output_device(self, devicename=None, output_item=None):
        if devicename == None or output_item == None:
            return []

        returned = []
        try:
            self.open_db()
            self.db_exec('select output_item, file_name '+\
                           'from output_devices '+\
                           'where device = ? '+\
                           'and output_item = ?',
                           (devicename, output_item))
            returned = self.db_cursor.fetchall()
        except Exception as e:
            print(repr(e))
        finally:
            self.close_db()

        return returned


