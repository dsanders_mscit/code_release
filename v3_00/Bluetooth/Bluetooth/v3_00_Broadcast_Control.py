################################################################################
#
# Program:      v3_00_Brodacast_Control.py
# Class:        v3_00_Broadcast_Control
# Objects:      None
# Component of: Broadcast
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Control the broadcasting of messages on behalf of a paired device.
#          Devices (other models) cannot access the Bluetooth devices directly,
#          they need to request services.
#
# Notes:   The Bluetooth model was one of the earliest and it does show. They
#          solution is good but the code is not the most Pythonesque. This model
#          is a good candidate for optimization.
#
################################################################################
from Bluetooth import Control, Pairing_Control
import datetime, time, json

#
# SuperClass.
# ----------------------------------------------------------------------------
class v3_00_Broadcast_Control(object):
    output_devices = []
    controller = Control.global_control

    #Constructor
    def __init__(self):
        pass

    # Controller for broadcast message
    def broadcast_message(self, devicename='Unknown', text=None, key=None):
        success = 'success'
        status = '200'
        message = 'Message has been broadcast.'
        data = {"device":devicename,
                "action":"broadcast",
                "message":text}

        self.controller.log(log_message='Broadcast request from "{0}"'\
            .format(devicename))

        # Get a pairing control object
        pair_control_object = Pairing_Control.global_pair_control

        # Get the pairing key. This let us ensure that only a device that is
        # paired is requesting the service.
        pairing_key = pair_control_object.check_pairing(devicename)

        # REQUIRES REVISION
        # Should change to better exception handling - because this was one of
        # the first models, the code isn't the most Pythonesque.
        if pairing_key == []:
            success = 'error'
            status = '404'
            message = 'Device is not paired'
            pairing_key = None
            self.controller.log(
                log_message='Broadcasting error: {0}'\
                    .format(message))
        elif pairing_key != key:
            success = 'error'
            status = '403'
            message = 'Device pairing key is incorrect!'
            pairing_key = None
            self.controller.log(
                log_message='Broadcasting error: {0}'\
                    .format(message))
        elif text == '' or text == None:
            success = 'error'
            status = '400'
            message = 'Cannot broadcast an empty message'
            pairing_key = None
            self.controller.log(
                log_message='Broadcasting error: {0}'\
                    .format(message))
        else:
            try:

                self.controller.log(
                     log_message='Broadcasting "{0}" on behalf of "{1}"' \
                        .format(text, devicename))

                # Get the current date and time
                now = datetime.datetime.now()
                tz = time.tzname[0]
                tzdst = time.tzname[1]

                # Get the list of output devices that have been configured. This
                # is really just a series of files, but designed to emulate 
                # multiple devices, e.g. hands free, puck, speaker, etc.
                #
                self.output_devices = pair_control_object.get_output_devices(
                    devicename
                )

                # Iterate through every file and output to it.
                for outputfile in self.output_devices:
                    f = open(outputfile[1],'a')
                    f.write('{0}: {1}'.format(now, ('-'*50)+"\n"))
                    f.write('{0}: Bluetooth output device: {1}'\
                            .format(now, outputfile[0])+"\n")
                    f.write('{0}: {1}'.format(now, ('-'*50)+"\n"))
                    f.write('{0}: Broadcast from: {1}'\
                        .format(now, devicename)+"\n")
                    f.write('{0}: Broadcast at: {0} ({1}/{2})'\
                        .format(now, tz, tzdst)+"\n")
                    f.write('{0}: Message: {1}'.format(now, text)+"\n")
                    f.write('{0}: {1}'.format(now, ('-'*50)+"\n"))
                    f.close()

                # Log the message as well.
                self.controller.log(('-'*77))
                self.controller.log('Bluetooth Broadcast: {0}'.format(text))
                self.controller.log(('-'*77))
                self.controller.log('Broadcast from: {0}'.format(devicename))
                self.controller.log('Broadcast at: {0} ({1}/{2})'\
                    .format(now, tz, tzdst))
                self.controller.log('Message: {0}'.format(text))
                self.controller.log(('-'*77))

            except Exception as e:
                self.controller.log('Exception {0}'.format(repr(e)))

        self.controller.log(
            log_message='Broadcasting on behalf of "{0}" finished. ({1} {2})'\
                .format(devicename, success, status))

        # Make an HTTP response
        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value

