################################################################################
#
# Program:      v3_00_Config_Output_Control.py
# Class:        v3_00_Config_Output_Control
# Objects:      None
# Component of: Broadcast
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: The controller to configure (add, delete, query) output devices
#          attached to the Bluetooth controller. 'devices' are really just files
#          but they are useful for the model as it provides a means of emulating
#          different devices, say an ear-piece which only one person could hear.
#
#          There are probably two methods missing from this class - set default
#          and get default. There probably should be a way of setting a device
#          to be the default rather than relying on default audio device.
#
# Notes:   The Bluetooth model was one of the earliest and it does show. They
#          solution is good but the code is not the most Pythonesque. This model
#          is a good candidate for optimization.
#
################################################################################
#
# Import required modules and packages
#
from flask_restful import Resource
from flask import Response
from Bluetooth.Control \
    import global_control as global_control
from Bluetooth.Pairing_Control \
    import global_pair_control as pair_control_object
import json, requests

class v3_00_Config_Output_Control(object):
    controller = global_control
    pair_controller = pair_control_object

    # Constructor. Does nothing but here so that it can be inherited and acted
    # upon by super().
    def __init__(self):
        pass


    def set_output(self, devicename=None, json_string=None):
        success = 'success'
        status = '200'
        message = 'Bluetooth output devices.'
        data = None

        try:
            # Validate JSON string is good.
            if json_string == None\
            or json_string == '':
                raise KeyError('Badly formed request!')

            # Load the JSON data into a dict.
            json_data = json.loads(json_string)

            # Get mandatory data. Should really be validating against a schema
            # however, see dissertation document for reasons why schemas were
            # not used.
            #
            key = json_data['key']
            output_item = json_data['output-item']
            file_name = json_data['file-name']

            # Simple key check. Should go to the Bluetooth database object and
            # get the key that was defined for the device at pairing time. As
            # this project is not designed to validate security, the default was
            # okay but in production, this would change to the randomly 
            # generated key being checked.
            #
            if not key == '1234-5678-9012-3456':
                raise IndexError('Bluetooth key incorrect.')

            self.controller.log(
                log_message=
                    'Request to add "{0}" to device "{1}" with filename {2}'\
                    .format(output_item, devicename, file_name))

            # Validate the device is paired. This should actually happen before
            # the key check above as that is how the key is validated.
            pairing_key = self.pair_controller.check_pairing(devicename)

            if pairing_key == []:
                raise ValueError('Device {0} is not paired'.format(devicename))
            else:
                # Add the output device
                state = self.pair_controller\
                    .add_output_device(devicename,
                                       output_item,
                                       'datavolume/'+devicename+'-'+file_name)
                print(state)

                # Build the data object for return
                data = {"device":devicename,
                        "output-item":output_item,
                        "file-name":'datavolume/'+devicename+'-'+file_name,
                        "state":"added" if state == True else "not added"}

                self.controller.log(
                    log_message=
                        'Output "{0}" added to device "{1}" (filename: {2})'\
                        .format(output_item, devicename, file_name))
        except KeyError as ke:
            # This typically means some JSON data was missing.
            success = 'error'
            status = '400'
            message = str(ke)
            self.controller.log(
                log_message='Output device error: A key is missing: {0}'\
                    .format(message))
        except ValueError as ve:
            # A value passes is wrong OR the device isn't paired (hence the 404)
            success = 'error'
            status = '404'
            message = str(ve)
            self.controller.log(
                log_message='Output device error: {0}'\
                    .format(message))
        except IndexError as ie:
            # Index Error is used to handle a bad key. This should really be a
            # custom exception. Enhancement in future - build custom exception
            # in base library.
            #
            success = 'error'
            status = '403'
            message = str(ie)
            self.controller.log(
                log_message='Output device error: {0}'\
                    .format(message))
        except Exception as e:
            self.controller.log(
                log_message='Output device error: {0}'\
                    .format(repr(e)))
            raise

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value


    def get_output(self, devicename):
        success = 'success'
        status = '200'
        message = 'Bluetooth output devices.'
        data = None

        try:
            self.controller.log(
                 log_message=
                    'Request to get list of outputs for device "{0}"'\
                    .format(devicename))

            # Make sure the device is paired
            pairing_key = self.pair_controller.check_pairing(devicename)

            if pairing_key == []:
                raise ValueError('Device is not paired')
            else:
                # Get the outputs for the device
                outputs = self.pair_controller.get_output_devices(devicename)

                # Create an empty list for returning
                output_list = []

                # Iterate around the data returned from the database and then
                # build the items into objects. This should really happen at
                # the persistence layer not in the object control.
                #
                for output in outputs:
                    output_list.append({"device":devicename,
                                        "output-item":output[0],
                                        "file-name":output[1],
                                        "state":"ready"
                                       })
                data = {"outputs":output_list}
                self.controller.log(
                    log_message=
                        'Outputs "{0}" setup for device "{1}"'\
                        .format(data['outputs'], devicename))
        except KeyError as ke:
            success = 'error'
            status = '400'
            message = 'Badly formed request!'
            self.controller.log(
                log_message='Output device error: A key is missing: {0}'\
                    .format(message))
        except ValueError as ve:
            success = 'error'
            status = '404'
            message = str(ve)
            self.controller.log(
                log_message='Output device error: {0}'\
                    .format(message))
        except IndexError as ie:
            success = 'error'
            status = '403'
            message = str(ie)
            self.controller.log(
                log_message='Output device error: {0}'\
                    .format(message))
        except Exception as e:
            self.controller.log('Exception: {0}'.format(repr(e)))
            raise

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value


    def delete_output(self, devicename=None, json_string=None):
        success = 'success'
        status = '200'
        message = 'Bluetooth output devices.'
        data = None

        try:
            # Check the JSON passed
            if json_string == None\
            or json_string == '':
                raise KeyError('Badly formed request!')

            json_data = json.loads(json_string)

            # Get mandatory parameters
            key = json_data['key']
            output_item = json_data['output-item']

            if not key == '1234-5678-9012-3456':
                raise IndexError('Bluetooth key incorrect.')

            self.controller.log(
                 log_message=
                    'Request to remove "{0}" from device "{1}"'\
                    .format(output_item, devicename))

            # Check the device given is actually paired.
            pairing_key = self.pair_controller.check_pairing(devicename)

            if pairing_key == []:
                raise ValueError('Device {0} is not paired'.format(devicename))
            else:
                # Remove the device output item
                state = self.pair_controller\
                    .remove_output_device(devicename, output_item)
                print(state)

                # Prepare the return object
                data = {"device":devicename,
                        "output-item":output_item,
                        "file-name":"n/a",
                        "state":"deleted" if state == True else "not deleted"}

                self.controller.log(
                    log_message=
                        'Output "{0}" removed from device "{1}"'\
                        .format(output_item, devicename))
        except KeyError as ke:
            success = 'error'
            status = '400'
            message = str(ke)
            self.controller.log(
                log_message='Output device error: A key is missing: {0}'\
                    .format(message))
        except ValueError as ve:
            success = 'error'
            status = '404'
            message = str(ve)
            self.controller.log(
                log_message='Output device error: {0}'\
                    .format(message))
        except IndexError as ie:
            success = 'error'
            status = '403'
            message = str(ie)
            self.controller.log(
                log_message='Output device error: {0}'\
                    .format(message))
        except Exception as e:
            self.controller.log(
                log_message='Output device error: {0}'\
                    .format(message))
            raise

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value

