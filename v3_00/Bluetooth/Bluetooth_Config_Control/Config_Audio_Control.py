# Import base library modules - From symbolic link to /base_lib
from Bluetooth_Config_Control.v3_00_Config_Audio_Control \
    import v3_00_Config_Audio_Control

class Config_Audio_Control(v3_00_Config_Audio_Control):
    def __init__(self):
        super(Config_Audio_Control, self).__init__()


# Global variable
audio_control_object = Config_Audio_Control()


