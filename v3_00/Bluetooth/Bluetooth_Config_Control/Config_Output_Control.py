# Import base library modules - From symbolic link to /base_lib
from Bluetooth_Config_Control.v3_00_Config_Output_Control \
    import v3_00_Config_Output_Control

class Config_Output_Control(v3_00_Config_Output_Control):
    def __init__(self):
        super(Config_Output_Control, self).__init__()


# Global variable
config_output_control_object = Config_Output_Control()
