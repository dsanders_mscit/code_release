################################################################################
#
# Program:      v3_00_Config_Audio_Control.py
# Class:        v3_00_Config_Audio_Control
# Objects:      None
# Component of: Broadcast
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: The controller to get the default audio device and return it as a
#          file.
#
# Notes:   The Bluetooth model was one of the earliest and it does show. They
#          solution is good but the code is not the most Pythonesque. This model
#          is a good candidate for optimization.
#
################################################################################
#
# Import required modules and packages
#
from flask_restful import Resource
from flask import Response, send_file
from Bluetooth import Control
import json

class v3_00_Config_Audio_Control(object):
    controller = None

    # Constructor.
    def __init__(self):
        # Set the control object
        self.controller = Control.global_control


    # Get the audio output the paired device has generated.
    def get_audio(self, devicename=None):
        success = 'success'
        status = '200'
        message = 'Get Bluetooth default audio file.'
        data = None

        return_response = None

        try:
            # Can't get audio for a null device. Shouldn't ever get to here
            # with a null devicename but checking is good.
            if devicename == None:
                raise ValueError('The device name must be provided.')

            # Build the filename. This should probably already be stored in the
            # KV store. Enhancement in future - get this from KV.
            file_name='/Bluetooth/datavolume/'+devicename+'-default-audio.txt'

            # Use Flask's built-in send_file to return the file.
            return_response = send_file(file_name)
        except ValueError as ve:
            success = 'error'
            status = 400
            message = str(ve)
            self.controller.log('VALUE ERROR >> {0}'.format(message))
            return_response=self.controller.do_response(message=message,
                                                        data=data,
                                                        status=status,
                                                        response=success)

        except OSError as ose:
            # OS Errors are raised when the OS encounters a problem. We should
            # really check that the error was 17 (file IO), but don't expect
            # any other OS errors, so assume that it was.
            success = 'success'
            status = 200
            message = 'There is no audio for the device. Are you sure '+\
                      'it is paired?'
            return_response=self.controller.do_response(message=message,
                                                        data=data,
                                                        status=status,
                                                        response=success)
        except Exception as e:
            success = 'error'
            status = 500
            error_msg = 'An exception occurred: {0}'.format(repr(e))
            print(error_msg)
            self.controller.log(error_msg, audio=False)
            return_response=self.controller.do_response(message=message,
                                                        data=data,
                                                        status=status,
                                                        response=success)

        return return_response

