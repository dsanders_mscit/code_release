################################################################################
#
# Program:      v3_00_Config_Logger_Control.py
# Class:        v3_00_Config_Logger_Control
# Objects:      None
# Component of: Broadcast
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: The standard base_lib provided logger functions.
#
################################################################################
#
# Import required modules and packages
#
from base_lib.Config_Logger import Config_Logger
from Bluetooth import Control

class v3_00_Config_Logger_Control(object):
    controller = None
    config_logger = None

    # Constructor
    def __init__(self):
        # Get the control object
        self.controller = Control.global_control

        # Get the config control object
        self.config_logger = Config_Logger(self.controller)


    # FOR ALL METHODS. SEE V1_00_CONFIG_LOGGER.PY IN BASE
    def get_logger(self):
        return self.config_logger.get_logger()

    def remove_logger(self, json_string=None):
        return self.config_logger.remove_logger(json_string)

    def set_logger(self, json_string=None):
        return self.config_logger.set_logger(json_string)

