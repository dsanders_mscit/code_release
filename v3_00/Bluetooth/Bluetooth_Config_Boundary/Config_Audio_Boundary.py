################################################################################
#
# Program:      Config_Audio_Boundary.py
# Class:        Config_Audio_Boundary
# Objects:      None
# Component of: Broadcast
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Define the configuration boundary methods to deliver the default 
#          audio device's output file. Supports GET only and delivers a text
#          file in response - it is up to the caller to save the file.
#
#
# Notes:   The Bluetooth model was one of the earliest and it does show. They
#          solution is good but the code is not the most Pythonesque. This model
#          is a good candidate for optimization.
#
################################################################################
#
# Import required modules and packages
#
from flask_restful import Resource, reqparse
from Bluetooth_Config_Control.Config_Audio_Control \
    import audio_control_object

class Config_Audio_Boundary(Resource):
    def get(self, devicename=None):
        return_state = audio_control_object.get_audio(devicename)
        return return_state

