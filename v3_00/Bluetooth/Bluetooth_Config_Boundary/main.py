################################################################################
#
# Program:      main.py
# Class:        None
# Objects:      None
# Component of: Broadcast
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: main Python program for the config Boundary. This code defines the
#          routes that will be offered by Flask (via the NGINX/UWSGI services)
#          and provide the configuraiton interfaces and boundaries to the model.
#
# References
# ----------
# Ronacher, A., 2013, 'Larger Applications' [ONLINE]. Available at: 
# http://flask.pocoo.org/docs/0.10/patterns/packages/ (Accessed: 04 November 
# 2015)
#
################################################################################

from Bluetooth import app, api
from Bluetooth.Control import global_control
from Bluetooth_Config_Boundary.Config_Output_Boundary \
    import Config_Output_Boundary
from Bluetooth_Config_Boundary.Config_Logger_Boundary \
    import Config_Logger_Boundary
from Bluetooth_Config_Boundary.Config_Audio_Boundary \
    import Config_Audio_Boundary

#
# Get the version of the API
#
version = global_control.get_value('version')

#
# All the requests below will end up as: http://server:port/version/path...
#
api.add_resource(Config_Logger_Boundary,
                 '/{0}/config/logger'.format(version))
api.add_resource(Config_Audio_Boundary,
                 '/{0}/config/audio/<string:devicename>'.format(version))
api.add_resource(Config_Output_Boundary,
                 '/{0}/config/output/<string:devicename>'.format(version))

