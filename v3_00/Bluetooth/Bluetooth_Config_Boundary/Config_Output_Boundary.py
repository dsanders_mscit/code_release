################################################################################
#
# Program:      Config_Output_Boundary.py
# Class:        Config_Output_Boundary
# Objects:      None
# Component of: Broadcast
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Define the boundary methods used to add, query, and remove output
#          devices from a paired device. Supports GET (query), POST (add), and
#          DELETE (remove).
#
# Notes:   The Bluetooth model was one of the earliest and it does show. They
#          solution is good but the code is not the most Pythonesque. This model
#          is a good candidate for optimization.
#
################################################################################
#
# Import required modules and packages
#
from flask_restful import Resource, reqparse
from Bluetooth_Config_Control.Config_Output_Control \
    import config_output_control_object

class Config_Output_Boundary(Resource):
    def get(self, devicename):
        return config_output_control_object.get_output(devicename=devicename)


    def post(self, devicename):
        raw_data = None
        # Get the raw data and decode it from binary to utf-8.
        raw_data = reqparse.request.get_data().decode('utf-8')
        return config_output_control_object.set_output(
            devicename=devicename,
            json_string=raw_data)


    def delete(self, devicename):
        raw_data = None
        # Get the raw data and decode it from binary to utf-8.
        raw_data = reqparse.request.get_data().decode('utf-8')
        return config_output_control_object.delete_output(
            devicename=devicename,
            json_string=raw_data)

