################################################################################
#
# Dockerfile
# -----------------------------------------------------------------------------
# Component of: Phone_Screen
# Version:      3.00
# Last Updated: 06 May 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
# Related Scripts
# -----------------------------------------------------------------------------
# bin/build.sh - Builds the Docker image. This is where the image name is set.
# bin/push.sh  - Puses the Docker image to the Docker hub. Requires credentials.
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 06 May 16    | Finalized version for submission.
#
################################################################################
#
# Purpose: build the Docker container image for the phone screen to enable
#          the application to run on any platform that supports Docker. NOTE,
#          this Docker image is NOT based on the base platform but on Debian
#          instead. The reason for the choice was to demonstrate the capability
#          to include various distributions.
#
################################################################################

#
FROM debian
# Update core packages
RUN apt-get update
RUN apt-get install -y \
    python3 \
    python3-pip \
    redis-server \
# Install python & clean packages
&& rm -rf /var/lib/apt/lists/*

# Install requests
RUN pip3 install requests

# Install Redis
RUN pip3 install redis

# Create the directory structures
RUN mkdir /Phone_Screen

COPY Phone_Screen.py /Phone_Screen/
COPY v3_00_Phone_Screen.py /Phone_Screen/

# Set the working directory for the container
WORKDIR /Phone_Screen/

# Set the entrypoint
ENTRYPOINT ["python3"]

# Set default parameters
CMD ["/Phone_Screen/Phone_Screen.py"]

