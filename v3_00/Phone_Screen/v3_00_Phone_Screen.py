################################################################################
#
# Program:      v3_00_Phone_Screen.py
# Class:        v3_00_Phone_Screen
# Objects:      Phone_Screen
# Component of: Phone_Screen
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Phone Screen emulates a person the phone owner, or someone else,
#          being able to view the phone screen. It connects to a phone service 
#          via Redis and prints all messages it receives to the screen.
#
################################################################################

# Import base library modules - From Bluetooth symbolic link to /base_lib
import redis, atexit, os, argparse

class v3_00_Phone_Screen(object):
    # Define the help text for the phone screen.
    gen_description='PhoneScreen.py emulates a phone screen by subscribing '+\
                    'to a redis pubsub queue and listening for messages of '+\
                    'a certain type; when those messages are received, the '+\
                    'phone screen prints the message to stdout. Stdout is '+\
                    'useful as the output can be captured to a file '+\
                    '(e.g. PhoneScreen.py arg1 arg2 > file.out) and multiple '+\
                    'phone screens can listen against a single phone, '+\
                    'emulating many people being able to see the screen.'
    srv_help='The name or IP address of the server. Please '+\
             'note; this is simply the server name '+\
             '(e.g. bob) and not the FQDN or http://server. '+\
             'The server name is used to build the URL '+\
             'for the redis server.'
    port_help='The port number (e.g. 16379) used by the redis '+\
              'server. Please note; this is simply the port '+\
              '(e.g. 16379). '+\
              'The port tells the Python redis client which '+\
              'port to use. '

    #
    # Constructor. Sets up the screen.
    #
    def __init__(self):
        try:
            # Parse the command line arguments given to the program.
            parser = argparse.ArgumentParser(description=self.gen_description)
            parser.add_argument('--server',
                                type=str,
                                help=self.srv_help,
                                required=True)
            parser.add_argument('--port',
                                type=int,
                                help=self.port_help,
                                required=True)
            args = parser.parse_args()
            self.server_name = args.server
            self.redis_port = args.port

            # Set a message so we can respond when the screen closes.
            self.close_action = 'Closed by '
        except KeyError as ke:
            print('An error occurred!')
            print('------------------')
            print('{0}'.format(ke))
        except:
            raise


    # Run. Starts a loop which continues until interrupted (^c) by the user or
    # by the phone it is connected to shutting down.
    #
    def run(self):
        print('Attempting to connect to {0} on port {1}'\
                .format(self.server_name, self.redis_port))

        # Get the redis connection.
        r = redis.StrictRedis(host=self.server_name,
                              port=self.redis_port,
                              db=0)
        r_pubsub = r.pubsub()

        # Subscribe to the Redis channel where screen output is delivered.
        r_pubsub.subscribe('output_screen')

        # Register an at exit function so we can gracefully close out.
        atexit.register(self.close_out)

        # Print a header
        print('')
        print('Screen display starting.')
        print('------------------------')
        print('')

        # Place within try...except block to catch ^c and shut downs.
        try:
            #
            # Loop forever and print whatever is received. There is one 
            # exception, if the message is NONE, then it is not printed. This
            # allows later versions of models to send null messages to connected
            # screens, know that nothing will be printed, and find the number of
            # connected screens.
            #
            for message in r_pubsub.listen():
                if type(message['data']) != int:
                    msg = message['data'].decode('utf-8').rstrip()
                    if msg.upper() != 'NONE':
                        print(message['data'].decode('utf-8').rstrip())
        except KeyboardInterrupt as ki:
            # The user interrupted the program.
            self.close_action += 'user'
        except redis.exceptions.ConnectionError as rce:
            # The Redis connection error signifies the phone has shutdown for
            # some reason, which we don't know. So, we exit gracefully.
            self.close_action += 'server; phone has probably been shutdown.'



    # The footer for the screen.
    def close_out(self):
        print('')
        print('Screen display terminated.')
        print('--------------------------')
        print('{0}'.format(self.close_action))


