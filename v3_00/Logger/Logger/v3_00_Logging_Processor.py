###############################################################################
#
# Program:      v3_00_Logging_Processor.py
# Class:        v3_00_Logging_Processor
# Objects:      Logging_Processor
# Component of: Logger
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Logging_Processor is run as a background thread. Its purpose is to
#          receive logging requests from the controller, enabling the controller
#          to return control back to the boundary and caller more quickly than
#          waiting to process a log write. In reality, there isn't a huge 
#          latency introduced in a single server model; however, there could be
#          in larger, distributed, or dispersed models.
#
################################################################################

import datetime, time, json, redis, requests

class v3_00_Logging_Processor(object):

    def __init__(self):
        pass

    # Entry point and background method. Notice that a control_object is passes
    # to the thread. This avoids the thread having to instantiate a new object
    # to access the global control. There should be checking below to ensure
    # that the control_object isn't null; however, it missed code freeze. D
    # Sanders, 24 April 2016.
    #
    def redis_processor(self, control_object=None):
        # Setup the Redis queue. Note the Redis information is hard-coded below
        # and should really get this from the global control, allowing the
        # logger to modify defaults. It's not significant though, because the
        # Redis service is encapsulated within the logger and NOT exposed 
        # outside the logger.
        #
        _redis = {'host':'localhost', 'port':6379, 'db':0}
        redis_instance = redis.StrictRedis(**_redis)
        redis_pubsub = redis_instance.pubsub()
        redis_pubsub.subscribe('central_logger')

        # Loop forever and handle messages that arrive on the pub-sub queue
        for message in redis_pubsub.listen():
            # We aren't interested in subscribe or unsubscribe message types, so
            # only process 'MESSAGE' messages.
            if message['type'].upper() == 'MESSAGE':
                # extract the message data
                message_data = message['data']

                # split the message into fields. NOTE, currently, the text of
                # the message sent to the logger is field 4; this introduces a
                # security risk - field 4 could contain text formatted to look
                # like this message to manipulate log time. The text field 
                # should change to field 5, since Python's split method will
                # only split the first 5 words. Not changed due to Code Freeze,
                # D Sanders, 24 April 2016.
                message_fields = message['data']\
                                    .decode('utf-8').split('<<*>>', 5)

                try:
                    # Move the message_fields elements to local variables for
                    # ease of reading / understanding.
                    sender = message_fields[0]
                    log_type = message_fields[1]
                    text = message_fields[2]
                    timestamp = message_fields[3]

                    # Use the global control to write the log.
                    control_object.log(sender=sender,
                                       log_type=log_type,
                                       message=text,
                                       timestamp=timestamp)

                except Exception as e:
                    print(repr(e))

