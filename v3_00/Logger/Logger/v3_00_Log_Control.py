###############################################################################
#
# Program:      v3_00_Log_Control.py
# Class:        v3_00_Log_Control
# Objects:      Log_Control
# Component of: Logger
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Log control to get, set, and delete log file entries on behalf of the
#          boundary.
#
################################################################################

from flask_restful import Resource, Api, reqparse, abort
from flask import Response, send_file
from Logger.Control import global_control
import datetime, time, json, requests, redis, sqlite3

#
# SuperClass.
# ----------------------------------------------------------------------------
class v3_00_Log_Control(object):
    controller = None
    redis = {'host':'localhost', 'port':6379, 'db':0}

    # Constructor.
    def __init__(self):
        self.controller = global_control

    # Get a copy of the log file and return it.
    def get_log_file(self):
        success = 'success'
        status = '200'
        message = 'Logging Service, update log.'
        data = None
        try:
            log_file = '{0}'\
                .format(self.controller.get_log_filename())
            print('Fetching {0}'.format(log_file))

            # Use the Flask built-in method to send a file.
            return_response = send_file(log_file)
        except Exception as e:
            error_msg = 'An exception occurred: {0}'.format(repr(e))
            print(error_msg)
            self.controller.log(error_msg, screen=False)

        return return_response


    # Get the log for specific service or model known by the name sender.
    #
    def get_log_by_sender(self, sender=None):
        success = 'success'
        status = '200'
        message = 'Logging Service, update log.'
        data = None

        # On very small Azure machines, it was discovered that Sqlite could be
        # too busy to respond - typically it was caused by a database lock.
        # Sqlite3 is designed to be lightweight persistence, rather than move
        # to a heavier persistence layer, a retry mechanism was built-in.
        #
        retry_limit = 4
        retry_count = 0
        log_returned = []

        # Infinite while loop. The loop actually continues until either the
        # operation is successful or the retry limit is reached.
        while True:
            try:
                success = 'success'
                status = '200'
                message = 'Logging Service, update log.'
                data = None

                # Get the log by the sender
                log_contents = self.controller.get_log(sender)
                if not log_contents in ([], None, ''):
                    # There is data in the log, so translate it from Sqlite3
                    # list to a list of objects. This should really be moved to
                    # the persistence layer but code freeze prevents move. D
                    # Sanders, 24 April 2016.
                    #
                    for log in log_contents:
                        log_returned.append(
                          {
                            "sender":log[0],
                            "log-type":log[2],
                            "message":log[3],
                            "timestamp":log[1]
                          }
                        )

                data = {"log":log_returned}
                break
            except sqlite3.OperationalError as soe:
                # An operational error in Sqlite3 means something has gone wrong
                # This exception handle was added after one smaller Azure 
                # machine caused serious problems when trying to read the log as
                # part of the simulation framework. This code handles the
                # exception, sets a retry count, and limit, and allows activity
                # to be retried.
                print('Exception: {0}'.format(str(soe)))
                message = 'Logging service reported unavailable'
                status = 500
                response = 'error'
                retry_count += 1
                if retry_count > retry_limit:
                    print('Exceeded maximum retries. Limit = {0}. Count = {1}'\
                        .format(retry_limit, retry_count))
                    break
                else:
                    print('Retrying operation.')

        return  self.controller.do_response(message=message,
                                              data=data,
                                              status=status,
                                              response=success)


    # Delete log clears the log. It either clears everything or nothing; there
    # should be no way to clear one or just a few records.
    #
    def delete_log(self, json_string=None):
        success = 'success'
        status = '200'
        message = 'Logging Service, clear log.'
        data = None

        try:
            if json_string == None\
            or json_string == '':
                raise KeyError('No JSON Data passed')

            json_data = json.loads(json_string)

            # Before allowing a clear operation, we validate the key. This code
            # should be changed to validate against a real key.
            key = json_data['key']
            if not key == '1234-5678-9012-3456':
                raise ValueError('Logging control key incorrect.')

            self.controller.delete_log()
            data = {'log':[]}
        except KeyError as ke:
            success = 'error'
            status = '400'
            message = 'Badly formed request! Missing {0}'.format(str(ke))
            self.controller.log('INTERNAL',
                            'unexpected',
                            message=message,
                            timestamp=str(datetime.datetime.now())
            )
        except ValueError as ve:
            message = str(ve)
            status = 403
            success = 'Error'
            self.controller.log('INTERNAL',
                            'unexpected',
                            message=message,
                            timestamp=str(datetime.datetime.now())
            )
        except Exception as e:
            message = repr(e)
            status = 500
            success = 'error'
            self.controller.log('INTERNAL',
                            'unexpected',
                            message=message,
                            timestamp=str(datetime.datetime.now())
            )

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value



    # Add a log entry
    def update_log(self, json_string=None):
        success = 'success'
        status = '200'
        message = 'Logging Service, update log.'
        data = None

        try:
            # Make sure the JSON passed is good.
            if json_string == None\
            or json_string == '':
                raise KeyError('Badly formed request!')

            # Load the JSON into a dict
            json_data = json.loads(json_string)

            # Get the sender and log type entries from the JSON.
            try:
                sender = json_data['sender']
                log_type = json_data['log-type']
            except:
                raise

            # Get the message from the JSON. A blank line is considered to be
            # a request for a timestamp in the log.
            try:
                text = json_data['message']
                if text in ('', None):
                    raise KeyError('set timestamp')
            except KeyError as ke:
                text = 'No message; timestamp recorded.'

            if sender in (None, '') \
            or log_type in (None, ''):
                raise ValueError(
                  'Value errors: sender and log-type cannot be null'
                )

            now = str(datetime.datetime.now())

            # Instead of impacting the http transaction and increasing the
            # latency, log entries are passed to a log processor via Redis. Note
            # that Redis is not guaranteed delivery - if the system goes down or
            # the log processor is unable to receive the published message, it
            # will be lost. For the purposes of the models, this is okay BUT if
            # required, a formal MQ bus could be used to ensure delivery.
            #

            # Instantiate the Redis connection.
            redis_instance = redis.StrictRedis(**self.redis)

            # Publish the message to the queue. Note the format of the message
            # is set to sender<<*>>log_type<<*>>text<<*>>datetime. The reason
            # for this format is to make it really easy to split strings in the
            # processor and it's highly unlikely that the format would be used
            # as a real message. The text of the message should really be the 
            # 4th parameter to avoid injection type attacks; however, in code
            # freeze so no change made. D Sanders, 24 April 2016
            #
            redis_instance.publish(
                'central_logger',
                '{0}<<*>>{1}<<*>>{2}<<*>>{3}'.format(
                    sender,
                    log_type,
                    text,
                    now
              )
            )

            data = {
              "sender":sender,
              "log-type":log_type,
              "message":text,
              "timestamp":now
            }
            print('Logging data: {0}'.format(data))

        except Exception as e:
            success = 'error'
            status = '400'
            message = repr(e)
            self.controller.log(
                'LOGGER',
                'INTERNAL ERROR',
                repr(e),
                str(datetime.datetime.now())
            )

        return  self.controller.do_response(message=message,
                                              data=data,
                                              status=status,
                                              response=success)


