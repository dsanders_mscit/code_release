###############################################################################
#
# Program:      v3_00_Logger_Database.py
# Class:        v3_00_Logger_Database
# Objects:      Logger_Database
# Component of: Logger
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Log control to get, set, and delete log file entries on behalf of the
#          boundary.
#
################################################################################

import sqlite3
from datetime import datetime
import time

class v3_00_Logger_Database(object):
    db_name = None
    db_conn = None
    db_cursor = None

    # Constructor. Like other models, server_name and port_number defaults will
    # be changed to default in the parameters. No change, code freeze - D 
    # Sanders, 24 April 2016.
    #
    def __init__(self, server_name=None, port_number=None):
        if server_name == None:
            server = 'localhost'
        else:
            server = server_name
        if port_number == None:
            port_number = '5000'
        else:
            port_number = str(port_number)

        self.db_name = 'datavolume/central-log-'+server+'-'+\
            port_number+'.db'
        self.db_conn = None
        self.db_cursor = None

        # Validate the log table exists
        self.validate_logging_table()


    # Open the database.
    def open_db(self):
        try:
            self.db_conn = sqlite3.connect(self.db_name)
            self.db_cursor = self.db_conn.cursor()
        except Exception as e:
            print(repr(e))
            if not self.db_cursor == None:
                self.db_cursor.close()
            if not self.db_conn == None:
                self.db_conn.close()


    # Validate the logging table. Note the delete from statement. Models, 
    # including the logger, are designed to be idempotent so the table 
    # validator is designed to remove any previous log entries if persistent
    # storage is used.
    #
    def validate_logging_table(self):
        _returned = None

        try:
            self.open_db()
            self.db_exec('delete from log')
            self.db_conn.commit()
        except sqlite3.OperationalError as oe:
            print(str(oe))
            self.db_cursor.execute(
                    'CREATE TABLE log( '+\
                    '  sender string not null,'
                    '  timestamp string not null, '+\
                    '  log_type string, '+\
                    '  message string, '+\
                    ' PRIMARY KEY(sender, timestamp)'+\
                    ')'
                )
        except Exception as e:
            print(repr(e))
            raise
        finally:
            self.close_db()


    # Execute sql statement - NOTE must never be called from outside this
    # module. Should really be called _db_exec to mark, if not enforce, that it
    # is a private method.
    def db_exec(self, sql_statement=None, sql_parameters=()):
        if sql_statement == None:
            return None

        _returned = None

        try:
            if self.db_cursor == None:
                raise Exception('Cursor does not exist!')
            self.db_cursor.execute(sql_statement, sql_parameters)
        except sqlite3.OperationalError as soe:
            raise
        except Exception as e:
            print(repr(e))
            raise


    # Close the database
    def close_db(self):
        if not self.db_cursor == None:
            self.db_cursor.close()
        if not self.db_conn == None:
            self.db_conn.close()
        self.db_cursor = None
        self.db_conn = None


######### Functional Methods ###################################################

    # Write a log entry
    def write_log(
        self,
        sender='unknown',
        log_type=None,
        message=None,
        timestamp=str(datetime.now())
    ):
        returned = False
        try:
            self.open_db()
            self.db_exec('insert or replace into log '+\
                           'values (?, ?, ?, ?)',
                           (sender,
                            timestamp, 
                            log_type, 
                            message)
                          )
            self.db_conn.commit()
            returned = True
        except Exception as e:
            print(repr(e))
        finally:
            self.close_db()
            return returned


    # Get the log
    def get_log(self):
        try:
            self.open_db()
        except Exception as e:
            print('Get log caused exception: {0}'.format(repr(e)))
            raise

        try:
            self.db_exec('select * from log')
            returned = self.db_cursor.fetchall()
            retry_count = 6
        except sqlite3.OperationalError as oe:
            raise
        except Exception as e:
            raise Exception('Get log caused exception: {0}'.format(repr(e)))

        try:
            self.close_db()
        except Exception as e:
            print('Get log caused exception: {0}'.format(repr(e)))
            raise

        if returned == []:
            returned = None
        elif retry_count < 6:
            raise sqlite3.OperationalError('Database too busy.')

        return returned


    # Get log entries for a specific sender
    def get_log_by_sender(self, sender=None):
        try:
            self.open_db()
            self.db_exec('select * from log '+\
                           'where sender = ?',
                           (sender,)
                          )
            returned = self.db_cursor.fetchall()
        except Exception as e:
            print(repr(e))
        finally:
            self.close_db()
            if returned == []:
                returned = None

        return returned


    # Get the log by a time range. NOTE, this is not currently used and was
    # planned functionality which missed code freeze and was cut.
    #
    def get_log_by_timerange(self, time_start=None, time_end=None):
        try:
            self.open_db()
            self.db_exec('select * from log '+\
                           'where timestamp >= ? '+\
                           'and timestamp <= time_end ',
                           (time_start, time_end)
                          )
            returned = self.db_cursor.fetchall()
        except Exception as e:
            print(repr(e))
        finally:
            self.close_db()
            if returned == []:
                returned = None

        return returned


    # Clear log. Delete existing entries from the log.
    def clear_log(
        self
    ):
        returned = False
        try:
            self.open_db()
            self.db_exec('delete from log')
            self.db_conn.commit()
            returned = True
        except Exception as e:
            print(repr(e))
        finally:
            self.close_db()

        return returned

