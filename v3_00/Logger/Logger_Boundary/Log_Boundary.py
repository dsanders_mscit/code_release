################################################################################
#
# Program:      Log_Boundary.py
# Class:        Log_Boundary, Log_File_Boundary, Log_Boundary_By_Sender
# Objects:      None
# Component of: Logger
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Provide the boundary routes for Logging functions. Log_Boundary 
#          allows callers to get, add to, or delete the log, supporting GET, 
#          POST, and DELETE. Log_File_Boundary allows callers to get a text file
#          version of the log and supports GET only. Log_Boundary_By_Senders
#          enables callers to get the log for a specific sender, supports GET
#          only.
#
################################################################################

from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Logger import app, api

from Logger.Log_Control \
    import global_log_control as log_control

class Log_Boundary(Resource):
    def get(self):
        return log_control.get_log_by_sender()


    def post(self):
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        return log_control.update_log(json_string=raw_data)

    def delete(self):
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        return log_control.delete_log(json_string=raw_data)


class Log_File_Boundary(Resource):
    def get(self):
        return log_control.get_log_file()


class Log_Boundary_By_Sender(Resource):
    def get(self, sender=None):
        return log_control.get_log_by_sender(sender)

