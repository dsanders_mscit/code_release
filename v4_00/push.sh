echo ""
echo "Pushing Phone"
cd Phone
./push.sh
echo ""
echo "Pushing Context"
cd ../Context
./push.sh
echo ""
echo "Pushing Notification Service"
cd ../Notification_Service
./push.sh
echo ""
echo "Pushing Monitor App"
cd ../Monitor_App
./push.sh
echo ""
echo "Pushing Door Bell"
cd ../Door_Bell
./push.sh
echo ""
echo "Pushing Presence"
cd ../Presence
./push.sh
echo ""
cd ..

