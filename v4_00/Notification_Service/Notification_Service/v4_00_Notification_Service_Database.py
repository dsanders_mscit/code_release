################################################################################
#
# Program:      v4_00_Notification_Service_Database.py
# Class:        v4_00_Notification_Service_Database
# Objects:      Notification_Service_Database
# Component of: Notification
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Persistence interface. Updated to support sensitivity and urgency.
#
################################################################################

import sqlite3, os
from Notification_Service.v3_00_Notification_Service_Database \
    import v3_00_Notification_Service_Database

class v4_00_Notification_Service_Database(v3_00_Notification_Service_Database):
    db_name = None
    db_conn = None
    db_cursor = None

    # Constructor. Inherits from v3_00.
    def __init__(self,
                 port_number='5000',
                 server_name='localhost'
    ):
        super(v4_00_Notification_Service_Database, self)\
            .__init__(
                port_number,
                server_name
            )


    # Notification table updated to include urgency and sensitivity fields. NOTE
    # an issue was detected that if persistence is used AND the table exists in
    # between versions BUT does NOT have the columns urgency and sensitivity 
    # then an error will occur. A better check would do select col1, col2, ...
    # so an operational error occurs with a missing column.
    #
    # Not fixed due to code freeze, D Sanders, 02 May 2016.
    def validate_notification_table(self):
        _returned = None

        try:
            self.open_db()
            self.db_exec('select * from notifications')
        except sqlite3.OperationalError as oe:
            print(str(oe))
            self.db_cursor.execute(
                    'CREATE TABLE notifications( '+\
                    '  id integer primary key autoincrement, '+\
                    '  sender string not null, '+\
                    '  recipient string not null,' +\
                    '  notification string not null, '+\
                    '  action string not null, '+\
                    '  event_date string not null, '+\
                    '  urgency integer not null, '+\
                    '  sensitivity integer not null '+\
                    ')'
                )
        except Exception as e:
            print(repr(e))
            raise
        finally:
            self.close_db()


    # Modified in v4_00, to add sensitivity & urgency
    def save_notification(
        self,
        sender=None,
        recipient=None,
        text=None,
        action=None,
        event_date=None,
        urgency=False,
        sensitivity=False
    ):
        returned = None
        try:
            self.open_db()
            self.db_exec('insert into notifications ('+\
                           ' sender, '+\
                           ' recipient, '+\
                           ' notification, '+\
                           ' action, '+\
                           ' event_date, '+\
                           ' urgency, '+\
                           ' sensitivity) '+\
                           'values (?,?,?,?,?,?,?)',
                           (
                                sender,
                                recipient,
                                text,
                                action,
                                event_date,
                                urgency,
                                sensitivity
                           )
           )
            self.db_conn.commit()
        except Exception as e:
            print(repr(e))
        finally:
            self.close_db()

        return True


