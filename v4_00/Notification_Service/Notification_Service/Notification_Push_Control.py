from Notification_Service.v4_00_Notification_Push_Control \
    import v4_00_Notification_Push_Control

class Notification_Push_Control(v4_00_Notification_Push_Control):
    def __init__(self):
        super(Notification_Push_Control, self).__init__()

global_notification_push_control = Notification_Push_Control()
