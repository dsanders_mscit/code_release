################################################################################
#
# Program:      v4_00_Notification_Push_Control.py
# Class:        v4_00_Notification_Push_Control
# Objects:      v4_00_Notification_Push_Control
# Component of: Notification_Service
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Notification push control. Updated to take account of sensitivity and
#          urgency flags. main method updated to account for the new parameters.
#
################################################################################
from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Notification_Service.Control import global_control
import datetime, time, json, requests

from Notification_Service.v3_00_Notification_Push_Control \
    import v3_00_Notification_Push_Control

class v4_00_Notification_Push_Control(v3_00_Notification_Push_Control):

    # Constructor. Inherits from v3_00.
    def __init__(self):
        super(v4_00_Notification_Push_Control, self).__init__()

    #
    # Modified to account for sensitivity and urgency. Much of v3_00 is repeated
    # here and probably should not be. The original version class needs re-
    # factoring to avoid this. Not implemented due to code freeze, D Sanders, 02
    # May 2016.
    #
    def push_notifications(
        self,
        json_string=None
    ):
        success = 'success'
        status = '200'
        message = 'Notification push request.'
        data = None

        self.controller.log('Push Request received.')
        try:
            # Check the JSON String passed
            if json_string == None\
            or json_string == '':
                raise KeyError('Badly formed request!')

            # Check that the JSON can be loaded
            json_data = json.loads(json_string)

            # Get the key and recipient from JSON. If these are not present,
            # then a Key Error exception is raised.
            key = json_data['key']
            recipient = json_data['recipient']

            # Check the key is valid.
            if not key == '1234-5678-9012-3456':
                status = '403'
                raise ValueError('Push notification control key incorrect.')

            # Check the recipient begins with http://
            if 'http://' not in recipient:
                status = '400'
                raise ValueError('Recipient must be a valid URL '+\
                                 'beginning with http://'
                )

            self.controller.log('Request received from {0}.'\
                .format(recipient))

            notification_list = []

            # Fetch all of the notifications persisted for the recipient.
            self.controller.log('Fetching persisted notifications for {0}.'\
                .format(recipient))
            notification_list = self.controller.fetch_notifications(recipient)

            self.controller.log('Found {0} notifications.'\
                .format(len(notification_list)))

            # If we found at least one...
            if not notification_list == None:
                counter = 0
                # Loop through all of the notifications found.
                for notification in notification_list:
                    counter += 1
                    self.controller.log('Requesting push of #{0}.'\
                        .format(counter))

                    #
                    # NEW CODE in v4_00
                    #
                    # SQLite3 stores bools as 0 and 1. So convert for to real
                    # booleans by comparing 1 for True and 0 for False.
                    urgency=notification[6]
                    if urgency in (None,'',[]):
                        urgency=False
                    elif urgency == 1:
                        urgency=True
                    else:
                        urgency=False

                    sensitivity=notification[7]
                    if sensitivity in (None,'',[]):
                        sensitivity=False
                    elif sensitivity == 1:
                        sensitivity=True
                    else:
                        sensitivity=False
                    #
                    # END NEW CODE
                    #


                    # QUEUE the notification NOT send it.
                    #
                    self.controller.queue_notification(
                            notification[1],
                            notification[2],
                            notification[3],
                            notification[4],
                            notification[5],
                            urgency,            # Defaults to False
                            sensitivity         # Defaults to False
                    )
                    self.controller.log('Clearing notification from db.')
                    self.controller.clear_notification(notification[0])

            if counter == 0:
                status = '404'
                success = 'error'

            # Prepare the data for return
            #   recipient          : who the push was for
            #   notification-count : how many notifications were requested to be
            #                        pushed.
            #
            data={
                    "recipient":recipient,
                    "notification-count":"{0}".format(counter),
                 }

            self.controller.log('Final status: {0}.'\
                .format(status))

        except KeyError as ke:
            success = 'error'
            status = '400'
            message = 'Badly formed request! {0}'.format(str(ke))
            self.controller.log(message)
        except ValueError as ve:
            success = 'error'
            message = str(ve)
            self.controller.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = 'Exception: {0}'.format(repr(e))
            self.controller.log(message)

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value

