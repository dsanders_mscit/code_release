################################################################################
#
# Program:      v4_00_Notification_Processor.py
# Class:        v4_00_Notification_Processor
# Objects:      v4_00_Notification_Processor
# Component of: Notification_Service
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Global control. Updated to take account of sensitivity and urgency
#          flags. redis processor modified to expect 7 fields instead of 5. The
#          use of defaults in the interface controllers means this class doesnt'
#          need to ensure backwards compatibility, only compat. with the
#          controllers, which ensure backwards compat.
#
################################################################################
import redis, requests, json, copy
import time

from Notification_Service.v3_00_Notification_Processor \
    import v3_00_Notification_Processor

class v4_00_Notification_Processor(v3_00_Notification_Processor):

    # Constructor. Inherits from v3_00 (there was no 3_01).
    def __init__(self):
        super(v4_00_Notification_Processor, self).__init__()

    #
    # Modified the number of fields expected in the Redis message. An important
    # note. There is very little difference in this method that the v3_00 
    # version, but because the way the object was designed, everything is 
    # repeated here. It would be better to go back and re-factor the original
    # class method and use a helper to strip the fields, prepare them into a
    # dict and process them after that. That would mean only the one or more 
    # helper methods would need superclassing, not the entire redis processor.
    #
    # Change not made due to code freeze, D Sanders, 02 May 2016.
    #
    def redis_processor(self, control_object=None):
        redis_pubsub = control_object.get_queue()

        # Wait forever for Redis messages
        for message in redis_pubsub.listen():
            # When a message arrives, make sure it is marked message. See Redis
            # doc. for other types.
            if message['type'].upper() == 'MESSAGE':
                control_object.log('Redis Processor: Message received!')

                message_data = message['data']
                #
                # v4_00 Change: Split the message into 7 fields.
                message_fields = \
                    message['data'].decode('utf-8').split('<<*>>', 7)

                try:
                    # Set the defaults
                    persist_notification = True
                    persist_reason = 'unknown'
                    time.sleep(2) # Let any caller finish what it was doing 
                                  # first, e.g. deleting from the database.
                    control_object.log('Redis Processor: '+\
                                       'Message parsed to: {0}'\
                                           .format(message_fields))

                    sender = message_fields[0]
                    recipient = message_fields[1]
                    text = message_fields[2]
                    action = message_fields[3]
                    event_date = message_fields[4]

                    #
                    # Get the urgency and sensitivity fields. The approach to 
                    # packaging the message means True and False were formatted
                    # into the string, making them 'True' and 'False' strings 
                    # not boolean datatypes. So we compare strings, force them
                    # to uppercase, and then set them as real booleans.
                    #
                    urgency = message_fields[5]
                    if 'FALSE' in urgency.upper():
                        urgency = False
                    else:
                        urgency = True

                    sensitivity = message_fields[6]
                    if 'FALSE' in sensitivity.upper():
                        sensitivity = False
                    else:
                        sensitivity = True

                    # Creat the payload for sending to the recipient.
                    payload_data = {
                        "key":"NS1234-5678-9012-3456",
                        "message":text,
                        "sender":sender,
                        "action":action,
                        "urgency":urgency,
                        "sensitivity":sensitivity
                    }

                    control_object.log('Redis Processor: Payload is '+\
                                       '{0}-{1}-{2}-{3}-{4}'\
                                          .format(payload_data['message'],
                                                  payload_data['sender'],
                                                  payload_data['action'],
                                                  payload_data['urgency'],
                                                  payload_data['sensitivity']
                                                  )
                    )

                    control_object.log(
                        'Redis Processor: issuing http request to {0}'\
                            .format(recipient)
                    )

                    # Send the HTTP post request.
                    request_response = requests.post(
                        recipient,
                        data=json.dumps(payload_data),
                        timeout=30
                    )

                    control_object.log(
                        'Redis Processor: http request response {0}'\
                            .format(request_response.status_code)
                    )


                    # Check the return status.
                    if request_response.status_code not in (200,201):
                        error_text = \
                            'Unable to communicate with phone due to error. '
                        if request_response.status_code == 404:
                            error_text += \
                                'The phone (or its URL for '+\
                                'notifications) could not be found and '+\
                                'the push returned a not found (404). '+\
                                'This normally signifies a spelling or '+\
                                'URL error in the recipient name. '+\
                                'Message causing error was "'+\
                                message_data.decode('utf-8')+'"'
                        else:
                            error_text += \
                                'Response status was {0}'\
                                .format(request_response.status_code)+\
                                '; '+json.dumps(request_response.json())

                        # Give a reason why we are persisting the notification -
                        # this is useful when reviewing the log.
                        persist_reason = error_text
                        control_object.log('Redis Processor: Stop ERROR')
                        control_object.log(error_text)
                    else:
                        persist_notification = False
                        control_object.log('Redis Processor: dispatched to {0}'\
                          .format(recipient))
                        control_object.log('Redis Processor: Stop SUCCESS')
                        control_object.log()
                except requests.exceptions.ConnectionError as rce:
                    # Communication error typically means not able to reach the
                    # host, etc.
                    persist_reason = str(rce)
                    control_object.log('Redis Processor: Stop WARNING')
                except KeyError as ke:
                    persist_reason = str(ke)
                    control_object.log(
                        'Redis: *** STOP ERROR: {0}'.format(str(ke))
                    )
                except Exception as e:
                    persist_reason = repr(e)
                    control_object.log(
                        'Redis: *** STOP ERROR: {0}'.format(repr(e))
                    )

                #
                # If the persist sentinel is set, then we need to call the 
                # method to persist it. We don't care what here, just that it is
                # persisted.
                #
                if persist_notification:
                    control_object.log('Redis Processor: Persisting '+\
                                       'notification because {0}'\
                                       .format(persist_reason))
                    control_object.persist_notification(
                        sender,
                        recipient,
                        text,
                        action,
                        event_date,
                        urgency,
                        sensitivity
                    )

