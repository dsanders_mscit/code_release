from Notification_Service.v4_00_Notification_Receiver \
    import v4_00_Notification_Receiver

class Notification_Receiver(v4_00_Notification_Receiver):
    def __init__(self):
        super(Notification_Receiver, self).__init__()


global_notification_receiver_control = Notification_Receiver()
