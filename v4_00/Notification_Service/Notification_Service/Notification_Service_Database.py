from Notification_Service.v4_00_Notification_Service_Database \
    import v4_00_Notification_Service_Database

class Notification_Service_Database(v4_00_Notification_Service_Database):
    def __init__(self,
                 port_number='5000',
                 server_name='localhost'):
        super(Notification_Service_Database, self)\
           .__init__(port_number,
                     server_name)


