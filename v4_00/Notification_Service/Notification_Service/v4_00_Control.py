################################################################################
#
# Program:      v4_00_Control.py
# Class:        v4_00_Control
# Objects:      v4_00_Control
# Component of: Notification_Service
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Global control. Updated to take account of sensitivity and urgency
#          flags. Persist notification and queue notification modified but
#          parameters set with defaults (to False) to ensure backwards 
#          compatibility.
#
################################################################################
from Notification_Service.Notification_Service_Database \
    import Notification_Service_Database

import datetime, time, json, redis, os, requests
from textwrap import wrap

from Notification_Service.v3_00_Control import v3_00_Control

class v4_00_Control(v3_00_Control):

    def __init__(self):
        super(v4_00_Control, self).__init__()


    def persist_notification(
        self,
        sender=None,
        recipient=None,
        notification=None,
        action=None,
        event_date=None,
        urgency=False,          # Default to False for backwards compat.
        sensitivity=False       # Default to False for backwards compat.
    ):
        self.Notification_Service_db.save_notification(
            sender,
            recipient,
            notification,
            action,
            event_date,
            urgency,
            sensitivity
        )
        self.log(
          'Persisted Notification: {0}-{1}-{2}-{3}-{4}-{5}-{6}'\
              .format(
                  sender,
                  recipient,
                  notification,
                  action,
                  event_date,
                  urgency,
                  sensitivity
              )
        )


    def queue_notification(
        self,
        sender=None,
        recipient=None,
        text=None,
        action=None,
        event_date=None,
        urgency=False,      # Default to False for backwards compat.
        sensitivity=False   # Default to False for backwards compat.
    ):
        _redis = {'host':self.get_value('redis_host'),
                   'port':self.get_value('redis_port'),
                   'db':self.get_value('redis_db')}
        redis_instance = redis.StrictRedis(**_redis)
        return redis_instance.publish(
            'notification_store',
            '{0}<<*>>{1}<<*>>{2}<<*>>{3}<<*>>{4}<<*>>{5}<<*>>{6}'.format(
                sender,
                recipient,
                text,
                action,
                event_date,
                urgency,
                sensitivity
            )
        )

