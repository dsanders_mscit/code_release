################################################################################
#
# Program:      v4_00_Notification_Receiver.py
# Class:        v4_00_Notification_Receiver
# Objects:      v4_00_Notification_Receiver
# Component of: Notification_Service
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Notification Receiver. Updated to take account of sensitivity and
#          urgency flags which are optional so not enforced. If not present in
#          the JSON, they default to False.
#
################################################################################
from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Notification_Service.Control import global_control
import datetime, time, json, requests, redis, copy
from Notification_Service.v3_00_Notification_Receiver \
    import v3_00_Notification_Receiver
#
# SuperClass.
# ----------------------------------------------------------------------------
class v4_00_Notification_Receiver(v3_00_Notification_Receiver):
    controller = None

    # Constructor. Inherits from v3_00.
    def __init__(self):
        super(v4_00_Notification_Receiver, self).__init__()


    # Modified to account for sensitivity and urgency.
    def incoming_notification(
        self,
        json_string=None
    ):
        success = 'success'
        status = '201'
        message = 'Notification received. Thank you.'
        data = None

        self.controller.log('Notification received.')

        if json_string == None\
        or json_string == '':
            success = 'error'
            status = '400'
            message = 'Badly formed request!'
        else:
            json_data = json.loads(json_string)
            continue_sentinel = True
            self.controller.log('Notification received - JSON is valid.')
            try:
                # Get the required fields in the JSON.
                #
                #   message - the message to be sent
                #   key - the control key; needed to send a notification to the
                #         notification service. This is the control key NOT the
                #         phone or other device key.
                #   sender - who sent the message; free-form text.
                #   action - what should (or could) the recipient do with the
                #            message; e.g. openmap, read text, etc. Free-form
                #            text.
                #   recipient - who is the notification for? See Swagger docs
                #
                # OPTIONAL Fields
                #
                #   urgency - true or false (not JSON lowercase).
                #   sensitivity - true or false
                #
                data = copy.deepcopy(json_data)
                self.controller.log(
                    'Notification received - Parsing parameters.'
                )
                text=json_data['message']
                key=json_data['key']
                sender=json_data['sender']
                action=json_data['action']
                recipient=json_data['recipient']
                if not key == '1234-5678-9012-3456':
                    status = '403'
                    raise ValueError('Notification control key incorrect.')

                if 'http://' not in recipient:
                    status = '400'
                    raise ValueError('Recipient must be a valid URL '+\
                                     'beginning with http://'
                    )

                #
                # NEW CODE v4_00 Additions on optional parameters. Notice that
                # the JSON key lookups are surrounded in try...except blocks, so
                # if they don't exist, instead of raising a key error, the
                # exception is caught, and the defaults provided. This approach
                # is suitable for ANY optional parameter.
                #
                self.controller.log(
                    'Notification received - Parsing optional parameters.'
                )
                try:
                    urgency=json_data['urgency']
                except Exception:
                    urgency=False
                try:
                    sensitivity=json_data['sensitivity']
                except Exception:
                    sensitivity=False
                #
                # END NEW CODE
                #

                self.controller.log('Notification from {0} for {1}'\
                                     .format(sender,recipient))

                self.controller.log('Notification is {0} with action: {1}'\
                                     .format(text, action))

            except KeyError as ke:
                success = 'error'
                status = '400'
                message = 'Badly formed request Missing {0}!'.format(ke)
                self.controller.log(message)
                continue_sentinel = False
            except ValueError as ve:
                success = 'error'
                message = str(ve)
                self.controller.log(message)
                continue_sentinel = False
            except Exception as e:
                success = 'error'
                status = '500'
                message = 'Exception: {0}'.format(repr(e))
                self.controller.log(message)
                continue_sentinel = False

            if continue_sentinel:
                try:
                    now = datetime.datetime.now()
                    tz = time.tzname[0]
                    tzdst = time.tzname[1]

                    # Dispatch notification to redis pub/sub to enable fast
                    # collection of notifications and then let another thread
                    # take time to process.

                    self.controller.log(
                        'Notification Receiver is queuing Notification '+\
                        'with data: '+\
                        '{0}-{1}-{2}-{3}-{4}-{5}-{6}'\
                            .format(
                                sender,
                                recipient,
                                text,
                                action,
                                str(now)+'('+tz+'/'+tzdst+')',
                                urgency,
                                sensitivity
                            )
                    )

                    # Queue the notification. The Notification Processor will
                    # deal with the message, we want to return control as soon
                    # as possible, to minimize latency.
                    #
                    # v4_00 - note the two new parameters - urgency and
                    # sensitivity.
                    #
                    self.controller.queue_notification(
                        sender,
                        recipient,
                        text,
                        action,
                        str(now)+'('+tz+'/'+tzdst+')',
                        urgency,            # NEW in v4_00
                        sensitivity         # NEW in v4_00
                    )
                except:
                    success = 'error'
                    status = '500'
                    message = 'Exception: {0}'.format(repr(e))
                    self.controller.log(message)

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value

