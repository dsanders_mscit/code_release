################################################################################
#
# Dockerfile
# -----------------------------------------------------------------------------
# Component of: Context
# Version:      4.00
# Last Updated: 06 May 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
# Related Scripts
# -----------------------------------------------------------------------------
# ./build.sh   - Builds the Docker image. This is where the image name is set.
# ./push.sh    - Pushes the Docker image to the Docker hub. Requires credentials
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 06 May 16    | Finalized version for submission.
#
################################################################################
#
# Purpose: build the Docker container image for the context engine that enables
#          the application to run on any platform that supports Docker.
#
################################################################################

#
# BASE IMAGE - IMPORTANT
# ======================
#
# Set the base image to the correct version, e.g. v4_00 | v2_00 | etc.
#
FROM dsanderscan/mscit_base
#
#
# ADDITIONAL APPS
# ===============
# Install additional apps for this module here. Remember to do apt-get update
# if needed.
#
#
# CREATE DIRECTORY STRUCTURES
# ===========================
RUN mkdir /Context
RUN mkdir /Context/Context
RUN mkdir /Context/Context_Boundary
RUN mkdir /Context/Context_Config_Boundary
RUN mkdir /Context/Context_Config_Control
RUN mkdir /Context/datavolume
#
#
# COPY FILES
# ==========
#
# Copy root files
#
COPY runserver.py /Context/
COPY startup.sh /Context/
COPY Context/main.py /Context/Context/
COPY Context/__init__.py /Context/Context/
#
#
# CONTROL
# -------
#
# There must ALWAYS be Control.py, v4_00_Control.py
#
COPY Context/Control.py /Context/Context/
COPY Context/v4_00_Control.py /Context/Context/
#
# There must ALWAYS be Context_Database.py, v4_00_Context_Database.py
#
COPY Context/Context_Database.py /Context/Context/
COPY Context/v4_00_Context_Database.py /Context/Context/
#
#
# Boundary
# --------
#
COPY Context_Boundary/main.py /Context/Context_Boundary/
#
# Config Boundary
#
COPY Context_Config_Boundary/main.py /Context/Context_Config_Boundary/
COPY Context_Config_Boundary/Config_Logger_Boundary.py /Context/Context_Config_Boundary/
#
# Config Control
#
COPY Context_Config_Control/Config_Logger_Control.py /Context/Context_Config_Control/
COPY Context_Config_Control/v4_00_Config_Logger_Control.py /Context/Context_Config_Control/
#
#
# Set the working directory for the container
#
WORKDIR /Context/
# Set the entrypoint
ENTRYPOINT ["/bin/bash"]
# Set default parameters
CMD ["/Context/startup.sh"]
#
# Create the symbolic link to the base library
#
RUN ln -s /base_lib /Context/base_lib


### Generated Boundary for service: State
COPY Context_Boundary/State_Boundary.py /Context/Context_Boundary/

### Generated Control for service: State
COPY Context/State_Control.py /Context/Context/

### Generated Control for service: v4_00_State
COPY Context/v4_00_State_Control.py /Context/Context/

### Generated Boundary for service: Query
COPY Context_Boundary/Query_Boundary.py /Context/Context_Boundary/

### Generated Control for service: Query
COPY Context/Query_Control.py /Context/Context/

### Generated Control for service: v4_00_Query
COPY Context/v4_00_Query_Control.py /Context/Context/

### Generated Boundary for service: Subscribe
COPY Context_Boundary/Subscribe_Boundary.py /Context/Context_Boundary/

### Generated Control for service: Subscribe
COPY Context/Subscribe_Control.py /Context/Context/

### Generated Control for service: v4_00_Subscribe
COPY Context/v4_00_Subscribe_Control.py /Context/Context/

