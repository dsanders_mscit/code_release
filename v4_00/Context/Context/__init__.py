################################################################################
#
# Program:      __init__.py
# Class:        None
# Objects:      None
# Component of: Context
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Initialization package for Python. 
#
# References
# ----------
# Ronacher, A., 2013, 'Larger Applications' [ONLINE]. Available at: 
# http://flask.pocoo.org/docs/0.10/patterns/packages/ (Accessed: 04 November 
# 2015)
#
################################################################################

# Import the module Flask from the flask package
from flask import Flask

# Import the module Api from the flask_restful package
from flask_restful import Api

# The app is this application and set when the Python file is run from the
# command line, e.g. python3 /some/folder/notes/runserver.py
app = Flask(__name__)

# Create an Api object inheriting app
api = Api(app)

# Import the main.py module
import Context.main

