################################################################################
#
# Program:      v4_00_Query_Control.py
# Class:        v4_00_Query_Control
# Objects:      Query_Control
# Component of: Context
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: The query control manages interaction with the context engine through
#          context queries that a subscriber pulls by issuing an http request
#          (managed by the boundaries) to context.
#
################################################################################
from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Context import Control
from base_lib.Context_Bad_Exception import Context_Bad_Exception
import datetime, time, json, requests

class v4_00_Query_Control(object):
    __controller = None

    # This would actually be a dictionary of words and probably come from an
    # open or proprietary source. It's here as an example only.
    sample_bad_words = [
        'bugger',
        'sexy',
        'nutter',
        'profanity'
    ]

    # Constructor
    def __init__(self):
        self.controller = Control.global_controller
        self.module_name = 'Query'
        self.method_name = 'unknown'


    # The main query process. A subscriber issues a query and the controller
    # responds with a context state.
    def process_query(
        self,
        subscriber=None,        # The subscriber
        json_string=None        # Raw JSON
    ):
        self.method_name = 'process_query'
        try:
            success = 'success'
            status = '200'
            message = 'Sample'
            data = None
            loading_json = False

            # Default to good.
            context_okay=True
            context_message="Context is good."
            context_states=[]

            self.controller.log('{0}-{1}: Process context query received.'\
                .format(self.module_name,
                        self.method_name)
            )

            # Get subscriber details
            self.controller.log('{0}-{1}: Validate {2} is a subscriber.'\
                .format(self.module_name,
                        self.method_name,
                        subscriber)
            )

            # Get the subscriber key from the KV store.
            sub_key = \
                self.controller.get_value('subscriber_{0}'.format(subscriber))

            # If it didn't exist or wasn't set, then the subscriber is NOT
            # subscribing.
            if sub_key == None:
                raise IndexError('{0} is not subscribing to context'\
                    .format(subscriber)
                )

            self.controller.log('{0}-{1}: {2} is a subscriber.'\
                .format(self.module_name,
                        self.method_name,
                        subscriber)
            )

            #
            # RE-FACTOR NEEDED: The following code block needs re-factoring,
            # and the code below pushed to base_lib as helper methods. Not
            # modified due to code freeze, D Sanders, 05 May 2016.
            #

            # Validate the raw data is valid.
            if not type(json_string) == str:
                raise KeyError('JSON data was not provided as '+\
                               'a string')

            self.controller.log('{0}-{1}: JSON data is a string.'\
                .format(self.module_name,
                        self.method_name)
            )

            if json_string in (None, ''):
                raise KeyError('No JSON data was provided, so '+\
                               'there were no keys')

            self.controller.log('{0}-{1}: JSON string is not empty.'\
                .format(self.module_name,
                        self.method_name)
            )

            # Set a sentinel around loading the JSON data so we know if it's
            # poorly formatted and can catch it in the exception.
            self.controller.log('{0}-{1}: Loading JSON'\
                .format(self.module_name,
                        self.method_name)
            )

            loading_json = True
            json_data = json.loads(json_string)
            loading_json = False

            self.controller.log('{0}-{1}: JSON is valid and loaded.'\
                .format(self.module_name,
                        self.method_name)
            )

            # Get the key. Repeat this approach for all parameters.
            key=json_data['key']
            notification=json_data['notification']
            #
            # Optional parameters
            #
            try:
                notification_sender=json_data['sender']
            except Exception:
                notification_sender='unknown'
            try:
                notification_sensitivity=json_data['sensitivity']
            except Exception:
                notification_sensitivity=False
            try:
                notification_urgency=json_data['urgency']
            except Exception:
                notification_urgency=False
            try:
                autosense=json_data['auto-sense']
            except Exception:
                autosense=True # Default to safe

            # Validate the key
            if (not type(key) == str) \
            or (not key == sub_key): # Change to correct key!
                raise ValueError('Key is incorrectly formed or incorrect')

            self.controller.log('{0}-{1}: Key was correct.'\
                .format(self.module_name,
                        self.method_name)
            )
            
            #
            # END RE-FACTOR NEEDED
            #

            # Check the sensitivity of the interaction currently taking place
            # or if the sender is in the sensitive app list. NOTE, the list is
            # hard coded to monitor_app at present; this will become a dataset
            # that is controlled by the end user, so they can set sensitive
            # apps.
            #
            if notification_sensitivity \
            and notification_sender.lower() == 'monitor_app':
                raise Context_Bad_Exception(
                    context_message=\
                        'The notification is marked as sensitive because '+\
                        'it comes from the monitor_app which has been marked '+\
                        'as extremly sensitive. The message may not be '+\
                        'suitable if people, particularly strangers, can see '+\
                        'it',
                        context_states=['marked-sensitive-app']
                )

            # AutoSense is a profanity checker. Each device interacting with
            # context sets their OWN autosense option, context merely checks it
            # and then decides whether to check for profanities.
            #
            if autosense:
                # Analyze the words. Return True if profanities exist.
                sensitive=self.analyze_message(notification)
                if sensitive:
                    raise Context_Bad_Exception(
                        context_message=\
                            'The notification contains sensitive words and '+\
                            'may not be suitable if people, particularly '+\
                            'strangers, can see.',
                        context_states=['profanity']
                    )

            # Text messages or short message service (SMS) messages are set to
            # be ignored for states. Everything else is checked.
            if not notification_sender in ('SMS'):
                states = self.controller.Context_db.get_subscriber_keys(
                    subscriber=subscriber
                )
            else:
                states = [] # Ignore states for SMS

            ###
            ### If there are states then context is bad
            ###
            #
            if len(states) > 0:
                context_okay=False
                context_message="Context fails because of states."
                context_states=""
                for state in states:
                    context_states+=state[0]+"---"

        # Context Bad Exception is a custom exception which can store info.
        # about states, message, and overrides for context failures. See the
        # base_lib for definition and explanation.
        #
        except Context_Bad_Exception as cbe:
            context_okay=False
            success = 'error'

            # If the failure allows the interaction to continue, say because the
            # message is urgent, then the override will be set.
            if cbe.is_override():
                success = 'success'
                context_okay=True

            # Return the list of states (reasons) for context failing.
            context_states=cbe.get_states()
            context_message=\
                "Context fails. Reason: {0}.  States: {1}"\
                    .format(
                        cbe.get_message(),
                        context_states
                    )

            # Note the return status is 200 - The context engine did respons OK
            # even though a context issue may be raised.
            status = 200
            message = '{0}-{1}: Context Exception >> {2}'\
                .format(self.module_name, self.method_name, str(cbe))
            self.controller.log(message)
        except KeyError as ke:
            success = 'error'
            status = 400
            message = '{0}-{1}: KeyError >> {2}'\
                .format(self.module_name, self.method_name, str(ke))
            self.controller.log(message)
        except IndexError as ie:
            success = 'error'
            status = 404
            message = '{0}-{1}: {2}'\
                .format(self.module_name, self.method_name, str(ie))
            self.controller.log(message)
        except ValueError as ve:
            success = 'error'
            status = 403
            message = '{0}-{1}: ValueError >> {2}'\
                .format(self.module_name, self.method_name, str(ve))
            if loading_json:
                status = 400
                message = '{0}-{1}: JSON >> {2}'\
                    .format(self.module_name,
                            self.method_name,
                            'The JSON data is badly formed. Please check')
            self.controller.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = '{0}-{1}: EXCEPTION >> {2}'\
                .format(self.module_name,
                        self.method_name,
                        repr(e))
            self.controller.log(message)
            print(error_text)


        data = {
            "context-okay":context_okay,
            "context-message":context_message,
            "context-states":context_states
        }

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)

        self.controller.log('{0}-{1}: Query returned: {2}'\
            .format(self.module_name,
                    self.method_name,
                    data)
        )

        return return_value


    # Analyze message performs a simple word check looking for words existing
    # within the profanity list. It needs optimized as it is currently O(N) and
    # processes every word in the list.
    def analyze_message(
        self,
        message=None
    ):
        if message == None:
            return True

        bad_word_found=False

        message_words = message.split()
        upper_bad_words = [x.upper() for x in self.sample_bad_words]
        for word in message_words:
            if word.upper() in upper_bad_words:
                bad_word_found = True
                break

        return bad_word_found
