################################################################################
#
# Program:      v4_00_Context_Database.py
# Class:        v4_00_Context_Database
# Objects:      Context_Database
# Component of: Context
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Persistence layer.
#
################################################################################
import sqlite3, os, datetime

class v4_00_Context_Database(object):
    db_name = None
    db_conn = None
    db_cursor = None
    controller = None

    # Constructor
    def __init__(self,
                 controller=None,
                 server_name='localhost',
                 port_number=5000
    ):
        # Validate a global control has been passed.
        if controller == None:
            raise Exception('Controller cannot be none!')

        self.controller = controller

        # Create or open the database.
        self.db_name = 'datavolume/{0}-{1}.db'\
                             .format(server_name, port_number)

        self.db_conn = None
        self.db_cursor = None

        # Make sure the context table exists, if not, create it.
        self.validate_Context_table()

        # Set the default staleness value to 1 minute.
        self.default_stale = 60 # Seconds before state becomes stale


    # clear_key removes a context key (e.g. a state) from the database, making
    # it fully unavailable - a stale key is still available if a query is
    # forced.
    def clear_key(self, subscriber=None, key=None):
        # Make sure the key and subscriber are not null.
        if key == None or subscriber == None:
            return None

        return_value = False
        try:
            self.open_db()
            self.db_exec('delete from context_kvs '+\
                         'where subscriber = ? '+\
                         'and kv_key = ? ',
                          (subscriber, key))
            self.db_conn.commit()
            return_value = None
        except Exception as e:
            raise
        finally:
            self.close_db()

        return return_value



    # Add a key to the context engine. This adds a context key (e.g. state) with
    # a staleness indicator (a date/time). If the stale_at parameter is null, 
    # then it sets to the default value of the current time plus the default
    # stale value.
    def set_key(self,
                subscriber=None,
                key=None,
                value=None,
                stale_at=None
    ):
        # Make sure the key and subscriber are good.
        if key == None or subscriber == None:
            return None

        # An additional check is required here on the stale_at parameter. It
        # should be checked to ensure it is of type datetime.datetime. Alternate
        # approach would be to pass the delta (i.e. 60 seconds, 1000 seconds, 
        # etc.) which might be better.
        #
        # Not modified due to code freeze, D Sanders, 04 May 2016.

        # If stale wasn't specified, set the default.
        if stale_at == None and value != None:
            now = datetime.datetime.now()
            then_stale = now + datetime.timedelta(seconds=self.default_stale)
        else:
            then_stale = stale_at

        return_value = False
        try:
            self.open_db()
            self.db_exec('insert or replace '+\
                         'into context_kvs values (?, ?, ?, ?)',
                          (subscriber, key, value, then_stale))
            self.db_conn.commit()
            return_value = value
        except Exception as e:
            raise
        finally:
            self.close_db()

        return return_value

    # Query a key from the context database. An optional parameter (only_fresh)
    # retrieves only keys which have not become stale. This is the default and
    # must be set to False to retrieve stale keys.
    def get_key(
        self,
        subscriber=None,
        key=None,
        only_fresh=True
    ):
        if key == None or subscriber == None:
            return None

        returned = None
        try:
            self.open_db()
            sql_statement='select value, stale_at '+\
                          'from context_kvs '+\
                          'where subscriber = ? '+\
                          'and kv_key = ? '

            # Add to the SQL Statement IF only_fresh is true. 
            if only_fresh:
                sql_statement += 'and stale_at > CURRENT_TIMESTAMP'

            self.db_exec(sql_statement,(subscriber, key))
            returned = self.db_cursor.fetchall()

            # Check what was returned. There could be anomolies that let many
            # keys be defined, so only the first is returned, hence the get_key
            # and not get_keys (which is implemented below as get_subscriber_
            # keys).

            if returned != []:
                returned = returned[0] # Only return first value
            elif returned == []:
                returned = None
            return_value = returned
        except Exception as e:
            raise
        finally:
            self.close_db()

        return return_value


    # Get ALL of the keys for a given subscriber. Like get_key allows 
    # distinction of stale/fresh keys. There is actually no benefit in having
    # this code - it simply calls get_keys. It might be better for controllers
    # to call get_keys and pass the wild card (%) but this then couples the
    # controllers and persistence layers (they need to know the wildcard char)
    # which may vary depending upon the implimentation. Needs re-factoring. Not 
    # modified due to code freeze, D Sanders, 04 May 2016.
    #
    def get_subscriber_keys(
        self, 
        subscriber=None,
        only_fresh=True
    ):
        print('in get_subscriber_keys')
        if subscriber == None:
            return []
        else:
            return self.get_keys(subscriber, '%', only_fresh)


    # Get ALL of the keys for a given subscriber. Like get_key allows 
    # distinction of stale/fresh keys.
    def get_keys(self, 
        subscriber=None, 
        key=None, 
        only_fresh=True
    ):
        print('in get_keys')
        if subscriber == None:
            return []

        print('Executing query in get_subscriber_keys')
        returned = []
        try:
            self.open_db()
            sql_statement='select kv_key, value, stale_at '+\
                          'from context_kvs '+\
                          'where subscriber = ? '

            if only_fresh:
                sql_statement += 'and stale_at > CURRENT_TIMESTAMP'

            print('Query statement is {0}'.format(sql_statement))
            self.db_exec(sql_statement,(subscriber,))

            return_value = self.db_cursor.fetchall()
            print('Return value is {0}'.format(return_value))
        except Exception as e:
            raise
        finally:
            self.close_db()

        return return_value


    # Helper to execute a SQL Statement. Should be private.
    def db_exec(self, sql_statement=None, sql_parameters=()):
        if sql_statement == None:
            return None

        _returned = None

        try:
            if self.db_cursor == None:
                raise Exception('Cursor does not exist!')
            self.db_cursor.execute(sql_statement, sql_parameters)
        except sqlite3.OperationalError as oe:
            raise
        except Exception as e:
            raise


    # Helper to close the database. Should be private.
    def close_db(self):
        if not self.db_cursor == None:
            self.db_cursor.close()
        if not self.db_conn == None:
            self.db_conn.close()
        self.db_cursor = None
        self.db_conn = None


    # Helper to open the database. Should be private.
    def open_db(self):
        try:
            # Reference to article showing how to correctly parse and map
            # data types, particularly dates.
            # http://stackoverflow.com/questions/1829872/
            # how-to-read-datetime-back-from-sqlite-as-a-datetime-
            # instead-of-string-in-python
            #
            self.db_conn = sqlite3.connect(
                self.db_name,
                detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES
            )
            self.db_cursor = self.db_conn.cursor()
        except Exception as e:
            if not self.db_cursor == None:
                self.db_cursor.close()
            if not self.db_conn == None:
                self.db_conn.close()
            raise


    def validate_Context_table(self):
        _returned = None

        try:
            self.open_db()
            self.db_exec('select * from module')
            self.db_conn.commit()
        except sqlite3.OperationalError as oe:
            self.db_cursor.execute(
                    'CREATE TABLE context_kvs ( '+\
                    '  subscriber string not null, '+\
                    '  kv_key string not null, '+\
                    '  value string, '+\
                    '  stale_at string, '+\
                    '  primary key (subscriber, kv_key) '+\
                    ');'
                )
        except Exception as e:
            raise
        finally:
            self.close_db()

