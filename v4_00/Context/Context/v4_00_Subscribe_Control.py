################################################################################
#
# Program:      v4_00_Subscribe_Control.py
# Class:        v4_00_Subscribe_Control
# Objects:      Subscribe_Control
# Component of: Context
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Controls the subscription and connection to the context engine, 
#          allowing callers to get the state of subscriptions, subscribe, and
#          cancel.
#
################################################################################
from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Context import Control
import datetime, time, json, requests

class v4_00_Subscribe_Control(object):
    __controller = None

    # Constructor
    def __init__(self):
        self.controller = Control.global_controller
        self.module_name = 'Subscribe'
        self.method_name = 'unknown'


    # Subscription query. This currently returns the subscription key. The API
    # is NOT protected and this allows anything (and anyone) to find the key.
    # In production, this would change to be protected and require 
    # authentication to execute this query.
    #
    def get_subscription(self, subscriber=None):
        self.method_name = 'get_subscription'
        success = 'success'
        status = 200
        message = 'Subscription information'
        data = None
        logdata = None

        try:
            # Validate parameters, JSON, and subscription info.
            #
            self.controller.log('{0}-{1}: Subscription info requested by '\
                .format(self.module_name,
                        self.method_name)+\
                '{0}.'.format(subscriber)
            )

            if type(subscriber) != str \
            or subscriber in (None, '', []):
                raise ValueError('Subscriber has to be specified to get '+\
                                 'subscription information.')

            self.controller.log('{0}-{1}: Fetching subscriber info for {2}.'\
                .format(self.module_name,
                        self.method_name,
                        subscriber)
            )

            # Query the database to get the values for the subscriber and the
            # call back key they provided during connection.
            sub_info = self.controller.get_value('subscriber_'+subscriber)
            call_back = self.controller.get_value('call_back_'+subscriber)

            # Check they are actually subscribing; if not, raise an error.
            if sub_info in ([], '', None):
                raise KeyError('{0} is not subscribing to context services.'\
                               .format(subscriber))

            # Prepare the data object for return.
            data={
                   "subscription-key":sub_info,
                   "call-back":call_back
                 }

            # Prepare the data object for logging - note the subscription key
            # is obfuscated in the log version.
            logdata={
                   "subscription-key":"obfuscated",
                   "call-back":call_back
                 }
        except ValueError as ve:
            success = 'error'
            status = 400
            message = '{0}-{1}: {2}'\
                .format(self.module_name, self.method_name, str(ve))
            data = None
            self.controller.log(message)
        except KeyError as ve:
            success = 'error'
            status = 404
            message = '{0}-{1}: {2}'\
                .format(self.module_name, self.method_name, str(ve))
            data = None
            self.controller.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        repr(e))
            data = {"exception":repr(e)}
            self.controller.log(message)
            print(error_text)

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)

        self.controller.log('{0}-{1}: Response returned: "{2}"'\
            .format(self.module_name,
                    self.method_name,
                    logdata)            # Note logdata NOT data is being logged.
        )

        return return_value


    # Subscribe to the context engine.
    def set_subscription(self, json_string=None, subscriber=None):
        self.method_name = 'set_subscription'
        success = 'success'
        status = 200
        message = 'Subscription order'
        data = None
        logdata = None

        try:
            # Ensure parameters passed are correct.
            if type(subscriber) != str \
            or subscriber in (None, '', []):
                raise ValueError('Subscriber has to be specified to '+\
                                 'subscribe to context services.')

            self.controller.log('{0}-{1}: Subscription request by '\
                .format(self.module_name,
                        self.method_name)+\
                '{0}.'.format(subscriber)
            )

            # Validate the raw data is valid
            if not type(json_string) == str:
                raise KeyError('JSON data was not provided as '+\
                               'a string')

            self.controller.log('{0}-{1}: JSON data is a string.'\
                .format(self.module_name,
                        self.method_name)
            )

            # Validate and load the JSON.
            if json_string in (None, ''):
                raise KeyError('No JSON data was provided, so '+\
                               'there were no keys')

            self.controller.log('{0}-{1}: JSON string is not empty.'\
                .format(self.module_name,
                        self.method_name)
            )

            loading_json = True
            json_data = json.loads(json_string)
            loading_json = False

            self.controller.log('{0}-{1}: Get call back for {2}.'\
                .format(self.module_name,
                        self.method_name,
                        subscriber)
            )

            # Get the callback details from the JSON and validate it.
            call_back=json_data['call-back']
            if (not type(call_back) == str) \
            or (call_back in (None, '', [])) \
            or (not call_back[0:7] == 'http://'):
                raise ValueError('Call back must be a valid URL beginning '+\
                                 'with http://')

            self.controller.log('{0}-{1}: Save sub. and call-back for {2}.'\
                .format(self.module_name,
                        self.method_name,
                        subscriber)
            )

            # Generate a key using the base_lib key generator.
            try:
                sub_info =  self.controller.key_gen.generate_key()
            except Exception:
                sub_info = None # Ignore any exceptions and set sub_info to None
                                # This is redundant code. It would be better to
                                # perform the two lines below here. IE set the
                                # key in the exception rather than set a 
                                # sentinel which is then checked and then sets
                                # the key. Not changed due to code freeze, D
                                # Sanders, 05 May 2016.

            if sub_info == None:
                sub_info = '1234-5678-9012-3456' # Fallback to simple key

            # Calculate the base url for this context engine. This assumes the
            # context engine is NOT in a load balanced environment. If it was
            # then it would be simple to modify the initialization of the 
            # global control (see v4_00_Control.py) to account for it.
            #
            base_url = 'http://{0}:{1}/{2}/'\
                .format(
                    self.controller.get_value('ip_addr'),
                    self.controller.get_value('port_number'),
                    self.controller.get_value('version')
                )

            # Create the URLs the caller should use for querying context and
            # modifying (get, set, remove) state. This provides a HATEOAS based
            # API because the caller retrieves data and is informed what they
            # can do with the data and linked data.
            #
            query_engine = base_url+'query/{0}'.format(subscriber)
            state_change = base_url+'state/{0}'\
                .format(subscriber)

            # Store the values for the subscription.
            self.controller.set_value('subscriber_'+subscriber, sub_info)
            self.controller.set_value('call_back_'+subscriber, call_back)

            # Prepare the return data object.
            data={
                "subscription-key":sub_info,
                "call-back":call_back,
                "query-engine":query_engine,
                "state-change":state_change
            }

            # Prepare a data object for logging which obfuscates the key.
            logdata={
                "subscription-key":"obfuscated",
                "call-back":call_back,
                "query-engine":query_engine,
                "state-change":state_change
            }
        except ValueError as ve:
            success = 'error'
            status = 400
            message = '{0}-{1}: {2}'\
                .format(self.module_name, self.method_name, str(ve))
            data = None
            self.controller.log(message)
        except KeyError as ve:
            success = 'error'
            status = 400
            message = '{0}-{1}: {2}'\
                .format(self.module_name, self.method_name, str(ve))
            data = None
            self.controller.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        repr(e))
            data = None
            self.controller.log(message)
            print(error_text)

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)

        self.controller.log('{0}-{1}: Response returned: "{2}"'\
            .format(self.module_name,
                    self.method_name,
                    logdata)
        )

        return return_value


    # Cancel subscription stops the caller from connecting or processing states
    # or querying context with the context engine. Currently, it removes the
    # connection but DOES NOT remove states set. It should enforce referential
    # integrity and this will be changed in future. Not modified due to code
    # freeze, D Sanders, 05 May 2016.
    #
    def cancel_subscription(self, subscriber=None, json_string=None):
        self.method_name = 'cancel_subscription'
        success = 'success'
        status = 200
        message = 'Subscription cancellation'
        data = None
        loading_json=False
        try:
            # Validate the paramaters passed and the JSON.
            if type(subscriber) != str \
            or subscriber in (None, '', []):
                raise ValueError('Subscriber has to be specified to '+\
                                 'cancel existing subscription.')

            self.controller.log('{0}-{1}: Cancel subscription request by '\
                .format(self.module_name,
                        self.method_name)+\
                '{0}.'.format(subscriber)
            )

            # Validate the raw data is valid
            if not type(json_string) == str:
                raise KeyError('JSON data was not provided as '+\
                               'a string')

            self.controller.log('{0}-{1}: JSON data is a string.'\
                .format(self.module_name,
                        self.method_name)
            )

            if json_string in (None, ''):
                raise KeyError('No JSON data was provided, so '+\
                               'there were no keys')

            self.controller.log('{0}-{1}: JSON string is not empty.'\
                .format(self.module_name,
                        self.method_name)
            )

            loading_json = True
            json_data = json.loads(json_string)
            loading_json = False

            self.controller.log('{0}-{1}: JSON valid. Get subscription key.'\
                .format(self.module_name,
                        self.method_name)
            )

            # Get the key provided in the JSON.
            key=json_data['key']

            self.controller.log('{0}-{1}: Check if subscriber {2} exists.'\
                .format(self.module_name,
                        self.method_name,
                        subscriber)
            )

            # Get the subscription key from the KV store.
            sub_info=self.controller.get_value('subscriber_'+subscriber)

            # Validate the key
            if (not type(key) == str) \
            or (sub_info in ([], '', None)) \
            or (not key == sub_info):
                raise KeyError('{0} key incorrect or not subscribed.'\
                               .format(subscriber))

            self.controller.log('{0}-{1}: Key was correct.'\
                .format(self.module_name,
                        self.method_name)
            )

            # Remove the subscriber information from the KV store.
            self.controller.clear_value('subscriber_'+subscriber)
            self.controller.clear_value('call_back_'+subscriber)

            #
            # An additional step is required HERE to enforce referential
            # integrity and remove ANY data from the persistence layer related
            # to this subscriber. Not added due to code freeze, D Sanders, 05
            # May 2016.
            #

            # Prepare the data object for return.
            data={"subscription-key":None, "call-back":None}
        except ValueError as ve:
            success = 'error'
            status = 400
            message = '{0}-{1}: {2}'\
                .format(self.module_name, self.method_name, str(ve))
            if loading_json:
                status = 400
                message = '{0}-{1}: {2}'\
                    .format(self.module_name,
                            self.method_name,
                            'The JSON data is badly formed. Please check')
            data = None
            self.controller.log(message)
        except KeyError as ve:
            success = 'error'
            status = 403
            message = '{0}-{1}: {2}'\
                .format(self.module_name, self.method_name, str(ve))
            data = None
            self.controller.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        repr(e))
            data = None
            self.controller.log(message)
            print(error_text)

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)

        self.controller.log('{0}-{1}: Response returned: "{2}"'\
            .format(self.module_name,
                    self.method_name,
                    data)
        )

        return return_value

