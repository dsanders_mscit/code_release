################################################################################
#
# Program:      v4_00_State_Control.py
# Class:        v4_00_State_Control
# Objects:      State_Control
# Component of: Context
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: The state control allows states to be queried, created, expired, and
#          removed from the context engine. States have a value and a time to
#          live which enables them to apply over a certain time period.
#
################################################################################
from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Context import Control
import datetime, time, json, requests

class v4_00_State_Control(object):

    # Constructor.
    def __init__(self):
        self.controller = Control.global_controller
        self.module_name = 'State'
        self.method_name = 'unknown'

    # Helper method. Validates the caller is actually a subscriber to the 
    # service. Should probably become a base_lib utility.
    def _validate_subscriber(self, subscriber):
        self.controller.log('{0}-{1}: Fetching subscriber info for {2}.'\
            .format(self.module_name,
                    self.method_name,
                    subscriber)
        )

        sub_info = self.controller.get_value('subscriber_'+subscriber)

        self.controller.log('{0}-{1}: Validating {2} is a  subscriber.'\
            .format(self.module_name,
                    self.method_name,
                    subscriber)
        )

        if sub_info in ([], '', None):
            raise KeyError('{0} is not subscribing to context services.'\
                               .format(subscriber))

        return sub_info


    # Validate the parameters passed are the correct type and contain values, ie
    # they're not empty.
    def _validate_parameters_passed(
        self,
        subscriber=None,
        state=None,
    ):
        if type(subscriber) != str \
        or subscriber in (None, '', []) \
        or type(state) != str \
        or state in (None, '', []):
            raise ValueError('Subscriber and state have to be specified '+\
                             'to get state information.')


    # Validate the JSON passed.
    def _validate_json_string(
        self,
        json_string=None
    ):
        # Validate the raw data is valid
        self.controller.log('{0}-{1}: Validating JSON'\
            .format(self.module_name,self.method_name)
        )

        if not type(json_string) == str:
            raise KeyError('JSON data was not provided as '+\
                           'a string')

        self.controller.log('{0}-{1}: JSON data is a string.'\
            .format(self.module_name,self.method_name)
        )

        if json_string in (None, ''):
            raise KeyError('No JSON data was provided, so '+\
                           'there is no object to load')

        self.controller.log('{0}-{1}: JSON string is not empty.'\
            .format(self.module_name,self.method_name)
        )


    # Validate the subscriber's key is correct. The key was provided when the
    # subscriber connected to the context engine.
    #
    def _validate_key(
        self,
        json_data=None,
        sub_key=None
    ):
        # Get the key. Repeat this approach for all parameters.
        key=json_data['key']

        # Validate the key
        if (not type(key) == str) \
        or (not key == sub_key):
            raise ValueError('Key is incorrectly formed or incorrect')

        self.controller.log('{0}-{1}: Key was correct.'\
            .format(self.module_name,
                    self.method_name)
        )

        return key


    #
    # Get a state stored in the context engine. States are stored on subscriber
    # levels, so unique for each subscriber. The only_fresh parameter ensures
    # that only active states are returned; setting it to False allows all 
    # states to be returned, even expired ones.
    #
    def get_state(
        self,
        subscriber=None,
        state=None,
        only_fresh=True
    ):
        # Change the method name (used in logging) to show whether only fresh
        # or fresh and stale results are being shown.
        self.method_name = 'get_state' +\
            ('_only_fresh' if only_fresh else '_with_stale')

        success = 'success'
        status = 200
        message = 'State enquiry - ' +\
            ('only fresh states' \
                 if only_fresh \
                 else 'fresh and stale states')
        data = None

        try:
            self.controller.log('{0}-{1}: State {2} requested by '\
                .format(self.module_name,
                        self.method_name,
                        state)+\
                '{0}.'.format(subscriber)
            )

            # Validate the data passed, subscriber, etc.
            self._validate_parameters_passed(subscriber, state)
            sub_info=self._validate_subscriber(subscriber)

            self.controller.log('{0}-{1}: Fetching state {2} info for {3}.'\
                .format(self.module_name,
                        self.method_name,
                        state,
                        subscriber)
            )

            # Get the state. The persistence layer will handle the selection of
            # fresh or fresh and stale, so the dataset return will only contain
            # the correct data; i.e. no further manipulation is required.
            #
            state_info = self.controller.Context_db.get_key(
                subscriber=subscriber,
                key=state,
                only_fresh=only_fresh
            )

            self.controller.log('{0}-{1}: State {2} = "{3}".'\
                .format(self.module_name,
                        self.method_name,
                        state,
                        state_info)
            )

            # For the state, if it exists, calculate the time to live. Staleness
            # is stored not TTL, meaning that we work out how many seconds the
            # state has left to live.
            #
            if not state_info in (None, [], ''):
                now = datetime.datetime.now()
                state_value = state_info[0]
                stale_at = state_info[1]
                self.controller.log('{0}-{1}: Calculate time to live.'\
                    .format(self.module_name,
                            self.method_name)
                )

                # Convert the date stored in Sqlite 3 to a Python date.
                ttl_date = datetime.datetime.strptime(stale_at,
                                                      '%Y-%m-%d %H:%M:%S.%f')

                # The state may have expired since we fetched it, so check that
                # it is still fresh.
                if ttl_date > now:
                    ttl_gap =  ttl_date - datetime.datetime.now()

                    #
                    # Avoiding capturing overflow error
                    # Reference: http://bugs.python.org/issue1569623
                    # Note, issue is NOT a bug.
                    #
                    if ttl_gap.seconds != 86399: 
                        ttl = '{0}.{1}'\
                                .format(ttl_gap.seconds, ttl_gap.microseconds)
                    else:
                        ttl = '0'
                else:
                    # The state has expired since fetching, so ttl is null.
                    ttl = None
                    stale_at = None

                # Finally, if there was a time to live, convert it to a float.
                if ttl != None:
                    ttl = float(ttl)
            else:
                state_value = None
                stale_at = None
                ttl = None

            # Prepare the data object for return.
            data = {
                     "subscriber":subscriber,
                     "state":state,
                     "value":state_value,
                     "time-to-live":ttl,
                     "stale-at":stale_at
                   }
        except ValueError as ve:
            success = 'error'
            status = 400
            message = '{0}-{1}: {2}'\
                .format(self.module_name, self.method_name, str(ve))
            data = None
            self.controller.log(message)
        except KeyError as ve:
            success = 'error'
            status = 404
            message = '{0}-{1}: {2}'\
                .format(self.module_name, self.method_name, str(ve))
            data = None
            self.controller.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        repr(e))
            data = None
            self.controller.log(message)
            print(error_text)

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)

        self.controller.log('{0}-{1}: Response returned: "{2}"'\
            .format(self.module_name,
                    self.method_name,
                    data)
        )

        return return_value


    # Set a state. Allows a subscriber to set a state, with a value, and a
    # staleness period. The staleness point signifies when the state no longer
    # applies, providing temporal control.
    #
    def set_state(
        self,
        subscriber=None,
        state=None,
        json_string=None
    ):
        self.method_name = 'set_state'
        try:
            loading_json=False
            success = 'success'
            status = '200'
            message = 'State set.'
            data = None

            self.controller.log('{0}-{1}: Sample update request received.'\
                .format(self.module_name,
                        self.method_name)
            )

            # Validate the subscriber and the JSON.
            self._validate_parameters_passed(subscriber, state)
            sub_info=self._validate_subscriber(subscriber)
            self._validate_json_string(json_string)

            # Set a sentinel around loading the JSON data so we know if it's
            # poorly formatted and can catch it in the exception.
            self.controller.log('{0}-{1}: Loading JSON'\
                .format(self.module_name,
                        self.method_name)
            )

            # Load the JSON into a dict.
            loading_json = True
            json_data = json.loads(json_string)
            loading_json = False

            self.controller.log('{0}-{1}: JSON is valid and loaded.'\
                .format(self.module_name,
                        self.method_name)
            )

            # Validate the subscriber's key.
            self._validate_key(json_data=json_data, sub_key=sub_info)

            # Get json data
            self.controller.log('{0}-{1}: Get state value from JSON.'\
                .format(self.module_name,
                        self.method_name)
            )

            # Get the value for the state.
            value = json_data['value']

            self.controller.log('{0}-{1}: Get time to live value from JSON.'\
                .format(self.module_name,
                        self.method_name)
            )

            #
            # Get the time to live. Note, although TTL is a float, setting
            # expects an integer value. The original purpose was to expect a
            # whole number; however, it probably should be a float too - it's
            # not unreasonable to expect a state to live for 0.5 seconds. Not
            # changed due to code freeze, D Sanders, 05 May 2016.
            #
            ttl = json_data['time-to-live']
            if type(ttl) != int:
                raise ValueError('time-to-live must be an integer of seconds')

            #
            # Calculate the date and time the state will become stale based on
            # the time to live. Add the timedelta (based on TTL) to the current
            # date time. Note timezones will affect this. It might be better to
            # ensure all time functions are in UTC.
            #
            stale_at = datetime.datetime.now() + datetime.timedelta(seconds=ttl)

            self.controller.log('{0}-{1}: Setting state {2} to {3} for {4}. '\
                .format(self.module_name,
                        self.method_name,
                        state,
                        value,
                        subscriber)+\
                'Stale at {0}'.format(stale_at)
            )

            # Persist the state.
            self.controller.Context_db.set_key(
                subscriber=subscriber,
                key=state,
                value=value,
                stale_at=stale_at
            )

            # Prepare the data object for return.
            data = {
                     "subscriber":subscriber,
                     "state":state,
                     "value":value,
                     "time-to-live":ttl,
                     "stale-at":str(stale_at)
                   }
        except KeyError as ke:
            success = 'error'
            status = 400
            message = '{0}-{1}: Key Error >> {2}'\
                .format(self.module_name, self.method_name, str(ke))
            data = {'error-message':str(ke)}
            self.controller.log(message)
        except ValueError as ve:
            success = 'error'
            status = 403
            message = '{0}-{1}: {2}'\
                .format(self.module_name, self.method_name, str(ve))
            data = {'error-message':str(ve)}
            if loading_json:
                status = 400
                message = '{0}-{1}: {2}'\
                    .format(self.module_name,
                            self.method_name,
                            'The JSON data is badly formed. Please check')
                data = {'error-message':'Bad JSON data'}
            self.controller.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        repr(e))
            data = {"exception":repr(e)}
            self.controller.log(message)
            print(error_text)

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)

        self.controller.log('{0}-{1}: Set state request returned: {2}'\
            .format(self.module_name,
                    self.method_name,
                    data)
        )

        return return_value


    # Clear a state. Although states expire and can, in effect, be left to die,
    # this provides a method to immediately revoke a state. It's useful if a
    # state is given a particularly long TTL.
    #
    def clear_state(
        self,
        subscriber=None,
        state=None,
        json_string=None
    ):
        self.method_name = 'clear_state'
        try:
            loading_json=False
            success = 'success'
            status = '200'
            message = 'State cleared.'
            data = None

            self.controller.log('{0}-{1}: Sample update request received.'\
                .format(self.module_name,
                        self.method_name)
            )

            # Validate parameters and JSON data.
            self._validate_parameters_passed(subscriber, state)
            sub_info=self._validate_subscriber(subscriber)
            self._validate_json_string(json_string)

            # Set a sentinel around loading the JSON data so we know if it's
            # poorly formatted and can catch it in the exception.
            self.controller.log('{0}-{1}: Loading JSON'\
                .format(self.module_name,
                        self.method_name)
            )

            loading_json = True
            json_data = json.loads(json_string)
            loading_json = False

            self.controller.log('{0}-{1}: JSON is valid and loaded.'\
                .format(self.module_name,
                        self.method_name)
            )

            # Validate the subscriber's key.
            self._validate_key(json_data=json_data, sub_key=sub_info)

            self.controller.log('{0}-{1}: Clearing state {2} for {3}'\
                .format(self.module_name,
                        self.method_name,
                        state,
                        subscriber)
            )

            # Clear the key from persistence.
            self.controller.Context_db.clear_key(
                subscriber=subscriber,
                key=state
            )

            # Check for other states. This allows us to tell the caller if there
            # are any other states still affecting this subscriber and gives
            # them an option to act on no more states being set.
            #
            states = self.controller.Context_db.get_subscriber_keys(
                subscriber=subscriber
            )
            state_count = len(states) or 0

            # Prepare the data object for return.
            data = {
                     "subscriber":subscriber,
                     "state":state,
                     "value":None,
                     "time-to-live":None,
                     "stale-at":None,
                     "states-remaining":state_count
                   }
        except KeyError as ke:
            success = 'error'
            status = 400
            message = '{0}-{1}: Key Error >> {2}'\
                .format(self.module_name, self.method_name, str(ke))
            data = {'error-message':str(ke)}
            self.controller.log(message)
        except ValueError as ve:
            success = 'error'
            status = 403
            message = '{0}-{1}: {2}'\
                .format(self.module_name, self.method_name, str(ve))
            data = {'error-message':str(ve)}
            if loading_json:
                status = 400
                message = '{0}-{1}: {2}'\
                    .format(self.module_name,
                            self.method_name,
                            'The JSON data is badly formed. Please check')
                data = {'error-message':'Bad JSON data'}
            self.controller.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        repr(e))
            data = {"exception":repr(e)}
            self.controller.log(message)
            print(error_text)

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)

        self.controller.log('{0}-{1}: Clear state request returned: {2}'\
            .format(self.module_name,
                    self.method_name,
                    data)
        )

        return return_value

################################################################################
# DEPRECATED CODE BLOCK - The code below is generated by the generator and
#                         should have been removed BUT wasn't before code freeze
#                         D Sanders, 05 May 2016.
################################################################################

    def not_implemented(self, route=None):
        self.method_name = 'not_implemented'

        success = 'error'
        status = '500'
        message = 'Not Implemented.'
        data = None

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)

        self.controller.log('{0}-{1}: Route {2} not implemented'\
            .format(self.module_name,
                    self.method_name,
                    route)
        )

        return return_value


    def sample_request(self):
        self.method_name = 'sample_request'
        try:
            success = 'success'
            status = '200'
            message = 'Sample'
            data = {"sample":"value"}

            self.controller.log('{0}-{1}: Sample request received.'\
                .format(self.module_name,
                        self.method_name)
            )
        except Exception as e:
            success = 'error'
            status = '500'
            message = '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        repr(e))
            data = {"exception":repr(e)}
            self.controller.log(message)
            print(error_text)

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)

        self.controller.log('{0}-{1}: Sample request returned: {2}'\
            .format(self.module_name,
                    self.method_name,
                    data)
        )

        return return_value

    def sample_update(
        self,
        json_string=None
    ):
        self.method_name = 'sample_update'
        try:
            success = 'success'
            status = '200'
            message = 'Sample'
            data = None

            self.controller.log('{0}-{1}: Sample update request received.'\
                .format(self.module_name,
                        self.method_name)
            )

            # Validate the raw data is valid
            if not type(json_string) == str:
                raise KeyError('JSON data was not provided as '+\
                               'a string')

            self.controller.log('{0}-{1}: JSON data is a string.'\
                .format(self.module_name,
                        self.method_name)
            )

            if json_string in (None, ''):
                raise KeyError('No JSON data was provided, so '+\
                               'there were no keys')

            self.controller.log('{0}-{1}: JSON string is not empty.'\
                .format(self.module_name,
                        self.method_name)
            )

            # Set a sentinel around loading the JSON data so we know if it's
            # poorly formatted and can catch it in the exception.
            self.controller.log('{0}-{1}: Loading JSON'\
                .format(self.module_name,
                        self.method_name)
            )

            loading_json = True
            json_data = json.loads(json_string)
            loading_json = False

            self.controller.log('{0}-{1}: JSON is valid and loaded.'\
                .format(self.module_name,
                        self.method_name)
            )

            # Get the key. Repeat this approach for all parameters.
            key=json_data['key']

            # Validate the key
            if (not type(key) == str) \
            or (not key == '1234-5678-9012-3456'): # Change to correct key!
                raise ValueError('Key is incorrectly formed or incorrect')

            self.controller.log('{0}-{1}: Key was correct.'\
                .format(self.module_name,
                        self.method_name)
            )

            # Do whatever updates need done.
            data = {"sample":"updated-value"}
        except KeyError as ke:
            success = 'error'
            status = 400
            message = '{0}-{1}: Key Error >> {2}'\
                .format(self.module_name, self.method_name, str(ke))
            data = {'error-message':str(ke)}
            self.controller.log(message)
        except ValueError as ve:
            success = 'error'
            status = 403
            message = '{0}-{1}: {2}'\
                .format(self.module_name, self.method_name, str(ve))
            data = {'error-message':str(ve)}
            if loading_json:
                status = 400
                message = '{0}-{1}: {2}'\
                    .format(self.module_name,
                            self.method_name,
                            'The JSON data is badly formed. Please check')
                data = {'error-message':'Bad JSON data'}
            self.controller.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        repr(e))
            data = {"exception":repr(e)}
            self.controller.log(message)
            print(error_text)

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)

        self.controller.log('{0}-{1}: Sample update request returned: {2}'\
            .format(self.module_name,
                    self.method_name,
                    data)
        )

        return return_value


