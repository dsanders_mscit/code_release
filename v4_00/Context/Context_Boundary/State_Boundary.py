################################################################################
#
# Program:      State_Boundary.py
# Class:        State_Boundary, State_Boundary_Force
# Objects:      State_Boundary, State_Boundary_Force
# Component of: Context
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Define the boundary methods that will be used to get, set, and clear
#          context states. Two boundaries are provided: state_boundary handles
#          fresh (i.e. not stale) states; state_boundary_force handles both
#          stale and fresh states and supports GET only.
#
################################################################################
#
# Import required modules and packages
#
from flask_restful import Resource, reqparse
from Context.State_Control import state_control_object

class State_Boundary_Force(Resource):
    def get(self, subscriber=None, state=None):
        return state_control_object.get_state(
            subscriber,
            state,
            only_fresh=False
        )

class State_Boundary(Resource):
    def get(self, subscriber=None, state=None):
        return state_control_object.get_state(subscriber, state)

    def post(self, subscriber=None, state=None):
        #
        # Get the data with the request and decode it to UTF-8
        #
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        #
        # Don't return results immediately. There may be a need to update 
        # headers afterward SO, get the return result, do anything needed, and
        # then return.
        #
        return_state = state_control_object\
                           .set_state(subscriber, state, raw_data)
        # Return
        return return_state

    def delete(self, subscriber=None, state=None):
        #
        # Get the data with the request and decode it to UTF-8
        #
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        #
        # Don't return results immediately. There may be a need to update 
        # headers afterward SO, get the return result, do anything needed, and
        # then return.
        #
        return_state = state_control_object\
                           .clear_state(subscriber, state, raw_data)
        # Return
        return return_state


