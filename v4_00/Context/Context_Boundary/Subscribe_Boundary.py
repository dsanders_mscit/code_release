################################################################################
#
# Program:      Subscribe_Boundary.py
# Class:        Subscribe_Boundary
# Objects:      Subscribe_Boundary
# Component of: Context
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Define the boundary methods that manage subscriptions (connections) 
#          to the context engine. Supports GET, POST, and DELETE.
#
################################################################################
#
# Import required modules and packages
#
from flask_restful import Resource, reqparse
from Context.Subscribe_Control import subscribe_control_object

class Subscribe_Boundary(Resource):
#
# Modified 02 April 2016. 
# 1. Removed PUT route
# 2. Added parameter subscriber
#
    def get(self, subscriber=None):
        return subscribe_control_object.get_subscription(subscriber)

    def post(self, subscriber=None):
        #
        # Get the data with the request and decode it to UTF-8
        #
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        #
        # Don't return results immediately. There may be a need to update 
        # headers afterward SO, get the return result, do anything needed, and
        # then return.
        #
        return_state = subscribe_control_object\
            .set_subscription(subscriber=subscriber, json_string=raw_data)
        return return_state

    def delete(self, subscriber=None):
        #
        # Get the data with the request and decode it to UTF-8
        #
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        #
        # Don't return results immediately. There may be a need to update 
        # headers afterward SO, get the return result, do anything needed, and
        # then return.
        #
        return_state = subscribe_control_object\
            .cancel_subscription(subscriber=subscriber, json_string=raw_data)
        return return_state


