################################################################################
#
# Program:      main.py
# Class:        None
# Objects:      None
# Component of: Context
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: main Python program for the Boundary package. This code defines the
#          routes that will be offered by Flask (via the NGINX/UWSGI services)
#          and provide the interfaces and boundaries to the model.
#
# References
# ----------
# Ronacher, A., 2013, 'Larger Applications' [ONLINE]. Available at: 
# http://flask.pocoo.org/docs/0.10/patterns/packages/ (Accessed: 04 November 
# 2015)
#
################################################################################

#
# Standard imports
#
from Context import app, api
from Context.Control import global_controller
#
# Get the version of the API
#
version = global_controller.get_value('version')
#
# Reference: Adamo, D. [2015], 'Handling CORS Requests in Flask(-RESTful) APIs'
# [ONLINE]. Available at: http://www.davidadamojr.com/handling-cors-requests-in-flask-restful-apis/
# (Accessed: 08 March 2016)
#
# Set cross origin resource sharing (CORS) in headers for ALL request responses
#
@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Authorization, Accept')
    response.headers.add('Access-Control-Allow-Methods',
        'GET,PUT,POST,OPTIONS,HEAD,DELETE')
    return response
#
# Sample service boundary
#
#from Context_Boundary.Sample_Boundary import Sample_Boundary
#api.add_resource(Sample_Boundary, '/{0}/sample'.format(version))
#
# Generated service boundaries
#


### Generated path for service: State
from Context_Boundary.State_Boundary \
    import State_Boundary, State_Boundary_Force
api.add_resource(State_Boundary, 
                 '/{0}/state/<string:subscriber>/<string:state>'\
                     .format(version))
api.add_resource(State_Boundary_Force, 
                 '/{0}/state/<string:subscriber>/<string:state>/force'\
                     .format(version))

### Generated path for service: Query
from Context_Boundary.Query_Boundary import Query_Boundary
api.add_resource(Query_Boundary, '/{0}/query/<string:subscriber>'.format(version))

### Generated path for service: Notify
#from Context_Boundary.Notify_Boundary import Notify_Boundary
#api.add_resource(Notify_Boundary, '/{0}/notify'.format(version))

### Generated path for service: Subscribe
# Modified 02 April 2016 - Add Parameter 'subscriber'
from Context_Boundary.Subscribe_Boundary import Subscribe_Boundary
api.add_resource(Subscribe_Boundary,
                 '/{0}/subscribe/<string:subscriber>'.format(version))
