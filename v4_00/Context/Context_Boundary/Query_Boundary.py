################################################################################
#
# Program:      Query_Boundary.py
# Class:        Query_Boundary
# Objects:      Query_Boundary
# Component of: Context
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Define the boundary methods that will be used to query context. It
#          currently uses POST and there is some doubt as to whether this is the
#          correct verb; it probably should be a query (GET) with query 
#          parameters. Not modified due to code freeze, D Sanders, 05 May 2016.
#
################################################################################
#
# Import required modules and packages
#
from flask_restful import Resource, reqparse
from Context.Query_Control import query_control_object

class Query_Boundary(Resource):
    def post(self, subscriber=None):
        #
        # Get the data with the request and decode it to UTF-8
        #
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        #
        # Don't return results immediately. There may be a need to update 
        # headers afterward SO, get the return result, do anything needed, and
        # then return.
        #
        return_state = query_control_object\
                           .process_query(subscriber=subscriber,
                                          json_string=raw_data)
        # Return
        return return_state


