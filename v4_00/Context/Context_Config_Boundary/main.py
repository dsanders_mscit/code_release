################################################################################
#
# Program:      main.py
# Class:        None
# Objects:      None
# Component of: Context
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: main Python program for the config Boundary. This code defines the
#          routes that will be offered by Flask (via the NGINX/UWSGI services)
#          and provide the configuraiton interfaces and boundaries to the model.
#
# References
# ----------
# Ronacher, A., 2013, 'Larger Applications' [ONLINE]. Available at: 
# http://flask.pocoo.org/docs/0.10/patterns/packages/ (Accessed: 04 November 
# 2015)
#
################################################################################

from Context import app, api
from Context.Control import global_controller
from Context_Config_Boundary.Config_Logger_Boundary \
    import Config_Logger_Boundary
#
# Get the version of the API
#
version = global_controller.get_value('version')

#
# Place config boundaries here
#
# End config boundaries here
#
api.add_resource(Config_Logger_Boundary,
                 '/{0}/config/logger'.format(version))

