################################################################################
#
# Program:      v4_00_Config_ShowOnLock_Control.py
# Class:        v4_00_Config_ShowOnLock_Control
# Objects:      Config_ShowOnLock_Control
# Component of: Phone
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: The configuration controller to manage showing notifications on the
#          lock screen.
#
################################################################################
#
# Import required modules and packages
#
from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Phone import Control
import datetime, time, json, requests

class v4_00_Config_ShowOnLock_Control(object):
    controller = None
    return_schema = {
        "show-on-lock":None
    }

    # Constructor
    def __init__(self):
        self.controller = Control.global_controller
        self.module_name = 'ShowOnLock'
        self.method_name = 'unknown'


    # override log
    def log(self, log_message):
        self.controller.log(
            '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        str(log_message))
        )
        

    # Validate the JSON string and return it as a dict
    def _validate_json_string(
        self,
        json_string=None
    ):
        # Validate the raw data is valid
        self.log('Validating JSON')

        if not type(json_string) == str:
            raise KeyError('JSON data was not provided as '+\
                           'a string')

        self.log('JSON data is a string.')

        if json_string in (None, ''):
            raise KeyError('No JSON data was provided, so '+\
                           'there is no object to load')

        self.log('JSON string is not empty.')
        
        return json.loads(json_string)


    # Validate the control key.
    def _validate_key(
        self,
        json_data=None
    ):
        key=json_data['key']

        # Validate the key
        if (not type(key) == str) \
        or (not key == '1234-5678-9012-3456'): # Change to correct key!
            raise ValueError('Key is incorrectly formed or incorrect')

        self.log('Key was correct.')

        return key


    def set_return_schema(
        self,
        value=None
    ):
        self.return_schema['show-on-lock']=value


    def get_show_on_lock(
        self, 
        state=None
    ):
        try:
            self.method_name = 'get_show_on_lock'
            success = 'success'
            status = '200'
            message = 'Phone show on lock status'
            data = None
            
            self.log('Checking if phone shows notifications on lock')
            show_on_lock=self.controller.get_value('show-on-lock')

            # The persistence layer used is Sqlite 3 which handles boolean
            # values as 0 (False) and 1 (True). We need to convert these binary
            # items into real True and False:
            #
            if show_on_lock in (None, '', []): # Default behaviour
                show_on_lock = False
            elif show_on_lock == 1:
                show_on_lock = True
            elif show_on_lock == 0:
                show_on_lock = False

            self.set_return_schema(show_on_lock)
            data=self.return_schema
        except Exception as e:
            success = 'error'
            status = '500'
            message = 'EXCEPTION >> {0}'.format(repr(e))
            self.log(message)
            print(message)

        self.log('Show on lock state reported as: "{0}"'.format(data))

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)

        return return_value



    def set_show_on_lock(
        self, 
        json_string=None
    ):
        try:
            self.method_name = 'set_show_on_lock'
            success = 'success'
            status = '200'
            message = 'Enable phone to show notifications on lock status'
            data = None
            
            json_data = self._validate_json_string(json_string)

            validating_key=True
            self._validate_key(json_data)
            validating_key=False

            self.log('Set phone to show notifications on lock')
            show_on_lock=self.controller.set_value('show-on-lock', True)

            self.set_return_schema(show_on_lock)
            data=self.return_schema
        except KeyError as ke:
            if validating_key:
                message = 'The control key is either incorrect or missing.'
                status = 403
            else:
                message = 'Key Error >> {0}'.format(str(ke))
                status = 400
            success = 'error'
            self.log(message)
        except ValueError as ke:
            success = 'error'
            status = 400
            message = 'Value Error >> {0}'.format(str(ke))
            self.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = 'EXCEPTION >> {0}'.format(repr(e))
            self.log(message)
            print(message)

        self.log('Show on lock state set: {0}'.format(data))

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)

        return return_value


    def clear_show_on_lock(
        self, 
        json_string=None
    ):
        try:
            self.method_name = 'get_show_on_lock'
            success = 'success'
            status = '200'
            message = 'Prevent phone showing notifications on lock status'
            data = None
            
            json_data = self._validate_json_string(json_string)

            validating_key=True
            self._validate_key(json_data)
            validating_key=False

            self.log('Set phone to not show notifications on lock')
            show_on_lock=self.controller.set_value('show-on-lock', False)

            self.set_return_schema(show_on_lock)
            data=self.return_schema
        except KeyError as ke:
            success = 'error'
            status = 400
            message = 'Key Error >> {0}'.format(str(ke))
            self.log(message)
        except ValueError as ke:
            success = 'error'
            status = 500
            message = 'Value Error >> {0}'.format(str(ke))
            self.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = 'EXCEPTION >> {0}'.format(repr(e))
            self.log(message)
            print(message)

        self.log('Show on lock state set: {0}'.format(data))

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)

        return return_value



