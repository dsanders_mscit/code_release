################################################################################
#
# Program:      v4_00_Config_State_Control.py
# Class:        v4_00_Config_State_Control
# Objects:      Config_State_Control
# Component of: Phone
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: The configuration controller to manage privacy states.
#
################################################################################
#
# Import required modules and packages
#
from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Phone import Control
import datetime, time, json, requests

class v4_00_Config_State_Control(object):
    controller = None

    # Schema. There are three attributes:
    #
    #    value: a value assigned to the state.
    #    time-to-live: a value, in seconds, for how long the state is valid.
    #    stale-at: a calculate time (in current timezone) at which staleness is
    #              reached.
    #
    return_schema = {
        "value":None,
        "time-to-live":None,
        "stale-at":None,
    }


    # Constructor
    def __init__(self):
        self.controller = Control.global_controller
        self.module_name = 'State'
        self.method_name = 'unknown'


    # Override log to provide additional information.
    def log(self, log_message):
        self.controller.log(
            '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        str(log_message))
        )
        

    # Validate that state is a string and not empty.
    def _validate_parameters_passed(
        self,
        state=None,
    ):
        if type(state) != str \
        or state in (None, '', []):
            raise ValueError('State name has to be specified '+\
                             'to get state information.')


    # Validate the JSON is good and return a dict.
    def _validate_json_string(
        self,
        json_string=None
    ):
        # Validate the raw data is valid
        self.log('Validating JSON')

        if not type(json_string) == str:
            raise KeyError('JSON data was not provided as '+\
                           'a string')

        self.log('JSON data is a string.')

        if json_string in (None, ''):
            raise KeyError('No JSON data was provided, so '+\
                           'there is no object to load')

        self.log('JSON string is not empty.')
        
        return json.loads(json_string)


    # Validate the control key is correct.
    def _validate_key(
        self,
        json_data=None
    ):
        key=json_data['key']

        # Validate the key
        if (not type(key) == str) \
        or (not key == '1234-5678-9012-3456'): # Change to correct key!
            raise ValueError('Key is incorrectly formed or incorrect')

        self.log('Key was correct.')

        return key


    # Update the schema
    def set_return_schema(
        self,
        value=None,
        ttl=None,
        stale_at=None
    ):
        self.return_schema['value']=value
        self.return_schema['time-to-live']=ttl
        self.return_schema['stale-at']=stale_at


    # Get the context engine used to store state. If there is no context engine,
    # then state cannot be set (or cleared).
    #
    def get_context_engine(self):
        self.log('Fetching context engine information')
        return_dict = {}

        return_dict['context_engine'] = \
            self.controller.get_value('context-engine')

        if return_dict['context_engine'] == None:
            raise KeyError('Not subscribed to any context engine; '+\
                           'state cannot be queried, modified, or cleared.')

        return_dict['context_key'] =\
            self.controller.get_value('context-engine-key')
        return_dict['context_url'] = \
            self.controller.get_value('context-engine-url')
        return_dict['context_query'] = \
            self.controller.get_value('context-query')
        return_dict['context_state'] = \
            self.controller.get_value('context-state')
        self.log('Returning data for context engine: {0}'\
            .format(return_dict['context_engine'])
        )
        return return_dict


    def get_state(
        self, 
        state=None
    ):
        try:
            self.method_name = 'get_state'
            success = 'success'
            status = '200'
            message = 'Context state'
            data = None
            
            context_engine = self.get_context_engine()

            state_url = '{0}/{1}'.format(
                context_engine['context_state'],
                state
            )

            self.log('Issuing request to {0}'.format(state_url))
            response_returned = requests.get(
                state_url
            )
            status_code = response_returned.status_code
            raw_text = response_returned.text
            self.log('Request returned status: {0}'.format(status_code))
            
            if not status_code in (200, 400, 403, 500):
                raise requests.exceptions.ConnectionError(
                    'Status is not ok. {0}'.format(raw_text)
                )

            got_json = response_returned.json()
            if status_code in (400,403,500):
                raise ValueError(
                    'Error in response. {0}: {1}'.format(
                        got_json['response'],
                        got_json['message']
                    )
                )

            state_value = got_json['data']['value']
            state_ttl = got_json['data']['time-to-live']
            state_stale_at = got_json['data']['stale-at']
            self.set_return_schema(state_value, 
                                   state_ttl, 
                                   state_stale_at)

            data={state:self.return_schema}
        ###
        ### Should consider fail-to-safe approach here. If state can't be
        ### detected, should we return a null value rather than raise and error?
        ### This is the config part rather than operational, so not sure.
        ###
        except requests.exceptions.ConnectionError as rce:
            success = 'error'
            status = '500'
            message = '{0}'.format(str(rce))
            self.log(message)
        except KeyError as ke:
            success = 'error'
            status = 400
            message = 'Key Error >> {0}'.format(str(ke))
            self.log(message)
        except ValueError as ke:
            success = 'error'
            status = 500
            message = 'Value Error >> {0}'.format(str(ke))
            self.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = 'EXCEPTION >> {0}'.format(repr(e))
            self.log(message)
            print(message)

        self.log('Context state found: {0}'.format(data))

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)

        return return_value



    def set_state(
        self, 
        state=None,
        json_string=None
    ):
        try:
            self.method_name = 'set_state'
            success = 'success'
            status = '200'
            message = 'Context state'
            data = None
            validating_key = False

            context_engine = self.get_context_engine()

            self._validate_parameters_passed(state)
            json_data = self._validate_json_string(json_string)

            validating_key=True
            self._validate_key(json_data)
            validating_key=False

            value = json_data['value']
            ttl = json_data['time-to-live']

            if type(ttl) != int \
            or (type(ttl) == int and ttl < 1):
                raise ValueError(
                    'The time to live must be a positive integer value, '+\
                    'greater than zero, representing the number of seconds '+\
                    'the state remains valid for.'
                )

            self.log('Forming payload')
            payload = {
                "key":context_engine['context_key'],
                "value":value,
                "time-to-live":ttl
            }

            state_url = '{0}/{1}'.format(
                context_engine['context_state'],
                state
            )

            self.log(
                'Issuing request to {0} '.format(state_url)+\
                'with payload "{0}"'.format(payload)
            )

            response_returned = requests.post(
                state_url,
                data=json.dumps(payload)
            )

            status_code = response_returned.status_code
            raw_text = response_returned.text

            self.log('Request returned status: {0}'.format(status_code))
            
            if not status_code in (200, 400, 403, 500):
                raise requests.exceptions.ConnectionError(
                    'Status is not ok. {0}'.format(raw_text)
                )

            got_json = response_returned.json()
            if status_code in (400,403,500):
                raise ValueError(
                    'Error in response. {0}: {1}'.format(
                        got_json['response'],
                        got_json['message']
                    )
                )

            state_value = got_json['data']['value']
            state_ttl = got_json['data']['time-to-live']
            state_stale_at = got_json['data']['stale-at']
            self.set_return_schema(state_value, 
                                   state_ttl, 
                                   state_stale_at)

            data={state:self.return_schema}
        except requests.exceptions.ConnectionError as rce:
            success = 'error'
            status = '500'
            message = '{0}'.format(str(rce))
            self.log(message)
        except KeyError as ke:
            if validating_key:
                message = 'The control key is either incorrect or missing.'
                status = 403
            else:
                message = 'Key Error >> {0}'.format(str(ke))
                status = 400
            success = 'error'
            self.log(message)
        except ValueError as ke:
            success = 'error'
            status = 400
            message = 'Value Error >> {0}'.format(str(ke))
            self.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = 'EXCEPTION >> {0}'.format(repr(e))
            self.log(message)
            print(message)

        self.log('Context state set: {0}'.format(data))

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)

        return return_value


    def clear_state(
        self, 
        state=None,
        json_string=None
    ):
        try:
            self.method_name = 'clear_state'
            success = 'success'
            status = '200'
            message = 'Context state'
            data = None
            
            context_engine = self.get_context_engine()

            self._validate_parameters_passed(state)
            json_data = self._validate_json_string(json_string)

            validating_key=True
            self._validate_key(json_data)
            validating_key=False

            self.log('Forming payload')
            payload = {
                "key":context_engine['context_key']
            }

            state_url = '{0}/{1}'.format(
                context_engine['context_state'],
                state
            )

            self.log(
                'Issuing request to {0} '.format(state_url)+\
                'with payload "{0}"'.format(payload)
            )

            response_returned = requests.delete(
                state_url,
                data=json.dumps(payload)
            )

            status_code = response_returned.status_code
            raw_text = response_returned.text

            self.log('Request returned status: {0}'.format(status_code))
            
            if not status_code in (200, 400, 403, 500):
                raise requests.exceptions.ConnectionError(
                    'Status is not ok. {0}'.format(raw_text)
                )

            got_json = response_returned.json()
            if status_code in (400,403,500):
                raise ValueError(
                    'Error in response. {0}: {1}'.format(
                        got_json['response'],
                        got_json['message']
                    )
                )

            state_value = got_json['data']['value']
            state_ttl = got_json['data']['time-to-live']
            state_stale_at = got_json['data']['stale-at']
            state_count = got_json['data']['states-remaining']

            if state_count < 1:
                self.controller.log(
                    'There are no more states, therefore check '+\
                    'for backlog of notifications.'
                )
                self.controller.handle_unlock()

            self.set_return_schema(state_value, 
                                   state_ttl, 
                                   state_stale_at)

            data={state:self.return_schema}
        except requests.exceptions.ConnectionError as rce:
            success = 'error'
            status = '500'
            message = '{0}'.format(str(rce))
            self.log(message)
        except KeyError as ke:
            success = 'error'
            status = 400
            message = 'Key Error >> {0}'.format(str(ke))
            self.log(message)
        except ValueError as ke:
            success = 'error'
            status = 500
            message = 'Value Error >> {0}'.format(str(ke))
            self.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = 'EXCEPTION >> {0}'.format(repr(e))
            self.log(message)
            print(message)

        self.log('Context state cleared: {0}'.format(data))

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)

        return return_value



