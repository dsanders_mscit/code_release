################################################################################
#
# Program:      v4_00_Config_Presence_Control.py
# Class:        v4_00_Config_Presence_Control
# Objects:      Config_Presence_Control
# Component of: Phone
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: The configuration controller for presence. Connects to a presence
#          engine to enable relationships of people near by to be checked.
#
################################################################################
#
# Import required modules and packages
#
from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Phone import Control
import datetime, time, json, requests

class v4_00_Config_Presence_Control(object):
    controller = None

    # The schema used by this class. There are three attributes:
    #
    #   presence-engine: the url to connect to the presence engine.
    #   user-id: the user of this phone who is registered in the presence engine
    #   people-url: the url to connect to and use for validating the 
    #               relationship of a person
    #
    return_schema = {
        "presence-engine":None,
        "user-id":None,
        "people-url":None
    }


    # Constructor.
    def __init__(self):
        self.controller = Control.global_controller
        self.module_name = 'Presence'
        self.method_name = 'unknown'


    # Override log to include more information.
    def log(self, log_message):
        self.controller.log(
            '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        str(log_message))
        )
        

    # Validate JSON string and return a dict. To become a base_lib helper.
    def _validate_json_string(
        self,
        json_string=None
    ):
        # Validate the raw data is valid
        self.log('Validating JSON')

        if not type(json_string) == str:
            raise KeyError('JSON data was not provided as '+\
                           'a string')

        self.log('JSON data is a string.')

        if json_string in (None, ''):
            raise KeyError('No JSON data was provided, so '+\
                           'there is no object to load')

        self.log('JSON string is not empty.')
        
        return json.loads(json_string)


    # Update the schema.
    def set_return_schema(
        self,
        presence_engine=None,
        user_id=None,
        people_url=None
    ):
        self.return_schema['presence-engine']=presence_engine
        self.return_schema['user-id']=user_id
        self.return_schema['people-url']=people_url


    # Get the presence engine info if defined.
    def get_presence(
        self, 
        state=None
    ):
        try:
            self.method_name = 'get_presence'
            success = 'success'
            status = '200'
            message = 'Phone presence engine.'
            data = None

            self.log('Getting phone presence engine.')
            presence = self.controller.get_value('presence-engine')
            user = self.controller.get_value('presence-user')
            people_url = self.controller.get_value('presence-people-url')
            self.log('Presence engine is {0}. User is {1}'\
                .format(presence, user)
            )

            self.set_return_schema(presence, user, people_url)

            data=self.return_schema
        except Exception as e:
            success = 'error'
            status = '500'
            message = 'EXCEPTION >> {0}'.format(repr(e))
            self.log(message)
            print(message)

        self.log('Phone presence engine reported as: "{0}"'\
            .format(data)
        )

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)

        return return_value



    # Set the presence engine. A validation check is made and the presence 
    # engine is connected to. The user is registered. Presence is then available
    #
    def set_presence(
        self, 
        json_string=None
    ):
        try:
            self.method_name = 'set_presence'
            success = 'success'
            status = '200'
            message = 'Set phone presence engine'
            data = None
            
            # Validate and load the JSON data.
            json_data = self._validate_json_string(json_string)

            # Get the required fields from the JSON dict. Fields not passed will
            # raise a Key Error.
            presence_engine = json_data['presence-engine']
            user_id = json_data['user-id']

            # Simple check: make sure the URL begins with http://
            if presence_engine[-5:] == '/user':
                presence_url = '{0}/{1}'.format(presence_engine, user_id)
            else:
                presence_url = '{0}/user/{1}'.format(presence_engine, user_id)

            self.log('Issuing subscription request to {0}'.format(presence_url))

            status_code = 500

            # Now register with the presence engine
            return_status = requests.post(
                presence_url,
                timeout=10
            )

            # Get the return status and raw text response
            status_code = return_status.status_code
            raw_text = return_status.text

            # Check the status is something we're expecting. If it's not, raise
            # a connection error.
            if not status_code in (200, 201, 400, 403, 500):
                raise requests.exceptions.ConnectionError(
                    'Status is not ok. {0}'.format(raw_text)
                )

            # Get the JSON the presence engine returned.
            got_json = return_status.json()

            # If it was a 400, 403, or 500 error, then the presence engine
            # reported a problem. Raise an error.
            if status_code in (400,403,500):
                raise ValueError(
                    'Error in response. {0}: {1}'.format(
                        got_json['response'],
                        got_json['message']
                    )
                )

            # Get the URL for checking a person's relationship that the presence
            # engine gave us. This is good HATEOAS as the presence engine is
            # telling the caller how to use the data, not just what the data is.
            people_url = got_json['data']['people-url']


            # Store the returned information.
            self.log('Set phone presence-engine in key store')
            presence=self.controller.set_value(
                'presence-engine', presence_engine
            )
            user=self.controller.set_value(
                'presence-user', user_id
            )
            people_url=self.controller.set_value(
                'presence-people-url', people_url
            )

            self.set_return_schema(presence, user, people_url)
            data=self.return_schema
        except requests.exceptions.ConnectionError as rce:
            message = 'Connection Error >> {0}'.format(str(rce))
            status = status_code
            success = 'error'
            self.log(message)
        except KeyError as ke:
            message = 'Key Error >> {0}'.format(str(ke))
            status = 400
            success = 'error'
            self.log(message)
        except ValueError as ke:
            success = 'error'
            status = 400
            message = 'Value Error >> {0}'.format(str(ke))
            self.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = 'EXCEPTION >> {0}'.format(repr(e))
            self.log(message)
            print(message)

        self.log('Presence engine set to: {0}'.format(data))

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)

        return return_value


    # Clear presence removes presence information from the phone and disconnects
    # from the presence engine.
    def clear_presence(
        self, 
        json_string=None
    ):
        try:
            self.method_name = 'clear_presence'
            success = 'success'
            status = '200'
            message = 'Disconnect phone presence engine.'
            data = None
            
            self.log('Disconnect phone presence engine.')

            status_code = 500

            # Get the stored values for the presence engine.
            presence_engine = self.controller.get_value('presence-engine')
            user_id = self.controller.get_value('presence-user')

            # If they are empty or null then presence is not connected.
            if presence_engine in ('',[],None)\
            or user_id in ('',[],None):
                raise ValueError(
                    'The phone is not connected to a presence engine.'
                )

            # Check if the URL given already has /user as the last five chars.
            if presence_engine[-5:] == '/user':
                presence_url = '{0}/{1}'.format(presence_engine, user_id)
            else:
                presence_url = '{0}/user/{1}'.format(presence_engine, user_id)

            # Now cancel subscription with the presence engine
            return_status = requests.delete(
                presence_url,
                timeout=10
            )

            # Get the status code and raw response text.
            status_code = return_status.status_code
            raw_text = return_status.text

            # Check it is something we were expecting. If it's not, then raise
            # an error.
            if not status_code in (200, 201, 400, 403, 500):
                raise requests.exceptions.ConnectionError(
                    'Status is not ok. {0}'.format(raw_text)
                )

            got_json = return_status.json()
            if status_code in (400,403,500):
                # If a 400, 403, or 500, then something went wrong, so raise it.
                raise requests.exceptions.ConnectionError(
                    'Error in response. {0}: {1}'.format(
                        got_json['response'],
                        got_json['message']
                    )
                )

            people_url = got_json['data']['people-url']

            # Clear the KV store of presence information
            self.log('Clear phone presence-engine in key store')

            presence=self.controller.clear_value('presence-engine')
            user=self.controller.set_value('presence-user')
            people_url=self.controller.set_value('presence-people-url')

            self.set_return_schema()
            data=self.return_schema
        except requests.exceptions.ConnectionError as rce:
            message = 'Connection Error >> {0}'.format(str(rce))
            status = status_code
            success = 'error'
            self.log(message)
        except KeyError as ke:
            success = 'error'
            status = 400
            message = 'Key Error >> {0}'.format(str(ke))
            self.log(message)
        except ValueError as ke:
            success = 'error'
            status = 404
            message = 'Value Error >> {0}'.format(str(ke))
            self.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = 'EXCEPTION >> {0}'.format(repr(e))
            self.log(message)
            print(message)

        self.log('Presence engine set to: {0}'.format(data))

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)

        return return_value



