################################################################################
#
# Program:      v4_00_Config_AutoSense_Control.py
# Class:        v4_00_Config_AutoSense_Control
# Objects:      Config_AutoSense_Control
# Component of: Phone
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: The configuration controller for autosense.
#
################################################################################
#
# Import required modules and packages
#
from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Phone import Control
import datetime, time, json, requests

class v4_00_Config_AutoSense_Control(object):
    controller = None
    return_schema = {
        "auto-sense":True
    }

    # Constructor
    def __init__(self):
        self.controller = Control.global_controller
        self.module_name = 'AutoSense'
        self.method_name = 'unknown'


    # Override log to include more detail.
    def log(self, log_message):
        self.controller.log(
            '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        str(log_message))
        )
        

    # Make sure the JSON is valid.
    def _validate_json_string(
        self,
        json_string=None
    ):
        # Validate the raw data is valid
        self.log('Validating JSON')

        if not type(json_string) == str:
            raise KeyError('JSON data was not provided as '+\
                           'a string')

        self.log('JSON data is a string.')

        if json_string in (None, ''):
            raise KeyError('No JSON data was provided, so '+\
                           'there is no object to load')

        self.log('JSON string is not empty.')
        
        return json.loads(json_string)


    # Validate the key. To become a base_lib helper in future.
    def _validate_key(
        self,
        json_data=None
    ):
        key=json_data['key']

        # Validate the key
        if (not type(key) == str) \
        or (not key == '1234-5678-9012-3456'): # Change to correct key!
            raise ValueError('Key is incorrectly formed or incorrect')

        self.log('Key was correct.')

        return key


    # Update the return schema
    def set_return_schema(
        self,
        value=None
    ):
        self.return_schema['auto-sense']=value


    # Check if autosense is on.
    def get_autosense(
        self, 
        state=None
    ):
        try:
            self.method_name = 'get_autosense'
            success = 'success'
            status = '200'
            message = 'Phone auto sensitivity status'
            data = None

            # get_autosense is in the global controller because it is used
            # in many different objects.
            self.set_return_schema(self.controller.get_autosense())

            data=self.return_schema
        except Exception as e:
            success = 'error'
            status = '500'
            message = 'EXCEPTION >> {0}'.format(repr(e))
            self.log(message)
            print(message)

        self.log('Phone auto sensitivity setting reported as: "{0}"'\
            .format(data)
        )

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)

        return return_value



    def set_autosense(
        self, 
        json_string=None
    ):
        try:
            self.method_name = 'set_autosense'
            success = 'success'
            status = '200'
            message = 'Set phone auto sensitivity to on'
            data = None
            
            json_data = self._validate_json_string(json_string)

            validating_key=True
            self._validate_key(json_data)
            validating_key=False

            self.log('Set phone to auto sensitivity on.')

            # Update the KV store.
            autosense=self.controller.set_value('autosense', True)

            self.set_return_schema(autosense)
            data=self.return_schema
        except KeyError as ke:
            if validating_key:
                message = 'The control key is either incorrect or missing.'
                status = 403
            else:
                message = 'Key Error >> {0}'.format(str(ke))
                status = 400
            success = 'error'
            self.log(message)
        except ValueError as ke:
            success = 'error'
            status = 400
            message = 'Value Error >> {0}'.format(str(ke))
            self.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = 'EXCEPTION >> {0}'.format(repr(e))
            self.log(message)
            print(message)

        self.log('Auto sensitivity state set: {0}'.format(data))

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)

        return return_value


    def clear_autosense(
        self, 
        json_string=None
    ):
        try:
            self.method_name = 'clear_autosense'
            success = 'success'
            status = '200'
            message = 'Turn phone auto sensitivity off.'
            data = None
            
            json_data = self._validate_json_string(json_string)

            validating_key=True
            self._validate_key(json_data)
            validating_key=False

            self.log('Set phone auto sensitivity to off.')

            # Update the KV store.
            autosense=self.controller.set_value('autosense', False)

            self.set_return_schema(autosense)
            data=self.return_schema
        except KeyError as ke:
            success = 'error'
            status = 400
            message = 'Key Error >> {0}'.format(str(ke))
            self.log(message)
        except ValueError as ke:
            success = 'error'
            status = 500
            message = 'Value Error >> {0}'.format(str(ke))
            self.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = 'EXCEPTION >> {0}'.format(repr(e))
            self.log(message)
            print(message)

        self.log('Show on lock state set: {0}'.format(data))

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)

        return return_value



