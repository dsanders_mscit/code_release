################################################################################
# NOT USED - FOR FUTURE WORK                                                   #
################################################################################
import json, requests

class v1_00_Config_Door_Bell_Control(object):
    controller = None

    def __init__(self, control=None, module=None):
        if control == None:
            raise Exception('Control was not passed. Config_Door_Bell cannot '+\
                            'initiate!')
        self.controller = control
        self.module_name = module or 'Module'
        self.method_name = 'unknown'


    def get_door_bell(self):
        try:
            self.method_name = 'get_context'
            success = 'success'
            status = '200'
            message = 'Context Engine'
            data = None

            self.controller.log('{0}-{1}: Get Context request received.'\
                .format(self.module_name,
                        self.method_name)
            )
            context = self.controller.get_value('context-engine')
            context_key = self.controller.get_value('context-engine-key')
            context_url = self.controller.get_value('context-engine-url')
            context_query = self.controller.get_value('context-query')
            context_state = self.controller.get_value('context-state')
            if context in ([], '', None):
                context = None

            data = {
                'context-engine':context,
                'key':context_key,
                'context-url':context_url,
                'context-query':context_query,
                'context-state':context_state
            }
        except Exception as e:
            success = 'error'
            status = '500'
            message = '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        repr(e))
            data = {"exception":repr(e)}
            self.controller.log(message)
            print(message)

        self.controller.log('{0}-{1}: Context data set: {2}'\
            .format(self.module_name,
                    self.method_name,
                    data)
        )

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)

        return return_value

    # UPDATED - 02 April 2016
    #           Remove context engine.
    #
    def clear_door_bell(self, json_string=None):
        success = 'success'
        status = '200'
        message = 'Central logging status.'
        data = None

        try:
            self.method_name = 'remove_context'
            self.controller.log('{0}-{1}: Remove request received.'\
                .format(self.module_name,
                        self.method_name)
            )

            context = self.controller.get_value('context-engine')
            self.controller.log('{0}-{1}: Current context set to {2}'\
                .format(self.module_name,
                        self.method_name,
                        context)
            )
            if context in (None, '', []):
                raise ValueError('The service is not subscribed to a '+\
                                 'context service, so cannot be removed.')

            self.controller.log('{0}-{1}: Clearing context.'\
                .format(self.module_name,
                        self.method_name)
            )

            self.controller.clear_value('context-engine')
            self.controller.clear_value('context-engine-key')
            self.controller.clear_value('context-engine-url')
            self.controller.clear_value('context-query')
            self.controller.clear_value('context-state')

            data = {
                'context-engine':None,
                'key':None,
                'context-url':None,
                'context-query':None,
                'context-state':None
            }
        except KeyError as ke:
            success = 'error'
            status = 400
            message = '{0}-{1}: Key Error >> {2}'\
                .format(self.module_name, self.method_name, str(ke))
            self.controller.log(message)
        except ValueError as ve:
            success = 'error'
            status = 400
            message = '{0}-{1}: {2}'\
                .format(self.module_name, self.method_name, str(ve))
            self.controller.log(message)
        except Exception as e:
            success = 'error'
            status = 500
            message = '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        repr(e))
            self.controller.log(message)
            print(message)

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        self.controller.log('{0}-{1}: Context data set: {2}'\
            .format(self.module_name,
                    self.method_name,
                    data)
        )

        return return_value


    def set_door_bell(self, json_string=None):
        success = 'success'
        status = '200'
        message = 'Central logging status.'
        data = None

        try:
            self.method_name = 'set_door_bell'
            self.controller.log('{0}-{1}: Set door bell request received.'\
                .format(self.module_name,
                        self.method_name)
            )

            if json_string == None\
            or json_string == '':
                raise KeyError('Badly formed request!')

            self.controller.log('{0}-{1}: Validating JSON.'\
                .format(self.module_name,
                        self.method_name)
            )

            json_data = json.loads(json_string)
            door_bell = json_data['door-bell']

            self.controller.log('{0}-{1}: Validating door bell starts http://.'\
                .format(self.module_name,
                        self.method_name)
            )
            if not door_bell[0:7] == 'http://':
                raise KeyError('Door bell must begin http:// and point to a '+\
                               'valid door bell URL')

            ip_addr = self.controller.get_value('ip_addr')
            server_name = self.controller.get_value('server_name')
            port_number = self.controller.get_value('port_number')
            version = self.controller.get_value('version')

            call_back='http://{0}:{1}/{2}/callback'\
                .format(
                    self.controller.get_value('ip_addr'),
                    self.controller.get_value('port_number'),
                    self.controller.get_value('version')
                )
            call_back_key = self.controller.get_value('callback-key')
            if call_back_key in ('',[],None):
                call_back_key = '1234-5678-9012-3456' # Fallback to default

            door_bell_url='{0}/{1}-{2}'\
                .format(
                    door_bell,
                    server_name,
                    port_number
                )

            door_bell_info = self.validate_door_bell(
                door_bell=door_bell,
                door_bell_url=door_bell_url,
                call_back=call_back,
                call_back_key=call_back_key
            )

            if not door_bell_info in ('',[], None):
                key=context_info['key']
                query=context_info['query']
                state=context_info['state']

            self.controller.set_value('context-engine', context)
            self.controller.set_value('context-engine-key', key)
            self.controller.set_value('context-engine-url', context_url)
            self.controller.set_value('context-query', query)
            self.controller.set_value('context-state', state)
            data = {
                      'context-engine':context,
                      'key': key,
                      'context-url':context_url,
                      'context-query':query,
                      'context-state':state
                    }
        except requests.exceptions.ConnectionError as rce:
            success = 'error'
            status = '500'
            message = '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        str(rce))
            self.controller.log(message)
        except KeyError as ke:
            success = 'error'
            status = 400
            message = '{0}-{1}: Key Error >> {2}'\
                .format(self.module_name, self.method_name, str(ke))
            data = {'error-message':str(ke)}
            self.controller.log(message)
        except ValueError as ve:
            success = 'error'
            status = 403
            message = '{0}-{1}: {2}'\
                .format(self.module_name, self.method_name, str(ve))
            data = {'error-message':str(ve)}
            if loading_json:
                status = 400
                message = '{0}-{1}: {2}'\
                    .format(self.module_name,
                            self.method_name,
                            'The JSON data is badly formed. Please check')
                data = {'error-message':'Bad JSON data'}
            self.controller.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        repr(e))
            data = {"exception":repr(e)}
            self.controller.log(message)
            print(message)

        self.controller.log('{0}-{1}: Context data set: {2}'\
            .format(self.module_name,
                    self.method_name,
                    data)
        )

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value


    # Added - 02 April 2016
    #         Validates context engine can actually be reached.
    # Updated - 03 April 2016
    #           Simplified
    def validate_door_bell(
        self,
        context=None,
        context_url=None,
        call_back=None,
        call_back_key=None
    ):
        validity = None
        key = None
        query = None
        state = None

        try:
            self.controller.log('{0}-{1}: '\
                                   .format(self.module_name,
                                           self.method_name)+\
                                'Validate request received.'
            )

            if context in ([], '', None) \
            or context_url in ([], '', None) \
            or call_back in ([], '', None):
                raise ValueError('Context engine is not set.')

            payload_data={
                "call-back":call_back,
                "call-back-key":call_back_key
            }

            self.controller.log('{0}-{1}: '\
                                   .format(self.module_name,
                                           self.method_name)+\
                                'Issuing get request to {0} '.format(context)+\
                                'with payload "{0}"'.format(payload_data)
            )

            request_response = requests.post(
                context_url,
                data=json.dumps(payload_data),
                timeout=30
            )

            response_raw = request_response.text
            response_status = request_response.status_code

            self.controller.log('{0}-{1}: '\
                                   .format(self.module_name,
                                           self.method_name)+\
                                'Request returned "{0}"'\
                                   .format(response_raw)
            )

            if response_status not in (200,201,400):
                raise requests.exceptions.ConnectionError(
                    'Unable to communicate with context engine. Status '+\
                    'returned was {0}. '.format(response_status)+\
                    'Raw data was {0}.'.format(response_raw)
                )

            response_data = json.loads(response_raw)

            if 'response' in response_data:
                self.controller.log('{0}-{1}: Response is in data.'\
                    .format(self.module_name,
                            self.method_name)
                )
                if not response_data['response'].upper() == 'SUCCESS':
                    raise requests.exceptions.ConnectionError(
                        'Unable to communicate with context engine. Status '+\
                        'returned was {0}'.format(response_data['message'])
                    )
                self.controller.log('{0}-{1}: Response is success.'\
                    .format(self.module_name,
                            self.method_name)
                )
                if not 'data' in response_data:
                    raise KeyError(
                        'No data was returned!'+\
                        'Response was {0}'.format(response_raw)
                    )
                self.controller.log('{0}-{1}: [data] is in Response.'\
                    .format(self.module_name,
                            self.method_name)
                )
                if not 'subscription-key' in response_data['data']:
                    raise KeyError(
                        'No subscription-key was returned!'+\
                        'Response was {0}'.format(response_raw)
                    )
                self.controller.log('{0}-{1}: [subscription-key] in Response.'\
                    .format(self.module_name,
                            self.method_name)
                )
                key = response_data['data']['subscription-key']
                query = response_data['data']['query-engine']
                state = response_data['data']['state-change']
            else:
                raise requests.exceptions.ConnectionError(
                    'Issue communicating with context engine!'+\
                    'Response was {0}'.format(response_raw)
                )
        except KeyError as ke:
            raise
        except ValueError as ve:
            raise
        except requests.exceptions.ConnectionError as rce:
            raise
        except Exception as e:
            raise
            print(message)

        self.controller.log('{0}-{1}: Context engine returned key: {2}'\
            .format(self.module_name,
                    self.method_name,
                    key)
        )

        return {'key':key, 'query':query, 'state':state}


