################################################################################
#
# Program:      Config_State_Boundary.py
# Class:        Config_State_Boundary
# Objects:      Config_State_Boundary
# Component of: Phone
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Configuration boundary for controlling states. Supports GET, POST,
#          and DELETE to query, set, or clear a state.
#
################################################################################

from flask_restful import Resource, reqparse
from Phone_Config_Control.Config_State_Control import state_control_object

class Config_State_Boundary(Resource):
    def get(self, state=None):
        return_state = state_control_object.get_state(state=state)
        return return_state


    def post(self, state=None):
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        return_state = state_control_object\
                           .set_state(state=state, json_string=raw_data)
        return return_state


    def delete(self, state=None):
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        return_state = state_control_object\
                           .clear_state(state=state, json_string=raw_data)
        return return_state

