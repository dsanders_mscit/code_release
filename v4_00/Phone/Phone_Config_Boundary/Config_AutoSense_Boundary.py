################################################################################
#
# Program:      Config_AutoSense_Boundary.py
# Class:        Config_AutoSense_Boundary
# Objects:      Config_AutoSense_Boundary
# Component of: Phone
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Configuration boundary for emulating an autosense profanity checker.
#          Supports GET, POST, and DELETE. Get -> is autosense on? POST -> set
#          autosense on. DELETE -> set autosense off.
#
################################################################################

from flask_restful import Resource, reqparse
from Phone_Config_Control.Config_AutoSense_Control \
    import autosense_control_object

class Config_AutoSense_Boundary(Resource):
    def get(self, state=None):
        return_state = autosense_control_object.get_autosense()
        return return_state


    def post(self, state=None):
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        return_state = autosense_control_object\
                           .set_autosense(json_string=raw_data)
        return return_state


    def delete(self, state=None):
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        return_state = autosense_control_object\
                           .clear_autosense(json_string=raw_data)
        return return_state

