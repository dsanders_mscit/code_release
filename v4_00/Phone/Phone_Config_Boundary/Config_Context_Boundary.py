################################################################################
#
# Program:      Config_Context_Boundary.py
# Class:        Config_Context_Boundary
# Objects:      Config_Context_Boundary
# Component of: Phone
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Configuration boundary for controlling context engine connections.
#          Supports GET, PUT, and DELETE. Get -> is context on? PUT -> set
#          context on. DELETE -> set context off.
#
################################################################################

from flask_restful import Resource, reqparse
from Phone.Control import global_controller
#from Phone_Config_Control.Config_Context_Control import context_control_object

class Config_Context_Boundary(Resource):
    def get(self):
        return_state = global_controller.context_control_object\
                            .get_context_engine()
        return return_state


    def put(self):
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        return_state = global_controller.context_control_object\
                            .set_context_engine(json_string=raw_data)
        return return_state


    def delete(self):
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        return_state = global_controller.context_control_object\
                            .clear_context_engine(json_string=raw_data)
        return return_state

