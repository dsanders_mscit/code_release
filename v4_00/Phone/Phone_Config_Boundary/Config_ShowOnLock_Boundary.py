################################################################################
#
# Program:      Config_ShowOnLock_Boundary.py
# Class:        Config_ShowOnLock_Boundary
# Objects:      Config_ShowOnLock_Boundary
# Component of: Phone
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Configuration boundary for controlling display of notifications on
#          lock screens. Supports GET, POST, and DELETE. Get -> are 
#          notifications shown on lock? POST -> set notifications to show when
#          locked. DELETE -> set notifications to NOT show when locked.
#
################################################################################

from flask_restful import Resource, reqparse
from Phone_Config_Control.Config_ShowOnLock_Control import showonlock_control_object

class Config_ShowOnLock_Boundary(Resource):
    def get(self):
        return_state = showonlock_control_object.get_show_on_lock()
        return return_state


    def post(self):
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        return_state = showonlock_control_object\
                           .set_show_on_lock(json_string=raw_data)
        return return_state


    def delete(self):
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        return_state = showonlock_control_object\
                           .clear_show_on_lock(json_string=raw_data)
        return return_state

