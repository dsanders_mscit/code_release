################################################################################
#
# Program:      Config_Presence_Boundary.py
# Class:        Config_Presence_Boundary
# Objects:      Config_Presence_Boundary
# Component of: Phone
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Configuration boundary for controlling presence engine connections.
#          Supports GET, POST, and DELETE. Get -> is presence on? POST -> set
#          presence on. DELETE -> set presence off.
#
################################################################################

from flask_restful import Resource, reqparse
from Phone_Config_Control.Config_Presence_Control \
    import presence_control_object

class Config_Presence_Boundary(Resource):
    def get(self):
        return_state = presence_control_object.get_presence()
        return return_state


    def post(self):
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        return_state = presence_control_object\
                           .set_presence(json_string=raw_data)
        return return_state


    def delete(self):
        return_state = presence_control_object.clear_presence()
        return return_state

