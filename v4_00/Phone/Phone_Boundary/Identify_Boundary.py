################################################################################
#
# Program:      Identify_Boundary.py
# Class:        Identify_Boundary
# Objects:      Identify_Boundary
# Component of: Phone
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: DO NOT USE - DEPRECATED
#
################################################################################
from flask_restful import Resource, reqparse
from Phone.Control import global_controller

class Identify_Boundary(Resource):
    def get(self):
        return global_controller.identify_control_object.identify_me()
################################################################################
#
# Purpose: DO NOT USE - DEPRECATED
#
################################################################################



