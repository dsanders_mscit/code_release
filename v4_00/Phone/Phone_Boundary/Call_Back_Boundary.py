################################################################################
#
# Program:      Call_Back_Boundary.py
# Class:        Call_Back_Boundary
# Objects:      Call_Back_Boundary
# Component of: Phone
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Call Back boundary handles requests to the phone to put a call back
#          in. Supports PUT only.
#
################################################################################

from flask_restful import Resource, reqparse
from Phone.Control import global_controller
from Phone.Call_Back_Control import global_call_back

class Call_Back_Boundary(Resource):
    def put(self):
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        try:
            return_value = global_call_back.answer(json_string=raw_data)
        except Exception as e:
            print(repr(e))
        return return_value



