################################################################################
#
# Program:      main.py
# Class:        None
# Objects:      None
# Component of: Phone
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Main program. Modified to add identify (NOT USED) and callback
#          boundaries.
#
################################################################################
from Phone import app, api
from Phone.Control import global_controller
from Phone_Boundary.Location_Boundary import Location_Boundary
from Phone_Boundary.Notification_Boundary import Notification_Boundary

#
# Get the version of the API
#
version = global_controller.get_value('version')

api.add_resource(Location_Boundary, '/{0}/location'.format(version))
api.add_resource(Notification_Boundary, '/{0}/notification'.format(version))


#
# v4_00 CHANGE
#
from Phone_Boundary.Identify_Boundary import Identify_Boundary
api.add_resource(Identify_Boundary, '/{0}/identify'.format(version))

from Phone_Boundary.Call_Back_Boundary import Call_Back_Boundary
api.add_resource(Call_Back_Boundary, '/{0}/callback'.format(version))
#
# END v4_00 CHANGE
#


# Reference: Adamo, D. [2015], 'Handling CORS Requests in Flask(-RESTful) APIs'
# [ONLINE]. Available at: http://www.davidadamojr.com/handling-cors-requests-in-flask-restful-apis/
# (Accessed: 08 March 2016)
@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Authorization, Accept')
    response.headers.add('Access-Control-Allow-Methods',
        'GET,PUT,POST,OPTIONS,HEAD,DELETE')
    return response

