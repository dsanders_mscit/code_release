################################################################################
#
# Dockerfile
# -----------------------------------------------------------------------------
# Component of: Phone
# Version:      4.00
# Last Updated: 06 May 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
# Related Scripts
# -----------------------------------------------------------------------------
# ./build.sh   - Builds the Docker image. This is where the image name is set.
# ./push.sh    - Pushes the Docker image to the Docker hub. Requires credentials
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 06 May 16    | Finalized version for submission.
#
################################################################################
#
# Purpose: build the Docker container image for the phone (version 4.00) that
#          enables the application to run on any platform that supports Docker.
#
################################################################################

#
# Use the base Python image which includes Python3, Flask, and Flask_Restful
# 
FROM dsanderscan/mscit_v3_01_phone
#
# copy the files required for the application
#
COPY Phone/__init__.py /Phone/Phone/
#
# Control
#
COPY Phone/Call_Back_Control.py /Phone/Phone/
COPY Phone/v4_00_Call_Back_Control.py /Phone/Phone
COPY Phone/Call_Back_Processor.py /Phone/Phone/
COPY Phone/v4_00_Call_Back_Processor.py /Phone/Phone
COPY Phone/Control.py /Phone/Phone/
COPY Phone/v4_00_Control.py /Phone/Phone
COPY Phone/Notification_Control.py /Phone/Phone/
COPY Phone/v4_00_Notification_Control.py /Phone/Phone
COPY Phone/Phone_Database.py /Phone/Phone/
COPY Phone/v4_00_Phone_Database.py /Phone/Phone
#
# Boundary
#
COPY Phone_Boundary/main.py /Phone/Phone_Boundary/
COPY Phone_Boundary/Identify_Boundary.py /Phone/Phone_Boundary/
COPY Phone_Boundary/Call_Back_Boundary.py /Phone/Phone_Boundary/
#
# v4_00 Config Boundary
#
COPY Phone_Config_Boundary/main.py /Phone/Phone_Config_Boundary/
COPY Phone_Config_Boundary/Config_Context_Boundary.py /Phone/Phone_Config_Boundary/
COPY Phone_Config_Boundary/Config_State_Boundary.py /Phone/Phone_Config_Boundary/
COPY Phone_Config_Boundary/Config_ShowOnLock_Boundary.py /Phone/Phone_Config_Boundary/
COPY Phone_Config_Boundary/Config_AutoSense_Boundary.py /Phone/Phone_Config_Boundary/
COPY Phone_Config_Boundary/Config_Presence_Boundary.py /Phone/Phone_Config_Boundary/
#
# v4_00 Config Control
#
#COPY Phone_Config_Control/Config_Context_Control.py /Phone/Phone_Config_Control/
#COPY Phone_Config_Control/v4_00_Config_Context_Control.py /Phone/Phone_Config_Control/
COPY Phone_Config_Control/Config_State_Control.py /Phone/Phone_Config_Control/
COPY Phone_Config_Control/v4_00_Config_State_Control.py /Phone/Phone_Config_Control/
COPY Phone_Config_Control/Config_ShowOnLock_Control.py /Phone/Phone_Config_Control/
COPY Phone_Config_Control/v4_00_Config_ShowOnLock_Control.py /Phone/Phone_Config_Control/
COPY Phone_Config_Control/Config_AutoSense_Control.py /Phone/Phone_Config_Control/
COPY Phone_Config_Control/v4_00_Config_AutoSense_Control.py /Phone/Phone_Config_Control/
COPY Phone_Config_Control/Config_Presence_Control.py /Phone/Phone_Config_Control/
COPY Phone_Config_Control/v4_00_Config_Presence_Control.py /Phone/Phone_Config_Control/

