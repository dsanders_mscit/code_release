################################################################################
#
# Program:      v4_00_Control.py
# Class:        v4_00_Control
# Objects:      Control
# Component of: Phone
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Global control. Updated for re-factoring, context control, key
#          generation and urgency/sensitivity flags.
#
################################################################################

# Import base library modules - From Bluetooth symbolic link to /base_lib
import base_lib

# Import context config
from base_lib.Config_Context import Config_Context

# Import Identify Control
from base_lib.Identify_Control import Identify_Control

# Import Key generator
from base_lib.Key import Key

# Import context exception
from base_lib.Context_Bad_Exception import Context_Bad_Exception

# Import flask modules 
from flask import Response

# Import standard packages
import datetime, time, json, os, redis, requests

# Import the version 3_01 class
from Phone.v3_01_Control import v3_01_Control

class v4_00_Control(v3_01_Control):

    # Constructor. Inherits from v3_01.
    def __init__(self):
        super(v3_01_Control, self).__init__()

        #
        # v4_00 Additions
        #
        module='Phone Control'

        # Make sure the device is unlocked
        self.lock_device(False)

        # Instantiate the identify control
        self.identify_control_object = \
            Identify_Control(self, module=module)

        # Instantiate the context control
        self.context_control_object = \
            Config_Context(self, module=module)

        # Instantiate the key generator
        self.key_control_object = Key(self, module=module)

        #
        # Code below generates a random key - not currently used because of
        # easy integration with door bell for testing. In production, the door
        # bell and phone would pair. In the simulation, keep it simple.
        #
        # callback_key=self.key_control_object.generate_key()
        #
        #
        callback_key = '1234-5678-9012-3456'
        self.set_value(
            'callback-key',
            callback_key
        )
        self.log('Generated callback key {0}'.format(callback_key))


    #
    # v4_00 Process Notification. Updated to support urgency and sensitivity.
    # This method performs two key activities: first, it ALWAYS persists the
    # notification; second, it trys to display/broadcast the notification by
    # calling process_context. If that call is successful, process_context will
    # issue the notification AND mark the persisted notification as processed.
    # Doing the calls in this sequence makes sure the notifications are always
    # processed and only processed once.
    #
    def process_notification(
        self,
        sender=None,                # Who sent the notification
        date_string=None,           # When
        notification=None,          # The notification
        action=None,                # The action, e.g. openmap, read, etc.
        urgency=False,              # Urgent or not?
        sensitivity=False           # Sensitve or not?
    ):
        try:
            # Step 1 - Always persist the notification.
            self.persist_notification(
                sender=sender,
                date_string=date_string,
                notification=notification,
                action=action,
                urgency=urgency,
                sensitivity=sensitivity
            )

            # Step 2 - Process it and check the context.
            self.process_context(
                sender=sender,
                date_string=date_string,
                notification=notification,
                action=action,
                urgency=urgency,
                sensitivity=sensitivity
            )

        except Exception as e:
            raise


    #
    # v4_00 get_autosense - used in multiple places so placed in control. Used
    # to decide whether autosense (primarily profanity checking) is enabled as
    # part of context.
    #
    def get_autosense(self):
        self.log('Checking if phone auto sensitivity is switched on.')

        # Get the value from the key value store
        autosense = self.get_value('autosense')

        # Process it
        if autosense in (None, '', []): # Default behaviour
            autosense = True # Default to safe
        elif autosense == 1:
            autosense = True
        elif autosense == 0:
            autosense = False
        else:
            autosense = True # Default to safe

        self.log('Phone auto sensitivity is {0}'.format(autosense))

        # Return it
        return autosense


    #
    # v4_00 Local Context Check. This method performs NO activities but it could
    # if there were a need for local checking; however, all context checking is
    # performed through the central context engine (emulating a service on the
    # phone) as this abstracts context checks from content.
    #
    def local_context_check(self,
                            kvstore=None,
                            devicename=None
    ):
        # Set the defaults
        return_context = True  # Default to an allowed context
        context_message = None
        
        #
        # Currently no local context checking occurs.
        #

        return return_context, context_message


    #
    # v4_00 Check if locked. Updated to take account of user choice to show
    # notifications on lock screen. Defaults to false if not set.
    #
    def check_if_locked(
        self
    ):
        # Set the default states. Notice this defaults to TRUE and context is
        # good if it can't be evaluated. Whether this should be TRUE or FALSE is
        # a matter for debate. It probably would be a phone/user based setting.
        #
        return_context = True  # Default to an allowed context
        context_message = None

        try:
            # Get the lock status. Default to LOCKED if not set.
            lock_status = self.get_lock_status()
            if lock_status in ('',[],None):
                lock_status = 'LOCKED'

            if lock_status.upper() == 'LOCKED':
                # Check if the user has set show on lock to enable notifications
                # to be shown on the lock screen.
                show_on_lock = self.get_value('show-on-lock')

                # Process the value from the KV store to convert Sqlite TRUE and
                # FALSE values from 1 and 0 respectively.
                #
                if show_on_lock in (None, '', []):
                    show_on_lock == False
                elif show_on_lock == 1:
                    show_on_lock == True
                elif show_on_lock == 0:
                    show_on_lock = False
                else:
                    show_on_lock = False

                # Perform the show on lock check. If it fails, raise exception.
                if not show_on_lock:
                    context_message = \
                        'The phone is locked and the user has either not set '+\
                        'the option to show on the lock screen or has left '+\
                        'the setting at its default (off).'
                    raise Context_Bad_Exception(context_message, ['locked'])

            # Passed the context check.
            context_message = 'Context check is good.'

        except Context_Bad_Exception as cbe:
            # The phone is locked and show on lock is not set.
            return_context = False
            context_message=\
                "Context fails. Reason: {0}.  States: {1}"\
                    .format(
                        cbe.get_message(),
                        cbe.get_states()
                    )
            self.log('{0}'.format(context_message))
        except Exception as e:
            # Fail to a safe status of LOCKED. Debatable whether this should be
            # safe or otherwise. Probably would be a user setting.
            #
            return_context = False
            context_message = 'Failed because of exception: {0}'\
                                  .format(repr(e))
            self.log('Context check failed. {0}'.format(context_message))

        return return_context, context_message


    #
    # v4_00 Change - detect number of screens connected (i.e. people viewing).
    # Returning the subscriber count from redis helps emulate detecting other
    # people (not phones - that is in Context). Every screen connected, excl.
    # the default screen, counts as an extra person able to see the screen.
    #
    def write_screen(self, output_line=None):
        # Get the Redis queue
        redis_instance = redis.StrictRedis(**self.redis_queue)

        # Issue the publish
        screen_viewers = redis_instance.publish(
            'output_screen',
            '{0}\n'.format(output_line)
            )

        # Return the number of subscribers
        return screen_viewers


    #
    # v4_00 Persist Notification. Updated to support urgency and sensitivity
    #
    def persist_notification(
        self,
        sender=None,
        date_string=None,
        notification=None,
        action=None,
        urgency=False,
        sensitivity=False
    ):
        # Tell the data layer to persist the notification. A future change 
        # should really see this become more Python-esque with a dictionary 
        # being passed; not a huge difference but more Python style.
        #
        self.phone_db.save_notification(
            sender,
            date_string,
            notification,
            action,
            urgency,
            sensitivity
        )


    #
    # v4_00 process_context. Use central context service. This is a major module
    # with considerable logic. It is highly complex and needs to be read very
    # carefully.
    #
    def process_context(
        self,
        sender=None,
        date_string=None,
        notification=None,
        action=None,
        urgency=False,
        sensitivity=False
    ):
        # Save original notification and sender
        note_issued = notification # Keep the original notification untouched
                                   # and use a local copy in case we need to
                                   # obfuscate it.

        sender_issued = sender     # As above

        # Get details of the Bluetooth device connected, if any. If not, then
        # bluetooth will be equal to None.
        bluetooth = self.get_bluetooth()

        try:
            # Set the defaults. Default to okay. As above, subject to debate,
            # interpretation, and probably preference.
            context_ok = True # default to okay
            context_message = 'No context check performed.'

            # Find out how many people can see the screen. Redis returns the
            # number of subscribers to a publication channel; therefore, sending
            # a blank message (which screens have been told to ignore, see 
            # Phone_Screen), simply counts the number of viewers. In real life,
            # tools can detect irises and how many people are 'close by' to the
            # screen.
            screen_viewers = self.write_screen()
            self.log('People able to see phone screen: {0}.'\
                .format(screen_viewers)
            )

            self.log('Process Notification: Getting states and values.',
                     screen=False)

            # Check if the device is locked. If it is, then it will set the
            # context and that will be handled later, after other checks.
            context_ok, context_message = self.check_if_locked()

            # If people can see the screen OR Bluetooth is connected, perform
            # a central context service check. This only occurs when the
            # notification can be seen or heard by someone else; if not, then
            # we assume that the owner of the phone wants to hear and see all
            # messages without interference. It should be noted that if the
            # context THINKS the user is alone he may not be.
            #
            if screen_viewers > 1 \
            or not bluetooth == None:
                # Locked will be picked up here and context fail EXCEPT when the
                # notification is marked urgent.
                if not context_ok \
                and not urgency:
                    self.log('Process Notification: Context issued '+\
                              'stop because >> {0} '.format(context_message)+\
                             'Notification will be persisted.',
                             screen=False)
                    return

                # Check local context. Doesn't do anything but it could.
                context_ok, context_message = \
                    self.local_context_check(
                        kvstore=self.kvstore,
                        devicename=self.get_value('phonename')
                    )

                # Check central context next
                try:
                    context_ok, context_message=\
                        self.check_central_context(
                            notification,
                            sender,
                            urgency=urgency,
                            sensitivity=sensitivity
                        )
                #
                # Context Bad Exception means the central Context Engine 
                # returned the exception and the phone now needs to handle it 
                # and decide what to do with it.
                #
                except Context_Bad_Exception as cbe:
                    # Log that a CBE occurred.
                    self.log('Context Bad Exception: {0}'.format(cbe))
                    self.log(
                        'Context Bad Exception: Checking sensitivty, urgency '+\
                        'and who is with phone user.'
                    )

                    # Check if the notification is URGENT and SENSITIVE
                    if urgency == True\
                    and sensitivity == True:
                        # Notification says urgent and sensitive. Obfuscate it
                        # and ask user if want to read it if they are with
                        # a certain class of person.
                        if 'with_stranger' in cbe.context_states \
                        or 'with_friend' in cbe.context_states \
                        or 'with_colleague' in cbe.context_states \
                        or 'with_friend-of-friend' in cbe.context_states:
                            self.log(
                                'Context Bad Exception: Message is marked '+\
                                'urgent and sensitive; because strangers, '+\
                                'colleagues, children, or friends of a '+\
                                'friend are present, '+\
                                'the notification will be obfuscated before '+\
                                'issuing.'
                            )
                            note_issued = 'You have an urgent notification. '+\
                                          'Want to read it?'
                            sender_issued = 'Phone'
                        elif 'with_family' in cbe.context_states \
                        or 'with_child' in cbe.context_states \
                        or 'with_spouse' in cbe.context_states\
                        or 'with_partner' in cbe.context_states:
                            pass # It's okay to issue the message
                        else:    # We don't know who is present.
                            context_ok = False
                            context_message = ''+\
                                'There are other people present and their '+\
                                'relationship cannot be determined.'
                    elif sensitivity == True:
                        # The notification is not urgent, but it is sensitive
                        #
                        # Check states for strangers, colleagues, and friend
                        # of a friend. The order of if statements ensures lowest
                        # common denominator - if a stranger and spouse are
                        # present, the stranger's presence will take precedent.
                        #
                        context_message = 'Sensitive message received '+\
                                          'while with other people who are '
                        if 'marked-sensitive-app' in cbe.context_states:
                            # The app (e.g. monitor app) was marked by the user
                            # as a sensitive app, never suitable for broadcast
                            # if multiple people are present.
                            context_ok = False
                            context_message = cbe.context_message
                        elif 'with_stranger' in cbe.context_states \
                        or 'with_colleague' in cbe.context_states \
                        or 'with_friend-of-friend' in cbe.context_states:
                            context_ok = False
                            context_message += 'strangers, colleagues, or '+\
                                               'friends of a friend.'
                        elif 'with_friend' in cbe.context_states:
                            context_ok = False
                            context_message += 'friends'
                        elif 'with_child' in cbe.context_states:
                            context_ok = False
                            context_message += 'juveniles'
                        elif 'with_family' in cbe.context_states:
                            context_ok = False
                            context_message += 'family'
                        elif 'with_spouse' in cbe.context_states\
                        or 'with_partner' in cbe.context_states:
                            context_ok = True
                            context_message = 'Passed'
                        else:
                            # ANY other state causes a bad context. There could
                            # be other states like 'busy' or 'driving'. The 
                            # phone would need to decide how to handle them.
                            context_ok = False
                            context_message = cbe.context_message
                        context_message += '.'
                    elif 'profanity' in cbe.context_states:
                        # The notification was neither urgent nor sensitive BUT
                        # it has been found to contain profanities. Decide if
                        # the notification should be raised based on who the
                        # person is with.
                        #
                        # NOTE: There is a flaw in the logic in this code. The
                        # context engine currently returns immediately without
                        # evaluating states IF the message is profane or comes
                        # from a specific app (see below). This means the code
                        # below will never evaluate TRUE. In the next version,
                        # context should be changed to add states to the 
                        # profanity processing.
                        #
                        if 'with_stranger' in cbe.context_states \
                        or 'with_colleague' in cbe.context_states \
                        or 'with_child' in cbe.context_states \
                        or 'with_friend-of-friend' in cbe.context_states:
                            self.log(
                                'Context Bad Exception: Message is profane '+\
                                '; because children, strangers, '+\
                                'colleagues, or friends of a friend present, '+\
                                'the notification will not be issued.'
                            )
                            context_ok = False
                            context_message = 'Message received with '+\
                                              'profanities while with other '+\
                                              'people who are strangers, '+\
                                              'colleagues, friends of a '+\
                                              'friend, or friend present.'
                        elif 'with_friend' in cbe.context_states \
                        or 'with_family' in cbe.context_states \
                        or 'with_spouse' in cbe.context_states\
                        or 'with_partner' in cbe.context_states:
                            pass # It's okay to issue the message
                        else:    # We don't know who is present.
                            #
                            # The ELSE will always be the code branch in the
                            # current form of the Context engine. Bug in Context
                            # prevents any states from being recorded IF the
                            # notification is profane. See above.
                            #
                            context_ok = False
                            context_message = ''+\
                                'There are other people present and their '+\
                                'relationship cannot be determined. So the '+\
                                'profane message fails context.'
                except KeyError as ke:
                    # KeyError should NOT occur. If it did, then something has
                    # gone wrong and we default to True - the context becomes
                    # good.
                    #
                    context_ok = True         # Default to True
                    context_message = None    # Default to True
                except Exception:
                    # General exceptions shouldn't occur; if they do raise them
                    #
                    raise # Handle any real exceptions.
                    
                self.log('Central context returned: {0} {1}'\
                    .format(context_ok, context_message))

            # If the context was bad, then log the reason and raise a CBE so 
            # that the context can be logged.
            if not context_ok:
                raise Context_Bad_Exception(
                    'Process Notification: Context issued '+\
                    'stop because >> {0} '.format(context_message)+\
                    'Notification will be persisted.'
                )

            # output_device is used in the Bluetooth issue code. See v3_01 for
            # further details. It looks like it doesn't do anything, but I think
            # it does :)
            #
            output_device = self.get_value('output_device')

            # Log states and values
            self.log('Process Notification: Bluetooth = {0}.'\
                         .format(bluetooth),
                     screen=False)

            self.log('Process Notification: Output_Device = {0}.'\
                         .format(output_device),
                     screen=False)

            self.log('Process Notification: Displaying notification',
                     screen=False)

            # Context is good - Display the notification
            self.display_notification(
                sender=sender_issued,
                date_string=date_string,
                notification=note_issued,
                action=action
            )

            self.log('Process Notification: Checking for Bluetooth.',
                     screen=False)

            # Context is good - Broadcast the notification if conencted to
            # Bluetooth.
            #
            if not bluetooth in ('',[],None):
                self.issue_bluetooth(
                    notification=note_issued,
                    bluetooth=bluetooth
                )

            self.log('Process Notification: Updating notification to read.',
                     screen=False)

            # Mark the notification as processed so it doesn't get processed
            # again. The persistance layer handles the update - it could delete
            # or mark it, the phone doesn't care.
            #
            self.phone_db.update_notification(
                sender=sender,
                date_string=date_string,
                notification=notification,
                action=action,
            )
        except Context_Bad_Exception as cbe:
            self.log('Process Notification: Context issued '+\
                      'stop because >> {0} '.format(cbe.context_message)+\
                     'Notification will be persisted.',
                     screen=False)
        except Exception as e:
            raise


    #
    # v4_00 check_central_context prepares the http request, the payload data,
    # and handles the return of the response. It catches exceptions but passes
    # them directly back to the caller.
    #
    def check_central_context(
        self,
        notification=None,
        sender='unknown',
        urgency=False,
        sensitivity=False
    ):
        # Set defaults.
        return_state = False
        return_message = None

        # Find out if autosense is on and convert the values from the key-value
        # store.
        #
        autosense=self.get_value('autosense')
        if autosense in (None, [], ''): # Default
            autosense=True
        elif autosense == 1:
            autosense=True
        elif autosense == 0:
            autosense=False
        else:
            autosense=True

        # Perform the query
        try:
            self.log('Fetching central context settings.')

            # Get the context information from the KV store.

            # The context engine is the URL to the context engine.
            context_engine = self.get_value('context-engine')

            # The context query is the URL to the query interface for the 
            # context engine
            context_query = self.get_value('context-query')

            # The key for the context engine was passed when the phone 
            # registered with the context engine. It needs this key to ask the
            # context engine to perform the check.
            context_key = self.get_value('context-engine-key')

            # Now we check that the context engine is actually defined. If it's
            # not, then raise an exception.
            if context_engine == None:
                raise KeyError('Context engine is not set. No central context.')

            self.log('Context engine is {0}'.format(context_engine))

            # Form a data payload with the information need to pass to the
            # context engine; self-explanatory.
            #
            payload = {
                "key":context_key, 
                "notification":notification,
                "sender":sender,
                "urgency":urgency,
                "sensitivity":sensitivity,
                "auto-sense":autosense
            }

            self.log('Issuing request to {0} with payload "{1}"'\
                .format(context_query, payload)
            )

            # Issue the http request using a POST method.
            response_returned = requests.post(
                context_query,
                data=json.dumps(payload),
                timeout=15
            )

            # Get the raw status code and returned text.
            response_status = response_returned.status_code
            response_raw = response_returned.text

            # IF the response was good (i.e. a 200 status)
            if response_status == 200:
                # Convert the response form JSON to a dict
                json_response = response_returned.json()

                # Check if there are context states, if there aren't then it
                # will be an empty dictionary. If there are states, then an
                # exception will be raised which the caller can deal with.
                if json_response['data']['context-states'] != []:
                    raise Context_Bad_Exception(
                        json_response['data']['context-message'],
                        json_response['data']['context-states']
                    )

                # If there were no states, then context is good.
                return_state = json_response['data']['context-okay']
                return_message = 'Message: {0}.  States: {1}'\
                    .format(json_response['data']['context-message'],
                            json_response['data']['context-states'])
            else:
                # If we're here, then something went wrong. Perhaps a bad http
                # URL, the key is wrong, or the server is down, etc.
                self.log('*** CONTEXT RESPONSE: {0}'.format(response_status))
                return_message = 'Central context error: {0}'\
                    .format(response_raw)
                return_state = True

        except Context_Bad_Exception:
            raise
        except KeyError:
            raise
        except ValueError:
            raise
        except Exception as e:
            raise


        self.log('{0}'.format(return_message))

        return return_state, return_message


