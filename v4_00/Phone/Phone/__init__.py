################################################################################
#
# Program:      __init__.py
# Class:        None
# Objects:      None
# Component of: Phone
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Initialization package for Python. Updated to include call back as a
#          background process. NOTE. __init__.py REPLACES not INHERIT the v3_00
#          as this is a program NOT a class.
#
# References
# ----------
# Ronacher, A., 2013, 'Larger Applications' [ONLINE]. Available at: 
# http://flask.pocoo.org/docs/0.10/patterns/packages/ (Accessed: 04 November 
# 2015)
#
################################################################################

# Import the module Flask from the flask package
from flask import Flask

# Import the module Api from the flask_restful package
from flask_restful import Api

# Import werkzueg
from werkzeug import serving

# Import atexit
import atexit

# Import the threading module
import threading

# The app is this application and set when the Python file is run from the
# command line, e.g. python3 /some/folder/notes/runserver.py
app = Flask(__name__)

# Create an Api object inheriting app
api = Api(app)

#
# NEW CODE: v4_00 - Add call back processing
#
from Phone.Control import global_controller as control
from Phone.Call_Back_Processor import Call_Back_Processor
cb_proc = Call_Back_Processor(control)
if not serving.is_running_from_reloader():
    thread_job = threading.Thread(
        target=cb_proc.processor,
        args=()
    )
    thread_job.setDaemon(True)
    thread_job.start()
#
# END NEW CODE
#


# Import the main.py module
import Phone.main

