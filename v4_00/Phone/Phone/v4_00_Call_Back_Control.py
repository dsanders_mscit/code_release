################################################################################
#
# Program:      v4_00_Call_Back_Control.py
# Class:        v4_00_Call_Back_Control
# Objects:      Call_Back_Control
# Component of: Phone
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Call back control. Answers call backs from other models (or calls),
#          responds back with acknowledgement, and then lets the call back
#          processor handle the processing. This approach allows very rapid, low
#          latency responses over HTTP. In the code below, 200 status responses
#          are returned, but they could be 202 calls with a URL to check and/or
#          pick up results when they're ready - that would be good HATEOAS
#          approach.
#
################################################################################

from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Phone import Control
import datetime, time, json, requests, os, redis

class v4_00_Call_Back_Control(object):
    # The default return schema. Not really used but useful for future 
    # operations, for example: ack might be negative if there were too many
    # call backs in process.
    #
    # verb and data simply return what was received.
    #
    return_schema = {
        "ack":None,
        "verb":None,
        "data":{}
    }

    #Constructor.
    def __init__(self):
        self.controller = Control.global_controller
        self.module_name = 'Call-Back'
        self.method_name = 'unknown'


    # Override default logging to include more detail.
    def log(self, log_message):
        self.controller.log(
            '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        str(log_message))
        )


    # Validate the JSON passed in. This is a candidate for inclusion in the
    # base_lib as it is used EVERYWHERE. Not implemented, due to code freeze,
    # D Sanders, 03 May 2016.
    #
    def _validate_json_string(
        self,
        json_string=None
    ):
        # Validate the raw data is valid
        self.log('Validating JSON')

        # If it's not a string, raise an exception.
        if not type(json_string) == str:
            raise KeyError('JSON data was not provided as '+\
                           'a string')

        self.log('JSON data is a string.')

        # If the string is empty, raise an exception.
        if json_string in (None, ''):
            raise KeyError('No JSON data was provided, so '+\
                           'there is no object to load')

        self.log('JSON string is not empty.')
        
        # Return the string as a dictionary.
        return json.loads(json_string)


    # Another candidate for base_lib. Check if the call back key is correct.
    # Really should be in base_lib. Not modified due to code freeze, D Sanders
    # 03 May 2016
    #
    def _validate_key(
        self,
        json_data=None,
        callback_key=None
    ):
        # Get the key. If it doesn't exist, an exception will raise.
        key=json_data['key']

        # If the call back key passed is null, fallback to the default of
        # 1234-5678-9012-3456.
        #
        c_key=callback_key or '1234-5678-9012-3456' # Fallback to default

        # Validate the key
        if (not type(key) == str) \
        or (key == None) \
        or (not key == c_key):
            raise KeyError('Key is incorrectly formed or incorrect')

        self.log('Key was correct.')

        return key


    # Update the return schema. Notice the defaults set the schema attributes to
    # null or empty - this is useful for calling set_return_schema() to clear
    # the return data.
    def set_return_schema(
        self,
        ack=None,
        verb=None,
        data={}
    ):
        self.return_schema['ack']=ack
        self.return_schema['verb']=verb
        self.return_schema['data']=data


    # The method used to answer calls. Takes a call, validates it to a certain
    # level and then publishes the call over Redis where it will be picked up
    # by the call back processor. No call processing is done at this point.
    #
    def answer(self, json_string=None):
        try:
            self.log('Call back answered a call.')
            self.method_name = 'answer'

            success = 'success'
            status = '200'
            message = 'Context state'
            data = None
            validating_key = False

            # Get the key used for call backs.
            callback_key = self.controller.get_value('callback-key')

            # Validate the JSON
            json_data = self._validate_json_string(json_string)

            # Validate the Key
            validating_key=True
            key = self._validate_key(json_data, callback_key)
            validating_key=False

            # Verb and data are optional. There could be a call back with no
            # verb or no data. They are surrounded in try...except blocks so 
            # that key errors can be caught and defaults substituted if the
            # values aren't present.
            #
            try:
                verb=json_data['verb']
            except Exception:
                verb=None

            try:
                data=json_data['data']
            except Exception:
                data={}

            # Publish the call on Redis. Note two extra parameters are included
            # in the message - type(verb) (should always be string) and 
            # type(data) (which could be anything - list, dict, etc.). This lets
            # the processor know what the data was originally as below it is
            # type cast to a string.
            #
            redis_instance = redis.StrictRedis(**self.controller.redis_queue)
            redis_instance.publish(
                'callback_processor',
                '{0}<<*>>{1}<<*>>{2}<<*>>{3}'.format(
                    verb,
                    type(verb),     # Should always be string
                    data,
                    type(data)      # Could be date, string, dict, list, etc.
                )
            )

            # Acknowledge the call
            self.set_return_schema('Hello!', verb, data)

            data=self.return_schema
        except KeyError as ke:
            # Either the control key is wrong OR a required JSON field is 
            # missing. Decided by the sentinel validating_key
            if validating_key:
                message = 'The control key is either incorrect or missing.'
                status = 403
            else:
                message = 'Key Error >> {0}'.format(str(ke))
                status = 400
            success = 'error'
            self.log(message)
        except ValueError as ke:
            # Some value error has occurred.
            success = 'error'
            status = 400
            message = 'Value Error >> {0}'.format(str(ke))
            self.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = 'EXCEPTION >> {0}'.format(repr(e))
            self.log(message)
            print(message)

        self.log('Call back answered: {0}'.format(data))

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)

        # This return happens quickly to minimize latency. Status above (^) 
        # could be set to 202 rather than 200 if the call back is going to
        # respond or produce outputs at some point.
        #
        return return_value

