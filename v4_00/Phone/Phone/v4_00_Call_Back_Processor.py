################################################################################
#
# Program:      v4_00_Call_Back_Processor.py
# Class:        v4_00_Call_Back_Processor
# Objects:      Call_Back_Processor
# Component of: Phone
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Call back processor. Processes and actions calls that were received
#          by the call back control and passed here via Redis. This object is
#          instantiated in __init__.py and the method processor runs as a
#          background process.
#
################################################################################

from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Phone_Config_Control.Config_State_Control import state_control_object

import datetime, time, json, requests, os, redis

class v4_00_Call_Back_Processor(object):
    # Return schema.
    return_schema = {
        "ack":None,
        "verb":None,
        "data":{}
    }

    # Constructor.
    def __init__(self, control=None):
        # There should really be error checking that control is not equal to 
        # None but it wasn't add (an error). Not fixed due to code freeze, D
        # Sanders, 03 May 2016.
        #
        self.controller = control
        self.module_name = 'Call-Back-Processor'
        self.method_name = 'unknown'


    # Override log to include additional detail AND decide whether to log to
    # screen or not.
    def log(self, log_message, screen=False):
        if not screen:
            self.controller.log(
                '{0}-{1}: {2}'\
                    .format(self.module_name,
                            self.method_name,
                            str(log_message))
            )
        else:
            self.controller.log('{0}'.format(str(log_message)),screen)


    # Background process. Loop until terminated waiting for Redis messages on a
    # pubsub queue called callback_processor. When a message is received, it is
    # parsed, compared to known actions, and actions initiated. NOTE, there is
    # NO RETURN PATH to the caller at this point UNLESS the caller provides 
    # their own call back (which is permitted).
    #
    def processor(self):
        # Get the Redis queue information and subscribe to it.
        redis_instance = redis.StrictRedis(**self.controller.redis_queue)
        redis_pubsub = redis_instance.pubsub()
        redis_pubsub.subscribe('callback_processor')
        self.method_name = 'processor'

        # Loop until terminated.
        for message in redis_pubsub.listen():
            # Check if the message really is a message. If it's anything else
            # (someone else subscribing or unsubscribing), ignore it.
            if message['type'].upper() == 'MESSAGE':
                self.log('Redis Processor: Message received!')

                # Split the data received into five fields. Assume the fields
                # are separated by <<*>>. There should be much better error
                # checking here and this needs to be fixed. Not modified due to
                # code freeze, D Sanders, 03 May 2016.
                #
                message_data = message['data']
                message_fields = message['data'].decode('utf-8')\
                    .split('<<*>>', 5)

                try:
                    # Get the verb and data types and put them in variables.
                    verb=message_fields[0].upper()
                    verb_type=message_fields[1]
                    data=message_fields[2]
                    data_type=message_fields[3]

                    # Check the verb:
                    
                    if verb == 'DOORBELL-RINGING':
                        # First condition not used - for the door bell which 
                        # wasn't implemented
                        self.process_doorbell_ringing(data, data_type)
                    elif verb == 'INCOMING-CALL':
                        # Not implemented. Was used for door bell dev. only.
                        self.process_incoming_call(data, data_type)
                    elif verb == 'FYI':
                        # Not implemented. Was used for door bell dev. only.
                        self.process_fyi(data, data_type)
                    elif verb == 'ISNEAR':
                        # A bluetooth service is telling this phone that someone
                        # or something is near by.
                        self.process_proximity('isnear', data, data_type)
                    elif verb == 'NOLONGERNEAR':
                        # A bluetooth service is telling this phone that someone
                        # or something is no longer near by.
                        self.process_proximity('nolongernear', data, data_type)
                except Exception as e:
                    success = 'error'
                    status = '500'
                    message = 'EXCEPTION >> {0}'.format(repr(e))
                    self.log(message)
                    print(message)


    # Process proximity handles calls received that indicate that someone or
    # something is near by. Logic is controlled by the verb (isnear or 
    # nolongernear) - default action taken: isnear
    #
    def process_proximity(
        self,
        verb='isnear',
        data=None,
        data_type=None
    ):
        # Make sure the data type that was passed was a dict. If it isn't then
        # we ignore the call. If it was, then this call tells the phone to 
        # connect to the Context engine (if it is using one) and update its
        # state to someone being near or no longer near.
        #
        if 'dict' in data_type:
            # Use the helper method to validate and convert the data to the 
            # proper JSON format (" not ' and null not None).
            data_dict = self._convert_to_json(data)

            # Get the data needed to connect to the context and presence engines
            context_state = self.controller.get_value('context-state')
            presence_engine = self.controller.get_value('presence-engine')
            people_url=self.controller.get_value('presence-people-url')
            presence_user=self.controller.get_value('presence-user')

            # If either context or presence is unavailable, then the phone 
            # cannot handle the call so ignores it.
            #
            if context_state in ('',[],None) \
            or presence_engine in ('',[],None) \
            or people_url in ('',[],None) \
            or presence_user in ('',[],None):
                self.log(
                    'Call back ignored proximity notification because '+\
                    'neither (or both) context or presence are set.'
                )
            else:
                # Extract from the passed data who is near.
                who_is_near = data_dict['nearby']

                # Form a URL to connect with the presence engine to a resource
                # represented by the variable who_is_near
                people_url = people_url + '/' + who_is_near

                self.log(
                    'Checking relationship of {0} to {1}'\
                        .format(who_is_near, presence_user)
                )

                # Execute an HTTP get request to the presence engine to see if
                # the person known as 'who_is_near' is known to the user. The
                # presence engine should return a relationship (friend, family,
                # ..., etc.) and will return 'stranger' if not known to the
                # user.
                #
                try:
                    return_status = requests.get(
                        people_url,
                        timeout=10
                    )
                    status_code = return_status.status_code
                    raw_text = return_status.text
                except IndexError as ie:
                    status_code = 404
                    raw_text = 'The presence engine stated the user {0}'\
                                .format(presence_user)+' '\
                               'did not exist.'
                except Exception as e:
                    status_code = 500
                    raw_text = 'Exception in callback/people_url.get. '+\
                        'EXCEPTION >>> {0}'.format(repr(e))

                if not status_code in (200, 201, 400, 403, 404, 500):
                    self.log(
                        'Bad status code returned from presence enquiry. '+\
                        'Status {0}, raw text: {1}'\
                            .format(status_code, raw_text)
                    )

                got_json = return_status.json()
                if status_code in (400,403,404,500):
                    self.log(
                        'Error in response. {0}: {1}'.format(
                            got_json['response'],
                            got_json['message']
                        )
                    )

                # Get the relationship from the HTTP request.
                relationship = got_json['data']['relationship']

                self.log('Preparing state change')

                # Set the time to live - by default, this is 1,000 seconds and
                # should probably be more realistic. It is set to this level for
                # test and modelling purposes.
                #
                ttl = 1000

                # Define the payload to pass to the context engine.
                payload = {
                    "key":"1234-5678-9012-3456",
                    "value":who_is_near,
                    "time-to-live":ttl
                }

                state = 'with_{0}_{1}'.format(relationship, who_is_near)
                
                self.log(
                    'Issuing set state command with state {0}, payload {1}'\
                        .format(state, payload)
                )

                if verb.upper() == 'ISNEAR':
                    # If the person is near, SET the state.
                    state_control_object.set_state(state,json.dumps(payload))
                    self.log('Person nearby is {0}'.format(who_is_near))
                    self.log('Relationship is {0}'.format(relationship))
                    self.log(
                        'State set is {0} with time to live of {1}'\
                            .format(state, ttl)
                    )
                else:
                    # If the person is no longer near, CLEAR the state.
                    state_control_object.clear_state(state,json.dumps(payload))
                    self.log(
                        'Person {0} is no longer nearby'.format(who_is_near)
                    )
                    self.log(
                        'State {0} removed.'\
                            .format(state)
                    )



    #
    # CODE BLOCK NOT USED - IGNORE - FOR FUTURE PURPOSES.
    #
    def process_fyi(
        self,
        data=None,
        data_type=None
    ):
        if 'dict' in data_type:
            data_dict = self._convert_to_json(data)
            call_from = data_dict['at-the-door']
            self.log('FYI, {0} is at the door.', screen=True)
            self.log('They have been told you are busy right now.', screen=True)
    #
    # END CODE BLOCK
    #


    #
    # CODE BLOCK NOT USED - IGNORE - FOR FUTURE PURPOSES.
    #
    def process_doorbell_ringing(
        self,
        data=None,
        data_type=None
    ):
        try:
            if 'dict' in data_type:
                self.log('The doorbell is ringing.', screen=True)
                data_dict = self._convert_to_json(data)
                call_for = data_dict['occupant']
                call_back = data_dict['call-back']
                call_back_key = data_dict['call-back-key']
                call_from = data_dict['at-the-door']

                self.log(
                    'The call is for: {0}, from: {1}'\
                        .format(call_for, call_from), 
                    screen=True
                )
                self.log('', screen=True)

                call_back_message="Hi, {0} will be with you shortly"\
                    .format(call_for)
                payload_data={
                    "verb":"incoming-call",
                    "data":
                        {
                         "call-from":call_for, 
                         "message":call_back_message
                        },
                    "key":call_back_key
                }

                self.controller.log(
                    'Responding to {0} stating "{1}".'\
                        .format(call_from, call_back_message),
                    screen=True
                )

                if not call_for == 'None':  # Notice STR comparison!!
                    self.log('Step 4 - Issuing Put request', screen=True)
                    request_response = requests.put(
                        call_back,
                        data=json.dumps(payload_data),
                        timeout=10
                    )
                self.controller.log(request_response.text)
        except Exception as e:
            self.controller.log('EXCEPTION >> {0}'.format(repr(e)))
    #
    # END CODE BLOCK
    #


    #
    # CODE BLOCK NOT USED - IGNORE - FOR FUTURE PURPOSES.
    #
    def process_incoming_call(
        self,
        data=None,
        data_type=None
    ):
        if 'dict' in data_type:
            self.log('Incoming Call.', screen=True)
            self.log(' ', screen=True)
            self.log('Information available is:', screen=True)

            data_dict = self._convert_to_json(data)
            for field in data_dict:
                self.log(
                    '  {0}:{1}'.format(field, data_dict[field]),screen=True
                )
    #
    # END CODE BLOCK
    #

    # Helper method to convert data passed as a string from the call back 
    # control to proper JSON, i.e. changing ' to " and None to null. This is
    # allows the JSON to be load using json.loads()
    #
    def _convert_to_json(
        self,
        data=None
    ):
        if data == None:
            return None

        # Replace ' with "
        modified_data = data.replace("'",'"')

        # Replace None with null
        modified_data = modified_data.replace("None",'null')

        # Load the JSON into a dict
        data_dict = json.loads(modified_data)
        return data_dict

