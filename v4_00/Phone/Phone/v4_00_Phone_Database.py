################################################################################
#
# Program:      v4_00_Phone_Database.py
# Class:        v4_00_Phone_Database
# Objects:      Phone_Database
# Component of: Phone
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Persistence layer. Modified to support sensitivity and urgency.
#
################################################################################

from Phone.v3_01_Phone_Database import v3_01_Phone_Database
import sqlite3, os

class v4_00_Phone_Database(v3_01_Phone_Database):

    # Constructor. Inherits from v3_01
    def __init__(self,
                 controller=None,
                 server_name='localhost',
                 port_number=5000
    ):
        super(v4_00_Phone_Database, self).__init__(controller,
                                                   server_name,
                                                   port_number)

    # Modified to add urgency and sensitivity
    def save_notification(
        self,
        sender=None,
        date_string=None,
        notification=None,
        action=None,
        urgency=False,
        sensitivity=False
    ):
        returned = None
        try:
            self.open_db()
            self.db_exec('insert into notifications '+\
                           ' (sender, date_string, notification, '+\
                           'action, notification_read, urgency, sensitivity) '+\
                           ' values (?,?,?,?, 0, ?,?)',
                           (
                            sender, 
                            date_string, 
                            notification, 
                            action,
                            urgency,
                            sensitivity
                           )
                        )
            self.db_conn.commit()

        except Exception as e:
            raise
        finally:
            self.close_db()

        return True


    # Modified to add urgency and sensitivity
    def get_notifications(
        self
    ):
        returned = []
        try:
            self.open_db()
            self.db_exec('select sender, date_string, notification, '+\
                           'action, urgency, sensitivity '+\
                           'from notifications '+\
                           'where notification_read = 0 '+\
                           'order by date_string')
            returned = self.db_cursor.fetchall()
        except Exception as e:
            raise
        finally:
            self.close_db()

        return returned


    # Modified to add urgency and sensitivity
    def validate_notification_table(self):
        _returned = None

        try:
            self.open_db()
            self.db_exec('select * from notifications')
            self.db_exec('delete from notifications '+\
                           'where notification_read != 0'
                          )
            self.db_conn.commit()
        except sqlite3.OperationalError as oe:
            self.db_cursor.execute(
                    'CREATE TABLE notifications( '+\
                    '  id integer primary key autoincrement, '+\
                    '  sender string not null, '+\
                    '  date_string string not null, '+\
                    '  notification string not null, '+\
                    '  action string not null, '+\
                    '  notification_read int not null, '+\
                    '  urgency int not null, '+\
                    '  sensitivity int not null '+\
                    ');'
                )
        except Exception as e:
            raise
        finally:
            self.close_db()

