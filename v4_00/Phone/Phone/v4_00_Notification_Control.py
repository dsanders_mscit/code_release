################################################################################
#
# Program:      v4_00_Notification_Control.py
# Class:        v4_00_Notification_Control
# Objects:      Notification_Control
# Component of: Phone
# Version:      3.01
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Global control. Updated for sensitivity and urgency.
#
################################################################################

from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Phone import Control
import datetime, time, json, requests, os
from Phone.v3_01_Notification_Control import v3_01_Notification_Control

class v4_00_Notification_Control(v3_01_Notification_Control):

    def __init__(self):
        super(v4_00_Notification_Control, self).__init__()


    #
    # v4_00 Modified to allow sensitivity and urgency. They are set as optional
    # parameters to ensure backwards compatibility with previous versions of the
    # class.
    #
    def incoming_notification(
        self,
        json_string=None
    ):
        success = 'success'
        status = '200'
        message = 'Notification received.'
        data = None
        now = datetime.datetime.now()
        tz = time.tzname[0]
        tzdst = time.tzname[1]

        date_string='{0} ({1}/{2})'.format(now, tz, tzdst)

        self.controller.log('Notification Control: Notification received.',
                              screen=False)

        try:
            json_data = json.loads(json_string)

            if json_string == None\
            or json_string == '':
                raise KeyError('No JSON Data provided!')

            self.controller.log('Notification Control: Parsing JSON data.',
                                  screen=False)
            text=json_data['message']
            key=json_data['key']


            #
            # v4_00 Additions
            #
            sender=json_data['sender']
            action=json_data['action']

            self.controller.log(
                'Notification Control: Parsing optional parameters.'
            )

            # Surround the optional parameters with their own try...except 
            # blocks. If the exception occurs, set the default value
            try:
                urgency=json_data['urgency']
            except Exception:
                urgency=False

            try:
                sensitivity=json_data['sensitivity']
            except Exception:
                sensitivity=False

            self.controller.log('Notification Control: Validate key.',
                                  screen=False)

            #
            # END v4_00 Additions
            #


            # Validate the key is correct. In real-life, this key would vary
            # and be set when the phone connected to the notifications service.
            #
            if not key == 'NS1234-5678-9012-3456':
                raise ValueError('Notification control key incorrect.')

            # Okay, key is good. Process the notifications (see 
            # v4_00_Control.py)
            #
            self.controller.process_notification(
                sender=sender,
                date_string=date_string,
                notification=text,
                action=action,
                urgency=urgency,
                sensitivity=sensitivity
            )

            data = {"sender":sender,
                    "action":action,
                    "event-date":date_string,
                    "notification":text,
                    "urgency":urgency,
                    "sensitivity":sensitivity}

        except KeyError as ke:
            success = 'error'
            status = '400'
            message = 'Badly formed request! {0}'.format(str(ke))
            self.controller.log('Incoming notification: Error {0}'\
                .format(str(ke)), screen=False)
        except ValueError as ve:
            success = 'error'
            status = '403'
            message = str(ve)
            self.controller.log(
              'Notification Control: {0}'.format(message),
              screen=False
            )
        except Exception as e:
            success = 'error'
            status = '500'
            message = 'Exception: {0}'.format(repr(e))
            self.controller.log(
              'Incoming notification: Error {0}'.format(message),
              screen=False
            )

        self.controller.log('Notification Control: Processing completed.',
                              screen=False)

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value


