from flask_restful import Resource, reqparse
from Door_Bell.Occupant_Control import occupant_control_object

class Occupant_Boundary(Resource):
    def get(self, occupant=None):
        return occupant_control_object\
            .get_occupant(occupant.lower())

    def post(self, occupant=None):
        #
        # Get the data with the request and decode it to UTF-8
        #
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        #
        # Don't return results immediately. There may be a need to update 
        # headers afterward SO, get the return result, do anything needed, and
        # then return.
        #
        return_state = occupant_control_object\
            .add_occupant(occupant.lower(), json_string=raw_data)
        # Return
        return return_state

    def delete(self, occupant=None):
        #
        # Get the data with the request and decode it to UTF-8
        #
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        #
        # Don't return results immediately. There may be a need to update 
        # headers afterward SO, get the return result, do anything needed, and
        # then return.
        #
        return_state = occupant_control_object\
            .remove_occupant(occupant, json_string=raw_data)
        # Return
        return return_state


