from flask_restful import Resource, reqparse
from Door_Bell.Ring_Control import ring_control_object

class Ring_Boundary(Resource):
    def put(self):
        #
        # Get the data with the request and decode it to UTF-8
        #
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        #
        # Don't return results immediately. There may be a need to update 
        # headers afterward SO, get the return result, do anything needed, and
        # then return.
        #
        return_state = ring_control_object\
            .ring(occupant=None,json_string=raw_data)
        return return_state

class Ring_Occupant_Boundary(Resource):
    def put(self, occupant):
        #
        # Get the data with the request and decode it to UTF-8
        #
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        #
        # Don't return results immediately. There may be a need to update 
        # headers afterward SO, get the return result, do anything needed, and
        # then return.
        #
        return_state = ring_control_object\
            .ring(occupant=occupant, json_string=raw_data)
        return return_state

