#
# Standard imports
#
from Door_Bell import app, api
from Door_Bell.Control import global_controller
#
# Get the version of the API
#
version = global_controller.get_value('version')
#
# Reference: Adamo, D. [2015], 'Handling CORS Requests in Flask(-RESTful) APIs'
# [ONLINE]. Available at: http://www.davidadamojr.com/handling-cors-requests-in-flask-restful-apis/
# (Accessed: 08 March 2016)
#
# Set cross origin resource sharing (CORS) in headers for ALL request responses
#
@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Authorization, Accept')
    response.headers.add('Access-Control-Allow-Methods',
        'GET,PUT,POST,OPTIONS,HEAD,DELETE')
    return response
#
# Sample service boundary
#
from Door_Bell_Boundary.Sample_Boundary import Sample_Boundary
api.add_resource(Sample_Boundary, '/{0}/sample'.format(version))
#
# Identify boundary
#
from Door_Bell_Boundary.Identify_Boundary import Identify_Boundary
api.add_resource(Identify_Boundary, '/{0}/identify'.format(version))
#
# Generated service boundaries
#


### Generated path for service: Delivery
from Door_Bell_Boundary.Delivery_Boundary import Delivery_Boundary
api.add_resource(Delivery_Boundary, '/{0}/delivery'.format(version))

### Generated path for service: Message
from Door_Bell_Boundary.Message_Boundary import Message_Boundary
api.add_resource(Message_Boundary, '/{0}/message'.format(version))

### Generated path for service: Ring
from Door_Bell_Boundary.Ring_Boundary \
    import  Ring_Boundary, \
            Ring_Occupant_Boundary
api.add_resource(Ring_Boundary, '/{0}/ring'.format(version))
api.add_resource(
    Ring_Occupant_Boundary,
    '/{0}/ring/<string:occupant>'.format(version)
)

### Generated path for service: Detect
from Door_Bell_Boundary.Detect_Boundary import Detect_Boundary
api.add_resource(Detect_Boundary, '/{0}/detect'.format(version))

### Generated path for service: Occupant
from Door_Bell_Boundary.Occupant_Boundary import Occupant_Boundary
api.add_resource(Occupant_Boundary, '/{0}/occupant/<string:occupant>'.format(version))
