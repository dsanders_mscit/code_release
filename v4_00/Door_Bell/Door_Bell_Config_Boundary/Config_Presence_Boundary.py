from flask_restful import Resource, reqparse
from Door_Bell_Config_Control.Config_Presence_Control \
    import config_presence_control_object

class Config_Presence_Boundary(Resource):
    def get(self):
        print('in get')
        return config_presence_control_object.get_presence()

    def post(self):
        #
        # Get the data with the request and decode it to UTF-8
        #
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        #
        # Don't return results immediately. There may be a need to update 
        # headers afterward SO, get the return result, do anything needed, and
        # then return.
        #
        return_state = config_presence_control_object\
            .set_presence(json_string=raw_data)

        # Return
        return return_state

    def delete(self):
        return_state = config_presence_control_object.clear_presence()
        return return_state


