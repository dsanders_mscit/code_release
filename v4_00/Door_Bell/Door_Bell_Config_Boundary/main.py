from Door_Bell import app, api
from Door_Bell.Control import global_controller
from Door_Bell_Config_Boundary.Config_Presence_Boundary \
    import Config_Presence_Boundary
from Door_Bell_Config_Boundary.Config_Logger_Boundary \
    import Config_Logger_Boundary
from Door_Bell_Config_Boundary.Config_Context_Boundary \
    import Config_Context_Boundary
#
# Get the version of the API
#
version = global_controller.get_value('version')

api.add_resource(Config_Presence_Boundary,
                 '/{0}/config/presence'.format(version))
#
# Place config boundaries here
#
# End config boundaries here
#
api.add_resource(Config_Logger_Boundary,
                 '/{0}/config/logger'.format(version))
api.add_resource(Config_Context_Boundary,
                 '/{0}/config/context'.format(version))

### Generated path for service: Social
from Door_Bell_Config_Boundary.Social_Boundary import Config_Social_Boundary
api.add_resource(Config_Social_Boundary, '/{0}/config/social'.format(version))

### Generated path for service: Calendar
from Door_Bell_Config_Boundary.Calendar_Boundary import Config_Calendar_Boundary
api.add_resource(Config_Calendar_Boundary, '/{0}/config/calendar'.format(version))
