from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Door_Bell import Control
import datetime, time, json, requests

#
# SuperClass.
# ----------------------------------------------------------------------------
class v1_00_Occupant_Control(object):
    __controller = None

    return_schema = {
        "occupant-id":None,
        "first-name":None,
        "family-name":None,
        "is-minor":None,
        "call-back":None,
        "call-back-key":None,
        "notification-service":None
    }

    def __init__(self):
        self.controller = Control.global_controller
        self.module_name = 'Occupant'
        self.method_name = 'unknown'


    def update_schema(
        self,
        occupant_id=None,
        first_name=None,
        family_name=None,
        is_minor=None,
        callback=None,
        callback_key=None,
        notification_service=None
    ):
        self.return_schema['occupant-id'] = occupant_id
        self.return_schema['first-name'] = first_name
        self.return_schema['family-name'] = family_name
        self.return_schema['is-minor'] = is_minor
        self.return_schema['call-back'] = callback
        self.return_schema['call-back-key'] = callback_key
        self.return_schema['notification-service'] = notification_service


    def _validate_parameters_passed(
        self,
        occupant=None
    ):
        if type(occupant) != str \
        or occupant in (None, '', []):
            raise ValueError('Occupant has to be specified '+\
                             'to modify occupant information.')


    def _validate_json_string(
        self,
        json_string=None
    ):
        return_value = None

        # Validate the raw data is valid
        self.controller.log('{0}-{1}: Validating JSON'\
            .format(self.module_name,self.method_name)
        )

        if not type(json_string) == str:
            raise KeyError('JSON data was not provided as '+\
                           'a string')

        self.controller.log('{0}-{1}: JSON data is a string.'\
            .format(self.module_name,self.method_name)
        )

        if json_string in (None, ''):
            raise KeyError('No JSON data was provided, so '+\
                           'there is no object to load')

        self.controller.log('{0}-{1}: JSON string is not empty.'\
            .format(self.module_name,self.method_name)
        )

        try:
            return_value=json.loads(json_string)
        except ValueError:
            raise ValueError('The JSON is poorly formed. Please check.')
        except Exception as e:
            raise

        return return_value


    def add_occupant(
        self,
        occupant=None,
        json_string=None
    ):
        self.method_name='add_occupant'

        self.controller.log('{0}-{1}: Add occupant request received.'\
            .format(self.module_name,
                    self.method_name)
        )

        success = 'success'
        status = 201
        message = 'Add occupant {0}'.format(occupant)
        data = None

        try:
            loading_json=False
            self._validate_parameters_passed(occupant)

            loading_json = True
            json_data = self._validate_json_string(json_string)

            # Set a sentinel around loading the JSON data so we know if it's
            # poorly formatted and can catch it in the exception.
            self.controller.log('{0}-{1}: Loading JSON'\
                .format(self.module_name,
                        self.method_name)
            )

            json_data = json.loads(json_string)
            loading_json = False

            first_name = json_data['first-name']
            family_name = json_data['family-name']
            try:
                is_minor = json_data['is-minor']
            except Exception:
                is_minor=False
            if type(is_minor) != bool:
                raise ValueError(
                    'is-minor provided as {0} instead of bool'\
                    .format(str(type(is_minor)))
                )

            try:
                callback = json_data['call-back']
            except Exception:
                callback = None

            try:
                callback_key = json_data['call-back-key']
            except Exception:
                callback_key = None

            try:
                notification_service = json_data['notification-service']
            except Exception:
                notification_service = None
                
            self.controller.log(
                '** DEBUG: Notification Service = '\
                    .format(notification_service)
            )

            self.controller.log('{0}-{1}: JSON is valid and loaded.'\
                .format(self.module_name,
                        self.method_name)
            )

            self.update_schema(
                occupant,
                first_name,
                family_name,
                is_minor,
                callback,
                callback_key,
                notification_service
            )

            self.controller.log('{0}-{1}: Saving {2} to the database.'\
                .format(self.module_name,
                        self.method_name,
                        occupant)
            )
            
            return_value=self.controller.Door_Bell_db.save_occupant(
                occupant,
                first_name,
                family_name,
                is_minor,
                callback,
                callback_key,
                notification_service
            )

            self.controller.log('{0}-{1}: Occupant {2} added.'\
                .format(self.module_name,
                        self.method_name,
                        occupant)
            )

            data=return_value
            self.controller.log('{0}-{1}: Add occupant request completed.'\
                .format(self.module_name,
                        self.method_name)
            )
        except KeyError as ke:
            success = 'error'
            status = 400
            message = '{0}-{1}: Key Error >> {2}'\
                .format(self.module_name, self.method_name, str(ke))
            self.controller.log(message)
        except ValueError as ve:
            success = 'error'
            status = 403
            message = '{0}-{1}: {2}'\
                .format(self.module_name, self.method_name, str(ve))
            if loading_json:
                status = 400
                message = '{0}-{1}: JSON Error: {2}'\
                    .format(self.module_name,
                            self.method_name,
                            str(ve))
            self.controller.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        repr(e))
            self.controller.log(message)
            print(error_text)

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)


        return return_value


    def get_occupant(
        self,
        occupant=None
    ):
        self.method_name='get_occupant'

        self.controller.log('{0}-{1}: Get details for occupant {2}'\
            .format(self.module_name,
                    self.method_name,
                    occupant)
        )

        success = 'success'
        status = '200'
        message = 'Query occupant {0}'.format(occupant)
        data = None

        try:
            loading_json=False

            self.controller.log('{0}-{1}: Validating parameter.'\
                .format(self.module_name,
                        self.method_name)
            )
            self._validate_parameters_passed(occupant)

            self.controller.log('{0}-{1}: Database query on occupant {2}'\
                .format(self.module_name,
                        self.method_name,
                        occupant)
            )
            data=self.controller.Door_Bell_db.get_occupant(occupant)
        except IndexError:
            success = 'error'
            status = 404
            message = '{0}-{1}: Occupant {2} has not been registered.'\
                .format(self.module_name, self.method_name, occupant)
            self.controller.log(message)
        except ValueError as ve:
            success = 'error'
            status = 403
            message = '{0}-{1}: {2}'\
                .format(self.module_name, self.method_name, str(ve))
            if loading_json:
                status = 400
                message = '{0}-{1}: JSON Error: {2}'\
                    .format(self.module_name,
                            self.method_name,
                            str(ve))
            self.controller.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        repr(e))
            self.controller.log(message)
            print(error_text)

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)


        return return_value


    def remove_occupant(
        self,
        occupant=None,
        json_string=None
    ):
        self.method_name='remove_occupant'

        self.controller.log('{0}-{1}: Remove occupant request received.'\
            .format(self.module_name,
                    self.method_name)
        )

        success = 'success'
        status = '200'
        message = 'Remove occupant {0}'.format(occupant)
        data = None

        try:
            loading_json=False
            self._validate_parameters_passed(occupant)

            loading_json = False
            self.controller.log('{0}-{1}: Removing {2} from the database.'\
                .format(self.module_name,
                        self.method_name,
                        occupant)
            )
            
            return_value=self.controller.Door_Bell_db.remove_occupant(
                occupant
            )

            self.controller.log('{0}-{1}: Occupant {2} removed.'\
                .format(self.module_name,
                        self.method_name,
                        occupant)
            )

            data=return_value

            self.controller.log('{0}-{1}: Remove occupant request completed.'\
                .format(self.module_name,
                        self.method_name)
            )
        except KeyError as ke:
            success = 'error'
            status = 400
            message = '{0}-{1}: Key Error >> {2}'\
                .format(self.module_name, self.method_name, str(ke))
            self.controller.log(message)
        except ValueError as ve:
            success = 'error'
            status = 403
            message = '{0}-{1}: {2}'\
                .format(self.module_name, self.method_name, str(ve))
            if loading_json:
                status = 400
                message = '{0}-{1}: JSON Error: {2}'\
                    .format(self.module_name,
                            self.method_name,
                            str(ve))
            self.controller.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        repr(e))
            self.controller.log(message)
            print(error_text)

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)


        return return_value



