from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from base_lib.Context_Bad_Exception import Context_Bad_Exception
from Door_Bell import Control
import datetime, time, json, requests, redis

#
# SuperClass.
# ----------------------------------------------------------------------------
class v1_00_Ring_Control(object):

    return_schema = {
        "occupant-id":None,
        "first-name":None,
        "family-name":None,
        "is-minor":None
    }
    
    friend_list = ['david', 'diane', 'cynthia', 'geraldine', 'iftkhar']
    foaf_list = ['bob', 'shehani', 'tim']
    expected_list = ['fedex']

    def __init__(self):
        self.controller = Control.global_controller
        self.module_name = 'Ring'
        self.method_name = 'unknown'

    def _validate_json_string(
        self,
        json_string=None
    ):
        return_value = None

        # Validate the raw data is valid
        self.controller.log('{0}-{1}: Validating JSON'\
            .format(self.module_name,self.method_name)
        )

        if not type(json_string) == str:
            raise KeyError('JSON data was not provided as '+\
                           'a string')

        self.controller.log('{0}-{1}: JSON data is a string.'\
            .format(self.module_name,self.method_name)
        )

        if json_string in (None, ''):
            raise KeyError('No JSON data was provided, so '+\
                           'there is no object to load')

        self.controller.log('{0}-{1}: JSON string is not empty.'\
            .format(self.module_name,self.method_name)
        )

        try:
            return_value=json.loads(json_string)
        except ValueError:
            raise ValueError('The JSON is poorly formed. Please check.')
        except Exception as e:
            raise

        return return_value


    def ring(
        self,
        occupant=None,
        json_string=None
    ):
        self.method_name='ring'

        self.controller.log('{0}-{1}: Ring door bell request.'\
            .format(self.module_name,
                    self.method_name)
        )

        success = 'success'
        status = 200
        message = 'The door bell is ringing.'
        data = None

        try:
            loading_json = True
            self.controller.log('{0}-{1}: Loading JSON'\
                .format(self.module_name,
                        self.method_name)
            )
            json_data = self._validate_json_string(json_string)
            json_data = json.loads(json_string)
            loading_json = False
            call_back = json_data['call-back']
            call_back_key = json_data['call-back-key']
            call_from = json_data['call-from']

            if occupant != None:
                occupant_details = self.controller\
                    .Door_Bell_db.get_occupant(occupant)

            return_state = self._handle_ring(
                call_back, 
                call_back_key, 
                occupant,
                call_from
            )
            data = {"door":"Door bell ringing. Please wait"}
        except Context_Bad_Exception as le:
            success = 'error'
            status = 409
            message = str(le)
            self.controller.log(message)
        except IndexError as ie:
            success = 'error'
            status = 404
            message = '{0} is not registered on this door. '.format(occupant)
            self.controller.log(message)
        except KeyError as ke:
            success = 'error'
            status = 400
            message = '{0}-{1}: Key Error >> {2} must be provided.'\
                .format(self.module_name, self.method_name, str(ke))
            self.controller.log(message)
        except ValueError as ve:
            success = 'error'
            status = 403
            message = '{0}-{1}: {2}'\
                .format(self.module_name, self.method_name, str(ve))
            if loading_json:
                status = 400
                message = '{0}-{1}: JSON Error: {2}'\
                    .format(self.module_name,
                            self.method_name,
                            str(ve))
            self.controller.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        repr(e))
            self.controller.log(message)
            print(error_text)

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)


        return return_value


    def _handle_ring(
        self,
        call_back=None,
        call_back_key=None,
        call_for=None,
        call_from=None
    ):
        if call_for == None :
            self.controller.log('Nothing to do. The door bell is ringing!')
            return False

        try:
            is_busy = False
            context_info = {
                "presence":None,
                "disturb":None,
                "available":None,
                "redirect":None,
                "expected":None,
                "relationship":None,
                "caller-message":None,
                "occupant-message":None,
                "contact-occupant":None,
                "contact-caller":None
            }

            #
            # Get the occupant info
            #
            self.controller.log('Fetching occupant info')
            occupant_details = self.controller\
                .Door_Bell_db.get_occupant(call_for)
            self.controller.log(
                'Occupant details {0}'.format(occupant_details)
            )
            if occupant_details['notification-service'] == None \
            or occupant_details['call-back'] == None:
                return


## Remove after validating use
            message_to_deliver = "{0} is ringing the door bell for {1}."\
                .format(call_from, call_for)
            caller_message = "{0} will be with you in a moment. Please wait."\
                .format(call_for)
## Remove End


            #
            # Get presence for the occupant
            #
            self.controller.log('First context check - occupant presence')
            context_info["presence"] = self._get_presence(call_for).lower()
            if context_info["presence"] == 'do-not-disturb':
                context_info["contact-occupant"] = False
                context_info["contact-caller"] = False
                context_info["occupant-message"] = None
            else:
                context_info["contact-occupant"] = True
                context_info["contact-caller"] = True
                context_info["occupant-message"] = \
                    "{0} is ringing the door bell for {1}."\
                        .format(call_from, call_for)
                context_info["caller-message"] = \
                    "{0} will be with you in a moment. Please wait."\
                        .format(call_for)

            #
            # Check the schedule - is the caller expected?
            #
            # In a production environment, this would be much more thorough
            # and call off to a calendar service. Here, the model simply checks
            # if the caller is FedEx.
            self.controller.log('Second context check - scheduled visitor')
            if call_from.lower() in self.expected_list:
                context_info["expected"] = True
                context_info["contact-caller"] = True
            else:
                context_info["expected"] = False


            #
            # Check social connections - is the caller a friend, friend of a
            # friend, or a stranger. This should link off to a foaf service but
            # for the purposes of this model, it is enough to constrain to
            # simple lists. We only check here IF the caller is not expected.
            #
            self.controller.log('Third context check - foaf')
            if not context_info['expected']:
                if call_from.lower() in self.friend_list:
                    context_info["relationship"] = 'friend'
                    context_info["contact-caller"] = True
                elif call_from.lower() in self.foaf_list:
                    context_info["relationship"] = 'foaf'
                    context_info["contact-caller"] = True
                else:
                    context_info["relationship"] = 'stranger'
                    context_info["contact-caller"] = False

            # 
            # TODO
            #
            # Process the call based on the context
            #

            #
            # Send notifications
            #
            if context_info['contact-occupant']:
                self._send_notification(
                    occupant_details['notification-service'],
                    "1234-5678-9012-3456",
                    message_to_deliver,
                    "Door Bell",
                    occupant_details['call-back'],
                    "none",
                    True
                )

            if context_info['contact-caller']:
                self._send_notification(
                    occupant_details['notification-service'],
                    "1234-5678-9012-3456",
                    caller_message,
                    "Door Bell",
                    call_back,
                    "none",
                    True
                )
            
            if is_busy:
                raise Context_Bad_Error(
                    '{0} is busy and un-able to come to the door at the moment'\
                        .format(call_for)
                )            

            return True
        except Exception as e:
            self.controller.log('EXCEPTION >> {0}') # Ignore exceptions but log



    def _send_notification(
        self,
        service=None,
        key=None,
        message=None,
        sender=None,
        recipient=None,
        action=None,
        urgency=False
    ):
        try:
            payload_data = {
                "key":key,
                "message":message,
                "sender":sender,
                "recipient":recipient,
                "action":action,
                "urgency":urgency
            }
            
            self.controller.log(
                'payload data is {0}'\
                    .format(payload_data)
            )

            request_response = requests.post(
                service,
                data=json.dumps(payload_data),
                timeout=20
            )
            
            self.controller.log(
                'Notification to occupant: response status: {0} {1}'\
                    .format(
                        request_response.status_code,
                        request_response.text
                    )
            )
        # IGNORE all errors - if we can't send the notification, it doesn't
        # matter.
        except requests.exceptions.ConnectionError as rce:
            pass
        except Exception:
            pass



    def _send_redis(
        self,
        redis_instance=None,
        field1=None,
        field2=None,
        field3=None,
        field4=None,
        field5=None
    ):
        if redis_instance == None:
            return
        self.controller.log('** DEBUG: Redis Instance: {0}'\
            .format(redis_instance))
        try:
            redis_instance.publish(
                'ring_processor',
                '{0}<<*>>{1}<<*>>{2}<<*>>{3}<<*>>{4}'.format(
                    field1,
                    field2,
                    field3,
                    field4,
                    field5
                )
            )
        except:
            raise

    def _get_presence(
        self,
        call_for=None,
        call_from=None
    ):
        return_state = 'away'
        try:
            if call_for == None:
                raise KeyError('Cant get context for no-one')

            presence_engine = self.controller.get_value('presence-engine')
            if presence_engine in (None, '', []):
                raise ValueError(
                    'There is no presence engine. So cannot get presence.'
                )

            presence_url = '{0}/{1}'.format(presence_engine, call_for)
            request_response = requests.get(
                presence_url,
                timeout=10
            )

            status_code = request_response.status_code
            if status_code in (200,201):
                response_data = request_response.json()
                return_state = response_data['data']['status']
                self.controller.log('Status is {0}'.format(return_state))

            self.controller.log('Check Presence response: {0} {1}'\
                .format(
                    request_repsonse.status_code,
                    request_response.text
                )
            )
        except KeyError as ke:
            self.controller.log('KEY ERROR >> {0}'.format(str(ke)))
        except ValueError as ke:
            self.controller.log('VALUE ERROR >> {0}'.format(str(ve)))
        except Exception as e:
            self.controller.log('EXCEPTION >> {0}'.format(repr(e)))

        return return_state

