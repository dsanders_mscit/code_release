# Import base library modules - From Bluetooth symbolic link to /base_lib
from Door_Bell.v1_00_Door_Bell_Database import v1_00_Door_Bell_Database

#
# SuperClass.
# ----------------------------------------------------------------------------
class Door_Bell_Database(v1_00_Door_Bell_Database):
    def __init__(self,
                 controller=None,
                 server_name='localhost',
                 port_number=5000):
        super(Door_Bell_Database, self).__init__(controller,
                                             server_name,
                                             port_number)

