# Import base library modules - From Bluetooth symbolic link to /base_lib
from Door_Bell.v1_00_Ring_Processor import v1_00_Ring_Processor

#
# SuperClass.
# ----------------------------------------------------------------------------
class Ring_Processor(v1_00_Ring_Processor):
    def __init__(self, control=None):
        super(Ring_Processor, self).__init__(control)

