# Import base library modules - From Bluetooth symbolic link to /base_lib
import base_lib
from base_lib.Responder import Responder
from base_lib.Config_Logger import Config_Logger
from base_lib.Logger import Logger
from base_lib.Environment import Environment
from base_lib.Key import Key
from base_lib.KVStore import KVStore
from base_lib.Identify_Control import Identify_Control
from base_lib.Config_Context import Config_Context

from Door_Bell.Door_Bell_Database import Door_Bell_Database

class v1_00_Control(object):
    log_file = None
    Door_Bell_db = None

    server_name=None
    port_number=0

    config_logger = None
    redis_queue = {'host':'localhost', 'port':6379, 'db':0}

    def __init__(self):
        self.module_name = 'Door-Bell-Control'
        self.method_name = '__init__'

        # Setup environment
        self.environment = Environment()
        port_number = self.environment['port_number']
        server_name = self.environment['server_name']
        host_ip = self.environment['ip_addr']
        version = self.environment['version']
        pre_filename = 'datavolume/{0}-{1}'\
                       .format(server_name, port_number)

        # Setup key generator
        self.key_gen = Key(controller=self, module='Control')

        # Setup responder
        self.responder = Responder()
        self.do_response = self.responder.do

        # Setup KV Store
        self.kvstore = KVStore(pre_filename+'-config.db')
        self.get_value = self.kvstore.get_key
        self.set_value = self.kvstore.set_key
        self.clear_value = self.kvstore.clear_key

        # Setup Logger
        self.logger = Logger(controller=self,
                             filename=pre_filename+'-log.txt',
                             sender='Door_Bell-{1}'\
                                 .format(server_name, port_number))
        self.log = self.logger.writelog
        self.db_logger = self.logger.db_logger
        
        # Setup Identify Control Object
        self.identify_control_object = \
            Identify_Control(self, module='Phone control')

        # Setup Context Control Object
        self.context_control_object = \
            Config_Context(self, module='Phone context')


        # General startup
        self.server_name = server_name
        self.port_number = port_number

        self.Door_Bell_db = Door_Bell_Database(self,
                                               self.server_name,
                                               self.port_number)

        self.log('Door_Bell {0}:{1} Started'\
                 .format(server_name, port_number))

        self.log('Setting environment variables to {0}'\
            .format(self.environment))
        self.set_value('server_name', server_name)
        self.set_value('port_number', port_number)
        self.set_value('ip_addr', host_ip)
        self.set_value('version', version)
        self.log('Stored environment variables')


