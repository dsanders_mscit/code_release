import sqlite3, os

class v1_00_Door_Bell_Database(object):
    db_name = None
    db_conn = None
    db_cursor = None
    controller = None

    def __init__(self,
                 controller=None,
                 server_name='localhost',
                 port_number=5000
    ):
        if controller == None:
            raise Exception('Controller cannot be none!')

        self.controller = controller

        self.db_name = 'datavolume/{0}-{1}.db'\
                             .format(server_name, port_number)

        self.db_conn = None
        self.db_cursor = None

        self.validate_occupants_table()

#
# New code
#

    def save_occupant(
        self,
        occupantid=None,
        first_name=None,
        family_name=None,
        is_minor=0,
        callback=None,
        callback_key=None,
        notification_service=None
    ):
        if occupantid == None or first_name == None or family_name == None:
            raise KeyError(
                'Userid, first name, and family name must be provided.'
            )

        returned = None
        try:
            self.open_db()
            self.db_exec('insert or replace into occupants '+\
                           ' (occupantid, '+\
                           '  first_name, '+\
                           '  family_name, '+\
                           '  is_minor, '+\
                           '  callback, '+\
                           '  callback_key, '+\
                           '  notification_service '+\
                           ') '+\
                           'values (?,?,?,?,?,?,?)',
                           (occupantid,
                            first_name, 
                            family_name, 
                            is_minor,
                            callback,
                            callback_key,
                            notification_service
                           )
                        )
            self.db_conn.commit()
        except Exception as e:
            self.controller.log(
                'Door_Bell Database: EXCEPTION >> {0}'.format(repr(e))
            )
            raise
        finally:
            self.close_db()

        return {
            'occupant-id':occupantid, 
            'first-name':first_name,
            'family-name':family_name,
            'is-minor':is_minor,
            'call-back':callback,
            'call-back-key':callback_key,
            'notification-service':notification_service
        }


    def get_occupant(
        self,
        occupantid=None
    ):
        if occupantid == None:
            raise KeyError(
                'Userid must be provided.'
            )

        returned = None
        try:
            self.open_db()
            self.db_exec('select * '+\
                         'from occupants '+\
                         'where occupantid = ?',
                         (occupantid, ))
            returned = self.db_cursor.fetchall()
            if returned in ('', [], None):
                raise IndexError('Not found.')
            returned = \
                {
                    'occupant-id':returned[0][0], 
                    'first-name':returned[0][1],
                    'family-name':returned[0][2],
                    'is-minor':True if returned[0][3] == 1 else False,
                    'call-back':returned[0][4],
                    'call-back-key':returned[0][5],
                    'notification-service':returned[0][6]
                }
        except IndexError as ve:
            self.controller.log(
                'Door_Bell Database: INDEX ERROR >> {0}'.format(repr(ve))
            )
            raise
        except Exception as e:
            self.controller.log(
                'Door_Bell Database: EXCEPTION >> {0}'.format(repr(e))
            )
            raise
        finally:
            self.close_db()

        return returned


    def remove_occupant(
        self,
        occupantid=None
    ):
        if occupantid == None:
            raise KeyError(
                'Userid must be provided.'
            )

        returned = None
        try:
            self.open_db()
            self.db_exec('delete from occupants '+\
                         'where occupantid = ?',
                         (occupantid, ))
            self.db_conn.commit()
            returned = \
                {
                    'occupant-id':None, 
                    'first-name':None,
                    'family-name':None,
                    'is-minor':None,
                    'call-back':None,
                    'call-back-key':None,
                    'notification-service':None
                }
        except Exception as e:
            self.controller.log(
                'Door_Bell Database: EXCEPTION >> {0}'.format(repr(e))
            )
            raise
        finally:
            self.close_db()

        return returned


#
# Generated code
#
    def db_exec(self, sql_statement=None, sql_parameters=()):
        if sql_statement == None:
            return None

        _returned = None

        try:
            if self.db_cursor == None:
                raise Exception('Cursor does not exist!')
            self.db_cursor.execute(sql_statement, sql_parameters)
        except sqlite3.OperationalError as oe:
            raise
        except Exception as e:
            raise


    def close_db(self):
        if not self.db_cursor == None:
            self.db_cursor.close()
        if not self.db_conn == None:
            self.db_conn.close()
        self.db_cursor = None
        self.db_conn = None


    def open_db(self):
        try:
            self.db_conn = sqlite3.connect(self.db_name)
            self.db_cursor = self.db_conn.cursor()
        except Exception as e:
            if not self.db_cursor == None:
                self.db_cursor.close()
            if not self.db_conn == None:
                self.db_conn.close()
            raise


    def validate_occupants_table(self):
        _returned = None

        try:
            self.open_db()
            self.db_exec('delete from occupants')
            self.db_conn.commit()
        except sqlite3.OperationalError as oe:
            self.db_cursor.execute(
                    'CREATE TABLE occupants ( '+\
                    '  occupantid string primary key, '+\
                    '  first_name string not null, '+\
                    '  family_name string not null, '+\
                    '  is_minor integer not null, '+\
                    '  callback string, '+\
                    '  callback_key string, '+\
                    '  notification_service string'+\
                    ');'
                )
        except Exception as e:
            raise
        finally:
            self.close_db()

