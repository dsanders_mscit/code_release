from flask_restful import Resource, Api, reqparse, abort
from flask import Response
import datetime, time, json, requests, os, redis

class v1_00_Ring_Processor(object):
    return_schema = {
        "ack":None,
        "verb":None,
        "data":{}
    }

    def __init__(self, control=None):
        self.controller = control
        self.module_name = 'Ring-Processor'
        self.method_name = 'unknown'


    def log(self, log_message):
        self.controller.log(
            '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        str(log_message))
        )


    def processor(self):
        redis_instance = redis.StrictRedis(**self.controller.redis_queue)
        redis_pubsub = redis_instance.pubsub()
        redis_pubsub.subscribe('ring_processor')
        self.method_name = 'processor'

        for message in redis_pubsub.listen():
            if message['type'].upper() == 'MESSAGE':
                self.log('Redis Processor: Message received!')

                message_data = message['data']
                message_fields = message['data'].decode('utf-8')\
                    .split('<<*>>', 4)
                for message in message_fields:
                    self.log('** DEBUG: Message field: {0}'.format(message))

                try:
                    verb = message_fields[0]
                    call_back = message_fields[1]
                    call_back_key = message_fields[2]
                    call_for = message_fields[3]
                    call_from = message_fields[4]
                    if call_for[0:4].upper() == "NONE":
                        call_for = None
                    if verb.upper() == "RING":
                        self.process_ring(
                            call_back,
                            call_back_key,
                            call_for,
                            call_from
                        )
                    elif verb.upper() == "FYI":
                        self.process_fyi(
                            call_back,
                            call_back_key,
                            call_for,
                            call_from
                        )
                    else:
                        self.process_unknown_command(verb)
                except Exception as e:
                    success = 'error'
                    status = '500'
                    message = 'EXCEPTION >> {0}'.format(repr(e))
                    self.log(message)
                    print(message)


    def process_unknown_command(
        self,
        verb=None
    ):
        self.controller.log(
            'RING PROCESSOR: Unknown command {0}'.format(verb)
        )


    def process_fyi(
        self,
        call_back=None,
        call_back_key=None,
        call_for=None,
        call_from=None
    ):
        payload_data={
            "verb":"fyi",
            "data":
                {"occupant":call_for, 
                 "at-the-door":call_from,
                }
        }
        self._deliver_payload(call_for, payload_data)


    def process_ring(
        self,
        call_back=None,
        call_back_key=None,
        call_for=None,
        call_from=None
    ):
        payload_data={
            "verb":"doorbell-ringing",
            "data":
                {"occupant":call_for, 
                 "at-the-door":call_from,
                 "call-back":call_back,
                 "call-back-key":call_back_key
                },
            "key":call_back_key
        }
        self._deliver_payload(call_for, payload_data)


    def _deliver_payload(
        self,
        call_for=None,
        payload_data=None
    ):
        try:
            owner_info = self.controller.Door_Bell_db.get_occupant(call_for)
            self.controller.log(
                'Calling {0} at {1}.'\
                .format(
                    owner_info['first-name'],
                    owner_info['call-back']
                )
            )

            if not owner_info == {}:
                request_response = requests.put(
                    owner_info['call-back'],
                    data=json.dumps(payload_data),
                    timeout=10
                )

            self.controller.log(request_response.text)
        except Exception as e:
            pass
