# Import base library modules - From Bluetooth symbolic link to /base_lib
from Door_Bell_Config_Control.v1_00_Config_Social_Control \
    import v1_00_Config_Social_Control

#
# SuperClass.
# ----------------------------------------------------------------------------
class Config_Social_Control(v1_00_Config_Social_Control):
    def __init__(self):
        super(Config_Social_Control, self).__init__()


config_social_control_object = Config_Social_Control()

