# Import base library modules - From Bluetooth symbolic link to /base_lib
from Door_Bell_Config_Control.v1_00_Config_Presence_Control \
    import v1_00_Config_Presence_Control

#
# SuperClass.
# ----------------------------------------------------------------------------
class Config_Presence_Control(v1_00_Config_Presence_Control):
    def __init__(self):
        super(Config_Presence_Control, self).__init__()


config_presence_control_object = Config_Presence_Control()

