# Import base library modules - From Bluetooth symbolic link to /base_lib
from Door_Bell_Config_Control.v1_00_Config_Calendar_Control \
    import v1_00_Config_Calendar_Control

#
# SuperClass.
# ----------------------------------------------------------------------------
class Config_Calendar_Control(v1_00_Config_Calendar_Control):
    def __init__(self):
        super(Config_Calendar_Control, self).__init__()


config_calendar_control_object = Config_Calendar_Control()

