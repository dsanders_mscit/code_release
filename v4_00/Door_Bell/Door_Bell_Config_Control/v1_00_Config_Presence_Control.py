from flask_restful import Resource
from flask import Response
from Door_Bell import Control

import json, requests

class v1_00_Config_Presence_Control(object):
    controller = None

    def __init__(self):
        self.controller = Control.global_controller
        self.module_name = 'Config Presence'
        self.method_name = 'unknown'
        
    def log(
        self,
        log_message=None
    ):
        self.controller.log(
            '{0}-{1}: {2}'\
                .format(
                    self.module_name,
                    self.method_name,
                    log_message
                )
        )


    def _validate_json_string(
        self,
        json_string=None
    ):
        return_value = None

        # Validate the raw data is valid
        self.controller.log('{0}-{1}: Validating JSON'\
            .format(self.module_name,self.method_name)
        )

        if not type(json_string) == str:
            raise KeyError('JSON data was not provided as '+\
                           'a string')

        self.controller.log('{0}-{1}: JSON data is a string.'\
            .format(self.module_name,self.method_name)
        )

        if json_string in (None, ''):
            raise KeyError('No JSON data was provided, so '+\
                           'there is no object to load')

        self.controller.log('{0}-{1}: JSON string is not empty.'\
            .format(self.module_name,self.method_name)
        )

        try:
            return_value=json.loads(json_string)
        except ValueError:
            raise ValueError('The JSON is poorly formed. Please check.')
        except Exception as e:
            raise

        return return_value


    def get_presence(self):
        self.method_name = 'get_presence'
        self.log('In get presence.')

        success = 'success'
        status = 200
        message = 'Presence Engine'
        data = None
        
        try:
            self.log('Fetching presence engine from key store')
            presence_engine = self.controller.get_value('presence-engine')
            self.log('Presence engine returned as {0}'.format(presence_engine))
            data = {"presence-engine":presence_engine}
        except Exception as e:
            success = 'error'
            status = '500'
            message = '{0}'.repr(e)
            self.log(repr(e))

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)

        self.log('Get presence complete.')

        return return_value


    def set_presence(
        self,
        json_string=None
    ):
        try:
            self.module_name = 'set-presence'
            self.controller.log('Set presence request received.')

            success = 'success'
            status = '200'
            message = 'Presence Engine'

            loading_json = True
            self.log('Loading JSON')
            json_data = self._validate_json_string(json_string)
            loading_json = False

            presence_engine = json_data['presence-engine']

            self.log('Setting presence engine from key store')
            self.controller.set_value('presence-engine', presence_engine)
            self.log('Presence engine set to {0}'.format(presence_engine))

            data = {
                "presence-engine":presence_engine
            }
        except KeyError as ke:
            success = 'error'
            status = 400
            message = str(ke)
            self.log(message)
        except ValueError as ve:
            success = 'error'
            status = 403
            message = str(ve)
            if loading_json:
                status = 400
                message = '{0}-{1}: JSON Error: {2}'\
                    .format(self.module_name,
                            self.method_name,
                            str(ve))
            self.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = repr(e)
            self.log(message)

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)

        self.controller.log('Presence request returned {0}'.format(data))

        return return_value


    def clear_presence(
        self
    ):
        try:
            self.module_name = 'clear-presence'
            self.controller.log('Clear presence request received.')

            success = 'success'
            status = '200'
            message = 'Presence Engine'

            self.log('Clearing presence engine from key store')
            self.controller.clear_value('presence-engine')
            self.log('Presence engine cleared')

            data = {
                "presence-engine":None
            }
        except KeyError as ke:
            success = 'error'
            status = 400
            message = str(ke)
            self.log(message)
        except ValueError as ve:
            success = 'error'
            status = 403
            message = str(ve)
            if loading_json:
                status = 400
                message = '{0}-{1}: JSON Error: {2}'\
                    .format(self.module_name,
                            self.method_name,
                            str(ve))
            self.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = repr(e)
            self.log(message)

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)

        self.controller.log('Presence request returned {0}'.format(data))

        return return_value


