echo ""
echo "Building Phone"
cd Phone
./build.sh
echo ""
echo "Building Context"
cd ../Context
./build.sh
echo ""
echo "Building Notification Service"
cd ../Notification_Service
./build.sh
echo ""
echo "Building Monitor App"
cd ../Monitor_App
./build.sh
echo ""
echo "Building Door Bell"
cd ../Door_Bell
./build.sh
echo ""
echo "Building Presence"
cd ../Presence
./build.sh
echo ""
cd ..

