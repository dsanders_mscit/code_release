from Presence.v1_00_Presence_Database import v1_00_Presence_Database

class Presence_Database(v1_00_Presence_Database):
    def __init__(self,
                 controller=None,
                 server_name='localhost',
                 port_number=5000):
        super(Presence_Database, self).__init__(controller,
                                             server_name,
                                             port_number)

