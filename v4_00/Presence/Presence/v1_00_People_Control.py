################################################################################
#
# Program:      v1_00_People_Control.py
# Class:        v1_00_People_Control
# Objects:      People_Control
# Component of: Presence
# Version:      1.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Control the definition of people within the presence engine and their
#          relationship to the user. Relationships can be: spouse/partner, 
#          family, child (any child, not neccessarily child of...), friend,
#          friend of a friend, or colleague. Currently, a person can only be one
#          of the states and they are not weighted.
#
################################################################################

from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Presence import Control
import datetime, time, json, requests

class v1_00_People_Control(object):
    __controller = None

    return_schema = {
        "user-id":None,
        "person":None,
        "relationship":None
    }
    
    supported_relationships = [
        'spouse',
        'partner',
        'family',
        'child',
        'friend',
        'friend-of-friend',
        'colleague'
    ]

    # Constructor
    def __init__(self):
        self.controller = Control.global_controller
        self.module_name = 'People'
        self.method_name = 'unknown'


    def update_schema(
        self,
        user_id=None,
        person=None,
        relationship=None
    ):
        self.return_schema['user-id'] = user_id
        self.return_schema['person'] = person
        self.return_schema['relationship'] = relationship


    def _validate_parameters_passed(
        self,
        user_id=None,
        person=None
    ):
        if type(user_id) != str \
        or user_id in (None, '', []) \
        or type(person) != str \
        or person in (None, '', []):
            raise ValueError('User ID and person must be specified '+\
                             'to modify user subscription.')

        user_id = self.controller.Presence_db.get_user(user_id)


    def _validate_json_string(
        self,
        json_string=None
    ):
        if json_string in (None, ''):
            return None

        return_value = None

        # Validate the raw data is valid
        self.controller.log('{0}-{1}: Validating JSON'\
            .format(self.module_name,self.method_name)
        )

        if not type(json_string) == str:
            raise KeyError('JSON data was not provided as '+\
                           'a string')

        self.controller.log('{0}-{1}: JSON data is a string.'\
            .format(self.module_name,self.method_name)
        )

        if json_string in (None, ''):
            raise KeyError('No JSON data was provided, so '+\
                           'there is no object to load')

        self.controller.log('{0}-{1}: JSON string is not empty.'\
            .format(self.module_name,self.method_name)
        )

        try:
            return_value=json.loads(json_string)
        except ValueError:
            raise ValueError('The JSON is poorly formed. Please check.')
        except Exception as e:
            raise

        return return_value


    def add_person(
        self,
        user_id=None,
        person=None,
        json_string=None
    ):
        self.method_name='add_person'

        self.controller.log('{0}-{1}: Person add request received.'\
            .format(self.module_name,
                    self.method_name)
        )

        success = 'success'
        status = 201
        message = 'Add person {0} known to {1}'.format(person, user_id)
        data = None

        try:
            relationship=None
            loading_json=False
            self._validate_parameters_passed(user_id, person)

            loading_json = True
            json_data = self._validate_json_string(json_string)

            # Set a sentinel around loading the JSON data so we know if it's
            # poorly formatted and can catch it in the exception.
            self.controller.log('{0}-{1}: Loading JSON'\
                .format(self.module_name,
                        self.method_name)
            )
            loading_json = False

            if json_data != None:
                relationship = json_data['relationship'].lower()
                self.controller.log('{0}-{1}: Relationship is {2}'\
                    .format(self.module_name,
                            self.method_name,
                            relationship)
                )
                if not relationship in self.supported_relationships:
                    raise ValueError(
                        '{0} is not a supported relationship. '\
                            .format(relationship)+\
                        'Supported relationships are {0}'\
                            .format(self.supported_relationships)
                    )
            else:
                loading_json = True
                raise ValueError(
                    'A valid relationship must be provided. Relationship can '+\
                    'be one of: {0}'.format(self.supported_relationships)
                )

            self.controller.log('{0}-{1}: JSON is valid and loaded.'\
                .format(self.module_name,
                        self.method_name)
            )

            self.update_schema(
                user_id,
                person,
                relationship
            )

            self.controller.log('{0}-{1}: Saving {2} to the database.'\
                .format(self.module_name,
                        self.method_name,
                        user_id)
            )
            
            return_value=self.controller.Presence_db.save_person(
                user_id,
                person,
                relationship
            )

            self.controller.log('{0}-{1}: Person {2} added as {3} of {4}.'\
                .format(self.module_name,
                        self.method_name,
                        person,
                        relationship,
                        user_id)
            )

            data=return_value
            self.controller.log('{0}-{1}: Add person request completed.'\
                .format(self.module_name,
                        self.method_name)
            )
        except KeyError as ke:
            success = 'error'
            status = 400
            message = '{0}-{1}: Key Error >> {2}'\
                .format(self.module_name, self.method_name, str(ke))
            self.controller.log(message)
        except IndexError as ie:
            success = 'error'
            status = 404
            message = '{0}-{1}: Index Error >> User {2} {3}'\
                .format(self.module_name, self.method_name, user_id, str(ie))
            self.controller.log(message)
        except ValueError as ve:
            success = 'error'
            status = 400
            message = '{0}-{1}: {2}'\
                .format(self.module_name, self.method_name, str(ve))
            if loading_json:
                status = 400
                message = '{0}-{1}: JSON Error: {2}'\
                    .format(self.module_name,
                            self.method_name,
                            str(ve))
            self.controller.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        repr(e))
            self.controller.log(message)
            print(error_text)

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)


        return return_value


    def get_person(
        self,
        user_id=None,
        person=None
    ):
        self.method_name='get_person'

        self.controller.log(
            '{0}-{1}: Get person details of {1} known to user {2}'\
                .format(self.module_name,
                        self.method_name,
                        person,
                        user_id)
        )

        success = 'success'
        status = '200'
        message = 'Details of person {0} known to {1}'.format(person, user_id)
        data = None

        try:
            loading_json=False
            searching_person=False

            self.controller.log('{0}-{1}: Validating parameter.'\
                .format(self.module_name,
                        self.method_name)
            )
            self._validate_parameters_passed(user_id, person)

            self.controller.log(
                '{0}-{1}: Database query on user_id {2} for {3}'\
                    .format(self.module_name,
                            self.method_name,
                            user_id,
                            person)
            )

            searching_person = True
            people_found=self.controller.Presence_db.get_people(user_id, person)

            self.controller.log('{0}-{1}: Query returned: {2}'\
                .format(self.module_name,
                        self.method_name,
                        people_found)
            )
            data = people_found[0]
        except IndexError as ie:
            if searching_person:
                self.controller.log('{0}-{1}: Query returned: {2}'\
                    .format(self.module_name,
                            self.method_name,
                            people_found)
                )
                data = {
                    'user-id':user_id, 
                    'person':person,
                    'relationship':'stranger'
                }
            else:
                success = 'error'
                status = 404
                message = '{0}-{1}: Index Error >> {2} {3}'\
                    .format(self.module_name, self.method_name, user_id, str(ie))
                self.controller.log(message)
        except ValueError as ve:
            success = 'error'
            status = 403
            message = '{0}-{1}: {2}'\
                .format(self.module_name, self.method_name, str(ve))
            if loading_json:
                status = 400
                message = '{0}-{1}: JSON Error: {2}'\
                    .format(self.module_name,
                            self.method_name,
                            str(ve))
            self.controller.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        repr(e))
            self.controller.log(message)
            print(error_text)

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)


        return return_value


    def remove_person(
        self,
        user_id=None,
        person=None
    ):
        self.method_name='remove_person'

        self.controller.log(
            '{0}-{1}: Cancel user subscription request received.'\
                .format(self.module_name,
                        self.method_name)
        )

        success = 'success'
        status = '200'
        message = 'Remove person {0} from known list of {1}'\
            .format(person, user_id)
        data = None

        try:
            loading_json=False
            self.controller.log('{0}-{1}: Validating parameter.'\
                .format(self.module_name,
                        self.method_name)
            )
            self._validate_parameters_passed(user_id, person)

            loading_json = False
            self.controller.log(
                '{0}-{1}: Removing {2} from known persons of {3}.'\
                    .format(self.module_name,
                            self.method_name,
                            person,
                            user_id
                    )
            )
            
            return_value=self.controller.Presence_db.remove_person(
                user_id,
                person
            )

            self.controller.log(
                '{0}-{1}: Person {2} removed from known persons of {3}.'\
                    .format(self.module_name,
                            self.method_name,
                            person,
                            user_id
                    )
            )

            data=return_value

            self.controller.log(
                '{0}-{1}: Remove person request completed.'\
                    .format(self.module_name,
                            self.method_name)
            )
        except KeyError as ke:
            success = 'error'
            status = 400
            message = '{0}-{1}: Key Error >> {2}'\
                .format(self.module_name, self.method_name, str(ke))
            self.controller.log(message)
        except ValueError as ve:
            success = 'error'
            status = 403
            message = '{0}-{1}: {2}'\
                .format(self.module_name, self.method_name, str(ve))
            if loading_json:
                status = 400
                message = '{0}-{1}: JSON Error: {2}'\
                    .format(self.module_name,
                            self.method_name,
                            str(ve))
            self.controller.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        repr(e))
            self.controller.log(message)
            print(error_text)

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)


        return return_value



