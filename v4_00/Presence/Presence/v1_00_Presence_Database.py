################################################################################
#
# Program:      v1_00_Presence_Database.py
# Class:        v1_00_Presence_Database
# Objects:      Presence_Database
# Component of: Presence
# Version:      1.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Persistence layer for presence engine.
#
################################################################################

import sqlite3, os

class v1_00_Presence_Database(object):
    db_name = None
    db_conn = None
    db_cursor = None
    controller = None

    # Constructor
    def __init__(self,
                 controller=None,
                 server_name='localhost',
                 port_number=5000
    ):
        if controller == None:
            raise Exception('Controller cannot be none!')

        self.controller = controller

        self.db_name = 'datavolume/{0}-{1}.db'\
                             .format(server_name, port_number)

        self.db_conn = None
        self.db_cursor = None

        self.validate_users_table()
        self.validate_people_table()


    # Insert or replace a user in the presence engine. Note, this is the user
    # of the presence engine NOT a person the user knows. This is generally
    # created when the connection to the presence engine is made.
    def save_user(
        self,
        userid=None,
        status='available'
    ):
        if userid == None:
            raise KeyError(
                'Userid must be provided.'
            )

        returned = None
        try:
            self.open_db()
            self.db_exec('insert or replace into users '+\
                           ' (userid, '+\
                           '  status '+\
                           '  ) '+\
                           'values (?, ?)',
                           (userid,
                            status
                           )
                        )
            self.db_conn.commit()
        except Exception as e:
            self.controller.log(
                'Presence Database: EXCEPTION >> {0}'.format(repr(e))
            )
            raise
        finally:
            self.close_db()

        return {
            'user-id':userid, 
            'status':status
        }


    # Get a user from the database. Returns the user and their status (busy,
    # available, etc.)
    def get_user(
        self,
        userid=None
    ):
        if userid == None:
            raise KeyError(
                'Userid must be provided.'
            )

        returned = None
        try:
            self.open_db()
            self.db_exec('select * '+\
                         'from users '+\
                         'where userid = ?',
                         (userid, ))
            returned = self.db_cursor.fetchall()
            if returned in ('', [], None):
                raise IndexError('Not found.')
            returned = \
                {
                    'user-id':returned[0][0], 
                    'status':returned[0][1]
                }
        except IndexError as ve:
            self.controller.log(
                'Presence Database: INDEX ERROR >> {0}'.format(repr(ve))
            )
            raise
        except Exception as e:
            self.controller.log(
                'Presence Database: EXCEPTION >> {0}'.format(repr(e))
            )
            raise
        finally:
            self.close_db()

        return returned


    # Remove user from the presence engine. Enforces referential integrity and
    # removes people known by the user.
    def remove_user(
        self,
        userid=None
    ):
        if userid == None:
            raise KeyError(
                'Userid must be provided.'
            )

        returned = None
        try:
            self.open_db()
            # Referntial cascade to delete people known to the user.
            self.db_exec('delete from people '+\
                         'where userid = ?',
                         (userid, ))
            self.db_exec('delete from users '+\
                         'where userid = ?',
                         (userid, ))
            self.db_conn.commit()
            returned = \
                {
                    'user-id':None, 
                    'status':None
                }
        except Exception as e:
            self.controller.log(
                'Presence Database: EXCEPTION >> {0}'.format(repr(e))
            )
            raise
        finally:
            self.close_db()

        return returned


    # Get all people or a specific person known to the user. If people are known
    # they are returned in a list of dict; if not an empty list is returned.
    def get_people(
        self,
        userid=None,
        person=None
    ):
        if userid == None:
            raise KeyError(
                'Userid must be provided.'
            )

        returned = None
        return_list = []
        try:
            self.open_db()
            sql_statement='select * from people where userid = ? '
            query_params=(userid,)
            if person != None:
                sql_statement += 'and person = ?'
                query_params += (person,)

            self.db_exec(sql_statement,
                         query_params)

            returned = self.db_cursor.fetchall()

            # Loop will only process if results are returned.
            for returned_item in returned:
                return_list.append(
                    {
                        'user-id':returned_item[0], 
                        'person':returned_item[1],
                        'relationship':returned_item[2]
                    }
                )
        except IndexError as ve:
            self.controller.log(
                'Presence Database: INDEX ERROR >> {0}'.format(repr(ve))
            )
            raise
        except Exception as e:
            self.controller.log(
                'Presence Database: EXCEPTION >> {0}'.format(repr(e))
            )
            raise
        finally:
            self.close_db()

        return return_list


    # Add a person known to the user. Note, this does not require a relationship
    # simply that the person is known - they may be (and default to) a stranger.
    def save_person(
        self,
        userid=None,
        person=None,
        relationship='stranger'
    ):
        if userid == None or person==None:
            raise KeyError(
                'Userid and person must be provided.'
            )

        returned = None
        try:
            self.open_db()
            self.db_exec('insert or replace into people '+\
                           ' (userid, '+\
                           '  person, '+\
                           '  relationship '+\
                           '  ) '+\
                           'values (?, ?, ?)',
                           (userid,
                            person,
                            relationship
                           )
                        )
            self.db_conn.commit()
            returned = {
                    'user-id':userid, 
                    'person':person,
                    'relationship':relationship
                }
        except Exception as e:
            self.controller.log(
                'Presence Database: EXCEPTION >> {0}'.format(repr(e))
            )
            raise
        finally:
            self.close_db()

        return returned


    # Removing a person effectively states the user no longer knows this person.
    def remove_person(
        self,
        userid=None,
        person=None
    ):
        if userid == None or person == None:
            raise KeyError(
                'Userid and person must be provided.'
            )

        returned = None
        try:
            self.open_db()
            self.db_exec('delete from people '+\
                         'where userid = ? '+\
                         'and person = ?',
                         (userid, person))
            self.db_conn.commit()
            returned = \
                {
                    'user-id':userid, 
                    'person':None,
                    'relationship':None
                }
        except Exception as e:
            self.controller.log(
                'Presence Database: EXCEPTION >> {0}'.format(repr(e))
            )
            raise
        finally:
            self.close_db()

        return returned


############ Helper Methods ####################################################

    def db_exec(self, sql_statement=None, sql_parameters=()):
        if sql_statement == None:
            return None

        _returned = None

        try:
            if self.db_cursor == None:
                raise Exception('Cursor does not exist!')
            self.db_cursor.execute(sql_statement, sql_parameters)
        except sqlite3.OperationalError as oe:
            raise
        except Exception as e:
            raise


    def close_db(self):
        if not self.db_cursor == None:
            self.db_cursor.close()
        if not self.db_conn == None:
            self.db_conn.close()
        self.db_cursor = None
        self.db_conn = None


    def open_db(self):
        try:
            self.db_conn = sqlite3.connect(self.db_name)
            self.db_cursor = self.db_conn.cursor()
        except Exception as e:
            if not self.db_cursor == None:
                self.db_cursor.close()
            if not self.db_conn == None:
                self.db_conn.close()
            raise


    def validate_users_table(self):
        _returned = None

        try:
            self.open_db()
            self.db_exec('delete from users')
            self.db_conn.commit()
        except sqlite3.OperationalError as oe:
            self.db_cursor.execute(
                    'CREATE TABLE users ( '+\
                    '  userid string not null primary key, '+\
                    '  status string not null'+\
                    ');'
                )
        except Exception as e:
            raise
        finally:
            self.close_db()


    def validate_people_table(self):
        _returned = None

        try:
            self.open_db()
            self.db_exec('delete from people')
            self.db_conn.commit()
        except sqlite3.OperationalError as oe:
            self.db_cursor.execute(
                    'CREATE TABLE people ( '+\
                    '  userid string not null, '+\
                    '  person string not null, '+\
                    '  relationship string not null, '+\
                    '  primary key (userid, person),'+\
                    '  foreign key (userid) references users (userid) '+\
                    ');'
                )
        except Exception as e:
            raise
        finally:
            self.close_db()

