################################################################################
#
# Program:      v1_00_Subscribe_Control.py
# Class:        v1_00_Subscribe_Control
# Objects:      Subscribe_Control
# Component of: Presence
# Version:      1.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Subscriber control manages user subscriptions (or relationships) with
#          the presence engine, allowing new subs, cancelling of subs, or 
#          querying if a subscription exists.
#
################################################################################

from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Presence import Control
import datetime, time, json, requests

class v1_00_Subscribe_Control(object):
    __controller = None

    # The return schema
    return_schema = {
        "user-id":None,         # The user subscribed to Presence
        "status":None,          # The user's current status (see below)
        "people-url":None       # The URL to query a person's relationship. Note
    }                           # this is the URL to issue a query to find out
                                # a person's relationship to THIS user.

    
    # The states of availability.
    supported_states = [
        'available',
        'busy',
        'away',
        'appear-away',
        'do-not-disturb'
    ]


    # Constructor.
    def __init__(self):
        self.controller = Control.global_controller
        self.module_name = 'Subscribe'
        self.method_name = 'unknown'


    # Update the data object that will be returned.
    def update_schema(
        self,
        user_id=None,
        status=None,
        people_url=None
    ):
        self.return_schema['user-id'] = user_id
        self.return_schema['status'] = status
        self.return_schema['people-url'] = people_url


    # Helper function to create the people URL that allows a subscribing object
    # to query, set, or delete people known to the user.
    def _get_url(
        self,
        user_id=None
    ):
        return 'http://{0}:{1}/{2}/people/{3}'\
            .format(
                self.controller.get_value('ip_addr'),
                self.controller.get_value('port_number'),
                self.controller.get_value('version'),
                user_id
            )


    # Validating the parameters passed are good - string, not empty, etc.
    def _validate_parameters_passed(
        self,
        user_id=None
    ):
        if type(user_id) != str \
        or user_id in (None, '', []):
            raise ValueError('User ID must be specified '+\
                             'to modify user subscription.')


    # Validate the JSON and return a dict of the JSON.
    def _validate_json_string(
        self,
        json_string=None
    ):
        if json_string in (None, ''):
            return None

        return_value = None

        # Validate the raw data is valid
        self.controller.log('{0}-{1}: Validating JSON'\
            .format(self.module_name,self.method_name)
        )

        if not type(json_string) == str:
            raise KeyError('JSON data was not provided as '+\
                           'a string')

        self.controller.log('{0}-{1}: JSON data is a string.'\
            .format(self.module_name,self.method_name)
        )

        if json_string in (None, ''):
            raise KeyError('No JSON data was provided, so '+\
                           'there is no object to load')

        self.controller.log('{0}-{1}: JSON string is not empty.'\
            .format(self.module_name,self.method_name)
        )

        # Load the JSON into a dict
        try:
            return_value=json.loads(json_string)
        except ValueError:
            raise ValueError('The JSON is poorly formed. Please check.')
        except Exception as e:
            raise

        return return_value


    # A user is subscribing to the subscription request. An unstructured user_id
    # and status must be given. The status is validated against supported_states
    # 
    def subscription_request(
        self,
        user_id=None,
        json_string=None
    ):
        self.method_name='subscription_request'

        self.controller.log('{0}-{1}: User subscription request received.'\
            .format(self.module_name,
                    self.method_name)
        )

        success = 'success'
        status = 201
        message = 'Add user {0}'.format(user_id)
        data = None

        try:
            # validate and load the JSON.
            loading_json=False
            self._validate_parameters_passed(user_id)

            loading_json = True
            json_data = self._validate_json_string(json_string)

            # Set a sentinel around loading the JSON data so we know if it's
            # poorly formatted and can catch it in the exception.
            self.controller.log('{0}-{1}: Loading JSON'\
                .format(self.module_name,
                        self.method_name)
            )
            loading_json = False

            if json_data != None:
                try:
                    # Get the status from the JSON
                    user_status = json_data['status'].lower()
                    self.controller.log('{0}-{1}: User status is {2}'\
                        .format(self.module_name,
                                self.method_name,
                                user_status)
                    )

                    # Check the status is valid.
                    if not user_status in self.supported_states:
                        raise ValueError(
                            '{0} is not a supported state. '\
                                .format(user_status)+\
                            'Supported states are {0}'\
                                .format(self.supported_states)
                        )
                except ValueError as ve:
                    loading_json=True
                    self.controller.log('{0}-{1}: EXCEPTION >> {2}'\
                        .format(self.module_name,
                                self.method_name,
                                repr(ve))
                    )
                    raise
                except Exception as e:
                    self.controller.log('{0}-{1}: EXCEPTION >> {2}'\
                        .format(self.module_name,
                                self.method_name,
                                repr(e))
                    )
                    user_status = 'available'
            else:
                # If the status is NOT provided, set it to a default.
                user_status = 'available'

            self.controller.log('{0}-{1}: JSON is valid and loaded.'\
                .format(self.module_name,
                        self.method_name)
            )

            self.controller.log('{0}-{1}: Saving {2} to the database.'\
                .format(self.module_name,
                        self.method_name,
                        user_id)
            )
            
            # Save the user to the database. At this point, we should really
            # generate a key for the user to secure the relationship but for
            # modelling purposes chose not to.
            #
            return_value=self.controller.Presence_db.save_user(
                user_id,
                user_status
            )

            self.controller.log('{0}-{1}: User {2} added.'\
                .format(self.module_name,
                        self.method_name,
                        user_id)
            )

            # Create the URL the caller can use to get, set, or remove people
            # known to the user.
            people_url = self._get_url(user_id)

            # Update the data object
            self.update_schema(
                user_id,
                user_status,
                people_url
            )

            # Set the data object to be returned.
            data=self.return_schema
            self.controller.log('{0}-{1}: Add user_id request completed.'\
                .format(self.module_name,
                        self.method_name)
            )
        except KeyError as ke:
            success = 'error'
            status = 400
            message = '{0}-{1}: Key Error >> {2}'\
                .format(self.module_name, self.method_name, str(ke))
            self.controller.log(message)
        except ValueError as ve:
            success = 'error'
            status = 403
            message = '{0}-{1}: {2}'\
                .format(self.module_name, self.method_name, str(ve))
            if loading_json:
                status = 400
                message = '{0}-{1}: JSON Error: {2}'\
                    .format(self.module_name,
                            self.method_name,
                            str(ve))
            self.controller.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        repr(e))
            self.controller.log(message)
            print(error_text)

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)


        return return_value


    # A query has been made about a connection to the Presence engine. Note, no
    # JSON is used (GET requests should NOT have a body) and the user id is part
    # of the URL path (i.e. a query parameter).
    #
    def subscription_info(
        self,
        user_id=None
    ):
        self.method_name='subscription_info'

        self.controller.log('{0}-{1}: Get details for user {2}'\
            .format(self.module_name,
                    self.method_name,
                    user_id)
        )

        success = 'success'
        status = '200'
        message = 'Query user {0}'.format(user_id)
        data = None

        try:
            loading_json=False

            self.controller.log('{0}-{1}: Validating parameter.'\
                .format(self.module_name,
                        self.method_name)
            )
            self._validate_parameters_passed(user_id)

            self.controller.log('{0}-{1}: Database query on user_id {2}'\
                .format(self.module_name,
                        self.method_name,
                        user_id)
            )

            # Query the database to see if the user exists. If the user is not
            # found, an index error will be raised, see the db layer for more:
            #
            #   v1_00_Presence_Database.py.
            #
            returned = self.controller.Presence_db.get_user(user_id)

            # Update the return object with the results.
            self.update_schema(
                returned['user-id'],
                returned['status'],
                self._get_url(user_id)
            )
            data=self.return_schema
        except IndexError:
            success = 'error'
            status = 404
            message = '{0}-{1}: User {2} has not been registered.'\
                .format(self.module_name, self.method_name, user_id)
            self.controller.log(message)
        except ValueError as ve:
            success = 'error'
            status = 403
            message = '{0}-{1}: {2}'\
                .format(self.module_name, self.method_name, str(ve))
            if loading_json:
                status = 400
                message = '{0}-{1}: JSON Error: {2}'\
                    .format(self.module_name,
                            self.method_name,
                            str(ve))
            self.controller.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        repr(e))
            self.controller.log(message)
            print(error_text)

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)


        return return_value


    # Cancelling a subscription removes the user (and anyone known to them) from
    # the presence engine. There should be additional logic here to validate the
    # caller is authorized to cancel the subscription.
    #
    def subscription_cancel(
        self,
        user_id=None,
        json_string=None
    ):
        self.method_name='subscription_cancel'

        self.controller.log(
            '{0}-{1}: Cancel user subscription request received.'\
                .format(self.module_name,
                        self.method_name)
        )

        success = 'success'
        status = '200'
        message = 'Cancel user {0} subscription'.format(user_id)
        data = None

        try:
            loading_json=False

            # Validate the parameters. Note, no JSON is actually used in this
            # method.
            self._validate_parameters_passed(user_id)

            loading_json = False
            self.controller.log('{0}-{1}: Removing {2} from the database.'\
                .format(self.module_name,
                        self.method_name,
                        user_id)
            )
            
            #
            # The logic to validate authority to remove subscription should go
            # here. Not added due to code freeze, D Sanders, 06 May 2016.
            #

            # Remove the user from the db. This will cause a referential removal
            # of people known to the users; however, all known persons are 
            # unique to the user and not shared.
            #
            return_value=self.controller.Presence_db.remove_user(
                user_id
            )

            self.controller.log('{0}-{1}: User {2} removed.'\
                .format(self.module_name,
                        self.method_name,
                        user_id)
            )

            # Update the return data object.
            self.update_schema(
                return_value['user-id'],
                return_value['status'],
                None
            )
            data=self.return_schema

            self.controller.log(
                '{0}-{1}: Cancel user subscription request completed.'\
                    .format(self.module_name,
                            self.method_name)
            )
        except KeyError as ke:
            success = 'error'
            status = 400
            message = '{0}-{1}: Key Error >> {2}'\
                .format(self.module_name, self.method_name, str(ke))
            self.controller.log(message)
        except ValueError as ve:
            success = 'error'
            status = 403
            message = '{0}-{1}: {2}'\
                .format(self.module_name, self.method_name, str(ve))
            if loading_json:
                status = 400
                message = '{0}-{1}: JSON Error: {2}'\
                    .format(self.module_name,
                            self.method_name,
                            str(ve))
            self.controller.log(message)
        except Exception as e:
            success = 'error'
            status = '500'
            message = '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        repr(e))
            self.controller.log(message)
            print(error_text)

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)


        return return_value



