################################################################################
#
# Program:      main.py
# Class:        None
# Objects:      None
# Component of: Presence
# Version:      1.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Main program. Defines routes for boundaries.
#
################################################################################
#
# Standard imports
#
from Presence import app, api
from Presence.Control import global_controller
#
# Get the version of the API
#
version = global_controller.get_value('version')
#
# Reference: Adamo, D. [2015], 'Handling CORS Requests in Flask(-RESTful) APIs'
# [ONLINE]. Available at: http://www.davidadamojr.com/handling-cors-requests-in-flask-restful-apis/
# (Accessed: 08 March 2016)
#
# Set cross origin resource sharing (CORS) in headers for ALL request responses
#
@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Authorization, Accept')
    response.headers.add('Access-Control-Allow-Methods',
        'GET,PUT,POST,OPTIONS,HEAD,DELETE')
    return response
#
# Identify boundary
#
from Presence_Boundary.Identify_Boundary import Identify_Boundary
api.add_resource(Identify_Boundary, '/{0}/identify'.format(version))
#
# Generated service boundaries
#


### Generated path for service: Subscribe
from Presence_Boundary.Subscribe_Boundary import Subscribe_Boundary
api.add_resource(Subscribe_Boundary, '/{0}/user/<string:user>'.format(version))

from Presence_Boundary.People_Boundary import People_Boundary
api.add_resource(People_Boundary, '/{0}/people/<string:user>/<string:person>'\
    .format(version))

# /user/<string:user>/<string:person>
# - add, delete, or get the person
#
#api.add_resource(Friends_Boundary,
#                 '/{0}/friends'.format(version))
#api.add_resource(Config_Spouse_Boundary,
#                 '/{0}/spouse/<string:spouse>'.format(version))
#api.add_resource(Config_Colleagues_Boundary,
#                 '/{0}/colleague'.format(version))

