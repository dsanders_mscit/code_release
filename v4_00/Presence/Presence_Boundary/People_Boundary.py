################################################################################
#
# Program:      People_Boundary.py
# Class:        People_Boundary
# Objects:      People_Boundary
# Component of: Presence
# Version:      1.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: People boundary provides the interfaces to get, set, and clear people
#          known to a given user. Supports GET, POST, and DELETE.
#
################################################################################

from flask_restful import Resource, reqparse
from Presence.People_Control import people_control_object

class People_Boundary(Resource):
    def get(self, user=None, person=None):
        return_value = people_control_object.get_person(
            user_id=user, 
            person=person
        )
        return return_value

    def post(self, user=None, person=None):
        #
        # Get the data with the request and decode it to UTF-8
        #
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        #
        # Don't return results immediately. There may be a need to update 
        # headers afterward SO, get the return result, do anything needed, and
        # then return.
        #
        return_state = people_control_object\
            .add_person(user_id=user, person=person, json_string=raw_data)
        # Return
        return return_state

    def delete(self, user=None, person=None):
        return_state = people_control_object\
            .remove_person(user_id=user, person=person)
        # Return
        return return_state


