################################################################################
#
# Program:      Subscribe_Boundary.py
# Class:        Subscribe_Boundary
# Objects:      Subscribe_Boundary
# Component of: Presence
# Version:      1.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Subscribe boundary provides the interfaces to get, set, and clear 
#          connections (subscriptions) to the presence engine for a given user.
#          Supports GET, POST, and DELETE.
#
################################################################################

from flask_restful import Resource, reqparse
from Presence.Subscribe_Control import subscribe_control_object

class Subscribe_Boundary(Resource):
    def get(self, user=None):
        return subscribe_control_object.subscription_info(user_id=user)

    def post(self, user=None):
        #
        # Get the data with the request and decode it to UTF-8
        #
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        #
        # Don't return results immediately. There may be a need to update 
        # headers afterward SO, get the return result, do anything needed, and
        # then return.
        #
        return_state = subscribe_control_object\
            .subscription_request(user_id=user,json_string=raw_data)
        # Return
        return return_state

    def delete(self, user=None):
        #
        # Get the data with the request and decode it to UTF-8
        #
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        #
        # Don't return results immediately. There may be a need to update 
        # headers afterward SO, get the return result, do anything needed, and
        # then return.
        #
        return_state = subscribe_control_object\
            .subscription_cancel(user_id=user,json_string=raw_data)
        # Return
        return return_state


