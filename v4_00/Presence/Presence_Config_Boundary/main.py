################################################################################
#
# Program:      main.py
# Class:        None
# Objects:      None
# Component of: Presence
# Version:      1.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Main program. Defines configuration routes.
#
################################################################################

from Presence import app, api
from Presence.Control import global_controller
from Presence_Config_Boundary.Config_Logger_Boundary \
    import Config_Logger_Boundary
#
# Get the version of the API
#
version = global_controller.get_value('version')

#
# Place config boundaries here
#
api.add_resource(Config_Logger_Boundary,
                 '/{0}/config/logger'.format(version))
#
# End config boundaries here
#

