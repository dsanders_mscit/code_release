################################################################################
#
# Program:      v4_00_Location_Processor.py
# Class:        v4_00_Location_Processor
# Objects:      Location_Processor
# Component of: Monitor_App
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Updated in version 4 to take account of sensitivity and urgency flags
#          when raising notifications.
#
################################################################################

from Monitor_App.v3_01_Location_Processor import v3_01_Location_Processor

import requests
import json
import time
from datetime import datetime

class v4_00_Location_Processor(v3_01_Location_Processor):

    # Constructor. Inherits from v3_01.
    def __init__(self):
        super(v4_00_Location_Processor, self).__init__()

    #
    # Modified in version 4.0. Adds two new parameters to the JSON used to raise
    # the notification - urgency (set to False) and sensitivity (set to True).
    # This ensures that notifications raised by the monitor app ARE marked as
    # sensitive. Having the two types of notification in separate classes means
    # different urgencies could be used.
    #
    def raise_location_notification(
        self,
        control_object=None,
        recipient=None,
        timeStamp=None,
        xy=(),
        hotspot_name=None,
        hotspot_desc=None
    ):
        try:
            control_object.log(
                'Location {0} '.format(xy)+' '\
                'hotspot notification being raised')

            service = control_object.get_value('service')

            if service == None:
                control_object.log
                (
                    'Location {0} '.format(xy)+' '\
                    'cannot be raised as there is no notification '+\
                    'service provider'
                )
                return

            message_to_send = control_object.get_location_message()\
                .format(hotspot_name, hotspot_desc)

            payload_data = {
                "key":"1234-5678-9012-3456",
                "message":message_to_send,
                "sender":"Monitor_App",
                "recipient":recipient,
                "action":"openMap{0}".format(xy),
                # v4_00 additions
                "urgency":False,
                "sensitivity":True
            }
            request_response = requests.post(
                service,
                data=json.dumps(payload_data)
            )
            log_string = str(request_response.status_code) + ': ' + \
                         str(request_response.json())
            control_object.log('Location notification request returned: {0}'\
                .format(log_string))
        except Exception as e:
            print(repr(e))

