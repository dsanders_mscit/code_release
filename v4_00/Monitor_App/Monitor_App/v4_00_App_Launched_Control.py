################################################################################
#
# Program:      v4_00_App_Control.py
# Class:        v4_00_App_Control
# Objects:      App_Control
# Component of: Monitor_App
# Version:      4.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: App control. Updated in version 4 to take account of sensitivity and
#          urgency flags when raising notifications.
#
################################################################################

from Monitor_App.v3_01_App_Launched_Control import v3_01_App_Launched_Control

from flask_restful import Resource, Api, reqparse, abort
from flask import Response
from Monitor_App.Control import global_control
import datetime, time, json, requests

class v4_00_App_Launched_Control(v3_01_App_Launched_Control):

    # Constructor. Inherits from v3_01.
    def __init__(self):
        super(v4_00_App_Launched_Control, self).__init__()

    #
    # Modified in version 4.0. Adds two new parameters to the JSON used to raise
    # the notification - urgency (set to False) and sensitivity (set to True).
    # This ensures that notifications raised by the monitor app ARE marked as
    # sensitive. Having the two types of notification in separate classes means
    # different urgencies could be used.
    #
    def app_launched(self, application=None, json_string=None):
        success = 'success'
        status = 200
        message = 'Monitor App detected an application launch.'
        data = {"application":application}

        try:
            self.controller.log('{0}'.format(message))

            recipient = self.controller.get_value('recipient')
            service = self.controller.get_value('service')

            monitored_apps = self.controller.get_app(application=application)
            if monitored_apps in ([], None, ''):
                raise ValueError('App {0} is not being monitored.'\
                    .format(application))

            self.controller.log(
                'Application {0} is on watch list.'.format(application)+\
                  ' Raising notification.'
            )

            message_to_send = self.controller.get_app_message()\
                .format(application)

            payload_data = {
                "key":"1234-5678-9012-3456",
                "message":"{0}".format(message_to_send),
                "sender":"Monitor_App",
                "recipient":recipient,
                "action":"none",
                "urgency":False,
                "sensitivity":True
            }

            request_response = requests.post(
                service,
                data=json.dumps(payload_data)
            )

            self.controller.log(
                'Notification raised with data: '\
                .format(payload_data))

            if request_response.status_code not in (200,201):
                raise ValueError(
                    '. A warning was issued with the status code '+\
                    '{0}'.format(request_response.status_code)+\
                    '. Response data was: '+\
                    '{0}'.format(request_response.json())
                )

            log_string = str(request_response.status_code) + ': ' + \
                         str(request_response.json())
            self.controller.log(log_string)
        except requests.exceptions.ConnectionError as rce:
            message = 'Unable to connect to the notification service '+\
                      'to issue notification for {0}!'.format(application)
            data = {"recipient":recipient or None,\
                    "notification-service":service or None}
            success = "error"
            status = 400
            self.controller.log(message)
        except ValueError as ve:
            message = str(ve)
            success = "error"
            status = 404
            data = {"recipient":recipient or None,\
                    "notification-service":service or None}
            self.controller.log(message)
        except Exception as e:
            success = "error"
            data = {"recipient":recipient or None,\
                    "notification-service":service or None}
            status = 400
            message = repr(e)
            self.controller.log('Exception! {0}'.format(message))

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value


