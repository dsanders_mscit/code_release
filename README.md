# studyFinal
David J. Sanders, University of Liverpool. As partial fulfillment of the requirements for the degree of Master of Science in Information Technology.

code_release is the source code for the modelling and simulation component of David Sanders' MSc in IT Research Project. The hypothesis for the project states that "Context-adaptive privacy in ubiquitous computing can be modelled and simulated in a software environment" and the source code in code_release is the full and final source in support of David J. Sanders Master of Science in Information Technology.

For more information, please refer to the dissertation and Appendix O (the manual).
