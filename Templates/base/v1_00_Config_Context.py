################################################################################
#
# Program:      v1_00_Config_Context.py
# Class:        v1_00_Config_Context
# Objects:      None
# Component of: base library
# Version:      1.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Library configuration control for context engine. Included in 
#          base_lib and used by instantiating against Config_Context()
#
################################################################################
import json, requests

class v1_00_Config_Context(object):
    controller = None

    # __init__ : Initializer / Constructor
    def __init__(self, control=None, module=None):

        # Control must be provided.
        if control == None:
            raise Exception('Control was not passed. Config_Context cannot '+\
                            'initiate!')

        # Instantiate required variables
        self.controller = control
        self.module_name = module or 'Module'
        self.method_name = 'unknown'


    # get_context_engine : Return the context engine the device is currently
    # connected to.
    def get_context_engine(self):
        try:
            # Set the method name - used in later versions of the logger to
            # write log messages in the format: module-method: log message.
            # To make the log easier to follow. Note: Not every module or
            # package has been updated to do this.
            self.method_name = 'get_context'

            # default the return schema
            success = 'success'
            status = '200'
            message = 'Context Engine'
            data = None

            self.controller.log('{0}-{1}: Get Context request received.'\
                .format(self.module_name,
                        self.method_name)
            )

            # Get the values from the key/value store for the context engine,
            # its key, url, query url, and state url.
            context = self.controller.get_value('context-engine')
            context_key = self.controller.get_value('context-engine-key')
            context_url = self.controller.get_value('context-engine-url')
            context_query = self.controller.get_value('context-query')
            context_state = self.controller.get_value('context-state')

            # if the context engine is not set, then ensure it is set to None
            # (null) and return it.
            if context in ([], '', None):
                context = None

            # Populate the data object for returning
            data = {
                'context-engine':context,       # URL for connecting to context
                'key':context_key,              # Key provided given by engine
                'context-url':context_url,      # URL including subscriber name
                'context-query':context_query,  # URL to query context
                'context-state':context_state   # URL to set context
            }
        except Exception as e:
            # If here, something has gone very wrong.
            success = 'error'
            status = '500'
            message = '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        repr(e))
            data = {"exception":repr(e)}
            self.controller.log(message)
            print(message)

        self.controller.log('{0}-{1}: Context data set: {2}'\
            .format(self.module_name,
                    self.method_name,
                    data)
        )

        # Make an http response object in preparation for returning.
        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)
        # If any headers need set, they can be set here.

        return return_value



    # clear_context_engine : Disassociate the device using this class from the
    # context engine. Must be conencted to disconnect.
    def clear_context_engine(self, json_string=None):
        success = 'success'
        status = '200'
        message = 'Central logging status.'
        data = None

        try:
            self.method_name = 'remove_context'
            self.controller.log('{0}-{1}: Remove request received.'\
                .format(self.module_name,
                        self.method_name)
            )

            # Get the current context engine. A check is made to ensure context
            # is currently subscribed to.
            context = self.controller.get_value('context-engine')
            self.controller.log('{0}-{1}: Current context set to {2}'\
                .format(self.module_name,
                        self.method_name,
                        context)
            )

            # If context is null, then we need to raise an exception, because
            # you cannot disconnect when not connected.
            if context in (None, '', []):
                raise ValueError('The service is not subscribed to a '+\
                                 'context service, so cannot be removed.')

            self.controller.log('{0}-{1}: Clearing context.'\
                .format(self.module_name,
                        self.method_name)
            )

            # Clear the KV store for all items related to context. This switches
            # off context. There is no need to disconnect from the context
            # engine, since we have destroyed our key and cannot contact it
            # again. If needed again, a new subscription would be required.
            #
            self.controller.clear_value('context-engine')
            self.controller.clear_value('context-engine-key')
            self.controller.clear_value('context-engine-url')
            self.controller.clear_value('context-query')
            self.controller.clear_value('context-state')

            # Prepare the data object for return.
            data = {
                'context-engine':None,
                'key':None,
                'context-url':None,
                'context-query':None,
                'context-state':None
            }
        except KeyError as ke:
            # This shouldn't be reached - there is no key based work other than
            # the kv store and it does not raise KeyError.
            success = 'error'
            status = 400
            message = '{0}-{1}: Key Error >> {2}'\
                .format(self.module_name, self.method_name, str(ke))
            self.controller.log(message)
        except ValueError as ve:
            # ValueError is used for when the context engine is not set - i.e.
            # asking to disconnect from an engine when it's not set is wrong.
            success = 'error'
            status = 400
            message = '{0}-{1}: {2}'\
                .format(self.module_name, self.method_name, str(ve))
            self.controller.log(message)
        except Exception as e:
            # If here, something has gone very wrong.
            success = 'error'
            status = 500
            message = '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        repr(e))
            self.controller.log(message)
            print(message)

        # Make an http response object in preparation for returning.
        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)
        # If any headers need set, set them here.

        self.controller.log('{0}-{1}: Context data set: {2}'\
            .format(self.module_name,
                    self.method_name,
                    data)
        )

        return return_value


    # set_context_engine : Associate the device using this class from with a
    # context engine.
    def set_context_engine(self, json_string=None):
        success = 'success'
        status = '200'
        message = 'Central logging status.'
        data = None

        try:
            self.method_name = 'set_context'
            self.controller.log('{0}-{1}: Set Log request received.'\
                .format(self.module_name,
                        self.method_name)
            )

            # Check the JSON passed was well formed. In later models, this is
            # moved to a per module function.
            # FUTURE CHANGE: This calls out to be a library function. It is used
            # everywhere and really should be a single function call.
            if json_string == None\
            or json_string == '':
                raise KeyError('Badly formed request!')

            self.controller.log('{0}-{1}: Validating JSON.'\
                .format(self.module_name,
                        self.method_name)
            )

            # Load the JSON into a dict and then get the value of the context
            # engine passed in JSON. If it doesn't exist, then a KeyError will
            # be raised and handled in the exception handler.
            json_data = json.loads(json_string)
            context = json_data['context-engine']

            self.controller.log('{0}-{1}: Validating Context starts http://.'\
                .format(self.module_name,
                        self.method_name)
            )

            # Check that the context engine actually begins with http:// 
            # if it doesn't raise a KeyError
            if not context[0:7] == 'http://':
                raise KeyError('Context must begin http:// and point to a '+\
                               'valid central logging URL')

            # Get environment values for the model. The IP Address, the
            # server's name, port number, and the version the model is 
            # presenting as. These are used to build the callback URL.
            ip_addr = self.controller.get_value('ip_addr')
            server_name = self.controller.get_value('server_name')
            port_number = self.controller.get_value('port_number')
            version = self.controller.get_value('version')

            # For Debug purposes only. Becomes available in the Docker Logs
            print('*** ')
            print('*** IP: {0}  Srv: {1}  Port: {2}  Ver. {3}'\
                  .format(ip_addr,server_name,port_number,version))
            print('*** ')

            # Define the call back address the context engine will use to 
            # communicate with the model. Not every model will have a call back
            # handler; if it doesn't, the context engine will not be able to get
            # in touch with it and will ignore it. Callback is the only means
            # for the context engine to connect back to the subscriber.
            #
            call_back='http://{0}:{1}/{2}/callback'\
                .format(
                    self.controller.get_value('ip_addr'),
                    self.controller.get_value('port_number'),
                    self.controller.get_value('version')
                )

            # Get a key we can give to the context engine so we know it has the
            # authority to call us. Set to default but could be a generated key
            # see v1_00_Key.py. Typically the key is generated in the global
            # control.
            #
            call_back_key = self.controller.get_value('callback-key')
            if call_back_key in ('',[],None):
                call_back_key = '1234-5678-9012-3456' # Fallback to default


            # Build the URL for the context engine. The url is the given context
            # engine's url (e.g. http://127.0.0.1:5001/v3_00/subscribe) with the
            # generated subscriber name (device name and port number), e.g.
            # serverName_43132 appended.
            #
            context_url='{0}/{1}-{2}'\
                .format(
                    context,
                    server_name,
                    port_number
                )


            # Validate the context engine is reachable and that it has allowed
            # us to connect. A dictionary is returned.
            context_info = self.validate_context_engine(
                context=context,
                context_url=context_url,
                call_back=call_back,
                call_back_key=call_back_key
            )


            # Parse the returned dictionary for the required information. This
            # is a truly RESTful API. In return for subscribing, the context
            # engine returns not just a key but also the URL for querying and
            # setting context states.
            #
            if not context_info in ('',[], None):
                key=context_info['key']
                query=context_info['query']
                state=context_info['state']


            # Then store the info in the key value store so it can be used later
            self.controller.set_value('context-engine', context)
            self.controller.set_value('context-engine-key', key)
            self.controller.set_value('context-engine-url', context_url)
            self.controller.set_value('context-query', query)
            self.controller.set_value('context-state', state)


            # Build the data object for returning.
            data = {
                      'context-engine':context,
                      'key': key,
                      'context-url':context_url,
                      'context-query':query,
                      'context-state':state
                    }
        except requests.exceptions.ConnectionError as rce:
            # Means the service wasn't reachable. Perhaps a bad address was
            # used or it's not available.
            success = 'error'
            status = '500'
            message = '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        str(rce))
            self.controller.log(message)
        except KeyError as ke:
            # Badly formed JSON. Usually a key is missing from the schema given.
            success = 'error'
            status = 400
            message = '{0}-{1}: Key Error >> {2}'\
                .format(self.module_name, self.method_name, str(ke))
            data = {'error-message':str(ke)}
            self.controller.log(message)
        except ValueError as ve:
            # Normally, used for a bad key (i.e. the security aspect) and so it
            # returns a 403 (Forbidden).
            success = 'error'
            status = 403
            message = '{0}-{1}: {2}'\
                .format(self.module_name, self.method_name, str(ve))
            data = {'error-message':str(ve)}

            # If JSON is being loaded, ValueError is used to indicate bad JSON
            if loading_json:
                status = 400
                message = '{0}-{1}: {2}'\
                    .format(self.module_name,
                            self.method_name,
                            'The JSON data is badly formed. Please check')
                data = {'error-message':'Bad JSON data'}
            self.controller.log(message)
        except Exception as e:
            # Something has gone very wrong.
            success = 'error'
            status = '500'
            message = '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        repr(e))
            data = {"exception":repr(e)}
            self.controller.log(message)
            print(message)

        self.controller.log('{0}-{1}: Context data set: {2}'\
            .format(self.module_name,
                    self.method_name,
                    data)
        )

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value


    # validate_context_engine : Make a call to the context engine, form a 
    # relationship, and return the control key, query link, and state link.
    def validate_context_engine(
        self,
        context=None,
        context_url=None,
        call_back=None,
        call_back_key=None
    ):
        validity = None
        key = None
        query = None
        state = None

        try:
            self.controller.log('{0}-{1}: '\
                                   .format(self.module_name,
                                           self.method_name)+\
                                'Validate request received.'
            )

            # Check parameters passed are good.
            if context in ([], '', None) \
            or context_url in ([], '', None) \
            or call_back in ([], '', None):
                raise ValueError('Context engine is not set.')

            # Build a payload for sending to the context engine. Provide the
            # call back URL and the generated call back key.
            payload_data={
                "call-back":call_back,
                "call-back-key":call_back_key
            }

            self.controller.log('{0}-{1}: '\
                                   .format(self.module_name,
                                           self.method_name)+\
                                'Issuing get request to {0} '.format(context)+\
                                'with payload "{0}"'.format(payload_data)
            )

            # Issue an HTTP POST request to the context engine. Up to 30 seconds
            # is given for the engine to respond.
            request_response = requests.post(
                context_url,
                data=json.dumps(payload_data),
                timeout=30
            )

            # Get the raw responses (text and status code)
            response_raw = request_response.text
            response_status = request_response.status_code

            self.controller.log('{0}-{1}: '\
                                   .format(self.module_name,
                                           self.method_name)+\
                                'Request returned "{0}"'\
                                   .format(response_raw)
            )

            # If it's not a 200, 201 (ok) or 400 (client error) then something
            # has gone wrong and an exception is raised.
            if response_status not in (200,201,400):
                raise requests.exceptions.ConnectionError(
                    'Unable to communicate with context engine. Status '+\
                    'returned was {0}. '.format(response_status)+\
                    'Raw data was {0}.'.format(response_raw)
                )


            # Convert the returned JSON to a dict. The structure of the
            # dictionary object should be an object:
            #
            # {
            #   "status":<integer>
            #   "success":<string>,
            #   "message":<string>,
            #   "data":
            #       {
            #         "subscription-key":<string>
            #         "query-engine":<string>
            #         "state-change":<string>
            #       }
            # }
            response_data = json.loads(response_raw)


            # Check the dict to get the data. This should really be schema
            # based; however, the researcher's use of schema validation tools
            # was problematic, so abandoned for manual approach. Schema would be
            # much better as it would allow change to data without changing code
            # i.e. better decoupling. To do in future.
            #
            if 'response' in response_data:
                self.controller.log('{0}-{1}: Response is in data.'\
                    .format(self.module_name,
                            self.method_name)
                )

                # Make sure the call was successful
                if not response_data['response'].upper() == 'SUCCESS':
                    raise requests.exceptions.ConnectionError(
                        'Unable to communicate with context engine. Status '+\
                        'returned was {0}'.format(response_data['message'])
                    )
                self.controller.log('{0}-{1}: Response is success.'\
                    .format(self.module_name,
                            self.method_name)
                )

                # Make sure there is data
                if not 'data' in response_data:
                    raise KeyError(
                        'No data was returned!'+\
                        'Response was {0}'.format(response_raw)
                    )
                self.controller.log('{0}-{1}: [data] is in Response.'\
                    .format(self.module_name,
                            self.method_name)
                )

                # Make sure the context engine gave provided a key
                if not 'subscription-key' in response_data['data']:
                    raise KeyError(
                        'No subscription-key was returned!'+\
                        'Response was {0}'.format(response_raw)
                    )
                self.controller.log('{0}-{1}: [subscription-key] in Response.'\
                    .format(self.module_name,
                            self.method_name)
                )

                #Get the data
                key = response_data['data']['subscription-key']
                query = response_data['data']['query-engine']
                state = response_data['data']['state-change']
            else:
                raise requests.exceptions.ConnectionError(
                    'Issue communicating with context engine!'+\
                    'Response was {0}'.format(response_raw)
                )
        # ALL exceptions simply raise the exception and pass exception handling
        # back to the calling module.
        except KeyError as ke:
            raise
        except ValueError as ve:
            raise
        except requests.exceptions.ConnectionError as rce:
            raise
        except Exception as e:
            raise
            print(message)

        self.controller.log('{0}-{1}: Context engine returned key: {2}'\
            .format(self.module_name,
                    self.method_name,
                    key)
        )

        # Return the data from the context engine as a dictionary.
        return {'key':key, 'query':query, 'state':state}


