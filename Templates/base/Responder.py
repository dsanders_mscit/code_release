################################################################################
#
# Program:      Responder.py
# Class:        Responder
# Objects:      None
# Component of: base library
# Version:      1.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Library control used to place the make response function into a comon
#          area. Used by all models.
#
################################################################################
from flask import Response
import json

class Responder(object):

    def do(self,
          status=200,
          response='success',
          data=None,
          message=''
    ):
        return_dict = {"status":status,         # HTTP status - eg 403, 201, etc
                       "response":response,     # success OR error
                       "data":data,             # user defined data object
                       "message":message}       # explanatory message

        return Response(
            json.dumps(return_dict),
            status=status,
            mimetype='application/json')        # mimetype - may change in
                                                # future if json-ld or json-
                                                # schema used.

