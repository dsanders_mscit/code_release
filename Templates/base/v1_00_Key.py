################################################################################
#
# Program:      v1_00_Key.py
# Class:        v1_00_Key
# Objects:      None
# Component of: base library
# Version:      1.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Library utility to generate random four word keys based of four
#          random characters.
#
################################################################################
import json, requests, random

class v1_00_Key(object):
    # Candidate characters for keys. Words will be generated on combinations of
    # these characters.
    key_chars = "abcdefghijklmnopqrstuvwxyz"+\
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ"+\
                "0123456789"


    # Constructor.
    def __init__(self, controller=None, module=None, caller=None):
        if controller == None:
            raise Exception('Control was not passed. Key cannot '+\
                            'initiate!')

        self.controller = controller
        self.module_name = module or 'Module'
        self.caller = caller or 'Not provided'
        self.method_name = 'unknown'


    # Helper function to generate words. Note that SystemRandom is used to 
    # ensure the OS generator is used and not Python's. This is more secure.
    def generate_word(self):
        gen_word = ""
        for i in range(4):
            gen_word += random.SystemRandom().choice(self.key_chars)
        return gen_word

    # Method to generate a four word key {key}-{key}-{key}-{key}. Each word is
    # four characters long and random. This generates a key like Tq41-ziK1-1opq-
    # 00lI
    def generate_key(self):
        try:
            self.method_name = 'generate_key'
            generated_key = None

            self.controller.log('{0}-{1}: Key generation request received.'\
                .format(self.module_name,
                        self.method_name)
            )

            generated_key = self.generate_word() + "-" +\
                            self.generate_word() + "-" +\
                            self.generate_word() + "-" +\
                            self.generate_word()

            self.controller.log('{0}-{1}: Key generation fulfilled.'\
                .format(self.module_name,
                        self.method_name)
            )
        except Exception as e:
            raise

        return generated_key


