################################################################################
#
# Program:      Logger.py
# Class:        Logger
# Objects:      None
# Component of: base library
# Version:      1.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Library control used to place the central logging functionality in a
#          common area. Provides the functionality to initialize log file, write
#          to the log file, and log database. Used by all models.
#
################################################################################
# Updates Required - change __ variable naming
################################################################################
import datetime, requests, json
from textwrap import wrap

class Logger(object):
    __filename=None
    __sender=None
    __central_logger=None
    __controller=None

    # Constructor.
    def __init__(self,
                 controller=None,
                 filename='logfile.txt',
                 sender='unknown'
    ):
        # The logger needs a global controller. If not provided, then an
        # exception is raised.
        if controller == None:
            raise Exception('Controller cannot be None!')

        # Store parameters passed. These are used by the logging object whenever
        # it writes to the log on behalf of a model / program.
        #
        self.__controller = controller      # The controller
        self.__filename = filename          # The filename, if specified
        self.__sender = sender              # The sender, if specified

        # Initialize the log file.
        self.file_clear()
        self.writelog('Initialize log file as {0}'.format(self.__filename))
        self.writelog('Sender set to {0}'.format(self.__sender))


    # Date/time now.
    def __now(self):
        return datetime.datetime.now()


    # Initialize the log file. The log file is a comma delimited file with four
    # fields: date/time, sender, log type, and log message. 
    def file_clear(self):
        try:
            f = open(self.__filename, 'w')
            f.write("{0},{1},{2},{3}\n"\
                .format(str(self.__now()),      # Date/time
                        self.__sender,          # Sender
                        'normal',               # Log type - typically normal
                        'log cleared.')         # Message
                )
        except Exception:
            raise
        finally:
            if not f == None:
                f.close()


    # Writelog performs the actual logging of the message. It is called from
    # almost EVERY model / program used in the research. It is passed two
    # parameters, a message and a boolean. The bool states whether the message
    # should be logged to local only (value = False) or local and central (
    # value = True). Default is both. Using False can be useful - for example,
    # if there was a central log exception, setting this to false avoids 
    # infinite loops.
    #
    def writelog(self, 
                 log_message=None,
                 log_to_central=True
    ):
        # Get the date/tome
        now = self.__now()

        # Clear the file ptr.
        f = None
        try:
            # Open the log file
            f = open(self.__filename, 'a')

            # If the log message is null or an empty string, send an empty
            # string to the log. Can be useful for time stamping the log.
            #
            if log_message == None or log_message == '':
                f.write("{0},{1},{2},{3}\n"\
                    .format(str(self.__now()),
                            self.__sender, 
                            'normal', 
                            ''))
            else:
                f.write('{0},{1},{2},"{3}"'\
                    .format(str(self.__now()),
                            self.__sender,
                            'normal',
                            log_message
                    )+"\n")

            # The log message is also printed. The models run as daemons in
            # Docker so users don't see these messages; however, they are
            # helpful when debugging as the model developer can view the Docker
            # logs and see these messages.
            #
            print('***LOG: {0}'.format(log_message))
        except Exception:
            raise
        finally:
            if not f == None:
                f.close()

        # Check if the central logger is defined - if it is, it's assumed it's
        # on.
        central_logger = self.__controller.kvstore.get_key('logger')
        if log_to_central and not (central_logger in ('', [], None)):
            try:
                self.db_logger(log_message=log_message, logger=central_logger)
            except requests.exceptions.ConnectionError as rce:
                pass # Ignore communication errors
            except Exception:
                raise


    # db_logger provides the ability to log the message to the central logging
    # service. There is no db interaction here - it actually sends the data to
    # the logger for persistence via an HTTP post request. If the request fails
    # or timeouts occur, the log is not written centrally and processing will
    # continue. Central logging is considered a secondary purpose and there is
    # no guarantee messages will be logged.
    #
    def db_logger(self,
                  log_message=None,
                  logger=None
    ):
        if logger == None:
            return
        try:
            # Define the payload
            payload_data = {
                "sender":self.__sender,     # Sender
                "log-type":"normal",        # Log type
                "message":log_message       # Message
            }
            requests.post(
                logger,                             # URL
                data=json.dumps(payload_data),      # Data payload
                timeout=10                          # Timeout. If nothing after 
            )                                       # 10s. ignore central logger
        except Exception as e:
            #
            # If something went wrong, write it to the local log but not the
            # central logger; otherwise, an infinite loop will occur.
            #
            self.writelog(log_message='Exception: {0}'.format(repr(e)),
                          log_to_central=False)
            pass



