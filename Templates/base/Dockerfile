################################################################################
#
# Dockerfile
# -----------------------------------------------------------------------------
# Component of: Base
# Version:      --
# Last Updated: 06 May 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
# Related Scripts
# -----------------------------------------------------------------------------
# ./build.sh   - Builds the Docker image. This is where the image name is set.
# ./push.sh    - Puses the Docker image to the Docker hub. Requires credentials.
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 06 May 16    | Finalized version for submission.
#
################################################################################
#
# Purpose: build the Docker container image for base image. The base is 
#          inherited by all models in the project and ensures core components,
#          nginx, Python 3, etc., are available in every model. The Dockerfile
#          enabling the base to be generated and run on any platform that 
#          supports Docker.
#
################################################################################

FROM ubuntu:14.04
MAINTAINER David Sanders <dsanders_can@outlook.com>
LABEL description="David Sanders, University of Liverpool. Base Image." Version="1.0"
RUN apt-get update
RUN apt-get install -y --fix-missing \
    curl \
    net-tools \
    nginx \
    openssl \
    python3 \
    python3-pip \
    redis-server \
    sqlite3 \
&& rm -rf /var/lib/apt/lists/*
#
# Install additional apps for this module
#
RUN pip3 install flask
RUN pip3 install flask-restful
RUN pip3 install uwsgi
RUN pip3 install --upgrade requests
RUN pip3 install --upgrade redis
#
# Setup the base library
#
RUN mkdir /base_lib
#
# copy the base_lib files
#
COPY __init__.py /base_lib/
COPY Config_Logger.py /base_lib/
COPY v1_00_Config_Logger.py /base_lib/
COPY Context_Bad_Exception.py /base_lib/
COPY v1_00_Context_Bad_Exception.py /base_lib/
COPY Config_Context.py /base_lib/
COPY v1_00_Config_Context.py /base_lib/
COPY Identify_Control.py /base_lib/
COPY v1_00_Identify_Control.py /base_lib/
COPY Environment.py /base_lib/
COPY Key.py /base_lib/
COPY v1_00_Key.py /base_lib/
COPY KVStore.py /base_lib/
COPY v1_00_KVStore.py /base_lib/
COPY v2_00_KVStore.py /base_lib/
COPY Logger.py /base_lib/
COPY Responder.py /base_lib/
#RUN pip3 install virtualenv
#RUN virtualenv flask
#RUN flask/bin/pip install flask
#RUN flask/bin/pip install flask-restful
#RUN flask/bin/pip install uwsgi
#RUN flask/bin/pip install requests

