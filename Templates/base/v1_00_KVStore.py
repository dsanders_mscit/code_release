################################################################################
#
# Program:      v1_00_KVStore.py
# Class:        v1_00_KVStore
# Objects:      None
# Component of: base library
# Version:      1.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Library configuration control for key value store. Included in 
#          base_lib and used by instantiating against KVStore(). The kv store
#          provides a simple tool for getting, setting, and clearing keys from
#          a simple and lightweight SQLite3 database.
#
################################################################################
import sqlite3

class v1_00_KVStore(object):
    db_name = 'datavolume/kvstore.db'
    db_conn = None
    db_cursor = None

    # Initialize the class. Make sure that a database name is provided. 
    # Currently the name is set to null and rejected if not provided - this
    # should probably be set to a default rather than null.
    #
    def __init__(self, db_name=None):
        if not db_name == None:
            self.db_name = db_name

        self.validate_config_table()


    # clear the key by deleting it from the kv store.
    def clear_key(self, key=None):
        if key == None:
            return None

        return_value = False
        try:
            self.open_db()
            self.db_exec('delete from config where key = ?',
                          (key,))
            self.db_conn.commit()
            return_value = None
        except Exception as e:
            raise
        finally:
            self.close_db()

        return return_value


    # Set the key by inserting it. NOTE this is an insert or replace so any
    # existing keys are updated using the same set call.
    def set_key(self, key=None, value=None):
        if key == None:
            return None

        return_value = False
        try:
            self.open_db()
            self.db_exec('insert or replace into config values (?,?)',
                          (key,value))
            self.db_conn.commit()
            return_value = value
        except Exception as e:
            raise
        finally:
            self.close_db()

        return return_value


    # Get the key value.
    def get_key(self, key=None):
        if key == None:
            return None

        returned = []
        try:
            self.open_db()
            self.db_exec('select value from config where key = ?',
                          (key,))
            returned = self.db_cursor.fetchall()
            if returned != []:
                returned = returned[0][0] # Only return first value
            elif returned == []:
                returned = None
            return_value = returned
        except Exception as e:
            raise
        finally:
            self.close_db()

        return return_value


    # Make sure the configuration table exists. If it doesn't create the table
    # and instantiate it. NOTE the command to check existence is delete - there
    # is NO persistence of data between runs. This would need to change to
    # an idempotent select statement if persistence is required.
    #
    def validate_config_table(self):
        _returned = None

        try:
            self.open_db()
            self.db_exec('delete from config')
            self.db_conn.commit()
        except sqlite3.OperationalError as oe:
            # Operational error means something went wrong with the Sqlite call
            # and typically this means the table didn't exist. The call could be
            # analysed more granually, but this is enough.
            #
            print('Initializing config table')
            self.db_cursor.execute(
                    'create table config '+\
                    '(key string primary key not null, value string)'
                )
        except Exception as e:
#            print(repr(e))
            raise
        finally:
            self.close_db()


######## HELPER METHODS ########################################################

# These helper methods access the database. They're used extensively throughout
# the code base and are prime candidate to place in the base library to achieve 
# DRYness.
#
    def open_db(self):
        try:
            self.db_conn = sqlite3.connect(self.db_name)
            self.db_cursor = self.db_conn.cursor()
        except Exception as e:
            print(repr(e))
            if not self.db_cursor == None:
                self.db_cursor.close()
            if not self.db_conn == None:
                self.db_conn.close()


    def db_exec(self, sql_statement=None, sql_parameters=()):
        if sql_statement == None:
            return None

        _returned = None

        try:
            if self.db_cursor == None:
                raise Exception('Cursor does not exist!')
            self.db_cursor.execute(sql_statement, sql_parameters)
        except sqlite3.OperationalError as oe:
            raise
        except Exception as e:
            print(repr(e))
            raise


    def close_db(self):
        if not self.db_cursor == None:
            self.db_cursor.close()
        if not self.db_conn == None:
            self.db_conn.close()
        self.db_cursor = None
        self.db_conn = None



