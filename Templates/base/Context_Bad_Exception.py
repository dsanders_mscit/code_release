from base_lib.v1_00_Context_Bad_Exception import v1_00_Context_Bad_Exception

class Context_Bad_Exception(v1_00_Context_Bad_Exception):
    def __init__(
        self, 
        context_message, 
        context_states=None,
        context_override=False
    ):
        super(Context_Bad_Exception, self)\
            .__init__(
                context_message,
                context_states,
                context_override
            )

