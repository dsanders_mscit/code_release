################################################################################
#
# Program:      v2_00_KVStore.py
# Class:        v2_00_KVStore
# Objects:      None
# Component of: base library
# Version:      2.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Library configuration control for key value store. Included in 
#          base_lib and used by instantiating against KVStore()
#
################################################################################
import sqlite3
from base_lib.v1_00_KVStore import v1_00_KVStore

# NOTE: The class is based on (inherits from) v1_00_KVStore
class v2_00_KVStore(v1_00_KVStore):

    # This is version 2, so we need to make a call to the super to initialize
    # the class correctly.
    def __init__(self, db_name=None):
        super(v2_00_KVStore, self).__init__(db_name)

    # Version 2 modification. Change the database query to return ALL matches
    # not just the first key encountered.
    def get_keys(self, key=None):
        if key == None:
            return None

        returned = []
        try:
            self.open_db()
            self.db_exec('select key, value from config where key like ?',
                          (key,))
            return_value = self.db_cursor.fetchall()
        except Exception as e:
            raise
        finally:
            self.close_db()

        return return_value

