################################################################################
#
# Program:      v1_00_Context_Bad_Exception.py
# Class:        v1_00_Context_Bad_Exception
# Objects:      None
# Component of: base library
# Version:      1.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Custom exception for handling bad context states.
#
################################################################################
class v1_00_Context_Bad_Exception(Exception):

    def __init__(
        self,
        context_message,
        context_states=None,
        context_override=False
    ):
        # Context message is the message that was provided. EG: The context is
        # bad because Jane is with Fred and Fred is a friend-of-a-friend not
        # a friend.
        self.context_message = context_message

        # Context states. The current context engine returns states as a string
        # due to redis processing. The string looks like:
        #    'context-state---another-state---someother-state-given---'
        # Not elegant, but works for the research purposes. Change in a future
        # version.
        self.context_states = context_states

        # A bool that suggests whether the context can be overriden (True) or
        # not (False). It's ALWAYS a suggestion - the handling program can
        # always override.
        self.context_override = context_override


    # Return a string. Useful in exception handling to provide simple 
    # functionality, such as:
    #
    # except Context_Bad_Exception as cbe:
    #     print('Exception {0}'.format(cbe))
    #
    def __str__(self):
        return repr(self.context_message)


    # Getter for states
    def get_states(self):
        return self.context_states


    # Getter for message
    def get_message(self):
        return self.context_message


    # Getter for override
    def is_override(self):
        return self.context_override

