################################################################################
#
# Program:      v1_00_Identify_Control.py
# Class:        v1_00_Identify_Control
# Objects:      None
# Component of: base library
# Version:      1.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: NOT currently used. Was going to be the bases of a device identifying
#          tool.
#
################################################################################
class v1_00_Identify_Control(object):

    def __init__(self, controller=None, module=None):
        if controller == None:
            raise Exception('Control was not passed. Config_Context cannot '+\
                            'initiate!')
        self.controller = controller
        self.module_name = module or 'Module'
        self.method_name = 'unknown'


    def log(
        self,
        log_message=None
    ):
        self.controller.log('{0}-{1}: {2}'\
            .format(self.module_name,
                    self.method_name,
                    log_message)
        )


    def identify_me(
        self
    ):
        self.method_name = 'identify_me'
        success = 'success'
        status = '200'
        message = 'Identify received.'
        data = None

        try:
            self.log('Identify request received.')
            server_name = self.controller.get_value('server_name')
            port_number = self.controller.get_value('port_number')
            identity_string = '{0}-{1}'.format(server_name, port_number)
            self.log('Returning: {0}'\
                .format(identity_string)
            )
            data = {"identity":identity_string}
        except Exception as e:
            success = 'error'
            status = '500'
            message = 'Exception: {0}'.format(repr(e))
            self.log('identify: Error {0}'.format(message))

        self.log('Processing completed.')

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value


