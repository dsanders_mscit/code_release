#
# Standard imports
#
from Module import app, api
from Module.Control import global_controller
#
# Get the version of the API
#
version = global_controller.get_value('version')
#
# Reference: Adamo, D. [2015], 'Handling CORS Requests in Flask(-RESTful) APIs'
# [ONLINE]. Available at: http://www.davidadamojr.com/handling-cors-requests-in-flask-restful-apis/
# (Accessed: 08 March 2016)
#
# Set cross origin resource sharing (CORS) in headers for ALL request responses
#
@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Authorization, Accept')
    response.headers.add('Access-Control-Allow-Methods',
        'GET,PUT,POST,OPTIONS,HEAD,DELETE')
    return response
#
# Sample service boundary
#
from Module_Boundary.Sample_Boundary import Sample_Boundary
api.add_resource(Sample_Boundary, '/{0}/sample'.format(version))
#
# Identify boundary
#
from Module_Boundary.Identify_Boundary import Identify_Boundary
api.add_resource(Identify_Boundary, '/{0}/identify'.format(version))
#
# Generated service boundaries
#

