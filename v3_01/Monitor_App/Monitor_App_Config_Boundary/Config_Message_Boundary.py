################################################################################
#
# Program:      Config_Message_Boundary.py
# Class:        Config_App_Message_Boundary, Config_Location_Message_Boundary
# Objects:      Config_App_Message_Boundary, Config_Location_Message_Boundary
# Component of: Monitor_App
# Version:      3.01
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Boundary for configuring the app and location messages. Both objects
#          support getting (GET) and setting (PUT) the messages.
#
################################################################################
from flask_restful import Resource, reqparse
from Monitor_App_Config_Control.Config_Message_Control \
    import logger_message_object

class Config_App_Message_Boundary(Resource):
    def get(self):
        return_state = logger_message_object.get_app_message()
        return return_state


    def put(self):
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        return_state = logger_message_object\
            .set_app_message(json_string=raw_data)

        return return_state

class Config_Location_Message_Boundary(Resource):
    def get(self):
        return_state = logger_message_object.get_location_message()
        return return_state


    def put(self):
        raw_data = None
        raw_data = reqparse.request.get_data().decode('utf-8')
        return_state = logger_message_object\
            .set_location_message(json_string=raw_data)
        return return_state


