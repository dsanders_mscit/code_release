################################################################################
#
# Program:      Config_Message_Control.py
# Class:        Config_Message_Control
# Objects:      Config_Message_Control
# Component of: Monitor_App
# Version:      3.01
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Controller for getting and setting obfuscation messages. Called by
#          the boundaries for app and location messages.
#
################################################################################
from flask_restful import Resource
from flask import Response
from Monitor_App import Control
import json, requests

class v3_01_Config_Message_Control(object):
    controller = None

    # Constructor
    def __init__(self):
        self.controller = Control.global_control


    def get_app_message(self):
        # Call the helper and tell it the message required is the app message
        return self.get_message('app_msg')


    def get_location_message(self):
        # Call the helper and tell it the message required is location message
        return self.get_message('location_msg')


    def set_app_message(self, json_string):
        # Call the helper and tell it the message required is the app message
        return self.set_message('app_msg', json_string)


    def set_location_message(self, json_string):
        # Call the helper and tell it the message required is location message
        return self.set_message('location_msg', json_string)


    # Get the message depending upon the message type (msg_type).
    def get_message(self, msg_type='app_msg'):
        success = 'success'
        status = '200'
        message = 'Get Monitor App app message'
        data = None

        self.controller.log('Request to get {0} message.'.format(msg_type))

        # Anything other than APP_MSG is treated as LOCATION_MSG
        if msg_type.upper() == 'APP_MSG':
            msg_text = self.controller.get_app_message()
        else:
            msg_text = self.controller.get_location_message()

        data={"text":msg_text}

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        self.controller.log('Monitor App {0} message is set to {1}.'\
            .format(msg_type, msg_text))

        return return_value


    def set_message(self, msg_type='app_msg', json_string=None):
        success = 'success'
        status = '200'
        message = 'Set Monitor App app message'
        data = None

        try:
            # Check the JSON is good
            if json_string == None\
            or json_string == '':
                raise KeyError('No JSON data provided!')

            # Load the JSON into a dict and get the key and message text
            json_data = json.loads(json_string)
            key = json_data['key']
            msg_text = json_data['text']

            # Check the key is correct.
            if not key == '1234-5678-9012-3456':
                raise ValueError('control key incorrect.')

            self.controller.log('Setting {0} message to : {1}'\
                                      .format(msg_type, msg_text)
                                 )

            data = {'text':msg_text}

            # Use the global controller to set either message. The global
            # control was used because the getter is used in many objects and
            # methods (set is only here) and it was considered best to keep the
            # helper methods together.
            #
            if msg_type.upper() == 'LOCATION_MSG':
                self.controller.set_location_message(msg_text)
            else:
                self.controller.set_app_message(msg_text)

            self.controller.log('Message {0} set to {1}'\
                                      .format(msg_type, msg_text)
                                 )
        except KeyError as ke:
            success = 'error'
            status = '400'
            message = 'Badly formed request! {0}'.format(ke)
            self.controller.log('Key Error exception: {0}'.format(message))
        except ValueError as ve:
            success = 'error'
            status = '403'
            message = str(ve)
            self.controller.log('Value Error exception: {0}'.format(message))
        except Exception as e:
            self.controller.log('General exception: {0}'.format(repr(e)))
            raise
            #return repr(e)

        return_value = self.controller.do_response(message=message,
                                                     data=data,
                                                     status=status,
                                                     response=success)

        return return_value


