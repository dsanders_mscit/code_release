################################################################################
#
# Program:      Imnear_Boundary.py
# Class:        Imnear_Boundary
# Objects:      Imnear_Boundary
# Component of: Broadcast
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Define the boundary methods that will be used to respond to devices
#          telling the Bluetooth service they are (or are no longer) near by. It
#          supports post (I am near) and delete (I am no longer near) operations
#
################################################################################
#
# Import required modules and packages
#
from flask_restful import Resource, reqparse
from Bluetooth \
    import app, api
from Bluetooth.Imnear_Control \
    import global_imnear_control as imnear_control_object
import json

class Imnear_Boundary(Resource):
    def post(self, devicename):
        return_state = imnear_control_object.handle_imnear(
            devicename=devicename, verb='isnear'
        )

        return return_state


    def delete(self, devicename):
        return_state = imnear_control_object.handle_imnear(
            devicename=devicename, verb='nolongernear'
        )

        return return_state

