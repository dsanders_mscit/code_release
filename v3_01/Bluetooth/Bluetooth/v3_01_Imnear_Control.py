################################################################################
#
# Program:      v3_00_Imnear_Control.py
# Class:        v3_00_Imnear_Control
# Objects:      Imnear_Control
# Component of: Broadcast
# Version:      3.00
# Last Updated: 17 April 2016
# Updated By:   D Sanders, University of Liverpool
# Student ID:   H00035340
#
################################################################################
#
# Updates
# -------
# Who        | When         | Why
# -----------|--------------|--------------------------------------------------
# D Sanders  | 17 April 16  | Finalized version for submission.
#
################################################################################
#
# Purpose: Controller for receivng notifications of nearness from other objects
#          (people or models). The controller receives the alert that an other
#          device is near (or leaves the proximity of) the Bluetooth device. The
#          Bluetooth system checks all the devices it is paired with and issues
#          a notice to them that the device/person/whatever is near by. If the
#          device paired doesn't understand IMNEAR requests, it will return a
#          404 and the Bluetooth system lets it pass.
#
################################################################################
from Bluetooth import Control, Pairing_Control
import datetime, time, json, requests

class v3_01_Imnear_Control(object):
    output_devices = []
    controller = Control.global_control

    # Override the default log and update to include module name and method name
    # for better traceability.
    #
    def log(self, log_message):
        self.controller.log(
            '{0}-{1}: {2}'\
                .format(self.module_name,
                        self.method_name,
                        str(log_message))
        )

    # Constructor
    def __init__(self):
        self.module_name='Imnear_Control'
        self.method_name='unknown'

    # Process all of the devices paired with this Bluetooth service (could be
    # zero or many) and alert them to an object being present or no longer
    # present.
    def _process_paired_devices(
        self,
        caller=None,        # Who said they were near or no longer near
        device_list=None,   # The list of devices paired with the Bluetooth svc
        verb='isnear'       # The action - isnear or 
    ):
        return_list = []

        # If the device list is empty, there is nothing to do. Just return.
        if device_list in ('',None,[]):
            return return_list

        caller_method_name = self.method_name
        self.method_name = '_process_paired_devices'

        self.log('Starting check of paired devices')

        # Loop through every paired device - this is O(N)
        for device in device_list:
            try:
                device_name = device[0]
                self.log('Checking device {0}'.format(device_name))
                device_connected = False

                # The device name given to the bluetooth service has a set
                # format - which needs to change - but it specifies set
                # attributes for the name:
                #
                #    IP Address - four words separated by _
                #    Port number - separated by _
                #    version - separated by _
                #
                # So, a device name would look like:
                #
                #    127_0_0_1_22123_v3_00
                #
                # Notice that the version has an underscore as part of it's text
                # and that does not affect the result. ONLY the first 5 
                # separations matter, anything in word 5 (the v3_00) is included
                # in word 5.
                #
                # Changing in future: The Bluetooth pairing needs to be extended
                # to include passing a call-back address. This is an easy change
                # but code freeze prevents current change, D Sanders, 02 May 
                # 2016.
                #

                # Split the device into 6 components based on separation by _
                # (underscore).
                data_extract = device_name.split('_',5)
                self.log('Data Extract is {0}'.format(data_extract))

                # If the device name separated successfully, process it, 
                # otherwise, ignore it.
                if len(data_extract) == 6 \
                and device_name != caller:
                    # Build the callback address. Future change: This needs to
                    # be modified to an address that is passed by the pairing
                    # device during pairing - HATEOAS.
                    self.log('Formimg IP Address')
                    device_addr = 'http://{0}.{1}.{2}.{3}:{4}/{5}/callback'\
                        .format(
                            data_extract[0],
                            data_extract[1],
                            data_extract[2],
                            data_extract[3],
                            data_extract[4],
                            data_extract[5]
                        )

                    self.log('IP Address is {0}'.format(device_addr))

                    # Build the payload for the call back
                    payload = {
                        "key":"1234-5678-9012-3456",
                        "verb":verb,
                        "data":{"nearby":caller}
                    }
                    self.log('Payload is {0}'.format(device_addr))

                    # Issue the callback request
                    return_state = requests.put(
                        device_addr,
                        data=json.dumps(payload),
                        timeout=10
                    )
                    status_code = int(return_state.status_code)

                    self.log('Return status is {0}'\
                        .format(return_state.status_code)
                    )

                    # If the request is a 200 - 200, 201, etc., then the
                    # request was handled, so set the connected (and informed)
                    # status to True.
                    if status_code > 199 and status_code < 299: #2xx ok status
                        device_connected = True
                else:
                    self.log('Poor data. Not processed.')
                pass
            except requests.exceptions.ConnectionError as rce:
                self.log('Request Exception {0}'.format(rce))
                pass # We ignore http errors.
            except Exception as e:
                self.log('EXCEPTION >>> {0}'.format(e))
                pass
            return_list.append(
                {
                    "device-name":device[0],
                    "notified":device_connected
                }
            )

        self.method_name = caller_method_name
        return return_list


    # Handle_Imnear is called by the boundary which passes a verb (isnear or
    # nolongernear). The method gets a list of paired devices, and then calls
    # the helper _process_paired_devices passing the verb.
    #
    def handle_imnear(
        self,
        devicename=None,
        verb='isnear'
    ):
        self.method_name = 'handle_imnear'
        try:
            self.log(
                'Bluetooth received {0} notification.'\
                    .format(
                        'I am near' if verb == 'isnear' else 'no longer near'
                    )
            )

            success = 'success'
            status = 200
            message = 'Proximity detection.'

            # Get a list of ALL devices paired with this Bluetooth service.
            paired_devices = self.controller.pairing_db.get_paired()

            # If there are devices, then call the helper.
            if paired_devices != []:
                paired_devices = self._process_paired_devices(
                    caller=devicename,
                    device_list=paired_devices,
                    verb=verb
                )
            else:
                paired_devices = None

            # Return the data object.
            data = \
            {
                "device-name":devicename,
                "notified-devices":paired_devices
            }

        except Exception as e:
            success = 'error'
            status = '500'
            message = 'EXCEPTION >> {0}'.format(repr(e))
            self.log(message)
            print(message)

        self.log('Call back answered: {0}'.format(data))

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)

        return return_value


    #
    # DEPRECATED BLOCK - DO NOT USE
    # =============================
    # To be removed after code freeze. D Sanders, 02 May 2016
    #
    def delete_imnear(
        self,
        devicename=None
    ):
        self.method_name = 'post_imnear'
        try:
            self.log('Bluetooth received I Am No Longer Near notification.')

            success = 'success'
            status = 200
            message = 'Proximity removed.'
            data = {"device-name":devicename}

        except Exception as e:
            success = 'error'
            status = '500'
            message = 'EXCEPTION >> {0}'.format(repr(e))
            self.log(message)
            print(message)

        self.log('Call back answered: {0}'.format(data))

        return_value = self.controller.do_response(message=message,
                                                   data=data,
                                                   status=status,
                                                   response=success)

        return return_value
    #
    # END DEPRECATED BLOCK - DO NOT USE
    # =================================
    #

