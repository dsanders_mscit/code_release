echo
echo "***"
echo "*** Pushing base"
echo "***"
echo
cd Templates/base
./push.sh
echo
echo "***"
echo "*** Pushing v3_00"
echo "***"
echo
cd ../../v3_00
./push.sh
echo
echo "***"
echo "*** Pushing v3_01"
echo "***"
echo
cd ../v3_01
./push.sh
echo
echo "***"
echo "*** Pushing v4_00"
echo "***"
echo
cd ../v4_00
./push.sh
cd ..
echo
echo "***"
echo "*** Complete"
echo "***"
echo


